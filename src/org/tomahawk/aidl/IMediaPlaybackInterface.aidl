// IMediaPlaybackInterface.aidl
package org.tomahawk.aidl;

// Declare any non-default types here with import statements

interface IMediaPlaybackInterface {
   void openFile(String path);
       void open(in long [] list, int position);
       int getQueuePosition();
       boolean isPlaying();
       void stop();
       void pause();
       void play();
       void prev();
       void next();
   	void toggleRepeat();
   	void toggleShuffle();
       long duration();
       long position();
       long seek(long pos);
       String getTrackName();
   	String getMediaPath();
       String getAlbumName();
       long getAlbumId();
       String getArtistName();
       long getArtistId();
       void enqueue(in long [] list, int action);
       long [] getQueue();
       void moveQueueItem(int from, int to);
       void setQueuePosition(int index);
   	void setQueueId(long id);
       String getPath();
       long getAudioId();
   	Uri getArtworkUri();
   	void setShuffleMode(int shufflemode);
       int getShuffleMode();
       int removeTracks(int first, int last);
       int removeTrack(long id);
       void setRepeatMode(int repeatmode);
       int getRepeatMode();
       int getMediaMountedCount();
       int getAudioSessionId();
       String getMediaUri();


   	boolean togglePause();
   	void plugHeadSet(boolean plug);
   	void unPlugHeadSet(boolean unplug);

   	long getGenreId();
}
