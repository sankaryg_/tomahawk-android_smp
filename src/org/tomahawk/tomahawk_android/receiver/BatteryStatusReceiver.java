package org.tomahawk.tomahawk_android.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.BatteryManager;

import org.tomahawk.tomahawk_android.events.ChargingEvent;

import de.greenrobot.event.EventBus;

/**
 * Created by YGS on 10/4/2015.
 */
public class BatteryStatusReceiver extends BroadcastReceiver {

    @Override
    public void onReceive(Context context, Intent intent) {

        ChargingEvent event = new ChargingEvent();
        event.setBatteryLevel(intent.getIntExtra(BatteryManager.EXTRA_LEVEL,0));
        EventBus.getDefault().post(event);
    }
}
