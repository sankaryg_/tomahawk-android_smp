package org.tomahawk.tomahawk_android.activities;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.DialogInterface.OnCancelListener;
import android.content.DialogInterface.OnClickListener;
import android.content.Intent;
import android.content.IntentFilter;
import android.media.AudioManager;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.FragmentActivity;

import org.tomahawk.tomahawk_android.R;

public class ErrorProgress extends FragmentActivity {

	private DialogFragment mFragment;

	private BroadcastReceiver mMountStateReceiver = new BroadcastReceiver() {

		@Override
		public void onReceive(Context context, Intent intent) {
			if (Intent.ACTION_MEDIA_MOUNTED.equals(intent.getAction())) {
				startActivity(new Intent(getApplicationContext(), TomahawkMainActivity.class));
				finish();
			}
		}

	};

	private static String errorMsg;

	@Override
	public void onCreate(Bundle icicle) {

		super.onCreate(icicle);
		setVolumeControlStream(AudioManager.STREAM_MUSIC);
		errorMsg =  (String)getIntent().getExtras().get("error");
		mFragment = new ScanningDialogFragment();
		mFragment.show(getSupportFragmentManager(), "scanning_dialog");

	}

	@Override
	public void onStart() {
		super.onStart();
		registerReceiver(mMountStateReceiver, new IntentFilter(Intent.ACTION_MEDIA_MOUNTED));
	}

	@Override
	public void onStop() {
		unregisterReceiver(mMountStateReceiver);
		super.onStop();
	}

	public static class ScanningDialogFragment extends DialogFragment implements OnClickListener,
			OnCancelListener {

		@Override
		public void onCancel(DialogInterface dialog) {
			getActivity().finish();
		}

		@Override
		public void onClick(DialogInterface dialog, int which) {

		}

		@Override
		public Dialog onCreateDialog(Bundle savedInstanceState) {

			AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());

			builder.setIcon(android.R.drawable.ic_dialog_alert);
			builder.setTitle(R.string.music);
			builder.setPositiveButton("Send", new OnClickListener() {

				@Override
				public void onClick(DialogInterface dialog, int which) {
					// TODO Auto-generated method stub

						Intent intentEmail = new Intent(Intent.ACTION_SEND);
						intentEmail.putExtra(Intent.EXTRA_EMAIL, new String[]{"ygsankar.87@gmail.com"});
						intentEmail.putExtra(Intent.EXTRA_SUBJECT, "Error report");
						intentEmail.putExtra(Intent.EXTRA_TEXT, errorMsg);
						intentEmail.setType("message/rfc822");
						startActivity(Intent.createChooser(intentEmail, "Select email provider"));

				}
			});

			builder.setMessage(errorMsg);

			return builder.create();

		}
	}

}
