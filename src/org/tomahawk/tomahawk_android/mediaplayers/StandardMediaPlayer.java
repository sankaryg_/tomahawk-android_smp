package org.tomahawk.tomahawk_android.mediaplayers;

import android.annotation.SuppressLint;
import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.PowerManager;
import android.os.SystemClock;
import android.preference.PreferenceManager;
import android.util.Log;

import com.h6ah4i.android.media.IBasicMediaPlayer;
import com.h6ah4i.android.media.IMediaPlayerFactory;
import com.h6ah4i.android.media.IReleasable;
import com.h6ah4i.android.media.audiofx.IAudioEffect;
import com.h6ah4i.android.media.audiofx.IBassBoost;
import com.h6ah4i.android.media.audiofx.IEnvironmentalReverb;
import com.h6ah4i.android.media.audiofx.IEqualizer;
import com.h6ah4i.android.media.audiofx.IHQVisualizer;
import com.h6ah4i.android.media.audiofx.ILoudnessEnhancer;
import com.h6ah4i.android.media.audiofx.IPreAmp;
import com.h6ah4i.android.media.audiofx.IPresetReverb;
import com.h6ah4i.android.media.audiofx.IVirtualizer;
import com.h6ah4i.android.media.audiofx.IVisualizer;
import com.h6ah4i.android.media.standard.StandardMediaPlayerFactory;

import org.tomahawk.libtomahawk.resolver.PipeLine;
import org.tomahawk.libtomahawk.resolver.Query;
import org.tomahawk.libtomahawk.resolver.Result;
import org.tomahawk.tomahawk_android.TomahawkApp;
import org.tomahawk.tomahawk_android.events.EventDefs;
import org.tomahawk.tomahawk_android.framework.AppEvent;
import org.tomahawk.tomahawk_android.model.BaseAudioEffectStateStore;
import org.tomahawk.tomahawk_android.model.BassBoostStateStore;
import org.tomahawk.tomahawk_android.model.EnvironmentalReverbStateStore;
import org.tomahawk.tomahawk_android.model.EqualizerStateStore;
import org.tomahawk.tomahawk_android.model.HQEqualizerStateStore;
import org.tomahawk.tomahawk_android.model.HQVisualizerStateStore;
import org.tomahawk.tomahawk_android.model.LoudnessEnhancerStateStore;
import org.tomahawk.tomahawk_android.model.MediaPlayerStateStore;
import org.tomahawk.tomahawk_android.model.PreAmpStateStore;
import org.tomahawk.tomahawk_android.model.PresetReverbStateStore;
import org.tomahawk.tomahawk_android.model.VirtualizerStateStore;
import org.tomahawk.tomahawk_android.utils.EnvironmentalReverbPresetsUtil;
import org.tomahawk.tomahawk_android.utils.EqualizerUtil;
import org.tomahawk.tomahawk_android.visualizer.VisualizerStateStore;

import java.io.IOException;
import java.util.concurrent.ConcurrentHashMap;

import de.greenrobot.event.EventBus;

/**
 * Created by android on 14/10/15.
 */
public class StandardMediaPlayer implements TomahawkMediaPlayer,IReleasable{
    private static final String TAG = StandardMediaPlayer.class.getSimpleName();

    private static class Holder {

        private static final StandardMediaPlayer instance = new StandardMediaPlayer(TomahawkApp.getContext());


    }

    public static final int PLAYER_STATE_IDLE = EventDefs.PlayerControlNotifyEvents.STATE_IDLE;
    public static final int PLAYER_STATE_INITIALIZED = EventDefs.PlayerControlNotifyEvents.STATE_INITIALIZED;
    public static final int PLAYER_STATE_PREPARING = EventDefs.PlayerControlNotifyEvents.STATE_PREPARING;
    public static final int PLAYER_STATE_PREPARED = EventDefs.PlayerControlNotifyEvents.STATE_PREPARED;
    public static final int PLAYER_STATE_STARTED = EventDefs.PlayerControlNotifyEvents.STATE_STARTED;
    public static final int PLAYER_STATE_PAUSED = EventDefs.PlayerControlNotifyEvents.STATE_PAUSED;
    public static final int PLAYER_STATE_STOPPED = EventDefs.PlayerControlNotifyEvents.STATE_STOPPED;
    public static final int PLAYER_STATE_PLAYBACK_COMPLETED = EventDefs.PlayerControlNotifyEvents.STATE_PLAYBACK_COMPLETED;
    public static final int PLAYER_STATE_END = EventDefs.PlayerControlNotifyEvents.STATE_END;
    public static final int PLAYER_STATE_ERROR = EventDefs.PlayerControlNotifyEvents.STATE_ERROR;

    private static final String PARCEL_KEY_MEDIA_PLAYER_STATE_STORE = "GlobalAppController.MediaPlayerStateStore";
    private static final String PARCEL_KEY_BASS_BOOST_STATE_STORE = "GlobalAppController.BassBoostStateStore";
    private static final String PARCEL_KEY_VIRTUALIZER_STATE_STORE = "GlobalAppController.VirtualizerStateStore";
    private static final String PARCEL_KEY_EQUALIZER_STATE_STORE = "GlobalAppController.EqualizerStateStore";
    private static final String PARCEL_KEY_LOUDNESS_ENHANCER_STATE_STORE = "GlobalAppController.LoudnessEnhancerStateStore";
    private static final String PARCEL_KEY_ENVIRONMENTAL_REVERB_STATE_STORE = "GlobalAppController.EnvironmentalReverbStateStore";
    private static final String PARCEL_KEY_PRESET_REVERB_STATE_STORE = "GlobalAppController.PresetReverbStateStore";
    private static final String PARCEL_KEY_VISUALIZER_STATE_STORE = "GlobalAppController.VisualizerStateStore";
    private static final String PARCEL_KEY_HQ_EQUALIZER_STATE_STORE = "GlobalAppController.HQEqualizerStateStore";
    private static final String PARCEL_KEY_PREAMP_STATE_STORE = "GlobalAppController.PreAmpStateStore";
    private static final String PARCEL_KEY_HQ_VISUALIZER_STATE_STORE = "GlobalAppController.HQVisualizerStateStore";

    private IBassBoost mBassBoost;
    private IVirtualizer mVirtualizer;
    private IEqualizer mEqualizer;
    private ILoudnessEnhancer mLoudnessEnhancer;
    private IEnvironmentalReverb mEnvironmentalReverb;
    private IPresetReverb mPresetReverb;
    private IEqualizer mHQEqualizer;
    private IPreAmp mPreAmp;

    // --- these fields are saved to / restored from Bundle --
    private MediaPlayerStateStore mPlayerStateStore;
    private BassBoostStateStore mBassBoostStateStore;
    private VirtualizerStateStore mVirtualizerStateStore;
    private EqualizerStateStore mEqualizerStateStore;
    private LoudnessEnhancerStateStore mLoudnessEnhancerStateStore;
    private EnvironmentalReverbStateStore mEnvironmentalReverbStateStore;
    private PresetReverbStateStore mPresetReverbStateStore;
    private VisualizerStateStore mVisualizerStateStore;
    private HQEqualizerStateStore mHQEqualizerStateStore;
    private PreAmpStateStore mPreAmpStateStore;
    private HQVisualizerStateStore mHQVisualizerStateStore;



    private static final int FG_STATE_INACTIVE = -1;
    private static final int FG_STATE_PLAYER_0 = 1;
    private static final int FG_STATE_PLAYER_1 = 2;

    private IBasicMediaPlayer mLibVLC;

    private IVisualizer mVisualizer;

    private IHQVisualizer mHQVisualizer;

    IMediaPlayerFactory iMediaPlayerFactory;

    private Context context;

    private TomahawkMediaPlayerCallback mMediaPlayerCallback;

    private Query mPreparedQuery;

    private Query mPreparingQuery;

    private final ConcurrentHashMap<Result, String> mTranslatedUrls
            = new ConcurrentHashMap<>();



     IBasicMediaPlayer.OnCompletionListener listener = new IBasicMediaPlayer.OnCompletionListener() {
        public void onCompletion(IBasicMediaPlayer mp) {
            mMediaPlayerCallback.onCompletion(mPreparedQuery);
        }
    };


    IBasicMediaPlayer.OnErrorListener errorListener = new IBasicMediaPlayer.OnErrorListener() {
        public boolean onError(IBasicMediaPlayer mp, int what, int extra) {
            switch (what) {
                case MediaPlayer.MEDIA_ERROR_SERVER_DIED:
                    mMediaPlayerCallback.onError("MediaPlayerEncounteredError");
                    return true;
                default:
                    Log.d("MultiPlayer", "Error: " + what + "," + extra);
                    break;
            }
            return false;
        }
    };

    public MediaPlayerStateStore getPlayerStateStore() {
        return mPlayerStateStore;
    }

    public BassBoostStateStore getBassBoostStateStore() {
        return mBassBoostStateStore;
    }

    public VirtualizerStateStore getVirtualizerStateStore() {
        return mVirtualizerStateStore;
    }

    public EqualizerStateStore getEqualizerStateStore() {
        return mEqualizerStateStore;
    }

    public LoudnessEnhancerStateStore getLoudnessEnhancerStateStore() {
        return mLoudnessEnhancerStateStore;
    }

    public EnvironmentalReverbStateStore getEnvironmentalReverbStateStore() {
        return mEnvironmentalReverbStateStore;
    }

    public PresetReverbStateStore getPresetReverbStateStore() {
        return mPresetReverbStateStore;
    }

    public VisualizerStateStore getVisualizerStateStore() {
        return mVisualizerStateStore;
    }

    public HQEqualizerStateStore getHQEqualizerStateStore() {
        return mHQEqualizerStateStore;
    }

    public PreAmpStateStore getPreAmpStateStore() {
        return mPreAmpStateStore;
    }

    public HQVisualizerStateStore getHQVisualizerStateStore() {
        return mHQVisualizerStateStore;
    }

    private StandardMediaPlayer(Context context) {
        this.context = context;

        mPlayerStateStore = new MediaPlayerStateStore();
        mBassBoostStateStore = new BassBoostStateStore();
        mVirtualizerStateStore = new VirtualizerStateStore();
        mEqualizerStateStore = new EqualizerStateStore();
        mLoudnessEnhancerStateStore = new LoudnessEnhancerStateStore();
        mEnvironmentalReverbStateStore = new EnvironmentalReverbStateStore();
        mPresetReverbStateStore = new PresetReverbStateStore();
        mVisualizerStateStore = new VisualizerStateStore();
        mHQEqualizerStateStore = new HQEqualizerStateStore();
        mPreAmpStateStore = new PreAmpStateStore();
        mHQVisualizerStateStore = new HQVisualizerStateStore();
        releaseAllPlayerResources();
        releaseFactory();

        iMediaPlayerFactory = new StandardMediaPlayerFactory(context);
        mLibVLC = iMediaPlayerFactory.createMediaPlayer();
        mLibVLC.setWakeMode(context,
                PowerManager.PARTIAL_WAKE_LOCK);
        createNormalAudioEffects();
        createAuxAudioEffects();
        createVisualizer();
        createHQEqualizer();
        createPreAmp();
        createHQVisualizer();
        MediaPlayerStateStore state = getPlayerStateStore();
        applyBassBoostStates(mBassBoost, mBassBoostStateStore);
        applyVirtualizerStates(mVirtualizer, mVirtualizerStateStore);
        applyEqualizerStates(mEqualizer, mEqualizerStateStore);
        applyLoudnessEnhancerStates(mLoudnessEnhancer, mLoudnessEnhancerStateStore);
        applyEnvironmentalReverbStates(mEnvironmentalReverb, mEnvironmentalReverbStateStore);
        applyPresetReverbStates(mPresetReverb, mPresetReverbStateStore);
        applyHQEqualizerStates(mHQEqualizer, mHQEqualizerStateStore);

        playerSetLooping(0, state.isLooping());
        playerSetVolume(0, state.getVolumeLeft(), state.getVolumeRight());
        playerSetAuxSendLevel(0, state.getAuxEffectSendLevel());
        // enable pre.amp
        setPreAmpEnabled(true);
        EventBus.getDefault().register(this);
    }

    private void setPreAmpEnabled(boolean enabled) {
        IAudioEffect effect = getPreAmp();

        BaseAudioEffectStateStore state = getPreAmpStateStore();

        handleAudioEffectEnabled(effect, state, enabled);

        // notify enabled state updated
        postAppEvent(
                EventDefs.Category.NOTIFY_PRE_AMP,
                EventDefs.PreAmpNotifyEvents.ENABLED_STATE_UPDATED,
                state.isEnabled() ? 1 : 0,
                0);

    }

    public void onEvent(AppEvent event){
        switch (event.category) {
            case EventDefs.Category.NAVIGATION_DRAWER:
                onReceiveNavigationDrawerEvent(event);
                break;
            case EventDefs.Category.PLAYER_CONTROL:
                onReceivePlayerControlEvent(event);
                break;
            case EventDefs.Category.BASSBOOST:
                onReceiveBassBoostEvent(event);
                break;
            case EventDefs.Category.VIRTUALIZER:
                onReceiveVirtualizerEvent(event);
                break;
            case EventDefs.Category.EQUALIZER:
                onReceiveEqualizerEvent(event);
                break;
            case EventDefs.Category.LOUDNESS_EHNAHCER:
                onReceiveLoudnessEnhancerEvent(event);
                break;
            case EventDefs.Category.PRESET_REVERB:
                onReceivePresetReverbEvent(event);
                break;
            case EventDefs.Category.ENVIRONMENTAL_REVERB:
                onReceiveEnvironmentalReverbEvent(event);
                break;
            case EventDefs.Category.VISUALIZER:
                //onReceiveVisualizerEvent(event);
                break;
            case EventDefs.Category.HQ_EQUALIZER:
                //onReceiveHQEqualizerEvent(event);
                break;
            case EventDefs.Category.PRE_AMP:
                onReceivePreAmpEvent(event);
                break;
            case EventDefs.Category.HQ_VISUALIZER:
                //onReceiveHQVisualizerEvent(event);
                break;
        }
    }

    private void onReceiveEqualizerEvent(AppEvent event) {
        switch (event.event) {
           case org.tomahawk.tomahawk_android.model.EventDefs.EqualizerReqEvents.SET_ENABLED: {
                boolean enabled = (event.arg1 != 0);
                setEqualizerEnabled(enabled);
            }
                break;
            case org.tomahawk.tomahawk_android.model.EventDefs.EqualizerReqEvents.SET_PRESET: {
                short preset = (short) event.arg1;
                IEqualizer equalizer = getEqualizer();
                EqualizerStateStore state = getEqualizerStateStore();

                state.getSettings().curPreset = preset;

                // apply
                if (equalizer != null) {
                    equalizer.usePreset(state.getSettings().curPreset);
                }

                // update band levels
                state.setSettings(
                        EqualizerUtil.PRESETS[state.getSettings().curPreset].settings);

                // notify
                postAppEvent(
                        EventDefs.Category.NOTIFY_EQUALIZER,
                        org.tomahawk.tomahawk_android.model.EventDefs.EqualizerNotifyEvents.PRESET_UPDATED, preset, 0);
                postAppEvent(
                        EventDefs.Category.NOTIFY_EQUALIZER,
                        org.tomahawk.tomahawk_android.model.EventDefs.EqualizerNotifyEvents.BAND_LEVEL_UPDATED,
                        -1 /* all bands */, 0);
            }
                break;
            case org.tomahawk.tomahawk_android.model.EventDefs.EqualizerReqEvents.SET_BAND_LEVEL: {
                short band = (short) event.arg1;
                float level = event.getArg2AsFloat();
                IEqualizer equalizer = getEqualizer();
                EqualizerStateStore state = getEqualizerStateStore();

                state.setNormalizedBandLevel(band, level, true);

                // apply
                if (equalizer != null) {
                    equalizer.setBandLevel(
                            band, state.getSettings().bandLevels[band]);
                }

                // notify
                postAppEvent(
                        EventDefs.Category.NOTIFY_EQUALIZER,
                        org.tomahawk.tomahawk_android.model.EventDefs.EqualizerNotifyEvents.BAND_LEVEL_UPDATED, band, 0);
            }
                break;
        }
    }
    private void onReceivePreAmpEvent(AppEvent event) {
        switch (event.event) {
           case EventDefs.PreAmpReqEvents.SET_ENABLED: {
                boolean enabled = (event.arg1 != 0);
                setPreAmpEnabled(enabled);
            }
                break;
            case EventDefs.PreAmpReqEvents.SET_LEVEL: {
                float level = event.getArg1AsFloat();
                IPreAmp preamp = getPreAmp();
                PreAmpStateStore state = getPreAmpStateStore();

                state.setLevelFromUI(level);

                // apply
                if (preamp != null) {
                    preamp.setLevel(state.getLevel());
                }

                // notify
                postAppEvent(
                        EventDefs.Category.NOTIFY_PRE_AMP,
                        EventDefs.PreAmpNotifyEvents.LEVEL_UPDATED, 0, 0);
            }
                break;
        }
    }

    private void onReceiveEnvironmentalReverbEvent(AppEvent event) {
        IEnvironmentalReverb envreverb = getEnvironmentalReverb();
        EnvironmentalReverbStateStore state = getEnvironmentalReverbStateStore();

        switch (event.event) {
            case EventDefs.EnvironmentalReverbReqEvents.SET_ENABLED: {
                boolean enabled = (event.arg1 != 0);
                setEnvironmentalReverbEnabled(enabled);
            }
                break;
            case EventDefs.EnvironmentalReverbReqEvents.SET_PRESET:
                state.setPreset(event.arg1);
                if (state.getPreset() >= 0) {
                    state.setSettings(EnvironmentalReverbPresetsUtil.getPreset(state.getPreset()));
                }

                // apply
                if (envreverb != null) {
                    envreverb.setProperties(state.getSettings());
                }

                // notify preset changed
                postAppEvent(
                        EventDefs.Category.NOTIFY_ENVIRONMENTAL_REVERB,
                        EventDefs.EnvironmentalReverbNotifyEvents.PRESET_UPDATED,
                        0, 0);

                // notify all parameters updated
                postAppEvent(
                        EventDefs.Category.NOTIFY_ENVIRONMENTAL_REVERB,
                        EventDefs.EnvironmentalReverbNotifyEvents.PARAMETER_UPDATED,
                        EventDefs.EnvironmentalReverbReqEvents.PARAM_INDEX_ALL,
                        0);

                break;
            case EventDefs.EnvironmentalReverbReqEvents.SET_PARAMETER:
                state.setPreset(-1); // unset current preset

                state.setNormalizedParameter(event.arg1, event.getArg2AsFloat());

                // apply
                if (envreverb != null) {
                    applyEnvironmentalReverbPresetParam(envreverb, state, event.arg1);
                }

                // notify parameter changed
                postAppEvent(
                        EventDefs.Category.NOTIFY_ENVIRONMENTAL_REVERB,
                        EventDefs.EnvironmentalReverbNotifyEvents.PARAMETER_UPDATED,
                        event.arg1, 0);

                // notify preset changed
                postAppEvent(
                        EventDefs.Category.NOTIFY_ENVIRONMENTAL_REVERB,
                        EventDefs.EnvironmentalReverbNotifyEvents.PRESET_UPDATED,
                        0, 0);
                break;
        }
    }

    private static void applyEnvironmentalReverbPresetParam(
            IEnvironmentalReverb envreverb,
            EnvironmentalReverbStateStore state, int index) {
        IEnvironmentalReverb.Settings settings = state.getSettings();

        switch (index) {
            case EnvironmentalReverbStateStore.PARAM_INDEX_DECAY_HF_RATIO:
                envreverb.setDecayHFRatio(settings.decayHFRatio);
                break;
            case EnvironmentalReverbStateStore.PARAM_INDEX_DECAY_TIME:
                envreverb.setDecayTime(settings.decayTime);
                break;
            case EnvironmentalReverbStateStore.PARAM_INDEX_DENSITY:
                envreverb.setDensity(settings.density);
                break;
            case EnvironmentalReverbStateStore.PARAM_INDEX_DIFFUSION:
                envreverb.setDiffusion(settings.diffusion);
                break;
            case EnvironmentalReverbStateStore.PARAM_INDEX_REFLECTIONS_DELAY:
                envreverb.setReflectionsDelay(settings.reflectionsDelay);
                break;
            case EnvironmentalReverbStateStore.PARAM_INDEX_REFLECTIONS_LEVEL:
                envreverb.setReflectionsLevel(settings.reflectionsLevel);
                break;
            case EnvironmentalReverbStateStore.PARAM_INDEX_REVERB_DELAY:
                envreverb.setReverbDelay(settings.reverbDelay);
                break;
            case EnvironmentalReverbStateStore.PARAM_INDEX_REVERB_LEVEL:
                envreverb.setReverbLevel(settings.reverbLevel);
                break;
            case EnvironmentalReverbStateStore.PARAM_INDEX_ROOM_HF_LEVEL:
                envreverb.setRoomHFLevel(settings.roomHFLevel);
                break;
            case EnvironmentalReverbStateStore.PARAM_INDEX_ROOM_LEVEL:
                envreverb.setRoomLevel(settings.roomLevel);
                break;
        }
    }

    private void onReceivePresetReverbEvent(AppEvent event) {
        IPresetReverb presetreverb = getPresetReverb();
        PresetReverbStateStore state = getPresetReverbStateStore();

        switch (event.event) {
           case EventDefs.PresetReverbReqEvents.SET_ENABLED: {
                boolean enabled = (event.arg1 != 0);
                setPresetReverbEnabled(enabled);
            }
                break;
            case EventDefs.PresetReverbReqEvents.SET_PRESET:
                state.getSettings().preset = (short) event.arg1;

                // apply
                if (presetreverb != null) {
                    presetreverb.setPreset(state.getSettings().preset);
                }

                // notify preset changed
                postAppEvent(
                        EventDefs.Category.NOTIFY_PRESET_REVERB,
                        EventDefs.PresetReverbNotifyEvents.PRESET_UPDATED,
                        0, 0);
                break;
        }
    }

    private void onReceiveLoudnessEnhancerEvent(AppEvent event) {
        switch (event.event) {
            case EventDefs.LoudnessEnhancerReqEvents.SET_ENABLED: {
                boolean enabled = (event.arg1 != 0);
                setLoudnessEnhancerEnabled(enabled);
            }
                break;
            case EventDefs.LoudnessEnhancerReqEvents.SET_TARGET_GAIN: {
                float gainmB = event.getArg2AsFloat();
                ILoudnessEnhancer loudnessEnhancer = getLoudnessEnhancer();
                LoudnessEnhancerStateStore state = getLoudnessEnhancerStateStore();

                state.setNormalizedTargetGainmB(gainmB);

                if (loudnessEnhancer != null && state.isEnabled()) {
                    loudnessEnhancer.setTargetGain(state.getSettings().targetGainmB);
                }
            }
                break;
        }
    }
    private void onReceiveVirtualizerEvent(AppEvent event) {
        switch (event.event) {
            case EventDefs.VirtualizerReqEvents.SET_ENABLED: {
                boolean enabled = (event.arg1 != 0);
                setVirtualizerEnabled(enabled);
            }
                break;
            case EventDefs.VirtualizerReqEvents.SET_STRENGTH: {
                float strength = event.getArg2AsFloat();
                IVirtualizer virtualizer = getVirtualizer();
                VirtualizerStateStore state = getVirtualizerStateStore();

                state.setNormalizedStrength(strength);

                if (virtualizer != null && state.isEnabled()) {
                    virtualizer.setStrength(state.getSettings().strength);
                }
            }
                break;
        }
    }

    private void onReceiveNavigationDrawerEvent(AppEvent event) {
        switch (event.event) {

            case EventDefs.NavigationDrawerReqEvents.CLICK_ITEM_ENABLE_SWITCH: {
                // enable/disable audio effects

                boolean enabled = (event.arg2 != 0);

                switch (event.arg1) {
                    case EventDefs.NavigationDrawerReqEvents.SECTION_INDEX_BASSBOOST:
                        setBassBoostEnabled(enabled);
                        break;
                    case EventDefs.NavigationDrawerReqEvents.SECTION_INDEX_VIRTUALIZER:
                        setVirtualizerEnabled(enabled);
                        break;
                    case EventDefs.NavigationDrawerReqEvents.SECTION_INDEX_EQUALIZER:
                        setEqualizerEnabled(enabled);
                        break;
                    case EventDefs.NavigationDrawerReqEvents.SECTION_INDEX_LOUDNESS_ENHANCER:
                        setLoudnessEnhancerEnabled(enabled);
                        break;
                    case EventDefs.NavigationDrawerReqEvents.SECTION_INDEX_PRESET_REVERB:
                        setPresetReverbEnabled(enabled);
                        break;
                    case EventDefs.NavigationDrawerReqEvents.SECTION_INDEX_ENVIRONMENTAL_REVERB:
                        setEnvironmentalReverbEnabled(enabled);
                        break;
                    case EventDefs.NavigationDrawerReqEvents.SECTION_INDEX_HQ_EQUALIZER:
                        setHQEqualizerEnabled(enabled);
                        break;
                }
            }
            break;
        }
    }

    private void onReceivePlayerControlEvent(AppEvent event) {
        MediaPlayerStateStore state = getPlayerStateStore();
        switch (event.event) {

            case EventDefs.PlayerControlReqEvents.PLAYER_SET_VOLUME_LEFT: {
                state.setVolumeLeft(event.getArg2AsFloat());

                // apply
                playerSetVolume(0, state.getVolumeLeft(), state.getVolumeRight());
                //playerSetVolume(1, state.getVolumeLeft(), state.getVolumeRight());
            }
                break;
            case EventDefs.PlayerControlReqEvents.PLAYER_SET_VOLUME_RIGHT: {
                state.setVolumeRight(event.getArg2AsFloat());

                // apply
                playerSetVolume(0, state.getVolumeLeft(), state.getVolumeRight());
                //playerSetVolume(1, state.getVolumeLeft(), state.getVolumeRight());
            }
                break;

            case EventDefs.PlayerControlReqEvents.PLAYER_ATTACH_AUX_EFFECT: {
                int effectType = event.arg1;

                if (state.getSelectedAuxEffectType() == effectType)
                    return;

                state.setSelectedAuxEffectType(effectType);

                // apply
                checkStateAndApplyAttachedAuxEffectSettings(0);
                //checkStateAndApplyAttachedAuxEffectSettings(1);
            }
                break;
            case EventDefs.PlayerControlReqEvents.PLAYER_SET_AUX_SEND_LEVEL: {
                state.setAuxEffectSendLevel(event.getArg2AsFloat());

                // apply
                playerSetAuxSendLevel(0, state.getAuxEffectSendLevel());
                //playerSetAuxSendLevel(1, state.getAuxEffectSendLevel());
            }
                break;
        }
    }
    private void postAppEvent(int category, int event, int arg1, int arg2) {
        EventBus.getDefault().post(new AppEvent(category, event, arg1, arg2));
    }

    private void onReceiveBassBoostEvent(AppEvent event) {
        switch (event.event) {
            case EventDefs.BassBoostReqEvents.SET_ENABLED: {
                boolean enabled = (event.arg1 != 0);
                setBassBoostEnabled(enabled);
            }
                break;
            case EventDefs.BassBoostReqEvents.SET_STRENGTH: {
                float strength = event.getArg2AsFloat();
                IBassBoost bassboost = getBassBoost();
                BassBoostStateStore state = getBassBoostStateStore();

                state.setNormalizedStrength(strength);

                if (bassboost != null && state.isEnabled()) {
                    bassboost.setStrength(state.getSettings().strength);
                }
            }
                break;
        }
    }

    private void checkStateAndApplyAttachedAuxEffectSettings(int index) {
        if(getLibVlcInstance().isPlaying()) {
            applyAttachedEuxEffectSettings(
                    getLibVlcInstance(),
                    getPlayerStateStore().getSelectedAuxEffectType());
        }
    }


    private void setBassBoostEnabled(boolean enabled) {
        IAudioEffect effect = getBassBoost();
        BaseAudioEffectStateStore state = getBassBoostStateStore();

        handleAudioEffectEnabled(effect, state, enabled);

        // notify enabled state updated
        postAppEvent(
                EventDefs.Category.NOTIFY_BASSBOOST,
                EventDefs.BassBoostNotifyEvents.ENABLED_STATE_UPDATED,
                state.isEnabled() ? 1 : 0,
                0);
    }

    private void setVirtualizerEnabled(boolean enabled) {
        IAudioEffect effect = getVirtualizer();
        BaseAudioEffectStateStore state = getVirtualizerStateStore();

        handleAudioEffectEnabled(effect, state, enabled);

        // notify enabled state updated
        postAppEvent(
                EventDefs.Category.NOTIFY_VIRTUALIZER,
                EventDefs.VirtualizerNotifyEvents.ENABLED_STATE_UPDATED,
                state.isEnabled() ? 1 : 0,
                0);
    }

    private void setEqualizerEnabled(boolean enabled) {
        IAudioEffect effect = getEqualizer();
        BaseAudioEffectStateStore state = getEqualizerStateStore();

        handleAudioEffectEnabled(effect, state, enabled);

        // notify enabled state updated
        postAppEvent(
                EventDefs.Category.NOTIFY_EQUALIZER,
                EventDefs.EqualizerNotifyEvents.ENABLED_STATE_UPDATED,
                state.isEnabled() ? 1 : 0,
                0);
    }

    private void setLoudnessEnhancerEnabled(boolean enabled) {
        ILoudnessEnhancer effect = getLoudnessEnhancer();
        BaseAudioEffectStateStore state = getLoudnessEnhancerStateStore();

        handleAudioEffectEnabled(effect, state, enabled);

        // notify enabled state updated
        postAppEvent(
                EventDefs.Category.NOTIFY_LOUDNESS_ENHANCER,
                EventDefs.LoudnessEnhancerNotifyEvents.ENABLED_STATE_UPDATED,
                state.isEnabled() ? 1 : 0,
                0);
    }

    private void setEnvironmentalReverbEnabled(boolean enabled) {
        int effectType = 1;

        if (!enabled) {
            if (getEnvironmentalReverbStateStore().isEnabled())
                effectType = 2;
            else
                effectType = 0;
        }
        getPlayerStateStore().setSelectedAuxEffectType(effectType);

        // apply
        checkStateAndApplyAttachedAuxEffectSettings(0);



        IAudioEffect effect = getEnvironmentalReverb();
        BaseAudioEffectStateStore state = getEnvironmentalReverbStateStore();

        handleAudioEffectEnabled(effect, state, enabled);

        // notify enabled state updated
        postAppEvent(
                EventDefs.Category.NOTIFY_ENVIRONMENTAL_REVERB,
                EventDefs.EnvironmentalReverbNotifyEvents.ENABLED_STATE_UPDATED,
                state.isEnabled() ? 1 : 0,
                0);
    }

    private void setPresetReverbEnabled(boolean enabled) {
        int effectType = 2;

        if (!enabled) {
            if (getPresetReverbStateStore().isEnabled())
                effectType = 1;
            else
                effectType = 0;
        }
        getPlayerStateStore().setSelectedAuxEffectType(effectType);

        // apply
        checkStateAndApplyAttachedAuxEffectSettings(0);

        IAudioEffect effect = getPresetReverb();
        BaseAudioEffectStateStore state = getPresetReverbStateStore();

        handleAudioEffectEnabled(effect, state, enabled);

        // notify enabled state updated
        postAppEvent(
                EventDefs.Category.NOTIFY_PRESET_REVERB,
                EventDefs.PresetReverbNotifyEvents.ENABLED_STATE_UPDATED,
                state.isEnabled() ? 1 : 0,
                0);
    }

    private void setHQEqualizerEnabled(boolean enabled) {
        IAudioEffect effect = getHQEqualizer();
        BaseAudioEffectStateStore state = getHQEqualizerStateStore();

        handleAudioEffectEnabled(effect, state, enabled);

        // notify enabled state updated
        postAppEvent(
                EventDefs.Category.NOTIFY_HQ_EQUALIZER,
                EventDefs.HQEqualizerNotifyEvents.ENABLED_STATE_UPDATED,
                state.isEnabled() ? 1 : 0,
                0);
    }




    private void handleAudioEffectEnabled(
            IAudioEffect effect, org.tomahawk.tomahawk_android.model.BaseAudioEffectStateStore state, boolean enabled) {
        state.setEnabled(enabled);

        // apply
        if (effect != null) {
            effect.setEnabled(state.isEnabled());
        }
    }

    private void playerSetVolume(int index, float leftVolume, float rightVolume) {
        IBasicMediaPlayer player = getLibVlcInstance();

        if (player != null) {
            player.setVolume(leftVolume, rightVolume);
        }
    }

    private void playerSetLooping(int index, boolean looping) {
        IBasicMediaPlayer player = getLibVlcInstance();

        if (player != null) {
            player.setLooping(looping);
        }
    }

    private void playerSetAuxSendLevel(int index, float level) {
        IBasicMediaPlayer player = getLibVlcInstance();

        if (player != null) {
            player.setAuxEffectSendLevel(level);
        }
    }

    private void createNormalAudioEffects() {
        IMediaPlayerFactory factory = iMediaPlayerFactory;
        IBasicMediaPlayer player0 = getLibVlcInstance();

        player0.setAudioSessionId(player0.getAudioSessionId());


        try {
            mBassBoost = factory.createBassBoost(player0);
        } catch (UnsupportedOperationException e) {
            // the effect is not supported
        }
        try {
            mVirtualizer = factory.createVirtualizer(player0);
        } catch (UnsupportedOperationException e) {
            // the effect is not supported
        }
        try {
            mEqualizer = factory.createEqualizer(player0);
        } catch (UnsupportedOperationException e) {
            // the effect is not supported
        }
        try {
            mLoudnessEnhancer = factory.createLoudnessEnhancer(player0);
        } catch (UnsupportedOperationException e) {
            // the effect is not supported
        }
    }

    private void createAuxAudioEffects() {
        try {
            mEnvironmentalReverb = iMediaPlayerFactory.createEnvironmentalReverb();
        } catch (UnsupportedOperationException e) {
            // the effect is not supported
        }

        try {
            mPresetReverb = iMediaPlayerFactory.createPresetReverb();
        } catch (UnsupportedOperationException e) {
            // the effect is not supported
        }
    }

    private void createHQEqualizer() {
        IMediaPlayerFactory factory = iMediaPlayerFactory;

        try {
            mHQEqualizer = factory.createHQEqualizer();
        } catch (UnsupportedOperationException e) {
            // the effect is not supported
        }
    }

    private void createPreAmp() {
        IMediaPlayerFactory factory = iMediaPlayerFactory;

        try {
            mPreAmp = factory.createPreAmp();
        } catch (UnsupportedOperationException e) {
            // the effect is not supported
        }
    }

    private void applyBassBoostStates(
            IBassBoost bassboost, BassBoostStateStore states) {
        if (bassboost == null)
            return;

        bassboost.setProperties(states.getSettings());
        bassboost.setEnabled(states.isEnabled());
    }

    private void applyVirtualizerStates(
            IVirtualizer virtualizer, VirtualizerStateStore states) {
        if (virtualizer == null)
            return;

        virtualizer.setProperties(states.getSettings());
        virtualizer.setEnabled(states.isEnabled());
    }

    private void applyEqualizerStates(
            IEqualizer equalizer, EqualizerStateStore states) {
        if (equalizer == null)
            return;

        equalizer.setProperties(states.getSettings());
        equalizer.setEnabled(states.isEnabled());
    }

    private void applyLoudnessEnhancerStates(
            ILoudnessEnhancer loudnessEnhancer, LoudnessEnhancerStateStore states) {
        if (loudnessEnhancer == null)
            return;

        loudnessEnhancer.setProperties(states.getSettings());
        loudnessEnhancer.setEnabled(states.isEnabled());
    }

    private void applyPresetReverbStates(
            IPresetReverb presetreverb, PresetReverbStateStore states) {
        if (presetreverb == null)
            return;

        presetreverb.setProperties(states.getSettings());
        presetreverb.setEnabled(states.isEnabled());
    }

    private void applyEnvironmentalReverbStates(
            IEnvironmentalReverb envreverb, EnvironmentalReverbStateStore states) {
        if (envreverb == null)
            return;

        envreverb.setProperties(states.getSettings());
        envreverb.setEnabled(states.isEnabled());
    }

    private void applyHQEqualizerStates(
            IEqualizer equalizer, HQEqualizerStateStore states) {
        if (equalizer == null)
            return;

        equalizer.setProperties(states.getSettings());
        equalizer.setEnabled(states.isEnabled());
    }

    public IBasicMediaPlayer getLibVlcInstance() {
        return mLibVLC;
    }

    public static StandardMediaPlayer get() {
        return Holder.instance;
    }

    public IHQVisualizer getHQVisualizer() {
        return mHQVisualizer;
    }

    @SuppressWarnings("unused")
    public void onEventAsync(PipeLine.StreamUrlEvent event) {
        mTranslatedUrls.put(event.mResult, event.mUrl);
        if (mPreparingQuery != null
                && event.mResult == mPreparingQuery.getPreferredTrackResult()) {
            prepare(mPreparingQuery);
        }
    }

    /**
     * Start playing the previously prepared {@link org.tomahawk.libtomahawk.collection.Track}
     */
    @Override
    public void start() throws IllegalStateException {
        Log.d(TAG, "start()");
        if (!getLibVlcInstance().isPlaying()) {
            getLibVlcInstance().start();//play();
        }
    }

    /**
     * Pause playing the current {@link org.tomahawk.libtomahawk.collection.Track}
     */
    @Override
    public void pause() throws IllegalStateException {
        Log.d(TAG, "pause()");
        if (getLibVlcInstance().isPlaying()) {
            getLibVlcInstance().pause();
        }
    }

    /**
     * Seek to the given playback position (in ms)
     */
    @Override
    public void seekTo(int msec) throws IllegalStateException {
        Log.d(TAG, "seekTo()");
        if (mPreparedQuery != null && !TomahawkApp.PLUGINNAME_BEATSMUSIC.equals(
                mPreparedQuery.getPreferredTrackResult().getResolvedBy().getId())) {
            getLibVlcInstance().seekTo(msec);//.setTime(msec);
        }
    }

    public IVisualizer getVisualizer() {
        return mVisualizer;
    }

    /**
     * Prepare the given url
     */
    private TomahawkMediaPlayer prepare(Query query) {
        release();
        mLibVLC.reset();
        mLibVLC.setOnPreparedListener(null);
        mPreparedQuery = null;
        mPreparingQuery = query;
        Result result = query.getPreferredTrackResult();
        String path;
        if (mTranslatedUrls.get(result) != null) {
            path = mTranslatedUrls.remove(result);
        } else {
            /*if (result.getResolvedBy() instanceof ScriptResolver) {
                ((ScriptResolver) result.getResolvedBy()).getStreamUrl(result);
                return this;
            } else*/ {
                path = result.getPath();
            }
        }

        try {
             if (path.startsWith("content://")) {
                 getLibVlcInstance().setDataSource(context,
                        Uri.parse(path));
            } else {
                 getLibVlcInstance().setDataSource(path);
                 /*getLibVlcInstance().setDataSource(context,
                         Uri.parse("file://"+path));*/
            }
            getLibVlcInstance().setAudioStreamType(AudioManager.STREAM_MUSIC);
           getLibVlcInstance().prepare();
        } catch (IOException ex) {

        } catch (IllegalArgumentException ex) {

        }
        getLibVlcInstance().setOnCompletionListener(listener);
        getLibVlcInstance().setOnErrorListener(errorListener);
        getLibVlcInstance().setOnPreparedListener(new IBasicMediaPlayer.OnPreparedListener() {
            @Override
            public void onPrepared(IBasicMediaPlayer mp) {
                applyMediaPlayerStates(mp, getPlayerStateStore());
                applyAttachedEuxEffectSettings(
                        mp, getPlayerStateStore().getSelectedAuxEffectType());
            }
        });
        SharedPreferences mPreferences =
                PreferenceManager.getDefaultSharedPreferences(TomahawkApp.getContext());
        int state = mPreferences.getInt("playstate",-1);
        int seekpos = mPreferences.getInt("seekpos",0);
//state == PlaybackService.PLAYBACKSERVICE_PLAYSTATE_PLAYING &&
        if (!getLibVlcInstance().isPlaying()) {
            getLibVlcInstance().start();
            if (seekpos>0){
                getLibVlcInstance().seekTo(seekpos);
            }
        }
        //getLibVlcInstance().playMRL(LibVLC.PathToURI(path));
         Log.d(TAG, "onPrepared()");
        mPreparedQuery = mPreparingQuery;
        mPreparingQuery = null;
        mMediaPlayerCallback.onPrepared(mPreparedQuery);
        //EventHandler.getInstance().addHandler(mVlcHandler);
        return this;
    }

    /**
     * Prepare the given url
     */
    @Override
    public TomahawkMediaPlayer prepare(Application application, Query query,
                                       TomahawkMediaPlayerCallback callback) {
        Log.d(TAG, "prepare()");
        mMediaPlayerCallback = callback;
        return prepare(query);
    }

    @Override
    public void release() {
        Log.d(TAG, "release()");

        //EventHandler.getInstance().removeHandler(mVlcHandler);
        //release();
        mPreparedQuery = null;
        mPreparingQuery = null;
        getLibVlcInstance().stop();
        //releaseAllPlayerResources();
        //releaseFactory();
        //getLibVlcInstance().stop();//getLibVlcInstance().stop();
    }

    private void applyMediaPlayerStates(
            IBasicMediaPlayer player,
            MediaPlayerStateStore states) {
        player.setLooping(states.isLooping());
        player.setVolume(states.getVolumeLeft(), states.getVolumeRight());
        player.setAuxEffectSendLevel(states.getAuxEffectSendLevel());
    }

    private void applyAttachedEuxEffectSettings(IBasicMediaPlayer player, int type) {
        IEnvironmentalReverb envReverb = getEnvironmentalReverb();
        IPresetReverb presetReverb = getPresetReverb();
        int effectId = 0;

        switch (type) {
           case EventDefs.PlayerControlReqEvents.AUX_EEFECT_TYPE_NONE:
                break;
            case EventDefs.PlayerControlReqEvents.AUX_EEFECT_TYPE_ENVIRONMENAL_REVERB:
                if (envReverb != null) {
                    effectId = envReverb.getId();
                }
                break;
            case EventDefs.PlayerControlReqEvents.AUX_EEFECT_TYPE_PRESET_REVERB:
                if (presetReverb != null) {
                    effectId = presetReverb.getId();
                }
                break;
        }

        if (player != null) {
            player.attachAuxEffect(effectId);
        }
    }

    /**
     * @return the current track position
     */
    @Override
    public int getPosition() {
        if (mPreparedQuery != null) {
            return (int) getLibVlcInstance().getCurrentPosition();//getTime();
        } else {
            return 0;
        }
    }
    private void createHQVisualizer() {
        IMediaPlayerFactory factory = iMediaPlayerFactory;

        try {
            mHQVisualizer = factory.createHQVisualizer();
        } catch (UnsupportedOperationException e) {
            // the effect is not supported
        }
    }
    private void createVisualizer() {
        if (mVisualizer == null) {
            IMediaPlayerFactory factory = iMediaPlayerFactory;
            IBasicMediaPlayer player0 = mLibVLC;

            try {
                mVisualizer = factory.createVisualizer(player0);
            } catch (UnsupportedOperationException e) {
                // the effect is not supported
            }
        }
    }

    @Override
    public boolean isPlaying(Query query) {
        return isPrepared(query) && getLibVlcInstance().isPlaying();
    }

    @Override
    public boolean isPreparing(Query query) {
        return mPreparingQuery != null && mPreparingQuery == query;
    }

    @Override
    public boolean isPrepared(Query query) {
        return mPreparedQuery != null && mPreparedQuery == query;
    }

    private void releaseAllPlayerResources() {
        playerRelease();

        safeRelease(mBassBoost);
        mBassBoost = null;

        safeRelease(mVirtualizer);
        mVirtualizer = null;

        safeRelease(mEqualizer);
        mEqualizer = null;

        safeRelease(mLoudnessEnhancer);
        mLoudnessEnhancer = null;

        safeRelease(mPresetReverb);
        mPresetReverb = null;

        safeRelease(mEnvironmentalReverb);
        mEnvironmentalReverb = null;

        safeRelease(mVisualizer);
        mVisualizer = null;

        safeRelease(mHQEqualizer);
        mHQEqualizer = null;

        safeRelease(mPreAmp);
        mPreAmp = null;

        safeRelease(mHQVisualizer);
        mHQVisualizer = null;
    }

    private void playerRelease() {
        IBasicMediaPlayer player = getLibVlcInstance();

        if (player != null) {
            try {
                player.release();
            } catch (Exception e) {

            }
        mLibVLC = null;
        }
    }
    private void releaseFactory() {
        safeRelease(iMediaPlayerFactory);
        iMediaPlayerFactory = null;
    }

    private static void safeRelease(IReleasable obj) {
        try {
            if (obj != null) {
                obj.release();
            }
        } catch (Exception e) {
        }
    }
    private IBassBoost getBassBoost() {
        return mBassBoost;
    }

    private IVirtualizer getVirtualizer() {
        return mVirtualizer;
    }

    private IEqualizer getEqualizer() {
        return mEqualizer;
    }

    private ILoudnessEnhancer getLoudnessEnhancer() {
        return mLoudnessEnhancer;
    }

    private IEnvironmentalReverb getEnvironmentalReverb() {
        return mEnvironmentalReverb;
    }

    private IPresetReverb getPresetReverb() {
        return mPresetReverb;
    }

    private IEqualizer getHQEqualizer() {
        return mHQEqualizer;
    }

    private IPreAmp getPreAmp() {
        return mPreAmp;
    }


  public static class CompatMediaPlayer extends MediaPlayer implements
            MediaPlayer.OnCompletionListener {

        private boolean mCompatMode = true;
        private MediaPlayer mNextPlayer;
        private OnCompletionListener mCompletion;

        public CompatMediaPlayer() {
            try {
                MediaPlayer.class.getMethod("setNextMediaPlayer",
                        MediaPlayer.class);
                mCompatMode = false;
            } catch (NoSuchMethodException e) {
                mCompatMode = true;
                super.setOnCompletionListener(this);
            }
        }

        @SuppressLint("NewApi")
        public void setNextMediaPlayer(MediaPlayer next) {
            if (mCompatMode) {
                mNextPlayer = next;
            } else {
                super.setNextMediaPlayer(next);
            }
        }

        @Override
        public void setOnCompletionListener(OnCompletionListener listener) {
            if (mCompatMode) {
                mCompletion = listener;
            } else {
                super.setOnCompletionListener(listener);
            }
        }

        @Override
        public void onCompletion(MediaPlayer mp) {
            if (mNextPlayer != null) {
                // as it turns out, starting a new MediaPlayer on the completion
                // of a previous player ends up slightly overlapping the two
                // playbacks, so slightly delaying the start of the next player
                // gives a better user experience
                SystemClock.sleep(50);

                mNextPlayer.start();
            }
            mCompletion.onCompletion(this);
        }
    }
}
