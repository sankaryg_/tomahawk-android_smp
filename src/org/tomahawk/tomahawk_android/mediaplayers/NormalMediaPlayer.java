package org.tomahawk.tomahawk_android.mediaplayers;

import android.annotation.SuppressLint;
import android.app.Application;
import android.content.Context;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.net.Uri;
import android.os.PowerManager;
import android.os.SystemClock;
import android.util.Log;

import org.tomahawk.libtomahawk.resolver.PipeLine;
import org.tomahawk.libtomahawk.resolver.Query;
import org.tomahawk.libtomahawk.resolver.Result;
import org.tomahawk.tomahawk_android.TomahawkApp;

import java.io.IOException;
import java.util.concurrent.ConcurrentHashMap;

import de.greenrobot.event.EventBus;

/**
 * Created by android on 14/10/15.
 */
public class NormalMediaPlayer implements TomahawkMediaPlayer{
    private static final String TAG = NormalMediaPlayer.class.getSimpleName();

    private static class Holder {

        private static final NormalMediaPlayer instance = new NormalMediaPlayer(TomahawkApp.getContext());


    }



    private CompatMediaPlayer mLibVLC;

    private Context context;

    private TomahawkMediaPlayerCallback mMediaPlayerCallback;

    private Query mPreparedQuery;

    private Query mPreparingQuery;

    private final ConcurrentHashMap<Result, String> mTranslatedUrls
            = new ConcurrentHashMap<>();



     MediaPlayer.OnCompletionListener listener = new MediaPlayer.OnCompletionListener() {
        public void onCompletion(MediaPlayer mp) {
            mMediaPlayerCallback.onCompletion(mPreparedQuery);
        }
    };


    MediaPlayer.OnErrorListener errorListener = new MediaPlayer.OnErrorListener() {
        public boolean onError(MediaPlayer mp, int what, int extra) {
            switch (what) {
                case MediaPlayer.MEDIA_ERROR_SERVER_DIED:
                    mMediaPlayerCallback.onError("MediaPlayerEncounteredError");
                    return true;
                default:
                    Log.d("MultiPlayer", "Error: " + what + "," + extra);
                    break;
            }
            return false;
        }
    };

    private NormalMediaPlayer(Context context) {
        this.context = context;
        mLibVLC = new CompatMediaPlayer();
        mLibVLC.setWakeMode(context,
                PowerManager.PARTIAL_WAKE_LOCK);
        EventBus.getDefault().register(this);
    }


    public CompatMediaPlayer getLibVlcInstance() {
        return mLibVLC;
    }

    public static NormalMediaPlayer get() {
        return Holder.instance;
    }

    @SuppressWarnings("unused")
    public void onEventAsync(PipeLine.StreamUrlEvent event) {
        mTranslatedUrls.put(event.mResult, event.mUrl);
        if (mPreparingQuery != null
                && event.mResult == mPreparingQuery.getPreferredTrackResult()) {
            prepare(mPreparingQuery);
        }
    }

    /**
     * Start playing the previously prepared {@link org.tomahawk.libtomahawk.collection.Track}
     */
    @Override
    public void start() throws IllegalStateException {
        Log.d(TAG, "start()");
        if (!getLibVlcInstance().isPlaying()) {
            getLibVlcInstance().start();//play();
        }
    }

    /**
     * Pause playing the current {@link org.tomahawk.libtomahawk.collection.Track}
     */
    @Override
    public void pause() throws IllegalStateException {
        Log.d(TAG, "pause()");
        if (getLibVlcInstance().isPlaying()) {
            getLibVlcInstance().pause();
        }
    }

    /**
     * Seek to the given playback position (in ms)
     */
    @Override
    public void seekTo(int msec) throws IllegalStateException {
        Log.d(TAG, "seekTo()");
        if (mPreparedQuery != null && !TomahawkApp.PLUGINNAME_BEATSMUSIC.equals(
                mPreparedQuery.getPreferredTrackResult().getResolvedBy().getId())) {
            getLibVlcInstance().seekTo(msec);//.setTime(msec);
        }
    }

    /**
     * Prepare the given url
     */
    private TomahawkMediaPlayer prepare(Query query) {
        release();
        mLibVLC.reset();
        mLibVLC.setOnPreparedListener(null);
        mPreparedQuery = null;
        mPreparingQuery = query;
        Result result = query.getPreferredTrackResult();
        String path;
        if (mTranslatedUrls.get(result) != null) {
            path = mTranslatedUrls.remove(result);
        } else {
            /*if (result.getResolvedBy() instanceof ScriptResolver) {
                ((ScriptResolver) result.getResolvedBy()).getStreamUrl(result);
                return this;
            } else*/ {
                path = result.getPath();
            }
        }

        try {
             if (path.startsWith("content://")) {
                 getLibVlcInstance().setDataSource(context,
                        Uri.parse(path));
            } else {
                 getLibVlcInstance().setDataSource(path);
                 /*getLibVlcInstance().setDataSource(context,
                         Uri.parse("file://"+path));*/
            }
            getLibVlcInstance().setAudioStreamType(AudioManager.STREAM_MUSIC);
           getLibVlcInstance().prepare();
        } catch (IOException ex) {

        } catch (IllegalArgumentException ex) {

        }
        getLibVlcInstance().setOnCompletionListener(listener);
        getLibVlcInstance().setOnErrorListener(errorListener);
        getLibVlcInstance().start();
        //getLibVlcInstance().playMRL(LibVLC.PathToURI(path));
         Log.d(TAG, "onPrepared()");
        mPreparedQuery = mPreparingQuery;
        mPreparingQuery = null;
        mMediaPlayerCallback.onPrepared(mPreparedQuery);
        //EventHandler.getInstance().addHandler(mVlcHandler);
        return this;
    }

    /**
     * Prepare the given url
     */
    @Override
    public TomahawkMediaPlayer prepare(Application application, Query query,
                                       TomahawkMediaPlayerCallback callback) {
        Log.d(TAG, "prepare()");
        mMediaPlayerCallback = callback;
        return prepare(query);
    }

    @Override
    public void release() {
        Log.d(TAG, "release()");
        //EventHandler.getInstance().removeHandler(mVlcHandler);
        //release();
        mPreparedQuery = null;
        mPreparingQuery = null;
        getLibVlcInstance().stop();
        //getLibVlcInstance().stop();//getLibVlcInstance().stop();
    }

    /**
     * @return the current track position
     */
    @Override
    public int getPosition() {
        if (mPreparedQuery != null) {
            return (int) getLibVlcInstance().getCurrentPosition();//getTime();
        } else {
            return 0;
        }
    }

    @Override
    public boolean isPlaying(Query query) {
        return isPrepared(query) && getLibVlcInstance().isPlaying();
    }

    @Override
    public boolean isPreparing(Query query) {
        return mPreparingQuery != null && mPreparingQuery == query;
    }

    @Override
    public boolean isPrepared(Query query) {
        return mPreparedQuery != null && mPreparedQuery == query;
    }

  public static class CompatMediaPlayer extends MediaPlayer implements
            MediaPlayer.OnCompletionListener {

        private boolean mCompatMode = true;
        private MediaPlayer mNextPlayer;
        private OnCompletionListener mCompletion;

        public CompatMediaPlayer() {
            try {
                MediaPlayer.class.getMethod("setNextMediaPlayer",
                        MediaPlayer.class);
                mCompatMode = false;
            } catch (NoSuchMethodException e) {
                mCompatMode = true;
                super.setOnCompletionListener(this);
            }
        }

        @SuppressLint("NewApi")
        public void setNextMediaPlayer(MediaPlayer next) {
            if (mCompatMode) {
                mNextPlayer = next;
            } else {
                super.setNextMediaPlayer(next);
            }
        }

        @Override
        public void setOnCompletionListener(OnCompletionListener listener) {
            if (mCompatMode) {
                mCompletion = listener;
            } else {
                super.setOnCompletionListener(listener);
            }
        }

        @Override
        public void onCompletion(MediaPlayer mp) {
            if (mNextPlayer != null) {
                // as it turns out, starting a new MediaPlayer on the completion
                // of a previous player ends up slightly overlapping the two
                // playbacks, so slightly delaying the start of the next player
                // gives a better user experience
                SystemClock.sleep(50);

                mNextPlayer.start();
            }
            mCompletion.onCompletion(this);
        }
    }
}
