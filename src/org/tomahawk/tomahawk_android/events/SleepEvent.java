package org.tomahawk.tomahawk_android.events;

/**
 * Created by YGS on 10/4/2015.
 */
public class SleepEvent {

    private long sleepLevel;

    public long getSleepLevel() {
        return sleepLevel;
    }

    public void setSleepLevel(long sleepLevel) {
        this.sleepLevel = sleepLevel;
    }
}
