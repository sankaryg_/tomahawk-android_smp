package org.tomahawk.tomahawk_android.events;

/**
 * Created by YGS on 10/4/2015.
 */
public class CountDownTimeEvent {

    private String currentTimeLevel;

    public String getCurrentTimeLevel() {
        return currentTimeLevel;
    }

    public void setCurrentTimeLevel(String currentTimeLevel) {
        this.currentTimeLevel = currentTimeLevel;
    }
}
