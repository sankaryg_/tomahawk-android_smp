package org.tomahawk.tomahawk_android.events;

/**
 * Created by YGS on 10/4/2015.
 */
public class VoiceEvent {

    private long remainingBattery;

    private boolean battertEventStarted;

    public void setBattertEventStarted(boolean battertEventStarted) {
        this.battertEventStarted = battertEventStarted;
    }

    public boolean getBattertEventStarted() {
        return battertEventStarted;
    }

    public long getRemainingBattery() {
        return remainingBattery;
    }

    public void setRemainingBattery(long remainingBattery) {
        this.remainingBattery = remainingBattery;
    }
}
