package org.tomahawk.tomahawk_android.events;

/**
 * Created by YGS on 10/4/2015.
 */
public class SleepRemainingEvent {

    private int remainingSleep;

    public int getRemainingSleep() {
        return remainingSleep;
    }

    public void setRemainingSleep(int remainingSleep) {
        this.remainingSleep = remainingSleep;
    }
}
