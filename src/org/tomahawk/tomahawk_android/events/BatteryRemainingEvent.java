package org.tomahawk.tomahawk_android.events;

/**
 * Created by YGS on 10/4/2015.
 */
public class BatteryRemainingEvent {

    private int remainingBattery;

    private boolean battertEventStarted;

    public void setBattertEventStarted(boolean battertEventStarted) {
        this.battertEventStarted = battertEventStarted;
    }

    public boolean getBattertEventStarted() {
        return battertEventStarted;
    }

    public int getRemainingBattery() {
        return remainingBattery;
    }

    public void setRemainingBattery(int remainingBattery) {
        this.remainingBattery = remainingBattery;
    }
}
