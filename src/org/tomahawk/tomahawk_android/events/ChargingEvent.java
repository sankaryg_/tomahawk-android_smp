package org.tomahawk.tomahawk_android.events;

/**
 * Created by YGS on 10/4/2015.
 */
public class ChargingEvent {

    private int batteryLevel;

    public int getBatteryLevel() {
        return batteryLevel;
    }

    public void setBatteryLevel(int batteryLevel) {
        this.batteryLevel = batteryLevel;
    }
}
