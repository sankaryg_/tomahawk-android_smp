
package org.tomahawk.tomahawk_android.dialogs;

import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.Spinner;
import android.widget.TextView;

import org.tomahawk.libtomahawk.utils.ViewUtils;
import org.tomahawk.tomahawk_android.R;
import org.tomahawk.tomahawk_android.TomahawkApp;
import org.tomahawk.tomahawk_android.events.BatteryRemainingEvent;
import org.tomahawk.tomahawk_android.events.ChargingEvent;

import de.greenrobot.event.EventBus;

public class BatteryStatusDialog extends DialogFragment implements OnSeekBarChangeListener {

	private SeekBar mSetTime;
	private TextView mTimeView;
	private String mPrompt;
	private int mProgress, mTimerTime;
	long mRemained;
	public static final String BATTERY_VALUES = "batterystatus";
	public static final String BATTERY_STARTED = "batterystarted";

	private AlertDialog mSleepTimerDialog;

	private String action;
	private Spinner mAlertTime;
	private LinearLayout mTimerLayout;
	private LinearLayout mTimerSelectedLayout;
	private TextView mSelectedText;
	private TextView mCountDownText;
	private TextView mCancelBtn;
	private TextView mStartStopBtn;
	private String alarmValue;
	private int spinnerPosition;
	private BatteryStatusDialog mActivity;
	private View mDialogView;
	private ArrayAdapter<String> apModeAdapter;
	private SharedPreferences sharedPreferences;

	@NonNull
	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		// Check if there is a playlist key in the provided arguments


		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		builder.setView(getDialogView());
		return builder.create();
	}
	public View getDialogView() {
		return mDialogView;
	}


	@Override
	public void onCreate(Bundle icicle) {

		super.onCreate(icicle);
		LayoutInflater inflater = getActivity().getLayoutInflater();
		mDialogView = inflater.inflate(R.layout.battery_timer, null);
		mAlertTime = (Spinner) mDialogView.findViewById(R.id.alert_addTask);
		mTimerLayout = (LinearLayout) mDialogView.findViewById(R.id.selectTimeLayout);
		mTimerSelectedLayout = (LinearLayout)mDialogView.findViewById(R.id.selectedTimeLayout);
		mSelectedText = (TextView)mDialogView.findViewById(R.id.selectedTimeText);
		mCountDownText = (TextView)mDialogView.findViewById(R.id.textView2);
		mCancelBtn = (TextView)mDialogView.findViewById(R.id.config_dialog_negative_button);
		mStartStopBtn = (TextView)mDialogView.findViewById(R.id.config_dialog_positive_button);
		DisplayMetrics dm = new DisplayMetrics();
		dm = getResources().getDisplayMetrics();
		mActivity = this;
		sharedPreferences = PreferenceManager.getDefaultSharedPreferences(TomahawkApp.getContext());
		alarmValue = ViewUtils.getAlarmSelected((String) sharedPreferences.getString(BATTERY_VALUES, "0"));
		apModeAdapter = new ArrayAdapter<String>(TomahawkApp.getContext(), R.layout.dropdown_header_textview, getResources().getStringArray(R.array.battery_array) );
		apModeAdapter.setDropDownViewResource(R.layout.dropdown_header_dropdown_textview);
		mAlertTime.setAdapter(apModeAdapter);

		ArrayAdapter<String> myAdap = (ArrayAdapter<String>) mAlertTime.getAdapter(); 
		if(alarmValue==null)
			spinnerPosition = 0;
		else
		spinnerPosition = myAdap.getPosition(alarmValue);

		//set the default according to value
		mAlertTime.setSelection(spinnerPosition);
		
	boolean started = sharedPreferences.getBoolean(BATTERY_STARTED,false);
			if(started){
				mStartStopBtn.setText("Stop");
				
			}else{
				mStartStopBtn.setText("Start");
				
			}
			if( started){
				mTimerLayout.setVisibility(View.GONE);
				mTimerSelectedLayout.setVisibility(View.VISIBLE);
				
				mSelectedText.setText("Music will stop exactly when battery level less than " + sharedPreferences.getString(BATTERY_VALUES, "0"));
				mCountDownText.setVisibility(View.VISIBLE);
				int level = sharedPreferences.getInt("battery",0);//GlobalMethods.getValueFromPreference(mActivity, GlobalMethods.INT_PREFERENCE, "battery");
				if(level > 0){
				mCountDownText.setText("Battery Status :"+level);
				}else
					mCountDownText.setText("");
				
			}else{
				mTimerLayout.setVisibility(View.VISIBLE);
				mTimerSelectedLayout.setVisibility(View.GONE);
				mCountDownText.setVisibility(View.GONE);

			}
			mStartStopBtn.setOnClickListener(new View.OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					String selectedTime = (mAlertTime.getSelectedItem().toString().split(" ")[1]);
					if(selectedTime.length()>0){
						sharedPreferences.edit().putString(BATTERY_VALUES,selectedTime).commit();//GlobalMethods.storeValuetoPreference(BatteryStatusDialog.this, GlobalMethods.STRING_PREFERENCE, Constants.BATTERY_VALUES, selectedTime);
					mTimerTime = Integer.parseInt(selectedTime);
					}
					if (mTimerTime >= 1 && mStartStopBtn.getText().equals("Start")) {
						

						mTimerLayout.setVisibility(View.GONE);
						mTimerSelectedLayout.setVisibility(View.VISIBLE);
						mSelectedText.setText("Music will stop exactly when battery level  " + mAlertTime.getSelectedItem().toString());
						//mCountDownText.setText(MediaUtils.getTimeFormat());
						sharedPreferences.edit().putBoolean(BATTERY_STARTED,true).commit();
						//GlobalMethods.storeValuetoPreference(mActivity, GlobalMethods.BOOLEAN_PREFERENCE, "battery_started", true);
						mStartStopBtn.setText("Stop");
						//mUtils.setRemainLevel(mTimerTime, true);
						BatteryRemainingEvent event = new BatteryRemainingEvent();
						event.setRemainingBattery(mTimerTime);
						event.setBattertEventStarted(true);
						EventBus.getDefault().post(event);
						mCountDownText.setVisibility(View.VISIBLE);
						int level = sharedPreferences.getInt("battery",0);//(Integer) GlobalMethods.getValueFromPreference(mActivity, GlobalMethods.INT_PREFERENCE, "battery");
						if(level > 0){
						mCountDownText.setText("Battery Status :"+level);
						}else
							mCountDownText.setText("");
						
					} else {
						mTimerLayout.setVisibility(View.VISIBLE);
						mTimerSelectedLayout.setVisibility(View.GONE);
						mSelectedText.setText("Music will stop exactly when battery level less than " + selectedTime);
						//mCountDownText.setText(MediaUtils.getTimeFormat());
						mStartStopBtn.setText("Start");
						sharedPreferences.edit().putBoolean(BATTERY_STARTED,false).commit();
						//GlobalMethods.storeValuetoPreference(mActivity, GlobalMethods.BOOLEAN_PREFERENCE, "battery_started", false);
						//unregisterReceiver(receiver)
						//mUtils.stopBatteryTimer();
						BatteryRemainingEvent event = new BatteryRemainingEvent();
						event.setRemainingBattery(-1);
						event.setBattertEventStarted(false);
						EventBus.getDefault().post(event);
						mCountDownText.setVisibility(View.GONE);
						//finish();
					}
					
		
				}
			});
		
			mCancelBtn.setOnClickListener(new View.OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					
					dismiss();
				}
			});

				}

	@Override
	public void onPause() {

		
		super.onPause();
	}

	@Override
	public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

		mTimerTime = progress;
		if (progress >= 1) {
			mPrompt = BatteryStatusDialog.this.getResources().getQuantityString(R.plurals.NNNminutes,
					mTimerTime, mTimerTime);
		} else {
			mPrompt = "Disabled";//SleepTimerDialog.this.getResources().getString(R.string.disabled);
		}
		mTimeView.setText(mPrompt);
	}

	@Override
	public void onStartTrackingTouch(SeekBar seekBar) {

	}

	@Override
	public void onStopTrackingTouch(SeekBar seekBar) {

	}

	@Override
	public void onResume() {

		super.onResume();
		if (mSleepTimerDialog != null && !mSleepTimerDialog.isShowing()) {
			mSleepTimerDialog.show();
		}
	}


	@Override
	public void onStart() {
		super.onStart();
		EventBus.getDefault().register(this);
	}

	public void onEvent(ChargingEvent event){

		int level = event.getBatteryLevel();
		if(level > 0){
			mCountDownText.setText("Battery Status :"+level);
			sharedPreferences.edit().putInt("battery",level).apply();
		}else
			mCountDownText.setText("");
	}

	public void  onEvent(BatteryRemainingEvent event){

		mRemained = event.getRemainingBattery();
	}
	public void onStop() {
		EventBus.getDefault().unregister(this);
		super.onStop();
		//FlurryAgent.onEndSession(this);
		if(sleepTimer!=null){
			sleepTimer.cancel();
		
		}
	}

	 SleepTimer sleepTimer = null;
	 long millis;
	 long countDownInterval = 1000;
	public  Intent intent;
	 class SleepTimer extends CountDownTimer{

		public  SleepTimer getInstance(long millisInFuture, long countDownInterval){
			millis  = millisInFuture;
			
			if(sleepTimer == null){
				
				sleepTimer = new SleepTimer(millis, countDownInterval);
				intent = new Intent();
				
			}
			
			return sleepTimer;
		}
		
		private SleepTimer(long millisInFuture, long countDownInterval1) {
			super(millisInFuture, countDownInterval1);
			// TODO Auto-generated constructor stub
			millis = millisInFuture;
		countDownInterval = countDownInterval1;
		}

		@TargetApi(Build.VERSION_CODES.HONEYCOMB)
		@Override
		public void onFinish() {
			// TODO Auto-generated method stub
			//mTimerLayout.setVisibility(View.VISIBLE);
			//mTimerSelectedLayout.setVisibility(View.GONE);
			//mSelectedText.setText("Music will stop exactly after " + selectedTime);
			//mCountDownText.setText(MediaUtils.getTimeFormat());
		}

		@Override
		public void onTick(long millisUntilFinished) {
			// TODO Auto-generated method stub
		String time  = (millisUntilFinished / 60000)+":"+(millisUntilFinished % 60000 / 1000);
		//mCountDownText.setText(time);
		}
		
	}

}
