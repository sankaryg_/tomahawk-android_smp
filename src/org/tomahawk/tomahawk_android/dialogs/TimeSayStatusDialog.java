
package org.tomahawk.tomahawk_android.dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.Spinner;
import android.widget.TextView;

import org.tomahawk.libtomahawk.utils.ViewUtils;
import org.tomahawk.tomahawk_android.R;
import org.tomahawk.tomahawk_android.TomahawkApp;
import org.tomahawk.tomahawk_android.events.BatteryRemainingEvent;
import org.tomahawk.tomahawk_android.events.CountDownTimeEvent;
import org.tomahawk.tomahawk_android.events.VoiceEvent;

import de.greenrobot.event.EventBus;

public class TimeSayStatusDialog extends DialogFragment implements OnSeekBarChangeListener {

	private SeekBar mSetTime;
	private TextView mTimeView;
	private String mPrompt;
	private long mProgress, mTimerTime;
	long mRemained;
	public static final String TIME_VALUES = "timestatus";
	public static final String TIME_STARTED = "timestarted";
	public static final String TIME_WHEN_PLAYING =  "time_when_playing";

	private AlertDialog mSleepTimerDialog;

	private String action;
	private Spinner mAlertTime;
	private LinearLayout mTimerLayout;
	private LinearLayout mTimerSelectedLayout;
	private TextView mSelectedText;
	private TextView titleText;
	private TextView mCountDownText;
	private TextView mCancelBtn;
	private TextView mStartStopBtn;
	private String alarmValue;
	private int spinnerPosition;
	private TimeSayStatusDialog mActivity;
	private View mDialogView;
	private LinearLayout checkBoxLayout;
	private CheckBox isPlayingCheckBox;
	private ArrayAdapter<String> apModeAdapter;
	private boolean checkedState;
	private SharedPreferences sharedPreferences;

	@NonNull
	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {
		// Check if there is a playlist key in the provided arguments


		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		builder.setView(getDialogView());
		return builder.create();
	}
	public View getDialogView() {
		return mDialogView;
	}


	@Override
	public void onCreate(Bundle icicle) {

		super.onCreate(icicle);
		LayoutInflater inflater = getActivity().getLayoutInflater();
		mDialogView = inflater.inflate(R.layout.battery_timer, null);
		mAlertTime = (Spinner) mDialogView.findViewById(R.id.alert_addTask);
		mTimerLayout = (LinearLayout) mDialogView.findViewById(R.id.selectTimeLayout);
		mTimerSelectedLayout = (LinearLayout)mDialogView.findViewById(R.id.selectedTimeLayout);
		mSelectedText = (TextView)mDialogView.findViewById(R.id.selectedTimeText);
		titleText = (TextView)mDialogView.findViewById(R.id.textView1);
		mCountDownText = (TextView)mDialogView.findViewById(R.id.textView2);
		mCancelBtn = (TextView)mDialogView.findViewById(R.id.config_dialog_negative_button);
		mStartStopBtn = (TextView)mDialogView.findViewById(R.id.config_dialog_positive_button);
		checkBoxLayout = (LinearLayout)mDialogView.findViewById(R.id.checkboxLayout);
		isPlayingCheckBox = (CheckBox)mDialogView.findViewById(R.id.checkbox1);

		isPlayingCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
					sharedPreferences.edit().putBoolean(TIME_WHEN_PLAYING,isChecked).apply();

			}
		});
		checkBoxLayout.setVisibility(View.VISIBLE);
		DisplayMetrics dm = new DisplayMetrics();
		dm = getResources().getDisplayMetrics();
		mActivity = this;
		titleText.setText("Know Time Through Voice");
		sharedPreferences = PreferenceManager.getDefaultSharedPreferences(TomahawkApp.getContext());
		alarmValue = ViewUtils.getVoiceData((String) sharedPreferences.getString(TIME_VALUES, "0"));
		apModeAdapter = new ArrayAdapter<String>(TomahawkApp.getContext(), R.layout.dropdown_header_textview, getResources().getStringArray(R.array.updateInterval) );
		apModeAdapter.setDropDownViewResource(R.layout.dropdown_header_dropdown_textview);
		mAlertTime.setAdapter(apModeAdapter);

		ArrayAdapter<String> myAdap = (ArrayAdapter<String>) mAlertTime.getAdapter(); 
		if(alarmValue==null)
			spinnerPosition = 0;
		else
		spinnerPosition = myAdap.getPosition(alarmValue);

		//set the default according to value
		mAlertTime.setSelection(spinnerPosition);
		checkedState = sharedPreferences.getBoolean(TIME_WHEN_PLAYING,false);
		isPlayingCheckBox.setChecked(checkedState);
	boolean started = sharedPreferences.getBoolean(TIME_STARTED,false);
			if(started){
				mStartStopBtn.setText("Stop");
				
			}else{
				mStartStopBtn.setText("Start");
				
			}
			if( started){
				mTimerLayout.setVisibility(View.GONE);
				mTimerSelectedLayout.setVisibility(View.VISIBLE);
				
				mSelectedText.setText("It will tell time through voice by the specified interval " + ViewUtils.getVoiceData((String) sharedPreferences.getString(TIME_VALUES, "0")));
				mCountDownText.setVisibility(View.VISIBLE);
				String level = sharedPreferences.getString("timer", null);//GlobalMethods.getValueFromPreference(mActivity, GlobalMethods.INT_PREFERENCE, "battery");
				if(level!=null && level.length() > 0){
				mCountDownText.setText("Current Time :"+ level);//ViewUtils.convertMillisToMinsSecs(level));
				}else
					mCountDownText.setText("");
				
			}else{
				mTimerLayout.setVisibility(View.VISIBLE);
				mTimerSelectedLayout.setVisibility(View.GONE);
				mCountDownText.setVisibility(View.GONE);

			}
			mStartStopBtn.setOnClickListener(new View.OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					String selectedTime = "0";
					if (mAlertTime.getSelectedItem().toString().contains(" ")) {
						selectedTime = (mAlertTime.getSelectedItem().toString().split(" ")[0]);
						if (selectedTime.length() > 0) {
							int seconds = Integer.parseInt(selectedTime)*60;
							sharedPreferences.edit().putString(TIME_VALUES, String.valueOf(seconds)).apply();//GlobalMethods.storeValuetoPreference(BatteryStatusDialog.this, GlobalMethods.STRING_PREFERENCE, Constants.BATTERY_VALUES, selectedTime);
							mTimerTime = (seconds);
						}
					}
					if (mTimerTime >= 1 && mStartStopBtn.getText().equals("Start")) {
						

						mTimerLayout.setVisibility(View.GONE);
						mTimerSelectedLayout.setVisibility(View.VISIBLE);
						mSelectedText.setText("It will tell time through voice by the specified interval " + mAlertTime.getSelectedItem().toString());
						//mCountDownText.setText(MediaUtils.getTimeFormat());
						sharedPreferences.edit().putBoolean(TIME_STARTED,true).apply();
						//GlobalMethods.storeValuetoPreference(mActivity, GlobalMethods.BOOLEAN_PREFERENCE, "battery_started", true);
						mStartStopBtn.setText("Stop");
						//mUtils.setRemainLevel(mTimerTime, true);
						VoiceEvent event = new VoiceEvent();
						event.setRemainingBattery(mTimerTime);
						event.setBattertEventStarted(true);
						EventBus.getDefault().post(event);
						mCountDownText.setVisibility(View.VISIBLE);
						String level = sharedPreferences.getString("timer", null);//(Integer) GlobalMethods.getValueFromPreference(mActivity, GlobalMethods.INT_PREFERENCE, "battery");
						if(level != null && level.length() > 0){
						mCountDownText.setText("Current Time :" + level);//ViewUtils.convertMillisToMinsSecs(level));
						}else
							mCountDownText.setText("");
						
					} else {
						mTimerLayout.setVisibility(View.VISIBLE);
						mTimerSelectedLayout.setVisibility(View.GONE);
						mSelectedText.setText("It will tell time through voice by the specified interval " + selectedTime);
						//mCountDownText.setText(MediaUtils.getTimeFormat());
						mStartStopBtn.setText("Start");
						sharedPreferences.edit().putBoolean(TIME_STARTED,false).apply();
						//GlobalMethods.storeValuetoPreference(mActivity, GlobalMethods.BOOLEAN_PREFERENCE, "battery_started", false);
						//unregisterReceiver(receiver)
						//mUtils.stopBatteryTimer();
						VoiceEvent event = new VoiceEvent();
						event.setRemainingBattery(-1);
						event.setBattertEventStarted(false);
						EventBus.getDefault().post(event);
						mCountDownText.setVisibility(View.GONE);
						//finish();
					}
					
		
				}
			});
		
			mCancelBtn.setOnClickListener(new View.OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					
					dismiss();
				}
			});

				}

	@Override
	public void onPause() {

		
		super.onPause();
	}

	@Override
	public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

		mTimerTime = progress;
		/*if (progress >= 1) {
			*//*mPrompt = TimeSayStatusDialog.this.getResources().getQuantityString(R.plurals.NNNminutes,
					mTimerTime, mTimerTime);*//*
		} else {
			mPrompt = "Disabled";//SleepTimerDialog.this.getResources().getString(R.string.disabled);
		}
		mTimeView.setText(mPrompt);*/
	}

	@Override
	public void onStartTrackingTouch(SeekBar seekBar) {

	}

	@Override
	public void onStopTrackingTouch(SeekBar seekBar) {

	}

	@Override
	public void onResume() {

		super.onResume();
	}


	@Override
	public void onStart() {
		super.onStart();
		EventBus.getDefault().register(this);
	}

	public void onEvent(CountDownTimeEvent event){

		String level = event.getCurrentTimeLevel();
		if(level != null && level.length() > 0){
			mCountDownText.setText("Current Time :" + level);//ViewUtils.convertMillisToMinsSecs(level));
			sharedPreferences.edit().putString("timer", level).apply();
		}else
			mCountDownText.setText("");
	}

	public void  onEvent(BatteryRemainingEvent event){

		mRemained = event.getRemainingBattery();
	}
	public void onStop() {
		EventBus.getDefault().unregister(this);
		super.onStop();
		//FlurryAgent.onEndSession(this);
	}

}
