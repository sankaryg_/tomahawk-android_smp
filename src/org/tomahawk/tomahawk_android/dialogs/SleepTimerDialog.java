
package org.tomahawk.tomahawk_android.dialogs;

import android.annotation.TargetApi;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.Spinner;
import android.widget.TextView;

import org.tomahawk.libtomahawk.utils.ViewUtils;
import org.tomahawk.tomahawk_android.R;
import org.tomahawk.tomahawk_android.TomahawkApp;
import org.tomahawk.tomahawk_android.events.SleepEvent;
import org.tomahawk.tomahawk_android.events.SleepRemainingEvent;
import org.tomahawk.tomahawk_android.services.PlaybackService;

import de.greenrobot.event.EventBus;

public class SleepTimerDialog extends DialogFragment implements OnSeekBarChangeListener
		{

	private SeekBar mSetTime;
	private TextView mTimeView;
	private String mPrompt;
	private int mProgress, mTimerTime;
	long mRemained;

	private AlertDialog mSleepTimerDialog;

	private String action;
	private Spinner mAlertTime;
	private LinearLayout mTimerLayout;
	private LinearLayout mTimerSelectedLayout;
	private TextView mSelectedText;
	private TextView mCountDownText;
	private TextView mCancelBtn;
	private TextView mStartStopBtn;
	private String alarmValue;
	private int spinnerPosition;
			private ArrayAdapter<String> apModeAdapter;
			private PlaybackService mPlaybackService;
			public static final String SLEEP_TIMER_VALUES = "sleeptimer";
			public static final String SLEEP_STARTED = "sleep_started";
	private SleepTimerDialog mActivity;

			private View mDialogView;
			private SharedPreferences sharedPreferences;

			@NonNull
			@Override
			public Dialog onCreateDialog(Bundle savedInstanceState) {
				// Check if there is a playlist key in the provided arguments
				AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
				builder.setView(getDialogView());
				return builder.create();
			}
			public View getDialogView() {
				return mDialogView;
			}
			public void setPlaybackService(PlaybackService mPlaybackService){
				this.mPlaybackService = mPlaybackService;
			}

			public SleepTimerDialog(){

			}
	@Override
	public void onCreate(Bundle icicle) {

		super.onCreate(icicle);
		LayoutInflater inflater = getActivity().getLayoutInflater();
		mDialogView = inflater.inflate(R.layout.sleep_timer, null);

		mAlertTime = (Spinner) mDialogView.findViewById(R.id.alert_addTask);
		mTimerLayout = (LinearLayout) mDialogView.findViewById(R.id.selectTimeLayout);
		mTimerSelectedLayout = (LinearLayout)mDialogView.findViewById(R.id.selectedTimeLayout);
		mSelectedText = (TextView)mDialogView.findViewById(R.id.selectedTimeText);
		mCountDownText = (TextView)mDialogView.findViewById(R.id.textView2);
		mCancelBtn = (TextView)mDialogView.findViewById(R.id.config_dialog_negative_button);
		mStartStopBtn = (TextView)mDialogView.findViewById(R.id.config_dialog_positive_button);
		DisplayMetrics dm = new DisplayMetrics();
		dm = getResources().getDisplayMetrics();
		mActivity = this;
		sharedPreferences = PreferenceManager.getDefaultSharedPreferences(TomahawkApp.getContext());
		alarmValue = ViewUtils.alarm(sharedPreferences.getString(SLEEP_TIMER_VALUES, "0"));// GlobalMethods.alarm((String)GlobalMethods.getValueFromPreference(SleepTimerDialog.this, GlobalMethods.STRING_PREFERENCE, Constants.SLEEP_TIMER_VALUES));
		//apModeAdapter = ArrayAdapter.createFromResource(TomahawkApp.getContext(), R.array.alert_array, android.R.layout.simple_spinner_item);
		apModeAdapter = new ArrayAdapter<String>(TomahawkApp.getContext(), R.layout.dropdown_header_textview, getResources().getStringArray(R.array.alert_array) );
		apModeAdapter.setDropDownViewResource(R.layout.dropdown_header_dropdown_textview);
		mAlertTime.setAdapter(apModeAdapter);
		ArrayAdapter<String> myAdap = (ArrayAdapter<String>) mAlertTime.getAdapter();
		if(alarmValue==null)
			spinnerPosition = 0;
		else
		spinnerPosition = myAdap.getPosition(alarmValue);

		//set the default according to value
		mAlertTime.setSelection(spinnerPosition);
		
		/*mSleepTimerDialog = new AlertDialog.Builder(this).create();
		mSleepTimerDialog.setVolumeControlStream(AudioManager.STREAM_MUSIC);*/

		mRemained =  mPlaybackService.sleepRemainingEvent() / 1000 ;

		

			boolean start = sharedPreferences.getBoolean(SLEEP_STARTED,false);//Boolean) GlobalMethods.getValueFromPreference(mActivity, GlobalMethods.BOOLEAN_PREFERENCE, "sleep_started");
			if(mRemained>0 && start){
				mStartStopBtn.setText("Stop");
				
			}else{
				mStartStopBtn.setText("Start");
				
			}
			if(mPlaybackService!=null && mPlaybackService.isPlaying() && mRemained > 0){
				if(sleepTimer!=null){
					sleepTimer.cancel();
				sleepTimer = null;
				}
				sleepTimer = new SleepTimer((mRemained)*1000, countDownInterval);
				sleepTimer.start();
				mTimerLayout.setVisibility(View.GONE);
				mTimerSelectedLayout.setVisibility(View.VISIBLE);
				
				mSelectedText.setText("Music will stop exactly after " +(mAlertTime.getSelectedItem().toString()));
				mCountDownText.setText(ViewUtils.getTimeFormat());
				
			}else{
				mTimerLayout.setVisibility(View.VISIBLE);
				mTimerSelectedLayout.setVisibility(View.GONE);


				SleepRemainingEvent event = new SleepRemainingEvent();
				event.setRemainingSleep(-1);
				EventBus.getDefault().post(event);

			}
			mStartStopBtn.setOnClickListener(new View.OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					String selectedTime = ViewUtils.convertAlarm(mAlertTime.getSelectedItem().toString());
					if(selectedTime.length()>0){
						sharedPreferences.edit().putString(SLEEP_TIMER_VALUES,selectedTime).commit();//GlobalMethods.storeValuetoPreference(SleepTimerDialog.this, GlobalMethods.STRING_PREFERENCE, Constants.SLEEP_TIMER_VALUES, selectedTime);
					mTimerTime = Integer.parseInt(selectedTime);
					}
					if (mTimerTime >= 1 && mStartStopBtn.getText().equals("Start")) {
						
						long milliseconds = mTimerTime * 1000;
						boolean gentle = true;//new PreferencesEditor(getApplicationContext())
								//.getBooleanPref(KEY_GENTLE_SLEEPTIMER, true);
						sleepTimer = new SleepTimer((mTimerTime)*1000, countDownInterval);
						sleepTimer.start();
						mTimerLayout.setVisibility(View.GONE);
						mTimerSelectedLayout.setVisibility(View.VISIBLE);
						sharedPreferences.edit().putBoolean(SLEEP_STARTED,true).commit();
						//GlobalMethods.storeValuetoPreference(mActivity, GlobalMethods.BOOLEAN_PREFERENCE, "sleep_started", true);
						mSelectedText.setText("Music will stop exactly after " + mAlertTime.getSelectedItem().toString());
						mCountDownText.setText(ViewUtils.getTimeFormat());
						mStartStopBtn.setText("Stop");
						//mUtils.startSleepTimer(milliseconds, gentle);
						SleepEvent event = new SleepEvent();
						event.setSleepLevel(milliseconds);
						EventBus.getDefault().post(event);
					} else {
						mTimerLayout.setVisibility(View.VISIBLE);
						mTimerSelectedLayout.setVisibility(View.GONE);
						mSelectedText.setText("Music will stop exactly after " + selectedTime);
						mCountDownText.setText(ViewUtils.getTimeFormat());
						mStartStopBtn.setText("Start");
						sharedPreferences.edit().putBoolean(SLEEP_STARTED,false).commit();
						//GlobalMethods.storeValuetoPreference(mActivity, GlobalMethods.BOOLEAN_PREFERENCE, "sleep_started", false);
						//unregisterReceiver(receiver)
						//mUtils.stopSleepTimer();
						SleepRemainingEvent event = new SleepRemainingEvent();
						event.setRemainingSleep(-1);
						EventBus.getDefault().post(event);
						//finish();
					}
					
		
				}
			});
		
			mCancelBtn.setOnClickListener(new View.OnClickListener() {
				
				@Override
				public void onClick(View v) {
					// TODO Auto-generated method stub
					if(sleepTimer!=null){
						sleepTimer.cancel();
						
					}
					dismiss();
				}
			});

	}

	@Override
	public void onPause() {

		if (mSleepTimerDialog != null && mSleepTimerDialog.isShowing()) {
			mSleepTimerDialog.dismiss();
		}
		super.onPause();
	}
			public void onEvent(SleepEvent event){
				/*Calendar now = Calendar.getInstance();
				mCurrentTimestamp = now.getTimeInMillis();
				mStopTimestamp = mCurrentTimestamp + event.getSleepLevel();

				mSleepTimerHandler.sendEmptyMessageDelayed(START_SLEEP_TIMER,
						event.getSleepLevel());*/

			}

	@Override
	public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

		mTimerTime = progress;
		if (progress >= 1) {
			mPrompt = SleepTimerDialog.this.getResources().getQuantityString(R.plurals.NNNminutes,
					mTimerTime, mTimerTime);
		} else {
			mPrompt = "Disabled";//SleepTimerDialog.this.getResources().getString(R.string.disabled);
		}
		mTimeView.setText(mPrompt);
	}

	@Override
	public void onStartTrackingTouch(SeekBar seekBar) {

	}

	@Override
	public void onStopTrackingTouch(SeekBar seekBar) {

	}

	@Override
	public void onResume() {

		super.onResume();
		if (mSleepTimerDialog != null && !mSleepTimerDialog.isShowing()) {
			mSleepTimerDialog.show();
		}
	}

	@Override
	public void onStart() {
		// TODO Auto-generated method stub
		super.onStart();
		EventBus.getDefault().register(this);
		//FlurryAgent.onStartSession(this, getString(R.string.flurry_key));
		//FlurryAgent.onEndSession(this);
	}
	
	public void onStop() {
		EventBus.getDefault().unregister(this);
		super.onStop();
		//FlurryAgent.onStartSession(this, getString(R.string.flurry_key));
		//FlurryAgent.onEndSession(this);
		if(sleepTimer!=null){
			sleepTimer.cancel();
		
		}
	}

	 SleepTimer sleepTimer = null;
	 long millis;
	 long countDownInterval = 1000;
	public  Intent intent;
	 class SleepTimer extends CountDownTimer{

		public  SleepTimer getInstance(long millisInFuture, long countDownInterval){
			millis  = millisInFuture;
			
			if(sleepTimer == null){
				
				sleepTimer = new SleepTimer(millis, countDownInterval);
				intent = new Intent();
				
			}
			
			return sleepTimer;
		}
		
		private SleepTimer(long millisInFuture, long countDownInterval1) {
			super(millisInFuture, countDownInterval1);
			// TODO Auto-generated constructor stub
			millis = millisInFuture;
		countDownInterval = countDownInterval1;
		}

		@TargetApi(Build.VERSION_CODES.HONEYCOMB)
		@Override
		public void onFinish() {
			// TODO Auto-generated method stub
			//mTimerLayout.setVisibility(View.VISIBLE);
			//mTimerSelectedLayout.setVisibility(View.GONE);
			//mSelectedText.setText("Music will stop exactly after " + selectedTime);
			//mCountDownText.setText(MediaUtils.getTimeFormat());
			//sendBroadcast(new Intent(Constants.UNREGISTER_BROADCAST_SLEEP_TIMER));
			mTimerLayout.setVisibility(View.VISIBLE);
			mTimerSelectedLayout.setVisibility(View.GONE);
			mSelectedText.setText("Music will stop exactly after " + mAlertTime.getSelectedItem().toString());
			mCountDownText.setText(ViewUtils.getTimeFormat());
			mStartStopBtn.setText("Start");
			sharedPreferences.edit().putBoolean(SLEEP_STARTED,false).commit();
			//GlobalMethods.storeValuetoPreference(mActivity, GlobalMethods.BOOLEAN_PREFERENCE, "sleep_started", false);
			//unregisterReceiver(receiver)
			//mUtils.stopSleepTimer();
		}

		@Override
		public void onTick(long millisUntilFinished) {
			// TODO Auto-generated method stub
		String time  = (millisUntilFinished / 60000)+":"+(millisUntilFinished % 60000 / 1000);
		mCountDownText.setText(time);
		}
		
	}

}
