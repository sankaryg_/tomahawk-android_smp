package org.tomahawk.tomahawk_android.dialogs;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.media.AudioManager;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;

import org.tomahawk.tomahawk_android.R;
import org.tomahawk.tomahawk_android.TomahawkApp;
import org.tomahawk.tomahawk_android.activities.TomahawkMainActivity;
import org.tomahawk.tomahawk_android.fragments.ContentHeaderFragment;
import org.tomahawk.tomahawk_android.fragments.EqualizerFragment_;
import org.tomahawk.tomahawk_android.fragments.TomahawkFragment;
import org.tomahawk.tomahawk_android.utils.FragmentUtils;
import org.tomahawk.tomahawk_android.utils.VolumeHelper;

/**
 * Created by YGS on 10/6/2015.
 */
public class VolumeStatusDialog extends DialogFragment {


    private View mDialogView;
    private AudioManager audioManager;
    private TextView mCancelBtn;
    private TextView mStartStopBtn;
    public static final String VOLUME_VALUES = "volume_value";
    private SharedPreferences sharedPreferences;
    private TomahawkMainActivity mActivity;
    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // Check if there is a playlist key in the provided arguments


        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setView(getDialogView());
        return builder.create();
    }
    public View getDialogView() {
        return mDialogView;
    }


    @Override
    public void onCreate(Bundle icicle) {

        super.onCreate(icicle);
        LayoutInflater inflater = getActivity().getLayoutInflater();
        mDialogView = inflater.inflate(R.layout.volume_settings, null);
        final TextView volumeNumber = (TextView) mDialogView
                .findViewById(R.id.volumeNumberText);
        ImageView defaultEqualizer = (ImageView) mDialogView
                .findViewById(R.id.defaultEqualizerBtn);
        final SeekBar volumeBar = (SeekBar) mDialogView
                .findViewById(R.id.volume_bar);
        mCancelBtn = (TextView)mDialogView.findViewById(R.id.config_dialog_negative_button);
        mStartStopBtn = (TextView)mDialogView.findViewById(R.id.config_dialog_positive_button);
        if (Build.VERSION.SDK_INT >= 9) {
            defaultEqualizer.setImageResource(R.drawable.eq);
        } else {
            defaultEqualizer.setImageResource(R.drawable.unmute);
        }
        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(TomahawkApp.getContext());
        volumeNumber.setText(String.valueOf(VolumeHelper
                .getMediaVolume(TomahawkApp.getContext())));
        audioManager = (AudioManager) TomahawkApp.getContext().getSystemService(Context.AUDIO_SERVICE);

        mCancelBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                audioManager.setStreamVolume(AudioManager.STREAM_MUSIC,
                        sharedPreferences.getInt(VOLUME_VALUES,0), 0);
                volumeNumber.setText(String.valueOf(VolumeHelper
                        .getMediaVolume(TomahawkApp.getContext())));
                dismiss();
            }
        });
        mStartStopBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                sharedPreferences.edit().putInt(VOLUME_VALUES,volumeBar.getProgress()).apply();
                dismiss();
            }
        });
        defaultEqualizer.setOnClickListener(new View.OnClickListener() {

            @SuppressLint("InlinedApi")
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub

                Bundle bundle = new Bundle();
                bundle.putInt(TomahawkFragment.CONTENT_HEADER_MODE,
                        ContentHeaderFragment.MODE_ACTIONBAR_FILLED);
                FragmentUtils.replace(mActivity, EqualizerFragment_.class,
                        bundle);
                dismiss();

            }
        });
        volumeBar.setMax(audioManager
                .getStreamMaxVolume(AudioManager.STREAM_MUSIC));
        volumeBar.setProgress(audioManager
                .getStreamVolume(AudioManager.STREAM_MUSIC));
        sharedPreferences.edit().putInt(VOLUME_VALUES,volumeBar.getProgress()).commit();
        volumeBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onStopTrackingTouch(SeekBar arg0) {
            }

            @Override
            public void onStartTrackingTouch(SeekBar arg0) {
            }

            @Override
            public void onProgressChanged(SeekBar arg0, int progress,
                                          boolean arg2) {
                audioManager.setStreamVolume(AudioManager.STREAM_MUSIC,
                        progress, 0);
                volumeNumber.setText(String.valueOf(VolumeHelper
                        .getMediaVolume(TomahawkApp.getContext())));

            }
        });



    }

    public void setActivity(TomahawkMainActivity tomahawkMainActivity){
        mActivity = tomahawkMainActivity;
    }
    @Override
    public void onPause() {


        super.onPause();
    }



    @Override
    public void onResume() {

        super.onResume();

    }


    @Override
    public void onStart() {
        super.onStart();
    }

    public void onStop() {
         super.onStop();

    }

}
