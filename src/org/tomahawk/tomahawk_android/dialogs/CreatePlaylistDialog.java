/* == This file is part of Tomahawk Player - <http://tomahawk-player.org> ===
 *
 *   Copyright 2013, Enno Gottschalk <mrmaffen@googlemail.com>
 *
 *   Tomahawk is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Tomahawk is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Tomahawk. If not, see <http://www.gnu.org/licenses/>.
 */
package org.tomahawk.tomahawk_android.dialogs;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import org.tomahawk.libtomahawk.collection.Collection;
import org.tomahawk.libtomahawk.collection.CollectionManager;
import org.tomahawk.libtomahawk.collection.Playlist;
import org.tomahawk.libtomahawk.collection.PlaylistEntry;
import org.tomahawk.libtomahawk.database.DatabaseHelper;
import org.tomahawk.libtomahawk.resolver.Query;
import org.tomahawk.libtomahawk.utils.VariousUtils;
import org.tomahawk.libtomahawk.utils.ViewUtils;
import org.tomahawk.tomahawk_android.R;
import org.tomahawk.tomahawk_android.TomahawkApp;
import org.tomahawk.tomahawk_android.activities.TomahawkMainActivity;
import org.tomahawk.tomahawk_android.fragments.ContentHeaderFragment;
import org.tomahawk.tomahawk_android.fragments.PlaylistEntriesFragment;
import org.tomahawk.tomahawk_android.fragments.TomahawkFragment;
import org.tomahawk.tomahawk_android.ui.widgets.ConfigEdittext;
import org.tomahawk.tomahawk_android.utils.FragmentUtils;

import java.util.ArrayList;
import java.util.Vector;

import de.greenrobot.event.EventBus;

/**
 * A {@link DialogFragment} which is presented for the user so that he can choose a name for the
 * {@link org.tomahawk.libtomahawk.collection.Playlist} he intends to create
 */
public class CreatePlaylistDialog extends ConfigDialog {

    private Playlist mPlaylist;

    private EditText mNameEditText;

    protected Collection mCollection;

    public static  ArrayList<String> mQueryKeys;

    protected ArrayList<Query> mQueryArray;

    public static  ArrayList<String> mEntryKeys;

    protected ArrayList<PlaylistEntry> mEntryArray;

    int type;

    String title;

    Long mRenameId;

    String mOriginalName;

    String mDialogMessage;

    String desc;

    Long audio_id;

    LayoutInflater inflater1;

    String mAlbumName,mArtistName,mTrackName,mGenreName,mFolderName,mFolderPath;

    private Vector<AlertDialog> dialogs = new Vector<AlertDialog>();


    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // Check if there is a playlist key in the provided arguments
        if (getArguments() != null && getArguments()
                .containsKey(TomahawkFragment.PLAYLIST)) {
            String playlistId = getArguments().getString(TomahawkFragment.PLAYLIST);
            mRenameId = Long.valueOf(playlistId);
            mPlaylist = null;//CollectionDbManager.get().getCollectionDb(CollectionManager.get().getCollection().getId()).getPlaylist(playlistId);
            if (mPlaylist == null) {
                mPlaylist = Playlist.getByKey(playlistId);
                if (mPlaylist == null) {
                    dismiss();
                }
            }
        }
        if (getArguments().containsKey(TomahawkFragment.COLLECTION_ID)) {
            mCollection = CollectionManager.get()
                    .getCollection(getArguments().getString(TomahawkFragment.COLLECTION_ID));
        }

        if (getArguments().containsKey(TomahawkFragment.ALBUM_NAME)) {
            mAlbumName = getArguments().getString(TomahawkFragment.ALBUM_NAME);
        }

        if (getArguments().containsKey(TomahawkFragment.ARTIST_NAME)) {
            mArtistName = getArguments().getString(TomahawkFragment.ARTIST_NAME);
        }

        if (getArguments().containsKey(TomahawkFragment.GENRE_NAME)) {
            mGenreName = getArguments().getString(TomahawkFragment.GENRE_NAME);
        }
        if (getArguments().containsKey(TomahawkFragment.FOLDER_NAME)) {
            mFolderName = getArguments().getString(TomahawkFragment.FOLDER_NAME);
        }

        if (getArguments().containsKey(TomahawkFragment.FOLDER_PATH)) {
            mFolderPath = getArguments().getString(TomahawkFragment.FOLDER_PATH);
        }

        if (getArguments().containsKey(TomahawkFragment.TRACK_NAME)) {
            mTrackName = getArguments().getString(TomahawkFragment.TRACK_NAME);
        }

        if (getArguments().containsKey(TomahawkFragment.QUERYARRAY)) {
            mQueryArray = new ArrayList<>();
            mQueryKeys = getArguments().getStringArrayList(TomahawkFragment.QUERYARRAY);
            for (String queryKey : mQueryKeys) {
                Query query = Query.getByKey(queryKey);
                if (query != null) {
                    mQueryArray.add(query);
                }
            }
        }

        if (getArguments().containsKey(TomahawkFragment.ENTRYARRAY)) {
            mEntryArray = new ArrayList<>();
            mEntryKeys = getArguments().getStringArrayList(TomahawkFragment.ENTRYARRAY);
            for (String queryKey : mEntryKeys) {
                PlaylistEntry query = PlaylistEntry.getByKey(queryKey);
                if (query != null) {
                    mEntryArray.add(query);
                }
            }
        }

        if (getArguments().containsKey(TomahawkFragment.DE_DIALOG_TYPE)){
            type = getArguments().getInt(TomahawkFragment.DE_DIALOG_TYPE);
        }

        if (getArguments().containsKey(TomahawkFragment.DE_TITLE)){
            title = getArguments().getString(TomahawkFragment.DE_TITLE);
        }

        if (getArguments().containsKey(TomahawkFragment.DE_LINE_1)){
            desc = getArguments().getString(TomahawkFragment.DE_LINE_1);
        }

        if (getArguments().containsKey(TomahawkFragment.AUDIO_ID)){
            audio_id = getArguments().getLong(TomahawkFragment.AUDIO_ID);
        }
        //set the proper flags for our edittext


        if (type == TomahawkFragment.RENAME_PLAYLIST_DIALOG){
            mNameEditText = (ConfigEdittext) addScrollingViewToFrame(R.layout.config_edittext);
            mNameEditText.setHint(R.string.name_playlist);
            mNameEditText.setOnEditorActionListener(mOnKeyboardEnterListener);
            mOriginalName = nameForId(mRenameId);
            String defaultname = mOriginalName;
            mNameEditText.setHint("");

            if (mRenameId < 0 || mOriginalName == null || defaultname == null) {
                Log.i("@@@@", "Rename failed: " + mRenameId + "/" + defaultname);
                dismiss();
            }
            String promptformat;
            if (mOriginalName.equals(defaultname)) {
                promptformat = getString(R.string.rename_playlist_same_prompt);
            } else {
                promptformat = getString(R.string.rename_playlist_diff_prompt);
            }

            String prompt = String.format(promptformat, mOriginalName, defaultname);
            mDialogMessage = prompt;
            mNameEditText.setSelection(mNameEditText.getText().length());
            mNameEditText.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                }

                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    String typedname = mNameEditText.getText().toString();
                    mOriginalName = typedname;
                    if (typedname.trim().length() == 0) {
                        mPositiveButton.setTextColor(getResources().getColor(R.color.light_grey));
                        mPositiveButton.setEnabled(false);
                    } else {
                        mPositiveButton.setTextColor(getResources().getColor(R.color.tomahawk_red));
                        mPositiveButton.setEnabled(true);
                        if (idForplaylist(typedname) >= 0
                                && !mOriginalName.equals(typedname)) {
                            mPositiveButton.setText(R.string.create_playlist_overwrite_text);
                        } else {
                            mPositiveButton.setText(R.string.create_playlist_create_text);
                        }
                    }
                }

                @Override
                public void afterTextChanged(Editable s) {

                }
            });
            setDialogTitle(getString(R.string.rename_playlist));
            setDialogMsg(mDialogMessage);
        }else if (type == TomahawkFragment.ADD_TO_PLAYLIST_DIALOG){
            inflater1 = (LayoutInflater) TomahawkApp.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            String[] cols = new String[] {
                    MediaStore.Audio.Playlists._ID,
                    MediaStore.Audio.Playlists.NAME
            };
            ContentResolver resolver = TomahawkApp.getContext().getContentResolver();
            if (resolver == null) {
                System.out.println("resolver = null");
            } else {
                String whereclause = MediaStore.Audio.Playlists.NAME + " != ''";
                Cursor cur = resolver.query(MediaStore.Audio.Playlists.EXTERNAL_CONTENT_URI,
                        cols, whereclause, null,
                        MediaStore.Audio.Playlists.NAME);
                addScrollingViewToFrame(customView(0,  TomahawkApp.getContext().getString(R.string.new_playlist)));
                //addScrollingViewToFrame(customView(1,  TomahawkApp.getContext().getString(R.string.current_playlist)));
                //playListLayout.addView(customView(1,  context.getString(R.string.new_playlist)));
                int i=1;
                if (cur != null && cur.getCount() > 0) {
                    cur.moveToFirst();
                    while (! cur.isAfterLast()) {
                        addScrollingViewToFrame(customView(i, cur.getLong(0) + ";:" + cur.getString(1)));
                        cur.moveToNext();
                    }
                }

                if (cur != null) {
                    cur.close();
                }
            }
            mBottomView.setVisibility(View.GONE);
            setDialogTitle(getString(R.string.add_playlist));
        }else if(type == TomahawkFragment.DELETE_DIALOG){
            mPositiveButton.setText(getString(R.string.context_menu_delete));
            setDialogTitle(getString(R.string.context_menu_delete));
            setDialogMsg(title);
        }else if (type == TomahawkFragment.RINGTONE_DIALOG){
            setDialogTitle(title);
            setDialogMsg(desc);
        }
        else {
            mNameEditText = (ConfigEdittext) addScrollingViewToFrame(R.layout.config_edittext);
            mNameEditText.setHint(R.string.name_playlist);
            mNameEditText.setOnEditorActionListener(mOnKeyboardEnterListener);
            setDialogTitle(getString(R.string.create_playlist));
        }

        if (mNameEditText!=null)
        ViewUtils.showSoftKeyboard(mNameEditText);

        //Set the textview's text to the proper title


        hideStatusImage();

        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setView(getDialogView());
        AlertDialog dialog = builder.create();
        dialogs.add(dialog);
        return dialog;
    }

    public View customView(final int i, final String string) {
        final View viewComment = inflater1.inflate(R.layout.multiple_delivery,
                null);
        viewComment.setTag(i);
        TextView pkg = (TextView) viewComment.findViewById(R.id.pkg);
        final TextView status = (TextView) viewComment
                .findViewById(R.id.status);
        pkg.setTag(i);
        pkg.setSingleLine(true);
        status.setVisibility(View.GONE);
        if(i!=0){
            Integer inte = Integer.parseInt(string.split(";:")[0]);
            pkg.setTag(inte);
            String abc = string.split(";:")[1];
            pkg.setText(abc);
        }
        else
            pkg.setText(string);
        pkg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int i = (Integer) v.getTag();
                if (i == 0) {
                    //New Playlist
                    ArrayList<Query> queries = mQueryArray != null ? mQueryArray : new ArrayList<Query>();
                    Playlist playlist = Playlist.fromQueryList(
                            TomahawkMainActivity.getLifetimeUniqueStringId(), "", null, queries);
                    CreatePlaylistDialog dialog = new CreatePlaylistDialog();
                    Bundle args = new Bundle();
                    args.putString(TomahawkFragment.PLAYLIST, playlist.getId());
                    args.putString(TomahawkFragment.COLLECTION_ID, mCollection.getId());
                    if (mQueryKeys != null && mQueryKeys.size() > 0) {
                        args.putStringArrayList(TomahawkFragment.QUERYARRAY, mQueryKeys);
                    }
                    if (mEntryKeys != null && mEntryKeys.size() > 0) {
                        args.putStringArrayList(TomahawkFragment.ENTRYARRAY, mEntryKeys);
                    }
                    dialog.setArguments(args);
                    dialog.show(getFragmentManager(), null);
                }/*else if (i==1){
                    //current playlist
                    ArrayList<Query> queries = mQueryArray != null ? mQueryArray : new ArrayList<Query>();
                    ((TomahawkMainActivity)getActivity()).getPlaybackService()
                            .addQueriesToQueue(queries);

                }*/ else {
                    //Add songs to selected playlist
                    Long playlist = Long.valueOf(string.split(";:")[0]);
                    String name = string.split(";:")[1];
                    if (name != null && name.length() > 0) {
                        ContentResolver resolver = TomahawkApp.getContext().getContentResolver();
                        int id = idForplaylist(name);
                        Uri uri;
                        if (id >= 0) {
                            uri = ContentUris.withAppendedId(MediaStore.Audio.Playlists.EXTERNAL_CONTENT_URI, id);
                            //mUtils.clearPlaylist(id);
                        } else {
                            ContentValues values = new ContentValues(1);
                            values.put(MediaStore.Audio.Playlists.NAME, name);
                            uri = resolver.insert(MediaStore.Audio.Playlists.EXTERNAL_CONTENT_URI, values);

                        }
                        if (uri != null) {
                            String playlistId = String.valueOf(playlist);//(Playlist) item).getId();
                            if (mEntryArray != null) {
                                ArrayList<PlaylistEntry> entries = mEntryArray;//new ArrayList<>();

                               /* for (Query query : mQueryArray) {
                                    entries.add(PlaylistEntry.get(playlistId, query,
                                            TomahawkMainActivity.getLifetimeUniqueStringId()));
                                }*/
                                CollectionManager.get().addPlaylistEntries(playlistId, entries);
                            }
                        }
                    }
                }
                dismiss();
            }

        });
        return viewComment;
    }

    private String nameForId(long id) {
        Cursor c = TomahawkApp.getContext().getContentResolver().query(MediaStore.Audio.Playlists.EXTERNAL_CONTENT_URI,
                new String[]{MediaStore.Audio.Playlists.NAME},
                MediaStore.Audio.Playlists._ID + "=?",
                new String[]{Long.valueOf(id).toString()},
                MediaStore.Audio.Playlists.NAME);
        String name = null;
        if (c != null) {
            c.moveToFirst();
            if (!c.isAfterLast()) {
                name = c.getString(0);
            }
        }
        c.close();
        return name;
    }

    /**
     * Persist a {@link org.tomahawk.libtomahawk.collection.Playlist} as a {@link
     * org.tomahawk.libtomahawk.collection.Playlist} in our database
     */
    private void savePlaylist() {

            if (type == TomahawkFragment.RENAME_PLAYLIST_DIALOG) {
                mOriginalName = mNameEditText.getText().toString();
                if (mOriginalName != null && mOriginalName.length() > 0) {
                    ContentResolver resolver = TomahawkApp.getContext().getContentResolver();
                    ContentValues values = new ContentValues(1);
                    values.put(MediaStore.Audio.Playlists.NAME, mOriginalName);
                    resolver.update(MediaStore.Audio.Playlists.EXTERNAL_CONTENT_URI,
                            values,
                            MediaStore.Audio.Playlists._ID + "=?",
                            new String[]{Long.valueOf(mRenameId).toString()});
                }
                DatabaseHelper.PlaylistsActionEvent event = new DatabaseHelper.PlaylistsActionEvent();
                EventBus.getDefault().post(event);
                }else if (type == TomahawkFragment.DELETE_DIALOG){
                    if (mAlbumName!=null){
                        CollectionManager.get().deleteAlbumTracks(mAlbumName);
                        ((TomahawkMainActivity) getActivity()).getPlaybackService().deleteCurrentPlaylistEntry(mEntryArray);
                    }else if (mFolderName != null){
                        CollectionManager.get().deleteFolderTracks(mFolderPath, mFolderName);
                        ((TomahawkMainActivity) getActivity()).getPlaybackService().deleteCurrentPlaylistEntry(mEntryArray);

                    }else if (mArtistName!=null){
                        CollectionManager.get().deleteArtistTracks(mArtistName);
                        ((TomahawkMainActivity) getActivity()).getPlaybackService().deleteCurrentPlaylistEntry(mEntryArray);

                    }else if (mGenreName!=null){
                        CollectionManager.get().deleteGenreTracks(mGenreName);
                        ((TomahawkMainActivity) getActivity()).getPlaybackService().deleteCurrentPlaylistEntry(mEntryArray);

                    }else if (mTrackName!=null){
                        CollectionManager.get().deleteTracks(mQueryArray.get(0).getBasicTrack().getTrackId());
                        ((TomahawkMainActivity) getActivity()).getPlaybackService().deleteCurrentPlaylistEntry(mEntryArray);

                    }
                    else if (mPlaylist != null){
                        CollectionManager.get().deletePlaylist(mPlaylist.getId());
                        Uri uri = ContentUris.withAppendedId(
                                MediaStore.Audio.Playlists.EXTERNAL_CONTENT_URI, Long.valueOf(mPlaylist.getId()));
                        TomahawkApp.getContext().getContentResolver().delete(uri, null, null);
                        DatabaseHelper.PlaylistsActionEvent event = new DatabaseHelper.PlaylistsActionEvent();
                        EventBus.getDefault().post(event);
                    }
            }else if (type == TomahawkFragment.RINGTONE_DIALOG){
                VariousUtils.setRingtone(TomahawkApp.getContext(),audio_id);
            }
            else {
                if (mPlaylist != null) {
                    ArrayList<PlaylistEntry> entries = (ArrayList<PlaylistEntry>) mPlaylist.getEntries();
                    Uri uri = null;

                    String playlistName = TextUtils.isEmpty(mNameEditText.getText().toString())
                            ? getString(R.string.playlist)
                            : mNameEditText.getText().toString();

                    {
                        if (playlistName != null && playlistName.length() > 0) {
                            ContentResolver resolver = TomahawkApp.getContext().getContentResolver();
                            int id = idForplaylist(playlistName);
                            if (id >= 0) {
                                uri = ContentUris
                                        .withAppendedId(
                                                MediaStore.Audio.Playlists.EXTERNAL_CONTENT_URI,
                                                id);
                                clearPlaylist(TomahawkApp.getContext(), id);
                            } else {
                                ContentValues values = new ContentValues(1);
                                values.put(MediaStore.Audio.Playlists.NAME, playlistName);
                                uri = resolver.insert(
                                        MediaStore.Audio.Playlists.EXTERNAL_CONTENT_URI,
                                        values);
                            }
                            if (uri != null) {

                                mPlaylist = Playlist.get(String.valueOf(Long.parseLong(uri.getLastPathSegment())));
                                mPlaylist.setName(playlistName);
                                mPlaylist.setEntries(entries);
                        /*long [] list =(mPlaylistAlbumItem.length() > 0) ? mUtils
                                .getSongListForAlbum(Long
                                        .parseLong(mPlaylistAlbumItem))
                                : (mPlaylistArtistItem.length() > 0) ? mUtils
                                .getSongListForArtist(Long
                                        .parseLong(mPlaylistArtistItem))
                                : new long[] { mPlaylistSelectedItem };
                        mUtils.addToPlaylist(list, Long.parseLong(uri.getLastPathSegment()));*/
                            }

                        }

                    }
                    CollectionManager.get().createPlaylist(mPlaylist);
                    if (entries != null && entries.size() > 0) {

                /*for (Query query : mQueryArray) {
                    entries.add(PlaylistEntry.get(String.valueOf(Long.parseLong(uri.getLastPathSegment())), query,
                            TomahawkMainActivity.getLifetimeUniqueStringId()));
                }*/
                        CollectionManager.get().addPlaylistEntries(String.valueOf(Long.parseLong(uri.getLastPathSegment())), entries);
                    }
                    Bundle bundle = new Bundle();
                    bundle.putString(TomahawkFragment.PLAYLIST, mPlaylist.getId());
                    bundle.putString(TomahawkFragment.COLLECTION_ID, mCollection.getId());
                    bundle.putInt(TomahawkFragment.CONTENT_HEADER_MODE,
                            ContentHeaderFragment.MODE_HEADER_DYNAMIC);
                    FragmentUtils.replace((TomahawkMainActivity) getActivity(),
                            PlaylistEntriesFragment.class, bundle);
                }
            }

        closeDialogs();
    }

    /**/
    public void closeDialogs() {
        for (AlertDialog dialog : dialogs)
            if (dialog.isShowing()) dialog.dismiss();
    }
    public static void clearPlaylist(Context context, int plid) {

        Uri uri = MediaStore.Audio.Playlists.Members.getContentUri("external", plid);
        context.getContentResolver().delete(uri, null, null);
        return;
    }

    private int idForplaylist(String name) {
        Cursor c = query(TomahawkApp.getContext(),
                MediaStore.Audio.Playlists.EXTERNAL_CONTENT_URI,
                new String[]{MediaStore.Audio.Playlists._ID},
                MediaStore.Audio.Playlists.NAME + "=?", new String[]{name},
                MediaStore.Audio.Playlists.NAME,0);
        int id = -1;
        if (c != null) {
            c.moveToFirst();
            if (!c.isAfterLast()) {
                id = c.getInt(0);
            }
            c.close();
        }
        return id;
    }

    public static Cursor query(Context context, Uri uri, String[] projection,
                               String selection, String[] selectionArgs, String sortOrder, int limit) {
        try {
            ContentResolver resolver = context.getContentResolver();
            if (resolver == null) {
                return null;
            }
            if (limit > 0) {
                uri = uri.buildUpon().appendQueryParameter("limit", "" + limit).build();
            }
            return resolver.query(uri, projection, selection, selectionArgs, sortOrder);
        } catch (UnsupportedOperationException ex) {
            return null;
        }

    }

    @Override
    protected void onEnabledCheckedChange(boolean checked) {
    }

    @Override
    protected void onConfigTestResult(Object component, int type, String message) {
    }

    @Override
    protected void onPositiveAction() {
        savePlaylist();
        dismiss();
    }

    @Override
    protected void onNegativeAction() {
        dismiss();
    }
}
