/* == This file is part of Tomahawk Player - <http://tomahawk-player.org> ===
 *
 *   Copyright 2012, Enno Gottschalk <mrmaffen@googlemail.com>
 *
 *   Tomahawk is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Tomahawk is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Tomahawk. If not, see <http://www.gnu.org/licenses/>.
 */
package org.tomahawk.tomahawk_android.fragments;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;

import org.tomahawk.tomahawk_android.R;
import org.tomahawk.tomahawk_android.TomahawkApp;
import org.tomahawk.tomahawk_android.adapters.FakePreferencesAdapter;
import org.tomahawk.tomahawk_android.utils.FakePreferenceGroup;

import java.util.ArrayList;
import java.util.List;

/**
 * {@link TomahawkListFragment} which fakes the standard {@link android.preference.PreferenceFragment}
 * behaviour. We need to fake it, because the official support library doesn't provide a {@link
 * android.preference.PreferenceFragment} class
 */
public class PreferenceAdvancedFragment extends TomahawkListFragment
        implements OnItemClickListener, SharedPreferences.OnSharedPreferenceChangeListener {

    private static final String TAG = PreferenceAdvancedFragment.class.getSimpleName();

    public static final String FAKEPREFERENCEFRAGMENT_ID_PREFRATEBTN = "pref_rate";

    public static final String FAKEPREFERENCEFRAGMENT_ID_FADEINOUT = "fade_in_out";

    public static final String FAKEPREFERENCEFRAGMENT_ID_VISUALIZER = "visualizer";

    public static final String FAKEPREFERENCEFRAGMENT_ID_HEADSET_PAUSE = "mheadeset_Pause";

    public static final String FAKEPREFERENCEFRAGMENT_ID_HEADSET_PLAY = "mheadeset_Play";

    public static final String FAKEPREFERENCEFRAGMENT_ID_LICENSES = "licenses";

    public static final String FAKEPREFERENCEFRAGMENT_KEY_PREFRATEBTN
            = "org.tomahawk.tomahawk_android.pref_rate";

    public static final String FAKEPREFERENCEFRAGMENT_KEY_FADEINOUT
            = "org.tomahawk.tomahawk_android.fade_in_out";

    public static final String FAKEPREFERENCEFRAGMENT_KEY_VISUALIZER
            = "org.tomahawk.tomahawk_android.visualizer";

    public static final String FAKEPREFERENCEFRAGMENT_KEY_HEADSET_PAUSE
            = "org.tomahawk.tomahawk_android.mheadset_Pause";

    public static final String FAKEPREFERENCEFRAGMENT_KEY_HEADSET_PLAY
            = "org.tomahawk.tomahawk_android.mheadset_Play";

    public static final String FAKEPREFERENCEFRAGMENT_KEY_LICENSES = "org.tomahawk.tomahawk_android.licenses";

    public static final int VISUALIZER_FFT = 0;

    public static final int VISUALIZER_WAVE = 1;

    public static final int VISUALIZER_OFF = 2;



    private SharedPreferences mSharedPreferences;

    /**
     * Called, when this {@link PreferenceAdvancedFragment}'s {@link android.view.View} has been
     * created
     */
    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        // Fetch our SharedPreferences from the PreferenceManager
        mSharedPreferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
        mSharedPreferences.registerOnSharedPreferenceChangeListener(this);

        // Set up the set of FakePreferences to be shown in this Fragment
        List<FakePreferenceGroup> fakePreferenceGroups = new ArrayList<>();
        FakePreferenceGroup prefGroup = new FakePreferenceGroup(
                getString(R.string.preferences_playback));
        prefGroup.addFakePreference(new FakePreferenceGroup.FakePreference(
                FakePreferenceGroup.FAKEPREFERENCE_TYPE_BUTTON,
                FAKEPREFERENCEFRAGMENT_ID_PREFRATEBTN,
                FAKEPREFERENCEFRAGMENT_KEY_PREFRATEBTN,
                getString(R.string.preferences_rate),
                "",false));
        prefGroup.addFakePreference(new FakePreferenceGroup.FakePreference(
                FakePreferenceGroup.FAKEPREFERENCE_TYPE_CHECKBOX,
                FAKEPREFERENCEFRAGMENT_ID_FADEINOUT,
                FAKEPREFERENCEFRAGMENT_KEY_FADEINOUT,
                getString(R.string.fade_in_title),
                getString(R.string.fade_in_summary),false));
        prefGroup.addFakePreference(new FakePreferenceGroup.FakePreference(
                FakePreferenceGroup.FAKEPREFERENCE_TYPE_SPINNER,
                FAKEPREFERENCEFRAGMENT_ID_VISUALIZER,
                FAKEPREFERENCEFRAGMENT_KEY_VISUALIZER,
                getString(R.string.visualizer_title),
                getString(R.string.visualizer_summary),false));


        prefGroup.addFakePreference(new FakePreferenceGroup.FakePreference(
                FakePreferenceGroup.FAKEPREFERENCE_TYPE_CHECKBOX,
                FAKEPREFERENCEFRAGMENT_ID_HEADSET_PAUSE,
                FAKEPREFERENCEFRAGMENT_KEY_HEADSET_PAUSE,
                getString(R.string.pause_headset_title),
                getString(R.string.pause_headset_summary),false));


        prefGroup.addFakePreference(new FakePreferenceGroup.FakePreference(
                FakePreferenceGroup.FAKEPREFERENCE_TYPE_CHECKBOX,
                FAKEPREFERENCEFRAGMENT_ID_HEADSET_PLAY,
                FAKEPREFERENCEFRAGMENT_KEY_HEADSET_PLAY,
                getString(R.string.play_headset_title),
                getString(R.string.play_headset_summary),false));

        prefGroup.addFakePreference(new FakePreferenceGroup.FakePreference(
                FakePreferenceGroup.FAKEPREFERENCE_TYPE_PLAIN,
                FAKEPREFERENCEFRAGMENT_ID_LICENSES,
                FAKEPREFERENCEFRAGMENT_KEY_LICENSES,
                getString(R.string.preferences_licenses),
                "",false));

        fakePreferenceGroups.add(prefGroup);
        final TomahawkApp app = (TomahawkApp)getActivity().getApplication();
        // Now we can push the complete set of FakePreferences into our FakePreferencesAdapter,
        // so that it can provide our ListView with the correct Views.
        FakePreferencesAdapter fakePreferencesAdapter = new FakePreferencesAdapter(getActivity(),
                getActivity().getLayoutInflater(), fakePreferenceGroups,app);
        setListAdapter(fakePreferencesAdapter);

        getListView().setOnItemClickListener(this);
        setupNonScrollableSpacer(getListView());
    }

    /**
     * Initialize
     */
    @Override
    public void onResume() {
        super.onResume();

        getListAdapter().notifyDataSetChanged();
    }

    /**
     * Called every time an item inside the {@link se.emilsjolander.stickylistheaders.StickyListHeadersListView}
     * is clicked
     *
     * @param parent   The AdapterView where the click happened.
     * @param view     The view within the AdapterView that was clicked (this will be a view
     *                 provided by the adapter)
     * @param position The position of the view in the adapter.
     * @param id       The row id of the item that was clicked.
     */
    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        FakePreferenceGroup.FakePreference fakePreference
                = (FakePreferenceGroup.FakePreference) getListAdapter().getItem(position);
        if (fakePreference.getType() == FakePreferenceGroup.FAKEPREFERENCE_TYPE_CHECKBOX) {
            // if a FakePreference of type "FAKEPREFERENCE_TYPE_CHECKBOX" has been clicked,
            // we edit the associated SharedPreference and toggle its boolean value
            SharedPreferences.Editor editor = mSharedPreferences.edit();
            boolean preferenceState = mSharedPreferences
                    .getBoolean(fakePreference.getStorageKey(), false);
            editor.putBoolean(fakePreference.getStorageKey(), !preferenceState);
            editor.commit();
        } /*else if (fakePreference.getType() == FakePreferenceGroup.FAKEPREFERENCE_TYPE_PLAIN) {
            String key = fakePreference.getKey();
            if (key.equals(FAKEPREFERENCEFRAGMENT_ID_EQUALIZER)) {
                Bundle bundle = new Bundle();
                bundle.putInt(TomahawkFragment.CONTENT_HEADER_MODE,
                        ContentHeaderFragment.MODE_ACTIONBAR_FILLED);
                FragmentUtils.replace((TomahawkMainActivity) getActivity(), EqualizerFragment_.class,
                        bundle);
            } else if (key.equals(FAKEPREFERENCEFRAGMENT_ID_SCROBBLEEVERYTHING)) {
                ((TomahawkMainActivity) getActivity()).askAccess();
            }
        }*/
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        getListAdapter().notifyDataSetChanged();
    }
}
