package org.tomahawk.tomahawk_android.fragments;

import android.content.Context;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.SwitchCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.widget.SeekBar;
import android.widget.Spinner;

import com.h6ah4i.android.media.IBasicMediaPlayer;

import org.tomahawk.tomahawk_android.R;
import org.tomahawk.tomahawk_android.events.EventDefs;
import org.tomahawk.tomahawk_android.framework.AppEvent;
import org.tomahawk.tomahawk_android.mediaplayers.StandardMediaPlayer;
import org.tomahawk.tomahawk_android.model.BaseAudioEffectStateStore;
import org.tomahawk.tomahawk_android.model.BassBoostStateStore;
import org.tomahawk.tomahawk_android.model.LoudnessEnhancerStateStore;
import org.tomahawk.tomahawk_android.model.MediaPlayerStateStore;
import org.tomahawk.tomahawk_android.model.PresetReverbStateStore;
import org.tomahawk.tomahawk_android.model.VirtualizerStateStore;

import de.greenrobot.event.EventBus;

/**
 * Created by android on 19/10/15.
 */
public class SoundEffects extends Fragment
        implements CompoundButton.OnCheckedChangeListener,SeekBar.OnSeekBarChangeListener,
        AdapterView.OnItemSelectedListener {


    private static final int POSITION_SEEKBAR_MAX = 10000;
    private static final int VOLUME_SEEKBAR_MAX = 1000;
    private static final int SEEKBAR_UPDATE_PERIOD = 250;
    private static final int SEEKBAR_UPDATE_STOP_DELAY = 3000;
    private static final int MSG_SEEKBAR_PERIODIC_UPDATE = 1;
    private static final int MSG_SEEKBAR_STOP_UPDATE = 2;
    private SeekBar mSeekBarLeftVolume;
    private SeekBar mSeekBarRightVolume;
    private SeekBar mSeekBarAuxSendLevel;
    private Spinner mSpinnerAuxEffectType;

    private SwitchCompat mBassBoostState;
    private SwitchCompat mVirtualizerState;
    private SwitchCompat mLoudnessState;
    private SwitchCompat mPresetReverbState;


    // constants
    private static final int PARAM_SEEKBAR_MAX = 1000;

    // fields
    private SeekBar mSeekBarStrength;
    // fields
    private SeekBar mVirtualizerSeekBarStrength;
    // fields
    private SeekBar mSeekBarTargetGain;
    // fields
    private Spinner mSpinnerPreset;
    // constants
    static final int SEEKBAR_MAX = 10000;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_effects, container, false);
        return rootView;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        obtainViewReferences(view);
       /* FrameLayout.LayoutParams  params = (FrameLayout.LayoutParams) view.getLayoutParams();
        params.topMargin = getResources().getDimensionPixelSize(R.dimen.header_clear_space_nonscrollable)+getResources().getDimensionPixelSize(R.dimen.pager_indicator_height);
*/    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        mSeekBarLeftVolume = null;
        mSeekBarRightVolume = null;
        mSeekBarAuxSendLevel = null;
        mSpinnerAuxEffectType = null;
        mSeekBarStrength = null;
        mVirtualizerSeekBarStrength = null;
        mSeekBarTargetGain = null;
        mSpinnerPreset = null;

    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    private BaseAudioEffectStateStore getAudioEfectStateStore(int index) {
        switch (index) {
            case EventDefs.NavigationDrawerReqEvents.SECTION_INDEX_BASSBOOST:
                return StandardMediaPlayer.get().getBassBoostStateStore();
            case EventDefs.NavigationDrawerReqEvents.SECTION_INDEX_VIRTUALIZER:
                return StandardMediaPlayer.get().getVirtualizerStateStore();
            case EventDefs.NavigationDrawerReqEvents.SECTION_INDEX_EQUALIZER:
                return StandardMediaPlayer.get().getEqualizerStateStore();
            case EventDefs.NavigationDrawerReqEvents.SECTION_INDEX_LOUDNESS_ENHANCER:
                return StandardMediaPlayer.get().getLoudnessEnhancerStateStore();
            case EventDefs.NavigationDrawerReqEvents.SECTION_INDEX_PRESET_REVERB:
                return StandardMediaPlayer.get().getPresetReverbStateStore();
            case EventDefs.NavigationDrawerReqEvents.SECTION_INDEX_ENVIRONMENTAL_REVERB:
                return StandardMediaPlayer.get().getEnvironmentalReverbStateStore();
            case EventDefs.NavigationDrawerReqEvents.SECTION_INDEX_VISUALIZER:
                return StandardMediaPlayer.get().getVisualizerStateStore();
            case EventDefs.NavigationDrawerReqEvents.SECTION_INDEX_HQ_EQUALIZER:
                return StandardMediaPlayer.get().getHQEqualizerStateStore();
            case EventDefs.NavigationDrawerReqEvents.SECTION_INDEX_HQ_VISUALIZER:
                return StandardMediaPlayer.get().getHQVisualizerStateStore();
            default:
                throw new IllegalArgumentException();
        }
    }

    public void onEvent(AppEvent event){
        switch (event.event) {
            case EventDefs.PlayerControlReqEvents.PLAYER_SET_VOLUME_LEFT: {
                StandardMediaPlayer.get().getPlayerStateStore().setVolumeLeft(event.getArg2AsFloat());

                // apply
                playerSetVolume(0, StandardMediaPlayer.get().getPlayerStateStore().getVolumeLeft(), StandardMediaPlayer.get().getPlayerStateStore().getVolumeRight());
                //playerSetVolume(1, getPlayerStateStore().getVolumeLeft(), getPlayerStateStore().getVolumeRight());
            }
            break;
            case EventDefs.PlayerControlReqEvents.PLAYER_SET_VOLUME_RIGHT: {
                StandardMediaPlayer.get().getPlayerStateStore().setVolumeRight(event.getArg2AsFloat());

                // apply
                playerSetVolume(0, StandardMediaPlayer.get().getPlayerStateStore().getVolumeLeft(), StandardMediaPlayer.get().getPlayerStateStore().getVolumeRight());
                //playerSetVolume(1, getPlayerStateStore().getVolumeLeft(), getPlayerStateStore().getVolumeRight());
            }
            break;
            case EventDefs.PlayerControlReqEvents.PLAYER_SET_AUX_SEND_LEVEL: {
                StandardMediaPlayer.get().getPlayerStateStore().setAuxEffectSendLevel(event.getArg2AsFloat());

                // apply
                playerSetAuxSendLevel(0, StandardMediaPlayer.get().getPlayerStateStore().getAuxEffectSendLevel());
                //playerSetAuxSendLevel(1, getPlayerStateStore().getAuxEffectSendLevel());
            }
            break;
            case EventDefs.PlayerControlReqEvents.PLAYER_ATTACH_AUX_EFFECT: {
                int effectType = event.arg1;

                if (StandardMediaPlayer.get().getPlayerStateStore().getSelectedAuxEffectType() == effectType)
                    return;

                StandardMediaPlayer.get().getPlayerStateStore().setSelectedAuxEffectType(effectType);

                // apply
                //checkStateAndApplyAttachedAuxEffectSettings(0);
                //checkStateAndApplyAttachedAuxEffectSettings(1);
               // applyAttachedEuxEffectSettings(StandardMediaPlayer.get().getLibVlcInstance(),getPlayerStateStore().getSelectedAuxEffectType());
            }
            break;
        }
        switch (event.category) {

            case EventDefs.Category.NOTIFY_BASSBOOST:
                updateAudioEffectsEnabledState(EventDefs.NavigationDrawerReqEvents.SECTION_INDEX_BASSBOOST);
                break;
            case EventDefs.Category.NOTIFY_VIRTUALIZER:
                updateAudioEffectsEnabledState(EventDefs.NavigationDrawerReqEvents.SECTION_INDEX_VIRTUALIZER);
                break;
            case EventDefs.Category.NOTIFY_EQUALIZER:
                updateAudioEffectsEnabledState(EventDefs.NavigationDrawerReqEvents.SECTION_INDEX_EQUALIZER);
                break;
            case EventDefs.Category.NOTIFY_LOUDNESS_ENHANCER:
                updateAudioEffectsEnabledState(EventDefs.NavigationDrawerReqEvents.SECTION_INDEX_LOUDNESS_ENHANCER);
                break;
            case EventDefs.Category.NOTIFY_PRESET_REVERB:
                updateAudioEffectsEnabledState(EventDefs.NavigationDrawerReqEvents.SECTION_INDEX_PRESET_REVERB);
                break;

            case EventDefs.Category.NOTIFY_HQ_EQUALIZER:
                updateAudioEffectsEnabledState(EventDefs.NavigationDrawerReqEvents.SECTION_INDEX_HQ_EQUALIZER);
                break;
        }
    }

    private void updateAudioEffectsEnabledState(int index) {


        boolean isEnabled = getAudioEfectStateStore(index).isEnabled();
        switch (index) {
            case EventDefs.NavigationDrawerReqEvents.SECTION_INDEX_BASSBOOST:
                mBassBoostState.setChecked(isEnabled);
                break;
            case EventDefs.NavigationDrawerReqEvents.SECTION_INDEX_VIRTUALIZER:
                mVirtualizerState.setChecked(isEnabled);
                break;
            case EventDefs.NavigationDrawerReqEvents.SECTION_INDEX_EQUALIZER:
                break;
            case EventDefs.NavigationDrawerReqEvents.SECTION_INDEX_LOUDNESS_ENHANCER:
                mLoudnessState.setChecked(isEnabled);
                break;
            case EventDefs.NavigationDrawerReqEvents.SECTION_INDEX_PRESET_REVERB:
                mPresetReverbState.setChecked(isEnabled);
                break;
            /*case EventDefs.NavigationDrawerReqEvents.SECTION_INDEX_ENVIRONMENTAL_REVERB:
                mEnvironmentalReverbState.setChecked(isEnabled);
                break;*/
            case EventDefs.NavigationDrawerReqEvents.SECTION_INDEX_VISUALIZER:
                break;
            case EventDefs.NavigationDrawerReqEvents.SECTION_INDEX_HQ_EQUALIZER:
                break;
            case EventDefs.NavigationDrawerReqEvents.SECTION_INDEX_HQ_VISUALIZER:
                break;
            default:
                throw new IllegalArgumentException();
        }

    }





    private void playerSetVolume(int index, float leftVolume, float rightVolume) {
        IBasicMediaPlayer player = StandardMediaPlayer.get().getLibVlcInstance();

        if (player != null) {
            player.setVolume(leftVolume, rightVolume);
        }
    }

    private void playerSetAuxSendLevel(int index, float level) {
        IBasicMediaPlayer player = StandardMediaPlayer.get().getLibVlcInstance();

        if (player != null) {
            player.setAuxEffectSendLevel(level);
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        setupViews();
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long l) {
        switch (parent.getId()) {
            case R.id.spinner_player_aux_effect_type:
                postAppEvent(EventDefs.Category.PLAYER_CONTROL,
                        EventDefs.PlayerControlReqEvents.PLAYER_ATTACH_AUX_EFFECT,
                        position, 0);
                break;
            case R.id.spinner_preset_reverb_preset:
                postAppEvent(EventDefs.Category.PRESET_REVERB,EventDefs.PresetReverbReqEvents.SET_PRESET, position, 0);
                break;
            case R.id.spinner_environment_reverb_preset:
                postAppEvent(EventDefs.Category.ENVIRONMENTAL_REVERB,
                        EventDefs.EnvironmentalReverbReqEvents.SET_PRESET,
                        (position - 1), 0);
                break;
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> adapterView) {

    }

    @Override
    public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {

        switch (compoundButton.getId()){
            case R.id.bassBoostState:
                postAppEvent(EventDefs.Category.NAVIGATION_DRAWER,
                        EventDefs.NavigationDrawerReqEvents.CLICK_ITEM_ENABLE_SWITCH,
                        EventDefs.NavigationDrawerReqEvents.SECTION_INDEX_BASSBOOST,
                        (isChecked) ? 1 : 0);
                postAppEvent(EventDefs.Category.BASSBOOST,EventDefs.BassBoostReqEvents.SET_ENABLED, (isChecked ? 1 : 0), 0);
                break;
            case R.id.virtualizerState:
                postAppEvent(EventDefs.Category.NAVIGATION_DRAWER,
                        EventDefs.NavigationDrawerReqEvents.CLICK_ITEM_ENABLE_SWITCH,
                        EventDefs.NavigationDrawerReqEvents.SECTION_INDEX_VIRTUALIZER,
                        (isChecked) ? 1 : 0);
                postAppEvent(EventDefs.Category.VIRTUALIZER,EventDefs.VirtualizerReqEvents.SET_ENABLED, (isChecked ? 1 : 0), 0);
                break;
            case R.id.loudnessState:
                postAppEvent(EventDefs.Category.NAVIGATION_DRAWER,
                        EventDefs.NavigationDrawerReqEvents.CLICK_ITEM_ENABLE_SWITCH,
                        EventDefs.NavigationDrawerReqEvents.SECTION_INDEX_LOUDNESS_ENHANCER,
                        (isChecked) ? 1 : 0);
                postAppEvent(EventDefs.Category.LOUDNESS_EHNAHCER,EventDefs.LoudnessEnhancerReqEvents.SET_ENABLED, (isChecked ? 1 : 0), 0);
                break;
            case R.id.presetReverbState:
                postAppEvent(EventDefs.Category.NAVIGATION_DRAWER,
                        EventDefs.NavigationDrawerReqEvents.CLICK_ITEM_ENABLE_SWITCH,
                        EventDefs.NavigationDrawerReqEvents.SECTION_INDEX_PRESET_REVERB,
                        (isChecked) ? 1 : 0);
                postAppEvent(EventDefs.Category.PRESET_REVERB,EventDefs.PresetReverbReqEvents.SET_ENABLED, (isChecked ? 1 : 0), 0);
                break;
            case R.id.environmentalReverbState:
                postAppEvent(EventDefs.Category.NAVIGATION_DRAWER,
                        EventDefs.NavigationDrawerReqEvents.CLICK_ITEM_ENABLE_SWITCH,
                        EventDefs.NavigationDrawerReqEvents.SECTION_INDEX_ENVIRONMENTAL_REVERB,
                        (isChecked) ? 1 : 0);
                postAppEvent(EventDefs.Category.ENVIRONMENTAL_REVERB,EventDefs.EnvironmentalReverbReqEvents.SET_ENABLED, (isChecked ? 1 : 0), 0);
                break;
        }
    }

    public void obtainViewReferences(View view){
        mSeekBarLeftVolume = (SeekBar)view.findViewById(R.id.seekbar_player_left_volume);
        mSeekBarRightVolume = (SeekBar)view.findViewById(R.id.seekbar_player_right_volume);
        mSeekBarAuxSendLevel = (SeekBar)view.findViewById(R.id.seekbar_player_aux_effect_send_level);
        mSpinnerAuxEffectType = (Spinner)view.findViewById(R.id.spinner_player_aux_effect_type);
        mBassBoostState = (SwitchCompat)view.findViewById(R.id.bassBoostState);
        mVirtualizerState = (SwitchCompat)view.findViewById(R.id.virtualizerState);
        mLoudnessState = (SwitchCompat)view.findViewById(R.id.loudnessState);
        mPresetReverbState = (SwitchCompat)view.findViewById(R.id.presetReverbState);
        mSeekBarStrength = (SeekBar)view.findViewById(R.id.seekbar_bassboost_strength);
        mVirtualizerSeekBarStrength = (SeekBar)view.findViewById(R.id.seekbar_virtualizer_strength);
        mSeekBarTargetGain = (SeekBar)view.findViewById(R.id.seekbar_loudness_enhancer_target_gain);
        mSpinnerPreset = (Spinner)view.findViewById(R.id.spinner_preset_reverb_preset);

        if (Build.VERSION.SDK_INT <19){
            view.findViewById(R.id.loudnessStateView).setVisibility(View.GONE);
            view.findViewById(R.id.loudNessView).setVisibility(View.GONE);
            view.findViewById(R.id.loudNessLayout).setVisibility(View.GONE);
        }else {
            view.findViewById(R.id.loudnessStateView).setVisibility(View.VISIBLE);
            view.findViewById(R.id.loudNessView).setVisibility(View.VISIBLE);
            view.findViewById(R.id.loudNessLayout).setVisibility(View.VISIBLE);
        }

        mSeekBarLeftVolume.setOnSeekBarChangeListener(this);
        mSeekBarRightVolume.setOnSeekBarChangeListener(this);
        mSeekBarAuxSendLevel.setOnSeekBarChangeListener(this);
        mSpinnerAuxEffectType.setOnItemSelectedListener(this);
        mBassBoostState.setOnCheckedChangeListener(this);
        mVirtualizerState.setOnCheckedChangeListener(this);
        mLoudnessState.setOnCheckedChangeListener(this);
        mPresetReverbState.setOnCheckedChangeListener(this);
        mSeekBarStrength.setOnSeekBarChangeListener(this);
        mVirtualizerSeekBarStrength.setOnSeekBarChangeListener(this);
        mSeekBarTargetGain.setOnSeekBarChangeListener(this);
        mSpinnerPreset.setOnItemSelectedListener(this);

    }



    private void setupViews() {
        Context context = getActivity();
        //GlobalAppController controller = ((TomahawkMainActivity)getActivity()).getController();

        {
            final ArrayAdapter<CharSequence> adapter =
                    ArrayAdapter.createFromResource(
                            context, R.array.aux_effect_names,
                            android.R.layout.simple_spinner_item);
            adapter.setDropDownViewResource(
                    android.R.layout.simple_spinner_dropdown_item);
            mSpinnerAuxEffectType.setAdapter(adapter);
        }

        mSeekBarLeftVolume.setMax(VOLUME_SEEKBAR_MAX);
        mSeekBarRightVolume.setMax(VOLUME_SEEKBAR_MAX);
        mSeekBarAuxSendLevel.setMax(VOLUME_SEEKBAR_MAX);

        MediaPlayerStateStore state = StandardMediaPlayer.get().getPlayerStateStore();

        mSeekBarLeftVolume.setProgress((int) (VOLUME_SEEKBAR_MAX * state.getVolumeLeft()));
        mSeekBarRightVolume.setProgress((int) (VOLUME_SEEKBAR_MAX * state.getVolumeRight()));
        mSeekBarAuxSendLevel
                .setProgress((int) (VOLUME_SEEKBAR_MAX * state.getAuxEffectSendLevel()));

        mSpinnerAuxEffectType.setSelection(state.getSelectedAuxEffectType());
        mBassBoostState.setChecked(getAudioEfectStateStore(EventDefs.NavigationDrawerReqEvents.SECTION_INDEX_BASSBOOST).isEnabled());
        mVirtualizerState.setChecked(getAudioEfectStateStore(EventDefs.NavigationDrawerReqEvents.SECTION_INDEX_VIRTUALIZER).isEnabled());
        mLoudnessState.setChecked(getAudioEfectStateStore(EventDefs.NavigationDrawerReqEvents.SECTION_INDEX_LOUDNESS_ENHANCER).isEnabled());
        mPresetReverbState.setChecked(getAudioEfectStateStore(EventDefs.NavigationDrawerReqEvents.SECTION_INDEX_PRESET_REVERB).isEnabled());
        BassBoostStateStore bassBoostStateStore = StandardMediaPlayer.get().getBassBoostStateStore();

        mSeekBarStrength.setMax(PARAM_SEEKBAR_MAX);

        mSeekBarStrength.setProgress(
                (int) (PARAM_SEEKBAR_MAX * bassBoostStateStore.getNormalizedStrength()));
        VirtualizerStateStore virtualizerStateStore = StandardMediaPlayer.get().getVirtualizerStateStore();

        mVirtualizerSeekBarStrength.setMax(PARAM_SEEKBAR_MAX);

        mVirtualizerSeekBarStrength.setProgress(
                (int) (PARAM_SEEKBAR_MAX * virtualizerStateStore.getNormalizedStrength()));

        LoudnessEnhancerStateStore loudnessEnhancerStateStore = StandardMediaPlayer.get().getLoudnessEnhancerStateStore();

        mSeekBarTargetGain.setMax(PARAM_SEEKBAR_MAX);

        mSeekBarTargetGain.setProgress(
                (int) (PARAM_SEEKBAR_MAX * loudnessEnhancerStateStore.getNormalizedTargetGainmB()));

        PresetReverbStateStore presetReverbStateStore = StandardMediaPlayer.get().getPresetReverbStateStore();

        {
            final ArrayAdapter<CharSequence> adapter =
                    ArrayAdapter.createFromResource(
                            context, R.array.aux_preset_reverb_preset_names,
                            android.R.layout.simple_spinner_item);

            adapter.setDropDownViewResource(
                    android.R.layout.simple_spinner_dropdown_item);
            mSpinnerPreset.setAdapter(adapter);
        }

        mSpinnerPreset.setSelection(presetReverbStateStore.getSettings().preset);


    }


    /*public MediaPlayerStateStore getPlayerStateStore(){
        return ((TomahawkMainActivity)getActivity()).getController().getPlayerStateStore();
    }*/

    private static float seekBarProgressToFloat(int progress) {
        return progress * (1.0f / SEEKBAR_MAX);
    }

    private static int floatToSeekbarProgress(float value) {
        return (int) (value * SEEKBAR_MAX);
    }

    private void postAppEvent(int category,int command, int arg1, float arg2) {
        switch (category) {
            case EventDefs.Category.PLAYER_CONTROL:
            EventBus.getDefault().post(new AppEvent(
                    EventDefs.Category.PLAYER_CONTROL,
                    command, arg1, arg2));
                break;
            case EventDefs.Category.BASSBOOST:
                EventBus.getDefault().post(new AppEvent(
                        EventDefs.Category.BASSBOOST,
                        command, arg1, arg2));

                break;
            case EventDefs.Category.VIRTUALIZER:
                EventBus.getDefault().post(new AppEvent(
                        EventDefs.Category.VIRTUALIZER,
                        command, arg1, arg2));

                break;
            case EventDefs.Category.LOUDNESS_EHNAHCER:
                EventBus.getDefault().post(new AppEvent(
                        EventDefs.Category.LOUDNESS_EHNAHCER,
                        command, arg1, arg2));
                break;
            case EventDefs.Category.PRESET_REVERB:
                EventBus.getDefault().post(new AppEvent(
                        EventDefs.Category.PRESET_REVERB,
                        command, arg1, arg2));
                break;
        }
        }

    private void postAppEvent(int category,int event, int arg1, int arg2) {
        switch (category) {
            case EventDefs.Category.NAVIGATION_DRAWER:
            EventBus.getDefault().post(new AppEvent(
                    EventDefs.Category.NAVIGATION_DRAWER,
                    event, arg1, arg2));
                break;
            case EventDefs.Category.BASSBOOST:
                EventBus.getDefault().post(new AppEvent(
                        EventDefs.Category.BASSBOOST,
                        event, arg1, arg2));
                break;
            case EventDefs.Category.VIRTUALIZER:
                EventBus.getDefault().post(new AppEvent(
                        EventDefs.Category.VIRTUALIZER,
                        event, arg1, arg2));
                break;
            case EventDefs.Category.LOUDNESS_EHNAHCER:
                EventBus.getDefault().post(new AppEvent(
                        EventDefs.Category.LOUDNESS_EHNAHCER,
                        event, arg1, arg2));
                break;
            case EventDefs.Category.PRESET_REVERB:
                EventBus.getDefault().post(new AppEvent(
                        EventDefs.Category.PRESET_REVERB,
                        event, arg1, arg2));
                break;
        }
        }
    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
        switch (seekBar.getId()) {

            case R.id.seekbar_player_left_volume:
                if (fromUser) {
                    postAppEvent(EventDefs.Category.PLAYER_CONTROL,
                            EventDefs.PlayerControlReqEvents.PLAYER_SET_VOLUME_LEFT,
                            0, (float) progress / VOLUME_SEEKBAR_MAX);
                }
                break;
            case R.id.seekbar_player_right_volume:
                if (fromUser) {
                    postAppEvent(EventDefs.Category.PLAYER_CONTROL,
                            EventDefs.PlayerControlReqEvents.PLAYER_SET_VOLUME_RIGHT,
                            0, (float) progress / VOLUME_SEEKBAR_MAX);
                }
                break;
            case R.id.seekbar_player_aux_effect_send_level:
                if (fromUser) {
                    postAppEvent(EventDefs.Category.PLAYER_CONTROL,
                            EventDefs.PlayerControlReqEvents.PLAYER_SET_AUX_SEND_LEVEL,
                            0, (float) progress / VOLUME_SEEKBAR_MAX);
                }
                break;
            case R.id.seekbar_bassboost_strength: {
                float strength = (float) progress / PARAM_SEEKBAR_MAX;
                postAppEvent(EventDefs.Category.BASSBOOST,EventDefs.BassBoostReqEvents.SET_STRENGTH, 0, strength);
            }
            break;
            case R.id.seekbar_virtualizer_strength: {
                float strength = (float) progress / PARAM_SEEKBAR_MAX;
                postAppEvent(EventDefs.Category.VIRTUALIZER,EventDefs.VirtualizerReqEvents.SET_STRENGTH, 0, strength);
            }
            break;

            case R.id.seekbar_loudness_enhancer_target_gain: {
                float strength = (float) progress / PARAM_SEEKBAR_MAX;
                postAppEvent(EventDefs.Category.LOUDNESS_EHNAHCER,EventDefs.VirtualizerReqEvents.SET_STRENGTH, 0, strength);
            }
            break;

        }


    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {

        }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {
    }
}
