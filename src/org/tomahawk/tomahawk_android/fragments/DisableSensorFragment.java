package org.tomahawk.tomahawk_android.fragments;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import org.tomahawk.libtomahawk.infosystem.SensorItem;
import org.tomahawk.tomahawk_android.R;
import org.tomahawk.tomahawk_android.TomahawkApp;
import org.tomahawk.tomahawk_android.activities.TomahawkMainActivity;
import org.tomahawk.tomahawk_android.adapters.SpinnerAdapter;
import org.tomahawk.tomahawk_android.services.PlaybackService;

import java.util.ArrayList;
import java.util.List;

import de.greenrobot.event.EventBus;

/**
 * Created by YGS on 9/26/2015.
 */
public class DisableSensorFragment extends ContentHeaderFragment implements View.OnClickListener{

    SpinnerAdapter mAdapter;
    List<SensorItem> mAdapterList = new ArrayList<>();
    int initialPage = -1;
    boolean initializing = true;
    Activity activity;
    TomahawkApp app;
    LinearLayout waveOverLayout;
    LinearLayout pocketLayout;
    LinearLayout hammerLayout;
    LinearLayout accelerometerLayout;
    LinearLayout proximityLayout;
    ImageView accelerometerImage;
    ImageView proximityImage;
    boolean proVersion = true;
    SharedPreferences preferences=null;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        getActivity().setTitle("");
        return inflater.inflate(R.layout.disavle_sensor, container, false);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        this.activity = activity;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        itemPosition = PreferenceManager.getDefaultSharedPreferences(getActivity()).getInt(TomahawkMainActivity.SENSOR_POSITION,-1);
        if (itemPosition == -1){
            itemPosition = 0;
        }
        app = (TomahawkApp)getActivity().getApplication();
        ((TomahawkMainActivity)getActivity()).getSupportActionBarS().setSelectedNavigationItem(itemPosition);
        waveOverLayout = (LinearLayout)view.findViewById(R.id.waveOverLayout);
        pocketLayout = (LinearLayout)view.findViewById(R.id.pocketLayout);
        hammerLayout = (LinearLayout)view.findViewById(R.id.hammerLayout);
        accelerometerLayout = (LinearLayout)view.findViewById(R.id.accelerometerLayout);
        proximityLayout = (LinearLayout)view.findViewById(R.id.proximityLayout);
        accelerometerImage = (ImageView)view.findViewById(R.id.custom_sensor_acc);
        proximityImage = (ImageView)view.findViewById(R.id.custom_sensor_prox);
        waveOverLayout.setOnClickListener(this);
        pocketLayout.setOnClickListener(this);
        hammerLayout.setOnClickListener(this);
        accelerometerLayout.setOnClickListener(this);
        proximityLayout.setOnClickListener(this);
        if (!proVersion){
            accelerometerImage.setImageResource(R.drawable.custom_acc_pro);
            proximityImage.setImageResource(R.drawable.custom_prox_pro);
        }else {
            accelerometerImage.setImageResource(R.drawable.custom_acc);
            proximityImage.setImageResource(R.drawable.custom_prox);
        }
        app.getSensorDataHelper().setIntentBroadcastEnabled(false);
        PlaybackService.DisableAllSensorEvent event = new PlaybackService.DisableAllSensorEvent();
        EventBus.getDefault().post(event);
        setupNonScrollableSpacer(getView());
    }

    @Override
    public void onClick(View v) {
        SharedPreferences preferences =
                PreferenceManager.getDefaultSharedPreferences(getActivity());
        if (v==waveOverLayout){
            preferences.edit().putInt(
                    TomahawkMainActivity.SENSOR_POSITION, 1)
                    .apply();
            ((OnSensorChange) activity).sensorChange();
        }else if (v==pocketLayout){
            preferences.edit().putInt(
                    TomahawkMainActivity.SENSOR_POSITION, 2)
                    .apply();
            ((OnSensorChange) activity).sensorChange();
        }else if (v==hammerLayout){
            preferences.edit().putInt(
                    TomahawkMainActivity.SENSOR_POSITION, 3)
                    .apply();
            ((OnSensorChange) activity).sensorChange();
        }else if (v==accelerometerLayout){
            if (proVersion){
                preferences.edit().putInt(
                        TomahawkMainActivity.SENSOR_POSITION, 4)
                        .apply();
                ((OnSensorChange) activity).sensorChange();
            }
        }else if (v==proximityLayout){
           if (proVersion){
               preferences.edit().putInt(
                       TomahawkMainActivity.SENSOR_POSITION, 5)
                       .apply();
               ((OnSensorChange) activity).sensorChange();
           }
        }
    }
}
