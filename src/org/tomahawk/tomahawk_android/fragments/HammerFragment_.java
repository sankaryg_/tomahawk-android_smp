package org.tomahawk.tomahawk_android.fragments;


import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.view.View;

import org.tomahawk.tomahawk_android.R;
import org.tomahawk.tomahawk_android.TomahawkApp;
import org.tomahawk.tomahawk_android.utils.FakePreferenceGroup;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by android on 24/9/15.
 */
public class HammerFragment_ extends TomahawkListFragment implements SharedPreferences.OnSharedPreferenceChangeListener{

    public static final String FAKEPREFERENCEFRAGMENT_ID_PREFHAMMER = "pref_hammer";

    public static final String FAKEPREFERENCEFRAGMENT_ID_PREFHAMMER_SENSITIVITY = "hammer_sensitivity";

    public static final String FAKEPREFERENCEFRAGMENT_ID_PREFHAMMER_BUTTON = "hammer_button";

    public static final String FAKEPREFERENCEFRAGMENT_KEY_PREFHAMMER
            = "org.tomahawk.tomahawk_android.prefhammer";

    public static final String FAKEPREFERENCEFRAGMENT_KEY_PREFHAMMER_SENSITIVITY
            = "org.tomahawk.tomahawk_android.hammer_sensitivity";

    public static final String FAKEPREFERENCEFRAGMENT_KEY_PREFHAMMER_BUTTON
            = "org.tomahawk.tomahawk_android.hammer_button";

    private SharedPreferences mSharedPreferences;

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mSharedPreferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
        mSharedPreferences.registerOnSharedPreferenceChangeListener(this);
        List<FakePreferenceGroup> fakePreferenceGroups = new ArrayList<>();
        FakePreferenceGroup prefGroup = new FakePreferenceGroup(
                getString(R.string.preferences_playback));
        prefGroup.addFakePreference(new FakePreferenceGroup.FakePreference(
                FakePreferenceGroup.FAKEPREFERENCE_TYPE_PLAIN,
                FAKEPREFERENCEFRAGMENT_ID_PREFHAMMER,
                FAKEPREFERENCEFRAGMENT_KEY_PREFHAMMER,
                getString(R.string.hammer),
                getString(R.string.preferences_hammer_line1),false));

        prefGroup.addFakePreference(new FakePreferenceGroup.FakePreference(
                FakePreferenceGroup.FAKEPREFERENCE_TYPE_SEEKBAR,
                FAKEPREFERENCEFRAGMENT_ID_PREFHAMMER_SENSITIVITY,
                FAKEPREFERENCEFRAGMENT_KEY_PREFHAMMER_SENSITIVITY,
                getString(R.string.preferences_hammer_sensitive_level),
                getString(R.string.preferences_pocket_line2),true));

        prefGroup.addFakePreference(new FakePreferenceGroup.FakePreference(
                FakePreferenceGroup.FAKEPREFERENCE_TYPE_BUTTON,
                FAKEPREFERENCEFRAGMENT_ID_PREFHAMMER_BUTTON,
                FAKEPREFERENCEFRAGMENT_KEY_PREFHAMMER_BUTTON,
                getString(R.string.wave_apply),
                "",false));
        fakePreferenceGroups.add(prefGroup);
        final TomahawkApp app = (TomahawkApp)getActivity().getApplication();
        // Now we can push the complete set of FakePreferences into our FakePreferencesAdapter,
        // so that it can provide our ListView with the correct Views.
        /*FakePreferencesAdapter fakePreferencesAdapter = new FakePreferencesAdapter(getActivity(),
                getActivity().getLayoutInflater(), fakePreferenceGroups,app);
        setListAdapter(fakePreferencesAdapter);*/

        //getListView().setOnItemClickListener(this);

        setupNonScrollableSpacer(getListView());

    }

    @Override
    public void onResume() {
        super.onResume();

        getListAdapter().notifyDataSetChanged();
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        getListAdapter().notifyDataSetChanged();
    }
}
