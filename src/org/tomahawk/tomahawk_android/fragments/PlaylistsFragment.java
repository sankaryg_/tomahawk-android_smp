/* == This file is part of Tomahawk Player - <http://tomahawk-player.org> ===
 *
 *   Copyright 2012, Enno Gottschalk <mrmaffen@googlemail.com>
 *
 *   Tomahawk is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Tomahawk is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Tomahawk. If not, see <http://www.gnu.org/licenses/>.
 */
package org.tomahawk.tomahawk_android.fragments;

import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.preference.PreferenceManager;
import android.view.View;

import org.jdeferred.DoneCallback;
import org.tomahawk.libtomahawk.collection.Collection;
import org.tomahawk.libtomahawk.collection.CollectionManager;
import org.tomahawk.libtomahawk.collection.ListItemString;
import org.tomahawk.libtomahawk.collection.Playlist;
import org.tomahawk.libtomahawk.collection.PlaylistEntry;
import org.tomahawk.libtomahawk.database.DatabaseHelper;
import org.tomahawk.libtomahawk.infosystem.InfoSystem;
import org.tomahawk.libtomahawk.infosystem.User;
import org.tomahawk.libtomahawk.resolver.Query;
import org.tomahawk.tomahawk_android.R;
import org.tomahawk.tomahawk_android.TomahawkApp;
import org.tomahawk.tomahawk_android.activities.TomahawkMainActivity;
import org.tomahawk.tomahawk_android.adapters.Segment;
import org.tomahawk.tomahawk_android.adapters.TomahawkListAdapter;
import org.tomahawk.tomahawk_android.dialogs.CreatePlaylistDialog;
import org.tomahawk.tomahawk_android.utils.FragmentUtils;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import de.greenrobot.event.EventBus;

/**
 * {@link TomahawkFragment} which shows a set of {@link org.tomahawk.libtomahawk.collection.Playlist}s
 * inside its {@link se.emilsjolander.stickylistheaders.StickyListHeadersListView}
 */
public class PlaylistsFragment extends TomahawkFragment {

    private final HashSet<User> mResolvingUsers = new HashSet<>();

    public static final String COLLECTION_PLAYLISTS_SHOW_TYPE = "org.tomahawk.tomahawk_android.collection_playlists_show_type";

    public static final String COLLECTION_PLAYLISTS_SPINNER_POSITION
            = "org.tomahawk.tomahawk_android.collection_playlists_spinner_position";

    public static final String COLLECTION_PLAYLISTS_EDIT = "org.tomahawk.tomahawk_android.collection_playlists_edit";

    public static final String COLLECTION_PLAYLISTS_ADD = "org.tomahawk.tomahawk_android.collection_playlists_add";
    final List<Segment> segments = new ArrayList<>();

    static int i = 0;

    public void onEvent(DatabaseHelper.PlaylistsActionEvent event){
        User.getSelf().done(new DoneCallback<User>() {
            @Override
            public void onDone(User user) {
                if (mUser == user) {
                    refreshPlaylists();
                } else {
                    mHideRemoveButton = true;
                }
            }
        });
        updateAdapter();
    }
    @SuppressWarnings("unused")
    public void onEventAsync(final DatabaseHelper.PlaylistsUpdatedEvent event) {
        final SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(TomahawkApp.getContext());
        User.getSelf().done(new DoneCallback<User>() {
            @Override
            public void onDone(User user) {
                if (mUser == user) {
                    //refreshPlaylists();
                    segments.clear();
                    if (event.mPlaylists!=null && event.mPlaylists.size() == 0){
                        Playlist mPlay = Playlist.fromEmptyList("001","Playlist Empty");
                        event.mPlaylists.add(mPlay);
                    }
                    if (preferences.getString(COLLECTION_PLAYLISTS_SHOW_TYPE,"grid").equals("grid")) {
                        segments.add(new Segment.Builder(event.mPlaylists)
                                .headerLayout(R.layout.dropdown_header)//single_line_list_header)
                                .headerStrings(constructDropdownItems())
                                .showType(COLLECTION_PLAYLISTS_SHOW_TYPE,selectShowTypeListener(COLLECTION_PLAYLISTS_SHOW_TYPE), addTextListener(COLLECTION_PLAYLISTS_EDIT), addEditorActionListener(),false)
                                .spinner(constructDropdownListener(
                                                COLLECTION_PLAYLISTS_SPINNER_POSITION),
                                        getDropdownPos(COLLECTION_PLAYLISTS_SPINNER_POSITION))
                                .showAsGrid(COLLECTION_PLAYLISTS_SHOW_TYPE, R.integer.grid_column_count, R.dimen.padding_superlarge,
                                        R.dimen.padding_superlarge)
                                .build());
                    }else {
                        segments.add(new Segment.Builder(event.mPlaylists)
                                .headerLayout(R.layout.dropdown_header)//single_line_list_header)
                                .headerStrings(constructDropdownItems())
                                .showType(COLLECTION_PLAYLISTS_SHOW_TYPE,selectShowTypeListener(COLLECTION_PLAYLISTS_SHOW_TYPE), addTextListener(COLLECTION_PLAYLISTS_EDIT), addEditorActionListener(),false)
                                .spinner(constructDropdownListener(
                                                COLLECTION_PLAYLISTS_SPINNER_POSITION),
                                        getDropdownPos(COLLECTION_PLAYLISTS_SPINNER_POSITION))
                                .build());
                    }

                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        @Override
                        public void run() {
                            fillAdapter(segments,null,mCollection);
                            showContentHeader(new ColorDrawable(getResources().getColor(R.color.black)));//R.drawable.playlists_header);
                        }
                    });
                }
            }
        });
    }


    @Override
    public void onResume() {
        super.onResume();
        final SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(TomahawkApp.getContext());

        User.getSelf().done(new DoneCallback<User>() {
            @Override
            public void onDone(User user) {
                if (mUser == user) {
                    refreshPlaylists();
                } else {
                    mHideRemoveButton = true;
                }
            }
        });
        preferences.edit().putBoolean(COLLECTION_PLAYLISTS_ADD,true).apply();
        if (mContainerFragmentClass == null) {
            getActivity().setTitle(getString(R.string.drawer_title_playlists).toUpperCase());
        }
        updateAdapter();
    }

    /**
     * Called every time an item inside a ListView or GridView is clicked
     *
     * @param view the clicked view
     * @param item the Object which corresponds to the click
     */
    @Override
    public void onItemClick(View view, Object item) {
        if (item instanceof Playlist) {
            String playlistId = ((Playlist) item).getId();
            if (mQueryArray != null) {
                ArrayList<PlaylistEntry> entries = new ArrayList<>();
                for (Query query : mQueryArray) {
                    entries.add(PlaylistEntry.get(playlistId, query,
                            TomahawkMainActivity.getLifetimeUniqueStringId()));
                }
                CollectionManager.get().addPlaylistEntries(playlistId, entries);
            }
            ((TomahawkMainActivity)getActivity()).getPlaybackService().setPlaylistSelectedId(playlistId);
            final Bundle bundle = new Bundle();
            if (mUser != null) {
                bundle.putString(TomahawkFragment.USER, mUser.getId());
            }
            mCollection.getQueries(Collection.SORT_ALPHA,playlistId).done(new DoneCallback<Playlist>() {
                @Override
                public void onDone(Playlist result) {
                    if (result != null) {
                        bundle.putString(TomahawkFragment.PLAYLIST, result.getId());
                        bundle.putString(TomahawkFragment.COLLECTION_ID, mCollection.getId());
                        bundle.putInt(CONTENT_HEADER_MODE,
                                ContentHeaderFragment.MODE_HEADER_DYNAMIC);
                        FragmentUtils.replace((TomahawkMainActivity) getActivity(),
                                PlaylistEntriesFragment.class, bundle);
                    }
                }
            });

        } else {
            ArrayList<Query> queries = mQueryArray != null ? mQueryArray : new ArrayList<Query>();
            Playlist playlist = Playlist.fromQueryList(
                    TomahawkMainActivity.getLifetimeUniqueStringId(), "", null, queries);
            CreatePlaylistDialog dialog = new CreatePlaylistDialog();
            Bundle args = new Bundle();
            args.putString(TomahawkFragment.PLAYLIST, playlist.getId());
            args.putString(TomahawkFragment.COLLECTION_ID, mCollection.getId());
            if (mQueryKeys != null && mQueryKeys.size() > 0){
                args.putStringArrayList(TomahawkFragment.QUERYARRAY, mQueryKeys);
            }
            dialog.setArguments(args);
            dialog.show(getFragmentManager(), null);
        }
        getArguments().remove(QUERYARRAY);
        mQueryArray = null;
    }

    @Override
    protected void createNewPlaylist() {
        super.createNewPlaylist();
        ArrayList<Query> queries = mQueryArray != null ? mQueryArray : new ArrayList<Query>();
        Playlist playlist = Playlist.fromQueryList(
                TomahawkMainActivity.getLifetimeUniqueStringId(), "", null, queries);
        CreatePlaylistDialog dialog = new CreatePlaylistDialog();
        Bundle args = new Bundle();
        args.putString(TomahawkFragment.PLAYLIST, playlist.getId());
        args.putString(TomahawkFragment.COLLECTION_ID, mCollection.getId());
        if (mQueryKeys != null && mQueryKeys.size() > 0){
            args.putStringArrayList(TomahawkFragment.QUERYARRAY, mQueryKeys);
        }
        dialog.setArguments(args);
        dialog.show(getFragmentManager(), null);
    }

    /**
     * Called every time an item inside a ListView or GridView is long-clicked
     *
     * @param item the Object which corresponds to the long-click
     */
    @Override
    public boolean onItemLongClick(View view, Object item) {
        return item.equals(CREATE_PLAYLIST_BUTTON_ID) || FragmentUtils
                .showContextMenu((TomahawkMainActivity) getActivity(), item, mCollection.getId(), false,
                        mHideRemoveButton);
    }

    /**
     * Update this {@link TomahawkFragment}'s {@link TomahawkListAdapter} content
     */
    @Override
    protected void updateAdapter() {
        if (!mIsResumed) {
            return;
        }


        if (mQueryArray != null) {
            // Add the header text item
            List textItems = new ArrayList();
            textItems.add(new ListItemString(
                    getResources().getQuantityString(R.plurals.add_to_playlist_headertext,
                            mQueryArray.size(), mQueryArray.size()), true));
            segments.add(new Segment.Builder(textItems).build());
        }

        User.getSelf().done(new DoneCallback<User>() {
            @Override
            public void onDone(User user) {
                final List playlists = new ArrayList();
                if (mUser == user) {
                    //playlists.add(CREATE_PLAYLIST_BUTTON_ID);
                }
                if (mUser.getPlaylists() == null) {
                    if (mUser == user) {
                        refreshPlaylists();
                    } else if (!mResolvingUsers.contains(mUser)) {
                        String requestId = InfoSystem.get().resolvePlaylists(mUser, false);
                        if (requestId != null) {
                            mCorrespondingRequestIds.add(requestId);
                        }
                        mResolvingUsers.add(mUser);
                    }
                } else {
                    //playlists.addAll(mUser.getPlaylists());
                    i = 0;
                    for (Playlist mPlaylist : mUser.getPlaylists()) {
                        mCollection.getQueries(Collection.SORT_ALPHA, mPlaylist.getId()).done(new DoneCallback<Playlist>() {
                            @Override
                            public void onDone(Playlist result) {
                                if (result != null) {
                                    if (!playlists.contains(result)) {
                                    playlists.add(result);
                                       /* */
                                    }
                                }
                                ++i;
                                if (i == mUser.getPlaylists().size()){
                                    DatabaseHelper.PlaylistsUpdatedEvent event = new DatabaseHelper.PlaylistsUpdatedEvent();
                                    event.mPlaylists = playlists;
                                    EventBus.getDefault().post(event);
                                }
                            }
                        });
                    }
                }

            }
        });
    }

    private void refreshPlaylists() {
        CollectionManager.get().refreshUserPlaylists(getDropdownPos(COLLECTION_PLAYLISTS_SPINNER_POSITION)).done(new DoneCallback<Void>() {
            @Override
            public void onDone(Void result) {
                if (!mAdapterUpdateHandler.hasMessages(ADAPTER_UPDATE_MSG)) {
                    mAdapterUpdateHandler.sendEmptyMessageDelayed(
                            ADAPTER_UPDATE_MSG, ADAPTER_UPDATE_DELAY);
                }
            }
        });
    }
    private List<Integer> constructDropdownItems() {
        List<Integer> dropDownItems = new ArrayList<>();
        dropDownItems.add(R.string.collection_dropdown_recently_added);
        dropDownItems.add(R.string.collection_dropdown_alpha);
        return dropDownItems;
    }
}
