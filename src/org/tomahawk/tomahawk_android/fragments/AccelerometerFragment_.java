package org.tomahawk.tomahawk_android.fragments;


import android.content.Context;
import android.content.SharedPreferences;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.AdapterView;

import org.tomahawk.tomahawk_android.R;
import org.tomahawk.tomahawk_android.TomahawkApp;
import org.tomahawk.tomahawk_android.sensor.PrefsHelper;
import org.tomahawk.tomahawk_android.utils.FakePreferenceGroup;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by android on 24/9/15.
 */
public class AccelerometerFragment_ extends TomahawkListFragment
        implements AdapterView.OnItemClickListener, SharedPreferences.OnSharedPreferenceChangeListener {

    private static final String TAG = PreferenceAdvancedFragment.class.getSimpleName();

    public static final String FAKEPREFERENCEFRAGMENT_ID_PREFACCELEROMETER = "pref_accelerometer";

    public static final String FAKEPREFERENCEFRAGMENT_ID_PREFACCELEROMETER_SHAKE = "pref_accelerometer_shake";

    public static final String FAKEPREFERENCEFRAGMENT_ID_PREFACCELEROMETER_SHAKE_XAXIS = "pref_accelerometer_shake_xaxis";

    public static final String FAKEPREFERENCEFRAGMENT_ID_PREFACCELEROMETER_SHAKE_XAXIS_SEEKBAR = "pref_accelerometer_shake_xaxis_seekbar";

    public static final String FAKEPREFERENCEFRAGMENT_ID_PREFACCELEROMETER_SHAKE_YAXIS = "pref_accelerometer_shake_yaxis";

    public static final String FAKEPREFERENCEFRAGMENT_ID_PREFACCELEROMETER_SHAKE_YAXIS_SEEKBAR = "pref_accelerometer_shake_yaxis_seekbar";

    public static final String FAKEPREFERENCEFRAGMENT_ID_PREFACCELEROMETER_SHAKE_ZAXIS = "pref_accelerometer_shake_zaxis";

    public static final String FAKEPREFERENCEFRAGMENT_ID_PREFACCELEROMETER_SHAKE_ZAXIS_SEEKBAR = "pref_accelerometer_shake_zaxis_seekbar";

    public static final String FAKEPREFERENCEFRAGMENT_ID_PREFACCELEROMETER_SHAKE_BUTTON = "pref_accelerometer_shake_button";

    public static final String FAKEPREFERENCEFRAGMENT_KEY_PREFACCELEROMETER
            = "org.tomahawk.tomahawk_android.pref_accelerometer";

    public static final String FAKEPREFERENCEFRAGMENT_KEY_PREFACCELEROMETER_SHAKE
            = "org.tomahawk.tomahawk_android.pref_accelerometer_shake";

    public static final String FAKEPREFERENCEFRAGMENT_KEY_PREFACCELEROMETER_SHAKE_XAXIS
            = "org.tomahawk.tomahawk_android.pref_accelerometer_shake_xaxis";

    public static final String FAKEPREFERENCEFRAGMENT_KEY_PREFACCELEROMETER_SHAKE_XAXIS_SEEKBAR
            = "org.tomahawk.tomahawk_android.pref_accelerometer_shake_xaxis_seekbar";

    public static final String FAKEPREFERENCEFRAGMENT_KEY_PREFACCELEROMETER_SHAKE_YAXIS
            = "org.tomahawk.tomahawk_android.pref_accelerometer_shake_yaxis";

    public static final String FAKEPREFERENCEFRAGMENT_KEY_PREFACCELEROMETER_SHAKE_YAXIS_SEEKBAR
            = "org.tomahawk.tomahawk_android.pref_accelerometer_shake_yaxis_seekbar";

    public static final String FAKEPREFERENCEFRAGMENT_KEY_PREFACCELEROMETER_SHAKE_ZAXIS
            = "org.tomahawk.tomahawk_android.pref_accelerometer_shake_zaxis";

    public static final String FAKEPREFERENCEFRAGMENT_KEY_PREFACCELEROMETER_SHAKE_ZAXIS_SEEKBAR
            = "org.tomahawk.tomahawk_android.pref_accelerometer_shake_zaxis_seekbar";

    public static final String FAKEPREFERENCEFRAGMENT_KEY_PREFACCELEROMETER_SHAKE_BUTTON
            = "org.tomahawk.tomahawk_android.pref_accelerometer_shake_button";



    private SharedPreferences mSharedPreferences;

    private SensorManager mSensorEventManager;

    /**
     * Called, when this {@link PreferenceAdvancedFragment}'s {@link android.view.View} has been
     * created
     */
    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        // Fetch our SharedPreferences from the PreferenceManager
        mSharedPreferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
        mSharedPreferences.registerOnSharedPreferenceChangeListener(this);

        mSensorEventManager = (SensorManager)getActivity().getSystemService(Context.SENSOR_SERVICE);



        // Set up the set of FakePreferences to be shown in this Fragment
        List<FakePreferenceGroup> fakePreferenceGroups = new ArrayList<>();
        FakePreferenceGroup prefGroup = new FakePreferenceGroup(
                getString(R.string.preferences_playback));
        android.content.SharedPreferences.Editor editor = mSharedPreferences.edit();
        PrefsHelper.updateSensorAvailability(editor, mSensorEventManager);
        if (!PrefsHelper.isAccelerometerAvailable(mSharedPreferences))
        {
            prefGroup.addFakePreference(new FakePreferenceGroup.FakePreference(
                    FakePreferenceGroup.FAKEPREFERENCE_TYPE_PLAIN,
                    FAKEPREFERENCEFRAGMENT_ID_PREFACCELEROMETER,
                    FAKEPREFERENCEFRAGMENT_KEY_PREFACCELEROMETER,
                    getString(R.string.accelerometer),
                    getString(R.string.accelerometer_not_available), false));
        }else {
            PrefsHelper.setCustomAccEnabled(mSharedPreferences.edit(), true);
            PrefsHelper.setCustomProxEnabled(mSharedPreferences.edit(), false);
            mSharedPreferences.edit().commit();
            prefGroup.addFakePreference(new FakePreferenceGroup.FakePreference(
                    FakePreferenceGroup.FAKEPREFERENCE_TYPE_PLAIN,
                    FAKEPREFERENCEFRAGMENT_ID_PREFACCELEROMETER,
                    FAKEPREFERENCEFRAGMENT_KEY_PREFACCELEROMETER,
                    getString(R.string.accelerometer),
                    getString(R.string.preferences_customize_line1), false));
            /*prefGroup.addFakePreference(new FakePreferenceGroup.FakePreference(
                    FakePreferenceGroup.FAKEPREFERENCE_TYPE_PLAIN,
                    FAKEPREFERENCEFRAGMENT_ID_PREFACCELEROMETER_SHAKE,
                    FAKEPREFERENCEFRAGMENT_KEY_PREFACCELEROMETER_SHAKE,
                    getString(R.string.preferences_accelerometer),
                    "", false));
*/

            prefGroup.addFakePreference(new FakePreferenceGroup.FakePreference(
                    FakePreferenceGroup.FAKEPREFERENCE_TYPE_SPINNER,
                    FAKEPREFERENCEFRAGMENT_ID_PREFACCELEROMETER_SHAKE_XAXIS,
                    FAKEPREFERENCEFRAGMENT_KEY_PREFACCELEROMETER_SHAKE_XAXIS,
                    getString(R.string.preferences_accelerometer),
                    getString(R.string.preferences_accelerometer_xaxis), false));

            prefGroup.addFakePreference(new FakePreferenceGroup.FakePreference(
                    FakePreferenceGroup.FAKEPREFERENCE_TYPE_SEEKBAR,
                    FAKEPREFERENCEFRAGMENT_ID_PREFACCELEROMETER_SHAKE_XAXIS_SEEKBAR,
                    FAKEPREFERENCEFRAGMENT_KEY_PREFACCELEROMETER_SHAKE_XAXIS_SEEKBAR,
                    "",
                    "", false));


            prefGroup.addFakePreference(new FakePreferenceGroup.FakePreference(
                    FakePreferenceGroup.FAKEPREFERENCE_TYPE_SPINNER,
                    FAKEPREFERENCEFRAGMENT_ID_PREFACCELEROMETER_SHAKE_YAXIS,
                    FAKEPREFERENCEFRAGMENT_KEY_PREFACCELEROMETER_SHAKE_YAXIS,
                    "",
                    getString(R.string.preferences_accelerometer_yaxis), false));

            prefGroup.addFakePreference(new FakePreferenceGroup.FakePreference(
                    FakePreferenceGroup.FAKEPREFERENCE_TYPE_SEEKBAR,
                    FAKEPREFERENCEFRAGMENT_ID_PREFACCELEROMETER_SHAKE_YAXIS_SEEKBAR,
                    FAKEPREFERENCEFRAGMENT_KEY_PREFACCELEROMETER_SHAKE_YAXIS_SEEKBAR,
                    "",
                    "", false));


            prefGroup.addFakePreference(new FakePreferenceGroup.FakePreference(
                    FakePreferenceGroup.FAKEPREFERENCE_TYPE_SPINNER,
                    FAKEPREFERENCEFRAGMENT_ID_PREFACCELEROMETER_SHAKE_ZAXIS,
                    FAKEPREFERENCEFRAGMENT_KEY_PREFACCELEROMETER_SHAKE_ZAXIS,
                    "",
                    getString(R.string.preferences_accelerometer_zaxis), false));

            prefGroup.addFakePreference(new FakePreferenceGroup.FakePreference(
                    FakePreferenceGroup.FAKEPREFERENCE_TYPE_SEEKBAR,
                    FAKEPREFERENCEFRAGMENT_ID_PREFACCELEROMETER_SHAKE_ZAXIS_SEEKBAR,
                    FAKEPREFERENCEFRAGMENT_KEY_PREFACCELEROMETER_SHAKE_ZAXIS_SEEKBAR,
                    "",
                    "", false));

            prefGroup.addFakePreference(new FakePreferenceGroup.FakePreference(
                    FakePreferenceGroup.FAKEPREFERENCE_TYPE_BUTTON,
                    FAKEPREFERENCEFRAGMENT_ID_PREFACCELEROMETER_SHAKE_BUTTON,
                    FAKEPREFERENCEFRAGMENT_KEY_PREFACCELEROMETER_SHAKE_BUTTON,
                    getString(R.string.wave_apply),
                    "", false));
        }
        fakePreferenceGroups.add(prefGroup);
        final TomahawkApp app = (TomahawkApp)getActivity().getApplication();
        // Now we can push the complete set of FakePreferences into our FakePreferencesAdapter,
        // so that it can provide our ListView with the correct Views.
        /*FakePreferencesAdapter fakePreferencesAdapter = new FakePreferencesAdapter(getActivity(),
                getActivity().getLayoutInflater(), fakePreferenceGroups,app);*/
        //setListAdapter(fakePreferencesAdapter);

        getListView().setOnItemClickListener(this);
        setupNonScrollableSpacer(getListView());
    }

    /**
     * Initialize
     */
    @Override
    public void onResume() {
        super.onResume();

        getListAdapter().notifyDataSetChanged();
    }

    /**
     * Called every time an item inside the {@link se.emilsjolander.stickylistheaders.StickyListHeadersListView}
     * is clicked
     *
     * @param parent   The AdapterView where the click happened.
     * @param view     The view within the AdapterView that was clicked (this will be a view
     *                 provided by the adapter)
     * @param position The position of the view in the adapter.
     * @param id       The row id of the item that was clicked.
     */
    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        getListAdapter().notifyDataSetChanged();
    }
}
