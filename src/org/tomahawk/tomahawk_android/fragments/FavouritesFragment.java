package org.tomahawk.tomahawk_android.fragments;

import android.view.View;

import org.jdeferred.DoneCallback;
import org.tomahawk.libtomahawk.collection.Collection;
import org.tomahawk.libtomahawk.collection.CollectionManager;
import org.tomahawk.libtomahawk.collection.Playlist;
import org.tomahawk.libtomahawk.collection.PlaylistEntry;
import org.tomahawk.libtomahawk.database.CollectionDbManager;
import org.tomahawk.libtomahawk.database.DatabaseHelper;
import org.tomahawk.libtomahawk.infosystem.InfoSystem;
import org.tomahawk.libtomahawk.infosystem.User;
import org.tomahawk.tomahawk_android.R;
import org.tomahawk.tomahawk_android.activities.TomahawkMainActivity;
import org.tomahawk.tomahawk_android.adapters.Segment;
import org.tomahawk.tomahawk_android.services.PlaybackService;
import org.tomahawk.tomahawk_android.views.FancyDropDown;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by YGS on 10/30/2015.
 */
public class FavouritesFragment extends TomahawkFragment{
    public static final int SHOW_MODE_LOVEDITEMS = 0;

    public static final int SHOW_MODE_PLAYBACKLOG = 1;

    public static final String COLLECTION_TRACKS_SPINNER_POSITION
            = "org.tomahawk.tomahawk_android.collection_tracks_spinner_position";

    private Set<String> mResolvingTopArtistNames = new HashSet<>();

    private Playlist mCurrentPlaylist;

    @SuppressWarnings("unused")
    public void onEvent(DatabaseHelper.PlaylistsUpdatedEvent event) {
        if (mPlaylist != null && mPlaylist.getId().equals(event.mPlaylistId)) {
            refreshUserPlaylists();
        }
        if (mPlaylist != null && mPlaylist.getId().equals(event.mPlaylistId)) {
            if (!mAdapterUpdateHandler.hasMessages(ADAPTER_UPDATE_MSG)) {
                mPlaylist = CollectionDbManager.get().getCollectionDb(mCollection.getId()).getPlaylist(mPlaylist.getId());
                mAdapterUpdateHandler.sendEmptyMessageDelayed(
                        ADAPTER_UPDATE_MSG, ADAPTER_UPDATE_DELAY);
            }
        }
    }

    @SuppressWarnings("unused")
    public void onEventMainThread(CollectionManager.UpdatedEvent event) {
        super.onEventMainThread(event);

        if (event.mUpdatedItemIds != null && event.mUpdatedItemIds.contains(mAlbum.getCacheKey())
                && mContainerFragmentClass == null) {
            showAlbumFancyDropDown();
        }
    }

    @Override
    public void onResume() {
        super.onResume();

        User.getSelf().done(new DoneCallback<User>() {
            @Override
            public void onDone(User user) {
                if (mUser != null) {
                    if (mShowMode == SHOW_MODE_PLAYBACKLOG) {
                        String requestId = InfoSystem.get().resolvePlaybackLog(mUser);
                        if (requestId != null) {
                            mCorrespondingRequestIds.add(requestId);
                        }
                    } else if (mShowMode == SHOW_MODE_LOVEDITEMS) {
                        mHideRemoveButton = true;
                        if (mUser == user) {
                            CollectionManager.get().fetchLovedItemsPlaylist();
                            refreshUserPlaylists();
                        } else {
                            String requestId = InfoSystem.get().resolveLovedItems(mUser);
                            if (requestId != null) {
                                mCorrespondingRequestIds.add(requestId);
                            }
                        }
                    }
                    if (mUser != user) {
                        mHideRemoveButton = true;
                    } else {
                        CollectionManager.get().fetchPlaylists();
                    }
                }
                if (mContainerFragmentClass == null) {
                    getActivity().setTitle("");
                }
                updateAdapter();
            }
        });
    }

    /**
     * Called every time an item inside a ListView or GridView is clicked
     *
     * @param view the clicked view
     * @param item the Object which corresponds to the click
     */
    @Override
    public void onItemClick(View view, Object item) {
        if (item instanceof PlaylistEntry) {
            PlaylistEntry entry = (PlaylistEntry) item;
            if (entry.getQuery().isPlayable()) {
                TomahawkMainActivity activity = (TomahawkMainActivity) getActivity();
                final PlaybackService playbackService = activity.getPlaybackService();
                if (playbackService != null) {
                    if (playbackService.getCurrentEntry() == entry) {
                        playbackService.playPause();
                    } else {
                        playbackService.setPlaylist(mCurrentPlaylist, entry);
                        playbackService.start();
                    }
                }
            }
        }
    }

    /**
     * Update this {@link org.tomahawk.tomahawk_android.fragments.TomahawkFragment}'s {@link
     * org.tomahawk.tomahawk_android.adapters.TomahawkListAdapter} content
     */
    @Override
    protected void updateAdapter() {
        if (!mIsResumed) {
            return;
        }

        mCollection.getQueries(Collection.FAVOURITES).done(new DoneCallback<Playlist>() {
            @Override
            public void onDone(final Playlist playlist) {
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        mCurrentPlaylist = playlist;
                        Segment segment = new Segment.Builder(playlist)
                                .headerLayout(R.layout.single_line_list_header)
                                .headerStrings(constructDropdownItems())
                                .spinner(constructDropdownListener(
                                                COLLECTION_TRACKS_SPINNER_POSITION),
                                        getDropdownPos(COLLECTION_TRACKS_SPINNER_POSITION))
                                .build();
                        fillAdapter(segment);
                    }
                }).start();
            }
        });

    }

    private void refreshUserPlaylists() {
        CollectionManager.get().refreshUserPlaylists(0).done(new DoneCallback<Void>() {
            @Override
            public void onDone(Void result) {
                if (!mAdapterUpdateHandler.hasMessages(ADAPTER_UPDATE_MSG)) {
                    mAdapterUpdateHandler.sendEmptyMessageDelayed(
                            ADAPTER_UPDATE_MSG, ADAPTER_UPDATE_DELAY);
                }
            }
        });
    }

    private List<Integer> constructDropdownItems() {
        List<Integer> dropDownItems = new ArrayList<>();
        dropDownItems.add(R.string.collection_dropdown_recently_added);
        dropDownItems.add(R.string.collection_dropdown_alpha);
        dropDownItems.add(R.string.collection_dropdown_alpha_artists);
        return dropDownItems;
    }

    private int getSortMode() {
        switch (getDropdownPos(COLLECTION_TRACKS_SPINNER_POSITION)) {
            case 0:
                return Collection.SORT_LAST_MODIFIED;
            case 1:
                return Collection.SORT_ALPHA;
            case 2:
                return Collection.SORT_ARTIST_ALPHA;
            default:
                return Collection.SORT_NOT;
        }
    }

    private void showAlbumFancyDropDown() {
        if (mAlbum != null) {
            CollectionManager.get().getAvailableCollections(mAlbum).done(
                    new DoneCallback<List<Collection>>() {
                        @Override
                        public void onDone(final List<Collection> result) {
                            int initialSelection = 0;
                            for (int i = 0; i < result.size(); i++) {
                                if (result.get(i) == mCollection) {
                                    initialSelection = i;
                                    break;
                                }
                            }
                            showFancyDropDown(initialSelection, mAlbum.getName(),
                                    FancyDropDown.convertToDropDownItemInfo(result),
                                    new FancyDropDown.DropDownListener() {
                                        @Override
                                        public void onDropDownItemSelected(int position) {
                                            mCollection = result.get(position);
                                            updateAdapter();
                                        }

                                        @Override
                                        public void onCancel() {
                                        }
                                    });
                        }
                    });
        }
    }
}
