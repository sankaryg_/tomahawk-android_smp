package org.tomahawk.tomahawk_android.fragments;


import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.AdapterView;

import org.tomahawk.tomahawk_android.R;
import org.tomahawk.tomahawk_android.TomahawkApp;
import org.tomahawk.tomahawk_android.utils.FakePreferenceGroup;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by android on 24/9/15.
 */
public class WaveOverFragment_ extends TomahawkListFragment implements AdapterView.OnItemClickListener, SharedPreferences.OnSharedPreferenceChangeListener{

    private static final String TAG = WaveOverFragment_.class.getSimpleName();

    public static final String FAKEPREFERENCEFRAGMENT_ID_PREFGENERAL = "pref_general";

    public static final String FAKEPREFERENCEFRAGMENT_ID_PREFGENERAL1 = "pref_general1";

    public static final String FAKEPREFERENCEFRAGMENT_ID_DOUBLEWAVE= "double_wave";

    public static final String FAKEPREFERENCEFRAGMENT_ID_LONGOVER = "long_over";

    public static final String FAKEPREFERENCEFRAGMENT_ID_WAVE_BUTTON = "wave_btn";

    public static final String FAKEPREFERENCEFRAGMENT_KEY_PREFGENERAL
            = "org.tomahawk.tomahawk_android.prefgeneral";

    public static final String FAKEPREFERENCEFRAGMENT_KEY_PREFGENERAL1
            = "org.tomahawk.tomahawk_android.prefgeneral1";

    public static final String FAKEPREFERENCEFRAGMENT_KEY_DOUBLEWAVE
            = "org.tomahawk.tomahawk_android.double_wave";

    public static final String FAKEPREFERENCEFRAGMENT_KEY_LONGOVER
            = "org.tomahawk.tomahawk_android.long_over";

    public static final String FAKEPREFERENCEFRAGMENT_KEY_WAVE_BUTTON
            = "org.tomahawk.tomahawk_android.wave_btn";

    private SharedPreferences mSharedPreferences;

    /**
     * Called, when this {@link PreferenceAdvancedFragment}'s {@link android.view.View} has been
     * created
     */
    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        // Fetch our SharedPreferences from the PreferenceManager
        mSharedPreferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
        mSharedPreferences.registerOnSharedPreferenceChangeListener(this);

        // Set up the set of FakePreferences to be shown in this Fragment
        List<FakePreferenceGroup> fakePreferenceGroups = new ArrayList<>();
        FakePreferenceGroup prefGroup = new FakePreferenceGroup(
                getString(R.string.preferences_playback));
        prefGroup.addFakePreference(new FakePreferenceGroup.FakePreference(
                FakePreferenceGroup.FAKEPREFERENCE_TYPE_PLAIN,
                FAKEPREFERENCEFRAGMENT_ID_PREFGENERAL,
                FAKEPREFERENCEFRAGMENT_KEY_PREFGENERAL,
                getString(R.string.wave_over),
                getString(R.string.preferences_general_wave_line1),false));

        prefGroup.addFakePreference(new FakePreferenceGroup.FakePreference(
                FakePreferenceGroup.FAKEPREFERENCE_TYPE_SEEKBAR,
                FAKEPREFERENCEFRAGMENT_ID_DOUBLEWAVE,
                FAKEPREFERENCEFRAGMENT_KEY_DOUBLEWAVE,
                getString(R.string.preferences_double_wave),
                getString(R.string.preferences_double_wave_action_line1),true));

        prefGroup.addFakePreference(new FakePreferenceGroup.FakePreference(
                FakePreferenceGroup.FAKEPREFERENCE_TYPE_SEEKBAR,
                FAKEPREFERENCEFRAGMENT_ID_LONGOVER,
                FAKEPREFERENCEFRAGMENT_KEY_LONGOVER,
                getString(R.string.preferences_long_wave),
                getString(R.string.preferences_long_wave_action_line1),false));

        prefGroup.addFakePreference(new FakePreferenceGroup.FakePreference(
                FakePreferenceGroup.FAKEPREFERENCE_TYPE_BUTTON,
                FAKEPREFERENCEFRAGMENT_ID_WAVE_BUTTON,
                FAKEPREFERENCEFRAGMENT_KEY_WAVE_BUTTON,
                getString(R.string.wave_apply),
                "",false));
        fakePreferenceGroups.add(prefGroup);
        final TomahawkApp app = (TomahawkApp)getActivity().getApplication();
        // Now we can push the complete set of FakePreferences into our FakePreferencesAdapter,
        // so that it can provide our ListView with the correct Views.
        /*FakePreferencesAdapter fakePreferencesAdapter = new FakePreferencesAdapter(getActivity(),
                getActivity().getLayoutInflater(), fakePreferenceGroups,app);
        setListAdapter(fakePreferencesAdapter);
*/
        getListView().setOnItemClickListener(this);

        setupNonScrollableSpacer(getListView());
    }

    /**
     * Initialize
     */
    @Override
    public void onResume() {
        super.onResume();

        getListAdapter().notifyDataSetChanged();
    }


    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        getListAdapter().notifyDataSetChanged();
    }
}
