package org.tomahawk.tomahawk_android.fragments;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;

import org.jdeferred.DoneCallback;
import org.tomahawk.libtomahawk.collection.Album;
import org.tomahawk.libtomahawk.collection.CollectionCursor;
import org.tomahawk.libtomahawk.collection.Genre;
import org.tomahawk.tomahawk_android.R;
import org.tomahawk.tomahawk_android.TomahawkApp;
import org.tomahawk.tomahawk_android.activities.TomahawkMainActivity;
import org.tomahawk.tomahawk_android.adapters.Segment;
import org.tomahawk.tomahawk_android.utils.FragmentUtils;

import java.util.ArrayList;
import java.util.List;


public class GenreFragment extends TomahawkFragment {

    public static final String COLLECTION_GENRES_SPINNER_POSITION
            = "sensitivemusicplayer.smartfoxsoft.com.sense.collection_genres_spinner_position";

    public static final String COLLECTION_GENRES_SHOW_TYPE = "org.tomahawk.tomahawk_android.collection_genres_show_type";

    public static final int SHOW_MODE_STARREDARTISTS = 1;

    public static final String COLLECTION_GENRE_EDIT = "org.tomahawk.tomahawk_android.collection_genre_edit";

    @Override
    public void onResume() {
        super.onResume();

        if (getArguments() != null) {
            if (getArguments().containsKey(SHOW_MODE)) {
                mShowMode = getArguments().getInt(SHOW_MODE);
            }


        }
        updateAdapter();
    }

    /**
     * Called every time an item inside a ListView or GridView is clicked
     *
     * @param view the clicked view
     * @param item the Object which corresponds to the click
     */
    @Override
    public void onItemClick(View view,final Object item) {
       /* if (item instanceof Genre) {
            Bundle bundle = new Bundle();
            bundle.putString(TomahawkFragment.GENRE, ((Genre) item).getCacheKey());
            //ArrayList<Album> genreCollection = mCollection.getGenreAlbums((Genre) item, false);
            if (mCollection != null
                    && mCollection.getGenreAlbums((Genre) item, false).size() > 0) {
                bundle.putString(TomahawkFragment.COLLECTION_ID, mCollection.getId());
            }
            bundle.putInt(CONTENT_HEADER_MODE,
                    ContentHeaderFragment.MODE_HEADER_DYNAMIC_PAGER);
            bundle.putLong(CONTAINER_FRAGMENT_ID,
                    SensitiveMainActivity.getSessionUniqueId());
            FragmentUtils.replace((SensitiveMainActivity) getActivity(), ArtistPagerFragment.class,
                    bundle);
        }*/
        if (item instanceof Genre) {
            Genre artist = (Genre) item;
            mCollection.getGenreAlbums(artist).done(new DoneCallback<CollectionCursor<Album>>() {
                @Override
                public void onDone(CollectionCursor<Album> cursor) {
                    Bundle bundle = new Bundle();
                    bundle.putString(TomahawkFragment.GENRE,
                            ((Genre) item).getCacheKey());
                    if (cursor != null && cursor.size() > 0) {
                        bundle.putString(TomahawkFragment.COLLECTION_ID, mCollection.getId());
                    }
                    if (cursor != null) {
                        cursor.close();
                    }
                    bundle.putInt(CONTENT_HEADER_MODE,
                            ContentHeaderFragment.MODE_HEADER_DYNAMIC);
                    bundle.putLong(CONTAINER_FRAGMENT_ID,
                            TomahawkMainActivity.getSessionUniqueId());
                    FragmentUtils.replace((TomahawkMainActivity) getActivity(),
                            AlbumsFragment.class, bundle);
                }
            });
        }
    }


    @Override
    protected void updateAdapter() {
       /* if (!mIsResumed) {
            return;
        }

        LayoutInflater layoutInflater = getActivity().getLayoutInflater();

        List artists = new ArrayList<>();

        if (getArguments().containsKey(TomahawkFragment.GENRE)
                && !TextUtils.isEmpty(getArguments().getString(TomahawkFragment.GENRE))) {
            mGenre = Genre.getArtistByKey(
                    getArguments().getString(TomahawkFragment.GENRE));
            if (mGenre == null) {
                getActivity().getSupportFragmentManager().popBackStack();
                return;
            } else {
                ArrayList<String> requestIds = InfoSystem.getInstance().resolve(mGenre, true);
                for (String requestId : requestIds) {
                    mCorrespondingRequestIds.add(requestId);
                }
            }
        }


        // showContentHeader(mGenre);
        if (mGenre != null) {
            final List<Collection> collections =
                    CollectionManager.getInstance().getAvailableCollections(mGenre);
            int initialSelection = 0;
            for (int i = 0; i < collections.size(); i++) {
                if (collections.get(i).getId().equals(
                        getArguments().getString(TomahawkFragment.COLLECTION_ID))) {
                    initialSelection = i;
                    break;
                }
            }
            getArguments().putString(TomahawkFragment.COLLECTION_ID,
                    collections.get(initialSelection).getId());
        }
        *//*if (mShowMode == SHOW_MODE_STARREDARTISTS) {
            ArrayList<Artist> starredArtists = DatabaseHelper.getInstance().getStarredArtists();
            for (Artist artist : starredArtists) {
                ArrayList<String> requestIds = InfoSystem.getInstance().resolve(artist, false);
                for (String requestId : requestIds) {
                    mCorrespondingRequestIds.add(requestId);
                }
            }
            artists.addAll(starredArtists);
            if (getListAdapter() == null) {
                TomahawkListAdapter tomahawkListAdapter =
                        new TomahawkListAdapter((TomahawkMainActivity) getActivity(),
                                layoutInflater, new Segment(artists), getListView(), this);
                setListAdapter(tomahawkListAdapter);
            } else {
                getListAdapter().setSegments(new Segment(artists), getListView());
            }
        } else*//* if (mGenreArray != null) {
            artists.addAll(mGenreArray);
            if (getListAdapter() == null) {
                TomahawkListAdapter tomahawkListAdapter =
                        new TomahawkListAdapter((SensitiveMainActivity) getActivity(),
                                layoutInflater, new Segment(artists), getListView(), this);
                setListAdapter(tomahawkListAdapter);
            } else {
                getListAdapter().setSegments(new Segment(artists), getListView());
            }
        } else {
            artists.addAll(CollectionManager.getInstance()
                    .getCollection(SensitiveApp.PLUGINNAME_USERCOLLECTION).getGenres());
            *//*for (Artist artist : DatabaseHelper.getInstance().getStarredArtists()) {
                if (!artists.contains(artist)) {
                    artists.add(artist);
                }
            }*//*
            SharedPreferences preferences =
                    PreferenceManager.getDefaultSharedPreferences(SensitiveApp.getContext());
            List<Integer> dropDownItems = new ArrayList<>();
            dropDownItems.add(R.string.collection_dropdown_recently_added);
            dropDownItems.add(R.string.collection_dropdown_alpha);
            AdapterView.OnItemSelectedListener spinnerClickListener
                    = new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position,
                        long id) {
                    SharedPreferences preferences =
                            PreferenceManager.getDefaultSharedPreferences(SensitiveApp.getContext());
                    int initialPos = preferences.getInt(COLLECTION_ARTISTS_SPINNER_POSITION, 0);
                    if (initialPos != position) {
                        preferences.edit().putInt(COLLECTION_ARTISTS_SPINNER_POSITION, position)
                                .commit();
                        updateAdapter();
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {
                }
            };
            int initialPos = preferences.getInt(COLLECTION_ARTISTS_SPINNER_POSITION, 0);
            if (initialPos == 0) {
                Collection userColl = CollectionManager.getInstance().getCollection(
                        SensitiveApp.PLUGINNAME_USERCOLLECTION);
                Collections.sort(artists, new TomahawkListItemComparator(
                        TomahawkListItemComparator.COMPARE_RECENTLY_ADDED,
                        userColl.getGenreAddedTimeStamps()));
            } else if (initialPos == 1) {
                Collections.sort(artists, new TomahawkListItemComparator(
                        TomahawkListItemComparator.COMPARE_ALPHA));
            }
            List<Segment> segments = new ArrayList<>();
            segments.add(new Segment(initialPos, dropDownItems, spinnerClickListener, artists,
                    R.integer.grid_column_count, R.dimen.padding_superlarge,
                    R.dimen.padding_superlarge));
            if (getListAdapter() == null) {
                TomahawkListAdapter tomahawkListAdapter =
                        new TomahawkListAdapter((SensitiveMainActivity) getActivity(),
                                layoutInflater, segments, getListView(), this);
                setListAdapter(tomahawkListAdapter);
            } else {
                getListAdapter().setSegments(segments, getListView());
            }
        }

        onUpdateAdapterFinished();*/

        if (!mIsResumed) {
            return;
        }
        final SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(TomahawkApp.getContext());

        if (mGenreArray != null) {
            fillAdapter(new Segment.Builder(mGenreArray).build());
        } else {
            final List<Genre> starredArtists;
            if (mCollection.getId().equals(TomahawkApp.PLUGINNAME_USERCOLLECTION)) {
               // starredArtists = DatabaseHelper.get().getStarredArtists();
            } else {
                starredArtists = null;
            }
            mCollection.getGenres(getSortMode(),preferences.getString(COLLECTION_GENRE_EDIT,""))
                    .done(new DoneCallback<CollectionCursor<Genre>>() {
                        @Override
                        public void onDone(final CollectionCursor<Genre> cursor) {
                            new Thread(new Runnable() {
                                @Override
                                public void run() {
                                    /*if (starredArtists != null) {
                                        cursor.mergeItems(getSortMode(), starredArtists);
                                    }*/
                                    Segment segment;
                                    if (preferences.getString(COLLECTION_GENRES_SHOW_TYPE,"grid").equals("grid")) {
                                        segment = new Segment.Builder(cursor)
                                                .headerLayout(R.layout.dropdown_header)
                                                .headerStrings(constructDropdownItems())
                                               .showType(COLLECTION_GENRES_SHOW_TYPE, selectShowTypeListener(COLLECTION_GENRES_SHOW_TYPE), addTextListener(COLLECTION_GENRE_EDIT), addEditorActionListener(),false)
                                               .spinner(constructDropdownListener(
                                                               COLLECTION_GENRES_SPINNER_POSITION),
                                                        getDropdownPos(
                                                                COLLECTION_GENRES_SPINNER_POSITION))
                                                .showAsGrid(COLLECTION_GENRES_SHOW_TYPE, R.integer.grid_column_count,
                                                        R.dimen.padding_superlarge,
                                                        R.dimen.padding_superlarge)
                                                .build();
                                    }else {
                                        segment = new Segment.Builder(cursor)
                                                .headerLayout(R.layout.dropdown_header)
                                                .headerStrings(constructDropdownItems())
                                                .showType(COLLECTION_GENRES_SHOW_TYPE, selectShowTypeListener(COLLECTION_GENRES_SHOW_TYPE), addTextListener(COLLECTION_GENRE_EDIT), addEditorActionListener(),false)
                                                .spinner(constructDropdownListener(
                                                                COLLECTION_GENRES_SPINNER_POSITION),
                                                        getDropdownPos(
                                                                COLLECTION_GENRES_SPINNER_POSITION))

                                                .build();
                                    }
                                    fillAdapter(segment,mCollection);
                                }
                            }).start();
                        }
                    });
        }
    }
    private int getSortMode() {
        switch (getDropdownPos(COLLECTION_GENRES_SPINNER_POSITION)) {
            case 0:
                return org.tomahawk.libtomahawk.collection.Collection.SORT_LAST_MODIFIED;
            case 1:
                return org.tomahawk.libtomahawk.collection.Collection.SORT_ALPHA;
            default:
                return org.tomahawk.libtomahawk.collection.Collection.SORT_NOT;
        }
    }
    private List<Integer> constructDropdownItems() {
        List<Integer> dropDownItems = new ArrayList<>();
        dropDownItems.add(R.string.collection_dropdown_recently_added);
        dropDownItems.add(R.string.collection_dropdown_alpha);
        return dropDownItems;
    }
}
