package org.tomahawk.tomahawk_android.fragments;


import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.view.View;

import org.tomahawk.tomahawk_android.R;
import org.tomahawk.tomahawk_android.TomahawkApp;
import org.tomahawk.tomahawk_android.utils.FakePreferenceGroup;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by android on 24/9/15.
 */
public class PocketSensorFragment_ extends TomahawkListFragment implements SharedPreferences.OnSharedPreferenceChangeListener{

    public static final String FAKEPREFERENCEFRAGMENT_ID_PREFPOCKET = "pref_pocket";

    public static final String FAKEPREFERENCEFRAGMENT_ID_PREFPOCKET_SENSITIVITY = "pocket_sensitivity";

    public static final String FAKEPREFERENCEFRAGMENT_ID_PREFPOCKET_BUTTON = "pocket_button";

    public static final String FAKEPREFERENCEFRAGMENT_KEY_PREFPOCKET
            = "org.tomahawk.tomahawk_android.prefpocket";

    public static final String FAKEPREFERENCEFRAGMENT_KEY_PREFPOCKET_SENSITIVITY
            = "org.tomahawk.tomahawk_android.pocket_sensitivity";

    public static final String FAKEPREFERENCEFRAGMENT_KEY_PREFPOCKET_BUTTON
            = "org.tomahawk.tomahawk_android.pocket_button";

    private SharedPreferences mSharedPreferences;

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        mSharedPreferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
        mSharedPreferences.registerOnSharedPreferenceChangeListener(this);
        List<FakePreferenceGroup> fakePreferenceGroups = new ArrayList<>();
        FakePreferenceGroup prefGroup = new FakePreferenceGroup(
                getString(R.string.preferences_playback));
        prefGroup.addFakePreference(new FakePreferenceGroup.FakePreference(
                FakePreferenceGroup.FAKEPREFERENCE_TYPE_PLAIN,
                FAKEPREFERENCEFRAGMENT_ID_PREFPOCKET,
                FAKEPREFERENCEFRAGMENT_KEY_PREFPOCKET,
                getString(R.string.pocket),
                getString(R.string.preferences_pocket_line1),false));

        prefGroup.addFakePreference(new FakePreferenceGroup.FakePreference(
                FakePreferenceGroup.FAKEPREFERENCE_TYPE_SEEKBAR,
                FAKEPREFERENCEFRAGMENT_ID_PREFPOCKET_SENSITIVITY,
                FAKEPREFERENCEFRAGMENT_KEY_PREFPOCKET_SENSITIVITY,
                getString(R.string.preferences_pocket_sensitive_level),
                getString(R.string.preferences_pocket_line2),true));

        prefGroup.addFakePreference(new FakePreferenceGroup.FakePreference(
                FakePreferenceGroup.FAKEPREFERENCE_TYPE_BUTTON,
                FAKEPREFERENCEFRAGMENT_ID_PREFPOCKET_BUTTON,
                FAKEPREFERENCEFRAGMENT_KEY_PREFPOCKET_BUTTON,
                getString(R.string.wave_apply),
                "",false));
        fakePreferenceGroups.add(prefGroup);
        final TomahawkApp app = (TomahawkApp)getActivity().getApplication();
        // Now we can push the complete set of FakePreferences into our FakePreferencesAdapter,
        // so that it can provide our ListView with the correct Views.
        /*FakePreferencesAdapter fakePreferencesAdapter = new FakePreferencesAdapter(getActivity(),
                getActivity().getLayoutInflater(), fakePreferenceGroups,app);
        setListAdapter(fakePreferencesAdapter);*/

        //getListView().setOnItemClickListener(this);

        setupNonScrollableSpacer(getListView());

    }

    @Override
    public void onResume() {
        super.onResume();

        getListAdapter().notifyDataSetChanged();
    }

    @Override
    public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        getListAdapter().notifyDataSetChanged();
    }
}
