/* == This file is part of Tomahawk Player - <http://tomahawk-player.org> ===
 *
 *   Copyright 2013, Enno Gottschalk <mrmaffen@googlemail.com>
 *
 *   Tomahawk is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Tomahawk is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Tomahawk. If not, see <http://www.gnu.org/licenses/>.
 */
package org.tomahawk.tomahawk_android.fragments;

import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;

import org.jdeferred.DoneCallback;
import org.tomahawk.libtomahawk.collection.Artist;
import org.tomahawk.libtomahawk.collection.Collection;
import org.tomahawk.libtomahawk.collection.CollectionManager;
import org.tomahawk.libtomahawk.collection.Genre;
import org.tomahawk.libtomahawk.infosystem.InfoRequestData;
import org.tomahawk.tomahawk_android.R;
import org.tomahawk.tomahawk_android.utils.FragmentInfo;
import org.tomahawk.tomahawk_android.views.FancyDropDown;

import java.util.ArrayList;
import java.util.List;

public class ArtistPagerFragment extends PagerFragment {

    private static final String TAG = ArtistPagerFragment.class.getSimpleName();

    private Artist mArtist;

    private Genre mGenre;

    private int mInitialPage = -1;

    @SuppressWarnings("unused")
    public void onEventMainThread(CollectionManager.UpdatedEvent event) {
        if (event.mUpdatedItemIds != null
                && event.mUpdatedItemIds.contains(mArtist.getCacheKey())) {
            updatePager();
        }
    }

    /**
     * Called, when this {@link org.tomahawk.tomahawk_android.fragments.ArtistPagerFragment}'s
     * {@link android.view.View} has been created
     */
    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        getActivity().setTitle("");
        if (getArguments() != null) {
            if (getArguments().containsKey(TomahawkFragment.CONTAINER_FRAGMENT_PAGE)) {
                mInitialPage = getArguments()
                        .getInt(TomahawkFragment.CONTAINER_FRAGMENT_PAGE);
            }
            if (getArguments().containsKey(TomahawkFragment.ARTIST)
                    && !TextUtils.isEmpty(getArguments().getString(TomahawkFragment.ARTIST))) {
                mArtist = Artist.getByKey(getArguments().getString(TomahawkFragment.ARTIST));
                if (mArtist == null) {
                    getActivity().getSupportFragmentManager().popBackStack();
                    return;
                }
            }else if (getArguments().containsKey(TomahawkFragment.GENRE)
                        && !TextUtils.isEmpty(getArguments().getString(TomahawkFragment.GENRE))) {
                    mGenre = Genre.getByKey(getArguments().getString(TomahawkFragment.GENRE));
                    if (mGenre == null) {
                        getActivity().getSupportFragmentManager().popBackStack();
                        return;
                    }
                    else{
                    /*HatchetCollection hatchetCollection = (HatchetCollection)
                            CollectionManager.get().getCollection(TomahawkApp.PLUGINNAME_HATCHET);
                    hatchetCollection.getArtistTopHits(mArtist).done(new DoneCallback<Playlist>() {
                        @Override
                        public void onDone(Playlist result) {
                            boolean full = false;
                            if (result == null) {
                                full = true;
                            }
                            ArrayList<String> requestIds = InfoSystem.get().resolve(mArtist, full);
                            for (String requestId : requestIds) {
                                mCorrespondingRequestIds.add(requestId);
                            }
                        }
                    });*/
                }
            }
        }

        updatePager();
    }

    private void updatePager() {
        if (mArtist != null)
        showContentHeader(mArtist);
        else if (mGenre!=null)
        showContentHeader(mGenre);
        setupPager(getFragmentInfoLists(), mInitialPage, null, 1);
        if (mArtist!=null) {
            CollectionManager.get().getAvailableCollections(mArtist)
                    .done(new DoneCallback<List<Collection>>() {
                        @Override
                        public void onDone(final List<Collection> result) {
                            int initialSelection = 0;
                            for (int i = 0; i < result.size(); i++) {
                                if (result.get(i).getId().equals(
                                        getArguments().getString(TomahawkFragment.COLLECTION_ID))) {
                                    initialSelection = i;
                                    break;
                                }
                            }
                            getArguments().putString(TomahawkFragment.COLLECTION_ID,
                                    result.get(initialSelection).getId());
                            showFancyDropDown(initialSelection, mArtist.getPrettyName().toUpperCase(),
                                    FancyDropDown.convertToDropDownItemInfo(result),
                                    new FancyDropDown.DropDownListener() {
                                        @Override
                                        public void onDropDownItemSelected(int position) {
                                            getArguments().putString(TomahawkFragment.COLLECTION_ID,
                                                    result.get(position).getId());
                                            fillAdapter(getFragmentInfoLists(), 0, 1);
                                        }

                                        @Override
                                        public void onCancel() {
                                        }
                                    });
                            setupAnimations();
                        }
                    });
        }else if(mGenre!=null){
            CollectionManager.get().getAvailableCollections(mGenre)
                    .done(new DoneCallback<List<Collection>>() {
                        @Override
                        public void onDone(final List<Collection> result) {
                            int initialSelection = 0;
                            for (int i = 0; i < result.size(); i++) {
                               // Log.d("test",result+"_"+result.get(i)+"_"+result.get(i).getId()+"_"+getArguments().getString(TomahawkFragment.COLLECTION_ID));
                                if (result.get(i).getId().equals(
                                        getArguments().getString(TomahawkFragment.COLLECTION_ID))) {
                                    initialSelection = i;
                                    break;
                                }
                            }
                            getArguments().putString(TomahawkFragment.COLLECTION_ID,
                                    result.get(initialSelection).getId());
                            /*showFancyDropDown(initialSelection, mGenre.getPrettyName().toUpperCase(),
                                    FancyDropDown.convertToDropDownItemInfo(result),
                                    new FancyDropDown.DropDownListener() {
                                        @Override
                                        public void onDropDownItemSelected(int position) {
                                            getArguments().putString(TomahawkFragment.COLLECTION_ID,
                                                    result.get(position).getId());
                                            fillAdapter(getFragmentInfoLists(), 0, 1);
                                        }

                                        @Override
                                        public void onCancel() {
                                        }
                                    });*/
                            setupAnimations();
                        }
                    });
        }
    }

    private List<FragmentInfoList> getFragmentInfoLists() {
        List<FragmentInfoList> fragmentInfoLists = new ArrayList<>();
        FragmentInfoList fragmentInfoList = new FragmentInfoList();
        FragmentInfo fragmentInfo = new FragmentInfo();
        fragmentInfo.mClass = AlbumsFragment.class;
        fragmentInfo.mTitle = getString(R.string.music);
        fragmentInfo.mBundle = getChildFragmentBundle();
        if (mArtist != null){
        fragmentInfo.mBundle
                .putString(TomahawkFragment.ARTIST, mArtist.getCacheKey());
        }
        else if(mGenre!=null){
            fragmentInfo.mBundle
                    .putString(TomahawkFragment.GENRE, mGenre.getCacheKey());
        }
        fragmentInfoList.addFragmentInfo(fragmentInfo);
        fragmentInfoLists.add(fragmentInfoList);


        return fragmentInfoLists;
    }

    @Override
    protected void onInfoSystemResultsReported(InfoRequestData infoRequestData) {
        if (mArtist != null)
        showContentHeader(mArtist);
        else if (mGenre!=null)
            showContentHeader(mGenre);
    }
}
