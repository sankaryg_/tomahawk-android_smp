/*
 *    Copyright (C) 2014 Haruki Hasegawa
 *
 *    Licensed under the Apache License, Version 2.0 (the "License");
 *    you may not use this file except in compliance with the License.
 *    You may obtain a copy of the License at
 *
 *        http://www.apache.org/licenses/LICENSE-2.0
 *
 *    Unless required by applicable law or agreed to in writing, software
 *    distributed under the License is distributed on an "AS IS" BASIS,
 *    WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *    See the License for the specific language governing permissions and
 *    limitations under the License.
 */


package org.tomahawk.tomahawk_android.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.widget.SwitchCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.TextView;

import org.tomahawk.tomahawk_android.R;
import org.tomahawk.tomahawk_android.framework.AppEvent;
import org.tomahawk.tomahawk_android.mediaplayers.StandardMediaPlayer;
import org.tomahawk.tomahawk_android.model.EqualizerStateStore;
import org.tomahawk.tomahawk_android.model.EventDefs;
import org.tomahawk.tomahawk_android.utils.EqualizerUtil;

import de.greenrobot.event.EventBus;

public class EqualizerFragment extends ContentHeaderFragment
        implements AdapterView.OnItemSelectedListener,
        SeekBar.OnSeekBarChangeListener {

    // constants
    private static final int NUM_BAND_VIEWS = 6;
    private static final int SEEKBAR_MAX = 1000;
    private SwitchCompat equalizerState;



    private Spinner mSpinnerPreset;
    private TextView[] mTextViewBandLevels = new TextView[NUM_BAND_VIEWS];
    private SeekBar[] mSeekBarBandLevels = new SeekBar[NUM_BAND_VIEWS];





    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_equalizer, container, false);
        return rootView;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        obtainViewReferences(view);
        /*FrameLayout.LayoutParams params = (FrameLayout.LayoutParams) view.getLayoutParams();
        params.topMargin = getResources().getDimensionPixelSize(R.dimen.header_clear_space_nonscrollable)+getResources().getDimensionPixelSize(R.dimen.pager_indicator_height);
        */setupViews();

        //showContentHeader(getView());
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        mTextViewBandLevels = null;
        mSeekBarBandLevels = null;
        mSpinnerPreset = null;
    }

    @Override
    public void onStart() {
        super.onStart();

        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();

        EventBus.getDefault().unregister(this);
    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
        if (fromUser) {
            short band = seekBarToBandNo(seekBar);
            float bandLevel = (float) progress / SEEKBAR_MAX;

            postAppEvent(EventDefs.EqualizerReqEvents.SET_BAND_LEVEL, band, bandLevel);
        }
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {
    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        switch (parent.getId()) {
            case R.id.spinner_equalizerb_preset:
                postAppEvent(EventDefs.EqualizerReqEvents.SET_PRESET, position, 0);
                break;
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {
    }

    public void onEvent(AppEvent event) {
        switch (event.category) {
            case EventDefs.Category.NOTIFY_EQUALIZER:
                onNotifyEqualizerEvent(event);
                break;
        }
    }

    private void onNotifyEqualizerEvent(AppEvent event) {
        switch (event.event) {
            case EventDefs.EqualizerNotifyEvents.PRESET_UPDATED:
                updatePresetSpinner();
                break;
            case EventDefs.EqualizerNotifyEvents.BAND_LEVEL_UPDATED: {
                int band = event.arg1;

                if (band < 0) {
                    // all
                    for (int i = 0; i < EqualizerUtil.NUMBER_OF_BANDS; i++) {
                        updateBandLevelSeekBar(i);
                    }
                } else {
                    updateBandLevelSeekBar(band);
                }
            }
                break;
        }
    }

    private void obtainViewReferences(View view) {
        mSpinnerPreset = (Spinner)view.findViewById(R.id.spinner_equalizerb_preset);
        mSpinnerPreset.setOnItemSelectedListener(this);
        equalizerState = (SwitchCompat)view.findViewById(R.id.equalizerState);
        mTextViewBandLevels[0] = (TextView)view.findViewById(R.id.textview_equalizer_band_0);
        mTextViewBandLevels[1] = (TextView)view.findViewById(R.id.textview_equalizer_band_1);
        mTextViewBandLevels[2] = (TextView)view.findViewById(R.id.textview_equalizer_band_2);
        mTextViewBandLevels[3] = (TextView)view.findViewById(R.id.textview_equalizer_band_3);
        mTextViewBandLevels[4] = (TextView)view.findViewById(R.id.textview_equalizer_band_4);
        mTextViewBandLevels[5] = (TextView)view.findViewById(R.id.textview_equalizer_band_5);

        mSeekBarBandLevels[0] = (SeekBar)view.findViewById(R.id.seekbar_equalizer_band_0);
        mSeekBarBandLevels[1] = (SeekBar)view.findViewById(R.id.seekbar_equalizer_band_1);
        mSeekBarBandLevels[2] = (SeekBar)view.findViewById(R.id.seekbar_equalizer_band_2);
        mSeekBarBandLevels[3] = (SeekBar)view.findViewById(R.id.seekbar_equalizer_band_3);
        mSeekBarBandLevels[4] = (SeekBar)view.findViewById(R.id.seekbar_equalizer_band_4);
        mSeekBarBandLevels[5] = (SeekBar)view.findViewById(R.id.seekbar_equalizer_band_5);
        mSeekBarBandLevels[0].setOnSeekBarChangeListener(this);
        mSeekBarBandLevels[1].setOnSeekBarChangeListener(this);
        mSeekBarBandLevels[2].setOnSeekBarChangeListener(this);
        mSeekBarBandLevels[3].setOnSeekBarChangeListener(this);
        mSeekBarBandLevels[4].setOnSeekBarChangeListener(this);
        mSeekBarBandLevels[5].setOnSeekBarChangeListener(this);
        equalizerState.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                postAppEvent(EventDefs.EqualizerReqEvents.SET_ENABLED, (isChecked ? 1 : 0), 0);
            }
        });
    }

    private void setupViews() {
        final Context context = getActivity();
        final int numBands = EqualizerUtil.NUMBER_OF_BANDS;

        for (int i = 0; i < NUM_BAND_VIEWS; i++) {
            int visibility = (i < numBands) ? View.VISIBLE : View.GONE;

            mSeekBarBandLevels[i].setVisibility(visibility);
            mTextViewBandLevels[i].setVisibility(visibility);
        }

        for (int i = 0; i < numBands; i++) {
            mTextViewBandLevels[i].setText(formatFrequencyText(
                    EqualizerUtil.CENTER_FREQUENCY[i]));

            mSeekBarBandLevels[i].setMax(SEEKBAR_MAX);
        }

        {
            String[] presetNames = new String[EqualizerUtil.NUMBER_OF_PRESETS];

            for (int i = 0; i < presetNames.length; i++) {
                presetNames[i] = EqualizerUtil.PRESETS[i].name;
            }

            ArrayAdapter<String> adapter = new ArrayAdapter<String>(
                    context, android.R.layout.simple_spinner_item,
                    presetNames);

            adapter.setDropDownViewResource(
                    android.R.layout.simple_spinner_dropdown_item);

            mSpinnerPreset.setAdapter(adapter);
        }

        updatePresetSpinner();
        for (int i = 0; i < numBands; i++) {
            updateBandLevelSeekBar(i);
        }
    }

    private void updatePresetSpinner() {
        EqualizerStateStore state = StandardMediaPlayer.get().getEqualizerStateStore();
        mSpinnerPreset.setSelection(state.getSettings().curPreset);
    }

    private void updateBandLevelSeekBar(int band) {
        EqualizerStateStore state = StandardMediaPlayer.get().getEqualizerStateStore();

        mSeekBarBandLevels[band].setProgress(
                (int) (SEEKBAR_MAX * state.getNormalizedBandLevel(band)));
    }

    private static short seekBarToBandNo(SeekBar seekBar) {
        switch (seekBar.getId()) {
            case R.id.seekbar_equalizer_band_0:
                return 0;
            case R.id.seekbar_equalizer_band_1:
                return 1;
            case R.id.seekbar_equalizer_band_2:
                return 2;
            case R.id.seekbar_equalizer_band_3:
                return 3;
            case R.id.seekbar_equalizer_band_4:
                return 4;
            case R.id.seekbar_equalizer_band_5:
                return 5;
            default:
                throw new IllegalArgumentException();
        }
    }

    private static String formatFrequencyText(int freq_millihertz) {
        final int freq = freq_millihertz / 1000;
        if (freq < 1000) {
            return freq + " Hz";
        } else {
            return (freq / 1000) + " kHz";
        }
    }

    private void postAppEvent(int event, int arg1, float arg2) {
        EventBus.getDefault().post(new AppEvent(
                EventDefs.Category.EQUALIZER, event, arg1, arg2));
    }

}
