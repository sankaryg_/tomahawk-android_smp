package org.tomahawk.tomahawk_android.fragments;

import android.app.Activity;
import android.content.SharedPreferences;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;

import org.tomahawk.libtomahawk.infosystem.InfoRequestData;
import org.tomahawk.libtomahawk.infosystem.SensorItem;
import org.tomahawk.tomahawk_android.R;
import org.tomahawk.tomahawk_android.activities.TomahawkMainActivity;
import org.tomahawk.tomahawk_android.adapters.SpinnerAdapter;
import org.tomahawk.tomahawk_android.utils.FragmentInfo;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by android on 24/9/15.
 */
public class SensorPagerFragment extends PagerFragment {


    SpinnerAdapter mAdapter;
    List<SensorItem> mAdapterList = new ArrayList<>();
    List<FragmentInfoList> fragmentInfoLists;
    int initialPage = -1;
    int itemPosition;
    Activity activity;
    boolean initializing = true;
    public static final int ACC_DEFAULT_SENSITIVITY = 2000;
    public static int MIN_TIME_BETWEEN_SHAKE_EVENT = 0;
    public static final int PROXIMITY_LONG_STAY_DURATION_DEFAULT_VALUE = 1000;
    public static int maxSensitivityValue = 2500;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);
        mAdapterList.add(new SensorItem(getString(R.string.drawer_title_sensors), getString(R.string.disable_sensor)));
        mAdapterList.add(new SensorItem(getString(R.string.drawer_title_sensors), getString(R.string.wave_over)));
        mAdapterList.add(new SensorItem(getString(R.string.drawer_title_sensors), getString(R.string.pocket)));
        mAdapterList.add(new SensorItem(getString(R.string.drawer_title_sensors), getString(R.string.hammer)));
        mAdapterList.add(new SensorItem(getString(R.string.drawer_title_sensors), getString(R.string.accelerometer)));
        mAdapterList.add(new SensorItem(getString(R.string.drawer_title_sensors), getString(R.string.proximity)));
        ((TomahawkMainActivity)getActivity()).getSupportActionBarS().setNavigationMode(ActionBar.NAVIGATION_MODE_LIST);
        ((TomahawkMainActivity)getActivity()).getSupportActionBarS().setListNavigationCallbacks(new SpinnerAdapter(getActivity(), R.layout.config_ask_access, mAdapterList), new ActionBar.OnNavigationListener() {
            @Override
            public boolean onNavigationItemSelected(int itemPosition, long itemId) {
                if (initializing){
                    initializing = false;
                }else {
                    SharedPreferences preferences =
                            PreferenceManager.getDefaultSharedPreferences(getActivity());
                    preferences.edit().putInt(
                            TomahawkMainActivity.SENSOR_POSITION, itemPosition)
                            .apply();
                    ((OnSensorChange) activity).sensorChange();
                }
                /*SensorChangeEvent event = new SensorChangeEvent();
                event.mItemPosition = itemPosition;
                EventBus.getDefault().post(event);*/
                return true;
            }
        });
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        getActivity().setTitle("");//getString(R.string.drawer_title_sensors).toUpperCase());

        int initialPage = -1;
        itemPosition = PreferenceManager.getDefaultSharedPreferences(getActivity()).getInt(TomahawkMainActivity.SENSOR_POSITION,-1);
        if (getArguments() != null) {
            if (getArguments().containsKey(TomahawkFragment.CONTAINER_FRAGMENT_PAGE)) {
                initialPage = getArguments().getInt(TomahawkFragment.CONTAINER_FRAGMENT_PAGE);
            }

        }
        if (itemPosition == -1){
            itemPosition = 0;
        }
        showContentHeader(
                new ColorDrawable(getResources().getColor(R.color.primary_background_inverted)));
        ((TomahawkMainActivity)getActivity()).getSupportActionBarS().setSelectedNavigationItem(itemPosition);
        fragmentInfoLists = new ArrayList<>();
        FragmentInfoList fragmentInfoList = new FragmentInfoList();
        FragmentInfo fragmentInfo = new FragmentInfo();
        fragmentInfo.mIconResId = R.drawable.ic_connect;
        fragmentInfo.mBundle = getChildFragmentBundle();
        switch (itemPosition){
            case 0:
                fragmentInfo.mTitle = getString(R.string.disable_sensor);
                fragmentInfo.mClass = DisableSensorFragment.class;
                break;
            case 1:
                fragmentInfo.mTitle = getString(R.string.wave_over);
                fragmentInfo.mClass = WaveOverFragment.class;
                break;
            case 2:
                fragmentInfo.mTitle = getString(R.string.pocket);
                fragmentInfo.mClass = PocketSensorFragment.class;
                break;
            case 3:
                fragmentInfo.mTitle = getString(R.string.hammer);
                fragmentInfo.mClass = HammerFragment.class;
                break;
            case 4:
                fragmentInfo.mTitle = getString(R.string.accelerometer);
                fragmentInfo.mClass = AccelerometerFragment.class;
                break;
            case 5:
                fragmentInfo.mTitle = getString(R.string.proximity);
                fragmentInfo.mClass = ProximityFragment.class;
                break;
        }
        fragmentInfoList.addFragmentInfo(fragmentInfo);
        fragmentInfoLists.add(fragmentInfoList);
        setupPager(fragmentInfoLists, initialPage, null, 2);
       /* List<FragmentInfoList> fragmentInfoLists = new ArrayList<>();
        FragmentInfoList fragmentInfoList = new FragmentInfoList();
        FragmentInfo fragmentInfo = new FragmentInfo();
        fragmentInfo.mClass = WaveOverFragment.class;
        fragmentInfo.mTitle = getString(R.string.wave_over);
        fragmentInfo.mIconResId = R.drawable.ic_connect;
        fragmentInfo.mBundle = getChildFragmentBundle();
        fragmentInfoList.addFragmentInfo(fragmentInfo);
        fragmentInfoLists.add(fragmentInfoList);

        fragmentInfoList = new FragmentInfoList();
        fragmentInfo = new FragmentInfo();
        fragmentInfo.mClass = PocketSensorFragment.class;
        fragmentInfo.mTitle = getString(R.string.pocket);
        fragmentInfo.mBundle = getChildFragmentBundle();
        fragmentInfoList.addFragmentInfo(fragmentInfo);
        fragmentInfoLists.add(fragmentInfoList);

        fragmentInfoList = new FragmentInfoList();
        fragmentInfo = new FragmentInfo();
        fragmentInfo.mClass = HammerFragment.class;
        fragmentInfo.mTitle = getString(R.string.hammer);
        fragmentInfo.mBundle = getChildFragmentBundle();
        fragmentInfoList.addFragmentInfo(fragmentInfo);
        fragmentInfoLists.add(fragmentInfoList);

        fragmentInfoList = new FragmentInfoList();
        fragmentInfo = new FragmentInfo();
        fragmentInfo.mClass = AccelerometerFragment.class;
        fragmentInfo.mTitle = getString(R.string.accelerometer);
        fragmentInfo.mBundle = getChildFragmentBundle();
        fragmentInfoList.addFragmentInfo(fragmentInfo);
        fragmentInfoLists.add(fragmentInfoList);

        fragmentInfoList = new FragmentInfoList();
        fragmentInfo = new FragmentInfo();
        fragmentInfo.mClass = ProximityFragment.class;
        fragmentInfo.mTitle = getString(R.string.proximity);
        fragmentInfo.mBundle = getChildFragmentBundle();
        fragmentInfoList.addFragmentInfo(fragmentInfo);
        fragmentInfoLists.add(fragmentInfoList);*/

      //  setupPager(fragmentInfoLists, initialPage, null, 2);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        this.activity = activity;
    }

    @Override
    protected void onInfoSystemResultsReported(InfoRequestData infoRequestData) {

    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.sensor_menu,menu);
        //MenuItem item = menu.findItem(R.id.action_switch);
        MenuItem item = menu.findItem(R.id.action_apply);
        MenuItem mSearchItem = menu.findItem(R.id.action_search);
        if (mSearchItem != null){
            mSearchItem.setVisible(false);
        }
        if (item != null) {
            //CharSequence title = item.getTitle();
           /* Switch action_bar_switch = (Switch) item.getActionView().findViewById(R.id.action_switch);
            if (action_bar_switch != null) {
                //action_bar_switch.setText(title);
                action_bar_switch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                        Toast.makeText(getActivity(),"Checked_"+isChecked,Toast.LENGTH_SHORT).show();
                    }
                });
            }*/
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        switch (id) {
           /* case R.id.action_switch:
                Toast.makeText(getActivity(), "action_switch", Toast.LENGTH_LONG).show();
                return true;*/
            case R.id.action_apply:
                //Toast.makeText(getActivity(), "action_switch", Toast.LENGTH_LONG).show();
                switch (itemPosition){
                    case 0:
                        break;
                    case 1:

                        break;
                    case 2:
                         break;
                    case 3:
                        break;
                    case 4:
                        break;
                    case 5:
                         break;
                }
                return true;

            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public interface OnSensorChange{
        public void sensorChange();

    }
}
