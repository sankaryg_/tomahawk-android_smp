package org.tomahawk.tomahawk_android.fragments;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import org.tomahawk.libtomahawk.infosystem.SensorItem;
import org.tomahawk.tomahawk_android.R;
import org.tomahawk.tomahawk_android.TomahawkApp;
import org.tomahawk.tomahawk_android.activities.TomahawkMainActivity;
import org.tomahawk.tomahawk_android.adapters.SpinnerAdapter;
import org.tomahawk.tomahawk_android.sensor.MyIntents;
import org.tomahawk.tomahawk_android.sensor.PrefsHelper;
import org.tomahawk.tomahawk_android.services.PlaybackService;
import org.tomahawk.tomahawk_android.views.SensorSeekBar;

import java.util.ArrayList;
import java.util.List;

import de.greenrobot.event.EventBus;

/**
 * Created by YGS on 9/26/2015.
 */
public class PocketSensorFragment extends ContentHeaderFragment implements View.OnClickListener {

    SpinnerAdapter mAdapter;
    List<SensorItem> mAdapterList = new ArrayList<>();
    int initialPage = -1;
    boolean initializing = true;
    Activity activity;
    TomahawkApp app;
    SensorSeekBar seekBar;
    Button applyBtn;
    SharedPreferences preferences = null;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        getActivity().setTitle("");
        return inflater.inflate(R.layout.pocket_fragment, container, false);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        this.activity = activity;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        itemPosition = PreferenceManager.getDefaultSharedPreferences(getActivity()).getInt(TomahawkMainActivity.SENSOR_POSITION,-1);
        app = (TomahawkApp)getActivity().getApplication();
        if (itemPosition == -1){
            itemPosition = 0;
        }
         ((TomahawkMainActivity)getActivity()).getSupportActionBarS().setSelectedNavigationItem(itemPosition);
        seekBar = (SensorSeekBar)view.findViewById(R.id.pocketSensitivitySeekbar);
        seekBar.setParsedMaxValue(2500);
        seekBar.setParsedMinValue(500);
        seekBar.setParsedProgress(PrefsHelper.getModePocketAccSensitivity(app.getPrefs(), 1000));
        app.getSensorDataHelper().setAccZSeekbar(seekBar);
        applyBtn = (Button)view.findViewById(R.id.okBtn);
        applyBtn.setOnClickListener(this);
        app.getSensorDataHelper().setIntentBroadcastEnabled(false);
        PlaybackService.DisableAllSensorEvent event = new PlaybackService.DisableAllSensorEvent();
        EventBus.getDefault().post(event);
        int i = seekBar.getParsedProgress();
        PrefsHelper.setModePocketAccSensitivity(app.getPrefEditor(), i);
        app.getPrefEditor().commit();
        app.getSensorModeHelper().resetPreset(2);
        app.getSensorDataHelper().setIntentBroadcastEnabled(true);
        MyIntents.selectSensorMode(app, 2, true, true);
        setupNonScrollableSpacer(getView());
    }

    @Override
    public void onClick(View v) {
        app.getSensorDataHelper().setIntentBroadcastEnabled(false);
        PlaybackService.DisableAllSensorEvent event = new PlaybackService.DisableAllSensorEvent();
        EventBus.getDefault().post(event);
        int i = seekBar.getParsedProgress();
        PrefsHelper.setModePocketAccSensitivity(app.getPrefEditor(), i);
        app.getPrefEditor().commit();
        app.getSensorModeHelper().resetPreset(2);
        app.getSensorDataHelper().setIntentBroadcastEnabled(true);
        MyIntents.selectSensorMode(app, 2, true, true);
    }
}
