/* == This file is part of Tomahawk Player - <http://tomahawk-player.org> ===
 *
 *   Copyright 2012, Enno Gottschalk <mrmaffen@googlemail.com>
 *
 *   Tomahawk is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Tomahawk is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Tomahawk. If not, see <http://www.gnu.org/licenses/>.
 */
package org.tomahawk.tomahawk_android.fragments;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;

import org.jdeferred.DoneCallback;
import org.tomahawk.libtomahawk.collection.Album;
import org.tomahawk.libtomahawk.collection.AlphaComparator;
import org.tomahawk.libtomahawk.collection.ArtistAlphaComparator;
import org.tomahawk.libtomahawk.collection.Collection;
import org.tomahawk.libtomahawk.collection.CollectionCursor;
import org.tomahawk.libtomahawk.collection.CollectionManager;
import org.tomahawk.libtomahawk.collection.LastModifiedComparator;
import org.tomahawk.libtomahawk.collection.Playlist;
import org.tomahawk.libtomahawk.collection.PlaylistEntry;
import org.tomahawk.libtomahawk.collection.UserCollection;
import org.tomahawk.tomahawk_android.R;
import org.tomahawk.tomahawk_android.TomahawkApp;
import org.tomahawk.tomahawk_android.activities.TomahawkMainActivity;
import org.tomahawk.tomahawk_android.adapters.Segment;
import org.tomahawk.tomahawk_android.adapters.TomahawkListAdapter;
import org.tomahawk.tomahawk_android.services.PlaybackService;
import org.tomahawk.tomahawk_android.utils.FragmentUtils;
import org.tomahawk.tomahawk_android.views.FancyDropDown;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * {@link TomahawkFragment} which shows a set of {@link Album}s inside its {@link
 * se.emilsjolander.stickylistheaders.StickyListHeadersListView}
 */
public class AlbumsFragment extends TomahawkFragment {

    public static final String COLLECTION_ALBUMS_SPINNER_POSITION
            = "org.tomahawk.tomahawk_android.collection_albums_spinner_position";

    public static final String COLLECTION_ALBUMS_SHOW_TYPE = "org.tomahawk.tomahawk_android.collection_albums_show_type";

    public static final String COLLECTION_ALBUMS_EDIT = "org.tomahawk.tomahawk_android.collection_albums_edit";


    public void onEventMainThread(AnimateEvent event) {
        if (mContainerFragmentId == event.mContainerFragmentId
                ) {
            animate(event.mPlayTime);
        }
    }

    @Override
    public void onResume() {
        super.onResume();

        if (mContainerFragmentClass == null) {
            getActivity().setTitle("");
        }
        final SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(TomahawkApp.getContext());

        updateAdapter();
    }

    /**
     * Called every time an item inside a ListView or GridView is clicked
     *
     * @param view the clicked view
     * @param item the Object which corresponds to the click
     */
    @Override
    public void onItemClick(View view, final Object item) {
        final TomahawkMainActivity activity = (TomahawkMainActivity) getActivity();
        if (item instanceof PlaylistEntry) {
            final PlaylistEntry entry = (PlaylistEntry) item;
            if (entry.getQuery().isPlayable()) {
                final PlaybackService playbackService = activity.getPlaybackService();
                if (playbackService != null) {
                    if (playbackService.getCurrentEntry() == entry) {
                        playbackService.playPause();
                    } else {
                        /*HatchetCollection collection = (HatchetCollection) mCollection;
                        collection.getArtistTopHits(mArtist).done(new DoneCallback<Playlist>() {
                            @Override
                            public void onDone(Playlist topHits) {
                                playbackService.setPlaylist(topHits, entry);
                                playbackService.start();
                            }
                        });*/
                    }
                }
            }
        } else if (item instanceof Album) {
            Album album = (Album) item;
            mCollection.getAlbumTracks(album).done(new DoneCallback<Playlist>() {
                @Override
                public void onDone(Playlist playlist) {
                    Bundle bundle = new Bundle();
                    bundle.putString(TomahawkFragment.ALBUM, ((Album) item).getCacheKey());
                    if (playlist != null) {
                        bundle.putString(TomahawkFragment.COLLECTION_ID, mCollection.getId());
                    }
                    bundle.putInt(CONTENT_HEADER_MODE,
                            ContentHeaderFragment.MODE_HEADER_DYNAMIC);
                    FragmentUtils.replace((TomahawkMainActivity) getActivity(),
                            PlaylistEntriesFragment.class, bundle);
                }
            });
        }
    }

    /**
     * Update this {@link TomahawkFragment}'s {@link TomahawkListAdapter} content
     */
    @Override
    protected void updateAdapter() {
        if (!mIsResumed) {
            return;
        }
        final SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(TomahawkApp.getContext());
        if (mArtist != null) {
            if (mContainerFragmentId != -1) {
                showContentHeader(mArtist);
                if (mContainerFragmentClass == null) {
                    showArtistFancyDropDown();
                }
            }
            if (!TomahawkApp.PLUGINNAME_HATCHET.equals(mCollection.getId())) {
                mCollection.getArtistAlbums(mArtist)
                        .done(new DoneCallback<CollectionCursor<Album>>() {
                            @Override
                            public void onDone(CollectionCursor<Album> cursor) {
                                Segment segment = new Segment.Builder(cursor)
                                        .headerLayout(R.layout.single_line_list_header)
                                        .headerString(mCollection.getName() + " "
                                                + getString(R.string.albums))
                                        .showAsGrid(COLLECTION_ALBUMS_SHOW_TYPE, R.integer.grid_column_count,
                                                R.dimen.padding_superlarge,
                                                R.dimen.padding_superlarge)
                                        .build();
                                fillAdapter(segment, mCollection);
                                /*if (mContainerFragmentId != -1)
                                    showContentHeader(mArtist, true);*/
                            }
                        });
            }
        }
        else  if (mGenre != null){
            if (mContainerFragmentId != -1) {
                showContentHeader(mGenre);
                if (mContainerFragmentClass == null) {
                    showGenreFancyDropDown();
                }
            }
            if (mCollection!=null) {
                if (!TomahawkApp.PLUGINNAME_HATCHET.equals(mCollection.getId())) {
                    mCollection.getGenreAlbums(mGenre)
                            .done(new DoneCallback<CollectionCursor<Album>>() {
                                @Override
                                public void onDone(CollectionCursor<Album> cursor) {
                                    Segment segment = new Segment.Builder(cursor)
                                            .headerLayout(R.layout.single_line_list_header)
                                            .headerString(mCollection.getName() + " "
                                                    + getString(R.string.albums))
                                            .showAsGrid(COLLECTION_ALBUMS_SHOW_TYPE, R.integer.grid_column_count,
                                                    R.dimen.padding_superlarge,
                                                    R.dimen.padding_superlarge)
                                            .build();
                                    fillAdapter(segment, mCollection);
                                    /*if (mContainerFragmentId != -1)
                                        showContentHeader(mGenre,true);*/
                                }
                            });
                }
            }
        }
        else if (mAlbumArray != null) {
            fillAdapter(new Segment.Builder(mAlbumArray).build());
        }else if (mGenreArray != null){
            fillAdapter(new Segment.Builder(mGenreArray).build());
        }
        else if (mUser != null) {
            Segment segment = new Segment.Builder(sortAlbums(mUser.getStarredAlbums()))
                    .headerLayout(R.layout.single_line_list_header)
                    .headerStrings(constructDropdownItems())
                    .spinner(constructDropdownListener(COLLECTION_ALBUMS_SPINNER_POSITION),
                            getDropdownPos(COLLECTION_ALBUMS_SPINNER_POSITION))
                    .showAsGrid(COLLECTION_ALBUMS_SHOW_TYPE,R.integer.grid_column_count,
                            R.dimen.padding_superlarge,
                            R.dimen.padding_superlarge)
                    .build();
            fillAdapter(segment);
        } else {
            final List<Album> starredAlbums;
            if (mCollection.getId().equals(TomahawkApp.PLUGINNAME_USERCOLLECTION)) {
                starredAlbums = null;//DatabaseHelper.get().getStarredAlbums();
            } else {
                starredAlbums = null;
            }
            mCollection.getAlbums(getSortMode(),preferences.getString(COLLECTION_ALBUMS_EDIT,"")).done(new DoneCallback<CollectionCursor<Album>>() {
                @Override
                public void onDone(final CollectionCursor<Album> cursor) {
                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            if (starredAlbums != null) {
                                cursor.mergeItems(getSortMode(), starredAlbums);
                            }
                            Segment segment;
                            if (preferences.getString(COLLECTION_ALBUMS_SHOW_TYPE,"grid").equals("grid"))
                             {
                                segment = new Segment.Builder(cursor)
                                        .headerLayout(R.layout.dropdown_header)//single_line_list_header)
                                        .headerStrings(constructDropdownItems())
                                        .showType(COLLECTION_ALBUMS_SHOW_TYPE,selectShowTypeListener(COLLECTION_ALBUMS_SHOW_TYPE),addTextListener(COLLECTION_ALBUMS_EDIT),addEditorActionListener(),false)
                                        .spinner(constructDropdownListener(
                                                        COLLECTION_ALBUMS_SPINNER_POSITION),
                                                getDropdownPos(COLLECTION_ALBUMS_SPINNER_POSITION))
                                        .showAsGrid(COLLECTION_ALBUMS_SHOW_TYPE, R.integer.grid_column_count,
                                                R.dimen.padding_superlarge,
                                                R.dimen.padding_superlarge)
                                        .build();
                            }
                            else{
                                segment = new Segment.Builder(cursor)
                                        .headerLayout(R.layout.dropdown_header)//single_line_list_header)
                                        .headerStrings(constructDropdownItems())
                                        .showType(COLLECTION_ALBUMS_SHOW_TYPE,selectShowTypeListener(COLLECTION_ALBUMS_SHOW_TYPE),addTextListener(COLLECTION_ALBUMS_EDIT),addEditorActionListener(),false)
                                        .spinner(constructDropdownListener(
                                                        COLLECTION_ALBUMS_SPINNER_POSITION),
                                                getDropdownPos(COLLECTION_ALBUMS_SPINNER_POSITION))

                                        .build();
                            }
                            fillAdapter(segment, mCollection);
                        }
                    }).start();
                }
            });
        }
    }

    private List<Integer> constructDropdownItems() {
        List<Integer> dropDownItems = new ArrayList<>();
        dropDownItems.add(R.string.collection_dropdown_recently_added);
        dropDownItems.add(R.string.collection_dropdown_alpha);
        dropDownItems.add(R.string.collection_dropdown_alpha_artists);
        return dropDownItems;
    }

    private int getSortMode() {
        switch (getDropdownPos(COLLECTION_ALBUMS_SPINNER_POSITION)) {
            case 0:
                return Collection.SORT_LAST_MODIFIED;
            case 1:
                return Collection.SORT_ALPHA;
            case 2:
                return Collection.SORT_ARTIST_ALPHA;
            default:
                return Collection.SORT_NOT;
        }
    }

    private List<Album> sortAlbums(java.util.Collection<Album> albums) {
        List<Album> sortedAlbums;
        if (albums instanceof List) {
            sortedAlbums = (List<Album>) albums;
        } else {
            sortedAlbums = new ArrayList<>(albums);
        }
        switch (getDropdownPos(COLLECTION_ALBUMS_SPINNER_POSITION)) {
            case 0:
                UserCollection userColl = (UserCollection) CollectionManager.get()
                        .getCollection(TomahawkApp.PLUGINNAME_USERCOLLECTION);
                Collections.sort(sortedAlbums,
                        new LastModifiedComparator<>(userColl.getAlbumTimeStamps()));
                break;
            case 1:
                Collections.sort(sortedAlbums, new AlphaComparator());
                break;
            case 2:
                Collections.sort(sortedAlbums, new ArtistAlphaComparator());
                break;
        }
        return sortedAlbums;
    }

    private void showArtistFancyDropDown() {
        if (mArtist != null) {
            CollectionManager.get().getAvailableCollections(mArtist).done(
                    new DoneCallback<List<Collection>>() {
                        @Override
                        public void onDone(final List<Collection> result) {
                            int initialSelection = 0;
                            for (int i = 0; i < result.size(); i++) {
                                if (result.get(i) == mCollection) {
                                    initialSelection = i;
                                    break;
                                }
                            }
                            showFancyDropDown(initialSelection, mArtist.getName(),
                                    FancyDropDown.convertToDropDownItemInfo(result),
                                    new FancyDropDown.DropDownListener() {
                                        @Override
                                        public void onDropDownItemSelected(int position) {
                                            mCollection = result.get(position);
                                            updateAdapter();
                                        }

                                        @Override
                                        public void onCancel() {
                                        }
                                    });
                        }
                    });
        }
    }

    private void showGenreFancyDropDown() {
        if (mGenre != null) {
            CollectionManager.get().getAvailableCollections(mGenre).done(
                    new DoneCallback<List<Collection>>() {
                        @Override
                        public void onDone(final List<Collection> result) {
                            int initialSelection = 0;
                            for (int i = 0; i < result.size(); i++) {
                                if (result.get(i) == mCollection) {
                                    initialSelection = i;
                                    break;
                                }
                            }
                            showFancyDropDown(initialSelection, mGenre.getName(),
                                    FancyDropDown.convertToDropDownItemInfo(result),
                                    new FancyDropDown.DropDownListener() {
                                        @Override
                                        public void onDropDownItemSelected(int position) {
                                            mCollection = result.get(position);
                                            updateAdapter();
                                        }

                                        @Override
                                        public void onCancel() {
                                        }
                                    });
                        }
                    });
        }
    }
}
