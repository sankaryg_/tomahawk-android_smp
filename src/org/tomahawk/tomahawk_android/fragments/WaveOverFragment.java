package org.tomahawk.tomahawk_android.fragments;

import android.app.Activity;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.SeekBar;
import android.widget.TextView;

import org.tomahawk.libtomahawk.infosystem.SensorItem;
import org.tomahawk.tomahawk_android.R;
import org.tomahawk.tomahawk_android.TomahawkApp;
import org.tomahawk.tomahawk_android.activities.TomahawkMainActivity;
import org.tomahawk.tomahawk_android.adapters.SpinnerAdapter;
import org.tomahawk.tomahawk_android.sensor.MyIntents;
import org.tomahawk.tomahawk_android.sensor.PrefsHelper;
import org.tomahawk.tomahawk_android.sensor.SensorModeHelper;
import org.tomahawk.tomahawk_android.services.PlaybackService;

import java.util.ArrayList;
import java.util.List;

import de.greenrobot.event.EventBus;

/**
 * Created by YGS on 9/26/2015.
 */
public class WaveOverFragment extends ContentHeaderFragment implements SeekBar.OnSeekBarChangeListener,View.OnClickListener{

    SpinnerAdapter mAdapter;
    List<SensorItem> mAdapterList = new ArrayList<>();
    int initialPage = -1;
    boolean initializing = true;
    Activity activity;
    TomahawkApp app;

    private TextView doubleWaveTime;
    private SeekBar doubleWaveSpeedSeekBar;

    private TextView longStayTime;
    private SeekBar longStaySpeedSeekBar;

    private Button applyBtn;

    private static int waveOverProgress;

    private static int longStayOverProgress;

    SharedPreferences preferences = null;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        getActivity().setTitle("");
        return inflater.inflate(R.layout.wave_fragment, container, false);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        this.activity = activity;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        itemPosition = PreferenceManager.getDefaultSharedPreferences(getActivity()).getInt(TomahawkMainActivity.SENSOR_POSITION,-1);
        app = (TomahawkApp)getActivity().getApplication();
        if (itemPosition == -1){
            itemPosition = 0;
        }
        ((TomahawkMainActivity)getActivity()).getSupportActionBarS().setSelectedNavigationItem(itemPosition);
        doubleWaveTime = (TextView)view.findViewById(R.id.doubleWaveSpeedDispTV);
        doubleWaveSpeedSeekBar = (SeekBar)view.findViewById(R.id.doubleWaveSpeedSB);
        doubleWaveSpeedSeekBar.setMax(100);
        longStayTime = (TextView)view.findViewById(R.id.longStayDurationDispTV);
        longStaySpeedSeekBar = (SeekBar)view.findViewById(R.id.longStayDurationSB);
        longStaySpeedSeekBar.setMax(100);
        applyBtn = (Button)view.findViewById(R.id.okBtn);
        int i = PrefsHelper.getWaveOverDoubleWaveSpeed(app.getPrefs());
        int j = PrefsHelper.getWaveOverLongStay(app.getPrefs());
        waveOverProgress = SensorModeHelper.fromDoubleWaveSpeedToRatio(PrefsHelper.getWaveOverDoubleWaveSpeed(app.getPrefs()));
        longStayOverProgress = SensorModeHelper.fromLongStayDurationToRatio(PrefsHelper.getWaveOverLongStay(app.getPrefs()));
        float f = (float)Math.round(i / 10) / 100F;
        doubleWaveTime.setText((new StringBuilder(" (")).append(String.format("%.2f", (f))).append("s) ").toString());
        doubleWaveSpeedSeekBar.setProgress(SensorModeHelper.fromDoubleWaveSpeedToRatio(i));
        float f1 = (float)Math.round(j / 10) / 100F;
        longStayTime.setText((new StringBuilder(" (")).append(String.format("%.2f", (f1))).append("s) ").toString());
        longStaySpeedSeekBar.setProgress(SensorModeHelper.fromLongStayDurationToRatio(j));
        doubleWaveSpeedSeekBar.setOnSeekBarChangeListener(this);
        longStaySpeedSeekBar.setOnSeekBarChangeListener(this);
        applyBtn.setOnClickListener(this);

        app.getSensorDataHelper().setIntentBroadcastEnabled(false);
        PlaybackService.DisableAllSensorEvent event = new PlaybackService.DisableAllSensorEvent();
        EventBus.getDefault().post(event);
        int k = SensorModeHelper.fromRatioToDoubleWaveSpeed(waveOverProgress);
        PrefsHelper.setWaveOverDoubleWaveSpeed(app.getPrefEditor(), k);
        int l = SensorModeHelper.fromRatioToLongStayDuration(longStayOverProgress);
        PrefsHelper.setWaveOverLongStay(app.getPrefEditor(), l);
        app.getPrefEditor().commit();
        app.getSensorModeHelper().resetPreset(1);
        MyIntents.selectSensorMode(app, 1, true, true);
        try {
            app.getSensorDataHelper().setIntentBroadcastEnabled(true);
        } catch (Exception exception) {
            exception.printStackTrace();
        }
        setupNonScrollableSpacer(getView());
    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {


        if (seekBar.getId() == R.id.doubleWaveSpeedSB) {
            doubleWaveTime.setText((new StringBuilder(" (")).append(String.format("%.2f", (double) SensorModeHelper.fromRatioToDoubleWaveSpeed(progress) / 1000D)).append("s)").toString());
            waveOverProgress = doubleWaveSpeedSeekBar.getProgress();
        }
        if(seekBar.getId() == R.id.longStayDurationSB){
            longStayTime.setText((new StringBuilder(" (")).append( String.format("%.2f", (double)SensorModeHelper.fromRatioToLongStayDuration(progress) / 1000D)).append("s)").toString());
            longStayOverProgress = longStaySpeedSeekBar.getProgress();
        }

    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {

    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {

    }

    @Override
    public void onClick(View v) {
        app.getSensorDataHelper().setIntentBroadcastEnabled(false);
        PlaybackService.DisableAllSensorEvent event = new PlaybackService.DisableAllSensorEvent();
        EventBus.getDefault().post(event);
        int k = SensorModeHelper.fromRatioToDoubleWaveSpeed(waveOverProgress);
        PrefsHelper.setWaveOverDoubleWaveSpeed(app.getPrefEditor(), k);
        int l = SensorModeHelper.fromRatioToLongStayDuration(longStayOverProgress);
        PrefsHelper.setWaveOverLongStay(app.getPrefEditor(), l);
        app.getPrefEditor().commit();
        app.getSensorModeHelper().resetPreset(1);
        MyIntents.selectSensorMode(app, 1, true, true);
        try {
            app.getSensorDataHelper().setIntentBroadcastEnabled(true);
        } catch (Exception exception) {
            exception.printStackTrace();
        }
    }
}
