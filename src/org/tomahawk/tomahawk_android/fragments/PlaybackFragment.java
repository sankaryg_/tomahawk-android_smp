/* == This file is part of Tomahawk Player - <http://tomahawk-player.org> ===
 *
 *   Copyright 2012, Enno Gottschalk <mrmaffen@googlemail.com>
 *
 *   Tomahawk is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Tomahawk is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Tomahawk. If not, see <http://www.gnu.org/licenses/>.
 */
package org.tomahawk.tomahawk_android.fragments;

import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.preference.PreferenceManager;
import android.provider.Settings;
import android.support.v7.app.ActionBar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AccelerateDecelerateInterpolator;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.nineoldandroids.animation.ObjectAnimator;
import com.nineoldandroids.animation.ValueAnimator;
import com.sothree.slidinguppanel.SlidingUpPanelLayout;

import org.jdeferred.DoneCallback;
import org.tomahawk.libtomahawk.collection.CollectionManager;
import org.tomahawk.libtomahawk.collection.Image;
import org.tomahawk.libtomahawk.collection.PlaylistEntry;
import org.tomahawk.libtomahawk.infosystem.SwipeInterface;
import org.tomahawk.libtomahawk.infosystem.User;
import org.tomahawk.libtomahawk.resolver.Query;
import org.tomahawk.libtomahawk.utils.ImageUtils;
import org.tomahawk.libtomahawk.utils.ViewUtils;
import org.tomahawk.tomahawk_android.R;
import org.tomahawk.tomahawk_android.TomahawkApp;
import org.tomahawk.tomahawk_android.activities.TomahawkMainActivity;
import org.tomahawk.tomahawk_android.adapters.AlbumArtSwipeAdapter;
import org.tomahawk.tomahawk_android.adapters.Segment;
import org.tomahawk.tomahawk_android.adapters.TomahawkListAdapter;
import org.tomahawk.tomahawk_android.dialogs.TimeSayStatusDialog;
import org.tomahawk.tomahawk_android.mediaplayers.StandardMediaPlayer;
import org.tomahawk.tomahawk_android.services.PlaybackService;
import org.tomahawk.tomahawk_android.utils.AnimationUtils;
import org.tomahawk.tomahawk_android.utils.FragmentUtils;
import org.tomahawk.tomahawk_android.views.AlbumArtViewPager;
import org.tomahawk.tomahawk_android.views.PlaybackFragmentFrame;
import org.tomahawk.tomahawk_android.views.PlaybackPanel;
import org.tomahawk.tomahawk_android.views.VisualizerCompat;
import org.tomahawk.tomahawk_android.views.VisualizerViewFftSpectrum;
import org.tomahawk.tomahawk_android.views.VisualizerViewWaveForm;
import org.tomahawk.tomahawk_android.views.VisualizerWrapper;

import java.util.ArrayList;
import java.util.List;

/**
 * This {@link android.support.v4.app.Fragment} represents our Playback view in which the user can
 * play/stop/pause. It is being shown as the topmost fragment in the {@link PlaybackFragment}'s
 * {@link se.emilsjolander.stickylistheaders.StickyListHeadersListView}.
 */
public class PlaybackFragment extends TomahawkFragment implements SwipeInterface{

    private AlbumArtSwipeAdapter mAlbumArtSwipeAdapter;

    private AlbumArtViewPager mAlbumArtViewPager;

    private int mOriginalViewPagerHeight;

    private Toast mToast;

    private TextView mArtistTextView;

    private TextView mTrackTextView;

    private ImageView mFavouriteAction;

    private ImageView mPlaylistAction;

    private ImageView mSensorAction;

    private ImageView mTimeSayAction;

    private FrameLayout mVisualizerView;

    private VisualizerViewFftSpectrum mVisualizerViewFftSpectrum;

    private VisualizerViewWaveForm mVisualizerViewWaveForm;

    private boolean mDisplayVisualizer = false;

    private VisualizerCompat mVisualizer;

    private boolean mShowFadeAnimation = false;

    private VisualizerWrapper.OnDataChangedListener mDataChangedListener = new VisualizerWrapper.OnDataChangedListener() {

        @Override
        public void onFftDataChanged(byte[] data, int len) {
            if (mVisualizerViewFftSpectrum != null) {
                mVisualizerViewFftSpectrum.updateVisualizer(data);
            }
        }

        @Override
        public void onWaveDataChanged(byte[] data, int len, boolean scoop) {

            if (mVisualizerViewWaveForm != null) {
                mVisualizerViewWaveForm.updateVisualizer(data, scoop);
            }
        }

    };

    private final static int DISABLE_VISUALIZER = 0;

    private final static int ENABLE_VISUALIZER = 1;

    Handler mVisualizerHandler = new Handler() {

        @Override
        public void handleMessage(Message msg) {
            mVisualizerHandler.removeCallbacksAndMessages(null);
            switch (msg.what) {
                case DISABLE_VISUALIZER:
                    mVisualizer.setEnabled(false);
                    break;
                case ENABLE_VISUALIZER:
                    mVisualizer.setEnabled(true);
                    break;
            }
        }
    };

    @Override
    public void top2bottom(View v) {
        SlidingUpPanelLayout slidingLayout =
                ((TomahawkMainActivity) getActivity()).getSlidingUpPanelLayout();
        slidingLayout.collapsePanel();
    }


    /*public abstract static class ShowContextMenuListener {

        public abstract void onShowContextMenu(Query query);

        public abstract void onDoubleTap(Query query);
    }

    private final ShowContextMenuListener mShowContextMenuListener = new ShowContextMenuListener() {

        @Override
        public void onShowContextMenu(Query query) {
            FragmentUtils.showContextMenu((TomahawkMainActivity) getActivity(), query, null, true,
                    true);
        }

        @Override
        public void onDoubleTap(Query query) {
            final ImageView imageView =
                    (ImageView) getView().findViewById(R.id.imageview_favorite_doubletap);
            if (DatabaseHelper.get().isItemLoved(query)) {
                ImageUtils.loadDrawableIntoImageView(TomahawkApp.getContext(), imageView,
                        R.drawable.ic_action_unfavorite_large);
            } else {
                ImageUtils.loadDrawableIntoImageView(TomahawkApp.getContext(), imageView,
                        R.drawable.ic_action_favorite_large);
            }
            AnimationUtils.fade(imageView, AnimationUtils.DURATION_CONTEXTMENU, true);
            Runnable r = new Runnable() {
                @Override
                public void run() {
                    AnimationUtils.fade(imageView, AnimationUtils.DURATION_CONTEXTMENU, false);
                }
            };
            new Handler().postDelayed(r, 2000);
            CollectionManager.get().toggleLovedItem(query);
        }
    };*/

    public void onEventMainThread(PlaybackService.PlayStateChangedEvent event) {
        SharedPreferences preferences =
                PreferenceManager.getDefaultSharedPreferences(getActivity());
        if ( ((TomahawkMainActivity)getActivity()).getPlaybackService() != null ){
            int audioSessionId = StandardMediaPlayer.get().getLibVlcInstance().getAudioSessionId();

            if (audioSessionId >0 || ((TomahawkMainActivity)getActivity()).getPlaybackService().isPlaying()) {
                mVisualizer = VisualizerWrapper.getInstance(
                        audioSessionId, 50);
                mDisplayVisualizer = (preferences.getInt(PreferenceAdvancedFragment.FAKEPREFERENCEFRAGMENT_KEY_VISUALIZER,
                        PreferenceAdvancedFragment.VISUALIZER_OFF))!=PreferenceAdvancedFragment.VISUALIZER_OFF?true:false;
                boolean mFftEnabled = (preferences.getInt(PreferenceAdvancedFragment.FAKEPREFERENCEFRAGMENT_KEY_VISUALIZER,
                        PreferenceAdvancedFragment.VISUALIZER_OFF))==PreferenceAdvancedFragment.VISUALIZER_FFT?true:false;
                boolean mWaveEnabled = (preferences.getInt(PreferenceAdvancedFragment.FAKEPREFERENCEFRAGMENT_KEY_VISUALIZER,
                        PreferenceAdvancedFragment.VISUALIZER_OFF))==PreferenceAdvancedFragment.VISUALIZER_WAVE?true:false;
                mVisualizerView.removeAllViews();

                if (mFftEnabled) {
                    mVisualizerView.addView(mVisualizerViewFftSpectrum);
                }
                if (mWaveEnabled) {
                    mVisualizerView.addView(mVisualizerViewWaveForm);
                }

                mVisualizer.setFftEnabled(mFftEnabled);
                mVisualizer.setWaveFormEnabled(mWaveEnabled);
                mVisualizer.setOnDataChangedListener(mDataChangedListener);

                setVisualizerView();
            }
        }
    }

    public void disPlayVisulizer(){
        SharedPreferences preferences =
                PreferenceManager.getDefaultSharedPreferences(getActivity());
        if ( ((TomahawkMainActivity)getActivity()).getPlaybackService() != null ){
            int audioSessionId = StandardMediaPlayer.get().getLibVlcInstance().getAudioSessionId();

            if (audioSessionId >0 || ((TomahawkMainActivity)getActivity()).getPlaybackService().isPlaying()) {
                mVisualizer = VisualizerWrapper.getInstance(
                        audioSessionId, 50);
                mDisplayVisualizer = (preferences.getInt(PreferenceAdvancedFragment.FAKEPREFERENCEFRAGMENT_KEY_VISUALIZER,
                        PreferenceAdvancedFragment.VISUALIZER_OFF))!=PreferenceAdvancedFragment.VISUALIZER_OFF?true:false;
                boolean mFftEnabled = (preferences.getInt(PreferenceAdvancedFragment.FAKEPREFERENCEFRAGMENT_KEY_VISUALIZER,
                        PreferenceAdvancedFragment.VISUALIZER_OFF))==PreferenceAdvancedFragment.VISUALIZER_FFT?true:false;
                boolean mWaveEnabled = (preferences.getInt(PreferenceAdvancedFragment.FAKEPREFERENCEFRAGMENT_KEY_VISUALIZER,
                        PreferenceAdvancedFragment.VISUALIZER_OFF))==PreferenceAdvancedFragment.VISUALIZER_WAVE?true:false;
                mVisualizerView.removeAllViews();

                if (mFftEnabled) {
                    mVisualizerView.addView(mVisualizerViewFftSpectrum);
                }
                if (mWaveEnabled) {
                    mVisualizerView.addView(mVisualizerViewWaveForm);
                }

                mVisualizer.setFftEnabled(mFftEnabled);
                mVisualizer.setWaveFormEnabled(mWaveEnabled);
                mVisualizer.setOnDataChangedListener(mDataChangedListener);

                setVisualizerView();
            }
        }
    }
    @SuppressWarnings("unused")
    public void onEventMainThread(TomahawkMainActivity.SlidingLayoutChangedEvent event) {
        switch (event.mSlideState) {
            case COLLAPSED:
            case EXPANDED:
                if (mAlbumArtSwipeAdapter != null) {
                    mAlbumArtSwipeAdapter.notifyDataSetChanged();
                }
                if (getListView() != null) {
                    getListView().smoothScrollToPosition(0);

                }

                try {
                    boolean mFftEnabled = (preferences.getInt(PreferenceAdvancedFragment.FAKEPREFERENCEFRAGMENT_KEY_VISUALIZER,
                            PreferenceAdvancedFragment.VISUALIZER_OFF))==PreferenceAdvancedFragment.VISUALIZER_FFT?true:false;
                    boolean mWaveEnabled = (preferences.getInt(PreferenceAdvancedFragment.FAKEPREFERENCEFRAGMENT_KEY_VISUALIZER,
                            PreferenceAdvancedFragment.VISUALIZER_OFF))==PreferenceAdvancedFragment.VISUALIZER_WAVE?true:false;
                    if (event.mSlideState == SlidingUpPanelLayout.PanelState.COLLAPSED) {
                        mVisualizerView.removeAllViews();
                        mVisualizer.setOnDataChangedListener(null);
                    }else {
                        mVisualizerView.removeAllViews();


                        if (mFftEnabled) {
                            mVisualizerView.addView(mVisualizerViewFftSpectrum);
                        }
                        if (mWaveEnabled) {
                            mVisualizerView.addView(mVisualizerViewWaveForm);
                        }

                        mVisualizer.setFftEnabled(mFftEnabled);
                        mVisualizer.setWaveFormEnabled(mWaveEnabled);
                        mVisualizer.setOnDataChangedListener(mDataChangedListener);
                        disPlayVisulizer();
                    }
                } catch (Exception e) {

                }
                if (event.mSlideState == SlidingUpPanelLayout.PanelState.EXPANDED){
                    //disPlayVisulizer();
                }
                /*if ( ((TomahawkMainActivity)getActivity()).getPlaybackService() != null ){
                    int audioSessionId = StandardMediaPlayer.get().getLibVlcInstance().getAudioSessionId();

                    if (audioSessionId >0 || ((TomahawkMainActivity)getActivity()).getPlaybackService().isPlaying()) {
                        mVisualizer = VisualizerWrapper.getInstance(
                                audioSessionId, 50);
                        mDisplayVisualizer = true;// mPrefs.getBooleanState(KEY_DISPLAY_VISUALIZER,
                        // false);
                        boolean mFftEnabled = true;*//*(Boolean) GlobalMethods
                      .getValueFromPreference(MediaPlayerActivity.this,
                              GlobalMethods.BOOLEAN_PREFERENCE,
                              PreferenceSettings.FFT_ENABLE);*//*// String.valueOf(VISUALIZER_TYPE_FFT_SPECTRUM).equals(
                        // mPrefs.getStringPref(KEY_VISUALIZER_TYPE, "1"));
                        boolean mWaveEnabled = false;*//*(Boolean) GlobalMethods
                      .getValueFromPreference(MediaPlayerActivity.this,
                              GlobalMethods.BOOLEAN_PREFERENCE,
                              PreferenceSettings.WAVE_ENABLE);*//*// String.valueOf(VISUALIZER_TYPE_WAVE_FORM).equals(
                        // mPrefs.getStringPref(KEY_VISUALIZER_TYPE, "1"));

                        mVisualizerView.removeAllViews();

                        if (mFftEnabled) {
                            mVisualizerView.addView(mVisualizerViewFftSpectrum);
                        }
                        if (mWaveEnabled) {
                            mVisualizerView.addView(mVisualizerViewWaveForm);
                        }

                        mVisualizer.setFftEnabled(mFftEnabled);
                        mVisualizer.setWaveFormEnabled(mWaveEnabled);
                        mVisualizer.setOnDataChangedListener(mDataChangedListener);

                        setVisualizerView();
                    }
                }*/

                break;
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setRestoreScrollPosition(false);
        try {
            float mTransitionAnimation = Settings.System.getFloat(
                    getActivity().getContentResolver(),
                    Settings.System.TRANSITION_ANIMATION_SCALE);
            if (mTransitionAnimation > 0.0) {
                mShowFadeAnimation = true;
            } else {
                mShowFadeAnimation = false;
            }

        } catch (Settings.SettingNotFoundException e) {
            e.printStackTrace();
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        return inflater.inflate(R.layout.playback_fragment, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

       ViewUtils.afterViewGlobalLayout(new ViewUtils.ViewRunnable(view) {
            @Override
            public void run() {
                /*if (getListView() != null) {
                    mHeaderScrollableHeight =
                            getLayedOutView().getHeight() - mHeaderNonscrollableHeight;
                    setupScrollableSpacer(getListAdapter(), getListView(), mAlbumArtViewPager);
                    setupNonScrollableSpacer(getListView());
                   
                }*/
            }
        });

        //getListView().setFastScrollEnabled(false);

        mAlbumArtViewPager = (AlbumArtViewPager) view.findViewById(R.id.albumart_viewpager);
        //mAlbumArtViewPager.setShowContextMenuListener(mShowContextMenuListener);
        mAlbumArtViewPager
                .setPlaybackService(((TomahawkMainActivity) getActivity()).getPlaybackService());
        mArtistTextView = (TextView)view.findViewById(R.id.artist_textview);
        mTrackTextView = (TextView) view.findViewById(R.id.track_textview);
        mFavouriteAction = (ImageView) view.findViewById(R.id.favourite_button);
        mPlaylistAction = (ImageView) view.findViewById(R.id.playlist_button);
        mSensorAction = (ImageView) view.findViewById(R.id.sensor_button);
        mTimeSayAction = (ImageView) view.findViewById(R.id.time_to_speak_button);
        view.findViewById(R.id.time_to_speak_button).setClickable(true);
        View viewVisualizer = (FrameLayout) view.findViewById(R.id.visualizer_view);//ViewUtils.ensureInflation(getView(),R.id.visualizer_stub,R.id.visualizer);
        mVisualizerViewFftSpectrum = new VisualizerViewFftSpectrum(getActivity());
        mVisualizerViewWaveForm = new VisualizerViewWaveForm(getActivity());
        if (viewVisualizer instanceof FrameLayout){
            mVisualizerView = (FrameLayout) viewVisualizer;
        }
        //showContentHeader(new ColorDrawable(getResources().getColor(R.color.full_transparent)));
        /*PlaybackSwipeDetector swipeDetector = new PlaybackSwipeDetector(this);
        FrameLayout playbackFragmentFrame = (FrameLayout) view.getParent();
        playbackFragmentFrame.setOnTouchListener(swipeDetector);*/
        getListView().setVisibility(View.GONE);
        PlaybackFragmentFrame playbackFragmentFrame = (PlaybackFragmentFrame) view.getParent();
        playbackFragmentFrame.setListView(getListView());
        playbackFragmentFrame.setPanelLayout(
                ((TomahawkMainActivity) getActivity()).getSlidingUpPanelLayout());
        if (((TomahawkMainActivity)getActivity()).getPlaybackService() != null) {
            if (CollectionManager.get().isItemLoved(((TomahawkMainActivity)getActivity()).getPlaybackService().getCurrentQuery())) {
                ImageUtils.loadDrawableIntoImageView(TomahawkApp.getContext(), mFavouriteAction,
                        R.drawable.ic_action_share);
            } else {
                ImageUtils.loadDrawableIntoImageView(TomahawkApp.getContext(), mFavouriteAction,
                        R.drawable.ic_action_favorites);
            }
                    /*AnimationUtils.fade(mFavouriteAction, AnimationUtils.DURATION_CONTEXTMENU, true);
                    Runnable r = new Runnable() {
                        @Override
                        public void run() {
                            AnimationUtils.fade(mFavouriteAction, AnimationUtils.DURATION_CONTEXTMENU, false);
                        }
                    };
                    new Handler().postDelayed(r, 2000);*/
          //  CollectionManager.get().toggleLovedItem(((TomahawkMainActivity)getActivity()).getPlaybackService().getCurrentQuery());
        }
        int itemPosition = PreferenceManager.getDefaultSharedPreferences(getActivity()).getInt(TomahawkMainActivity.SENSOR_POSITION,-1);
        switch (itemPosition){
            case 0:
                mSensorAction.setImageResource(R.drawable.ic_connect);
                break;
            case 1:
                mSensorAction.setImageResource(R.drawable.wave_over);
                break;
            case 2:
                mSensorAction.setImageResource(R.drawable.pocket);
                break;
            case 3:
                mSensorAction.setImageResource(R.drawable.hammer);
                break;
            case 4:
                mSensorAction.setImageResource(R.drawable.custom_acc);
                break;
            case 5:
                mSensorAction.setImageResource(R.drawable.custom_prox);
                break;
        }
        if (mContainerFragmentClass == null) {
            getActivity().setTitle("");
        }

        //Set listeners on our buttons
        view.findViewById(R.id.imageButton_shuffle).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // onShuffleClicked();
            }
        });
        view.findViewById(R.id.imageButton_repeat).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                //  onRepeatClicked();
                SlidingUpPanelLayout slidingLayout =
                        ((TomahawkMainActivity) getActivity()).getSlidingUpPanelLayout();
                slidingLayout.collapsePanel();
            }
        });
        view.findViewById(R.id.favourite_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (((TomahawkMainActivity)getActivity()).getPlaybackService() != null) {
                    if (CollectionManager.get().isItemLoved(((TomahawkMainActivity)getActivity()).getPlaybackService().getCurrentQuery())) {
                        ImageUtils.loadDrawableIntoImageView(TomahawkApp.getContext(), mFavouriteAction,
                                R.drawable.ic_action_share);
                    } else {
                        ImageUtils.loadDrawableIntoImageView(TomahawkApp.getContext(), mFavouriteAction,
                                R.drawable.ic_action_favorites);
                    }
                    /*AnimationUtils.fade(mFavouriteAction, AnimationUtils.DURATION_CONTEXTMENU, true);
                    Runnable r = new Runnable() {
                        @Override
                        public void run() {
                            AnimationUtils.fade(mFavouriteAction, AnimationUtils.DURATION_CONTEXTMENU, false);
                        }
                    };
                    new Handler().postDelayed(r, 2000);*/
                    CollectionManager.get().toggleLovedItem(((TomahawkMainActivity)getActivity()).getPlaybackService().getCurrentQuery());
                }
            }
        });
        view.findViewById(R.id.playlist_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (getListView() != null){
                    if (getListView().getVisibility() == View.GONE){
                        getListView().setBackgroundColor(getResources().getColor(R.color.full_transparent));
                        getListView().setVisibility(View.VISIBLE);
                    }else {
                        getListView().setVisibility(View.GONE);
                    }
                }
            }
        });
        view.findViewById(R.id.sensor_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                User.getSelf().done(new DoneCallback<User>() {
                    @Override
                    public void onDone(User user) {
                        ((TomahawkMainActivity) getActivity()).getSupportActionBarS().setNavigationMode(ActionBar.NAVIGATION_MODE_LIST);
                        final Bundle bundle = new Bundle();
                        bundle.putString(TomahawkFragment.USER, user.getId());
                        bundle.putInt(TomahawkFragment.CONTENT_HEADER_MODE,
                                ContentHeaderFragment.MODE_ACTIONBAR_FILLED);
                        int itemPosition = PreferenceManager.getDefaultSharedPreferences(getActivity()).getInt(TomahawkMainActivity.SENSOR_POSITION,-1);
                        FragmentUtils
                                .replace((TomahawkMainActivity) getActivity(),((TomahawkMainActivity) getActivity()).getCurrentSensorFragment(itemPosition), bundle);
                    }
                });
            }
        });
        view.findViewById(R.id.time_to_speak_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                TimeSayStatusDialog dialog = new TimeSayStatusDialog();
                dialog.show(getFragmentManager(),"volume");
            }
        });
        /*View closeButton = view.findViewById(R.id.close_button);
        closeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                SlidingUpPanelLayout slidingLayout =
                        ((TomahawkMainActivity) getActivity()).getSlidingUpPanelLayout();
                slidingLayout.collapsePanel();
            }
        });
        TextView closeButtonText = (TextView) closeButton.findViewById(R.id.close_button_text);
        closeButtonText.setText(getString(R.string.button_close).toUpperCase());*/

        SharedPreferences preferences =
                PreferenceManager.getDefaultSharedPreferences(getActivity());
        if (!preferences.getBoolean(
                TomahawkMainActivity.COACHMARK_PLAYBACKFRAGMENT_NAVIGATION_DISABLED, false)) {
            final View coachMark = ViewUtils.ensureInflation(view,
                    R.id.playbackfragment_navigation_coachmark_stub,
                    R.id.playbackfragment_navigation_coachmark);
            coachMark.findViewById(R.id.close_button).setOnClickListener(
                    new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            coachMark.setVisibility(View.GONE);
                        }
                    });
            coachMark.setVisibility(View.VISIBLE);
        }


    }

    @Override
    public void onResume() {
        super.onResume();

        PlaybackService playbackService = ((TomahawkMainActivity) getActivity())
                .getPlaybackService();

        onPlaylistChanged();

        mAlbumArtSwipeAdapter = new AlbumArtSwipeAdapter((TomahawkMainActivity) getActivity(),
                getActivity().getLayoutInflater(), mAlbumArtViewPager);
        mAlbumArtSwipeAdapter.setPlaybackService(playbackService);
        mAlbumArtViewPager.setAdapter(mAlbumArtSwipeAdapter);

        setupAlbumArtAnimation();

        refreshTrackInfo();
        //refreshRepeatButtonState();
        //refreshShuffleButtonState();
        updateAdapter();
        if(((TomahawkMainActivity) getActivity()).getSlidingUpPanelLayout().isPanelExpanded())
        ((TomahawkMainActivity) getActivity()).getPlaybackPanel().showPlaybackPanel(PlaybackPanel.PlaybackState.MINI);
    }

    private void disableVisualizer(boolean animation) {
        if (mVisualizer != null) {
            if (mVisualizerView.getVisibility() == View.VISIBLE) {
                mVisualizerView.setVisibility(View.INVISIBLE);
                if (mShowFadeAnimation) {
                    mVisualizerView.startAnimation(android.view.animation.AnimationUtils
                            .loadAnimation(getActivity(), android.R.anim.fade_out));
                }
                if (animation) {
                    mVisualizerHandler.sendEmptyMessageDelayed(
                            DISABLE_VISUALIZER,
                            android.view.animation.AnimationUtils.loadAnimation(getActivity(),
                                    android.R.anim.fade_out).getDuration());
                } else {
                    mVisualizerHandler.sendEmptyMessage(DISABLE_VISUALIZER);
                }

            }
        }

    }

    private void enableVisualizer() {
        if (mVisualizer != null) {
            if (mVisualizerView.getVisibility() != View.VISIBLE) {
                mVisualizerView.setVisibility(View.VISIBLE);
                mVisualizerHandler.sendEmptyMessage(ENABLE_VISUALIZER);
                if (mShowFadeAnimation) {
                    mVisualizerView.startAnimation(android.view.animation.AnimationUtils
                            .loadAnimation(getActivity(), android.R.anim.fade_in));
                }
            }
        }
    }

    public void setUIColor(int color) {

        mVisualizerViewFftSpectrum.setColor(color);
        mVisualizerViewWaveForm.setColor(color);
        // mTouchPaintView.setColor(color);
    }

    private void setVisualizerView() {
        try {
            if (((TomahawkMainActivity)getActivity()).getPlaybackService()!= null && ((TomahawkMainActivity)getActivity()).getPlaybackService().isPlaying() && mDisplayVisualizer) {
                enableVisualizer();
            } else {
                disableVisualizer(false);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Called every time an item inside a ListView or GridView is clicked
     *
     * @param view the clicked view
     * @param item the Object which corresponds to the click
     */
    @Override
    public void onItemClick(View view, Object item) {
        PlaybackService playbackService = ((TomahawkMainActivity) getActivity())
                .getPlaybackService();
        if (playbackService != null) {
            if (item instanceof PlaylistEntry) {
                PlaylistEntry entry = (PlaylistEntry) item;
                if (entry.getQuery().isPlayable()) {
                    // if the user clicked on an already playing track
                    if (playbackService.getCurrentEntry() == entry) {
                        playbackService.playPause();
                    } else {
                        playbackService.setCurrentEntry(entry);
                    }
                }
            }
        }
    }

    /**
     * Called every time an item inside a ListView or GridView is long-clicked
     *
     * @param item the Object which corresponds to the long-click
     */
    @Override
    public boolean onItemLongClick(View view, Object item) {
        if (item != null) {
            TomahawkMainActivity activity = (TomahawkMainActivity) getActivity();
            AnimationUtils
                    .fade(activity.getPlaybackPanel(), AnimationUtils.DURATION_CONTEXTMENU, false);
            return FragmentUtils
                    .showContextMenu((TomahawkMainActivity) getActivity(), item, null, false, true);
        }
        return false;
    }

    /**
     * If the PlaybackService signals, that it is ready, this method is being called
     */
    @Override
    public void onPlaybackServiceReady() {
        super.onPlaybackServiceReady();

        PlaybackService playbackService = ((TomahawkMainActivity) getActivity())
                .getPlaybackService();
        if (playbackService != null) {
            if (mAlbumArtSwipeAdapter != null) {
                mAlbumArtSwipeAdapter.setPlaybackService(playbackService);
                refreshTrackInfo();
                //refreshRepeatButtonState();
                //refreshShuffleButtonState();
            }
            mAlbumArtViewPager.setPlaybackService(playbackService);
        }
        onPlaylistChanged();
    }

    /**
     * Called when the PlaybackServiceBroadcastReceiver received a Broadcast indicating that the
     * track has changed inside our PlaybackService
     */
    @Override
    public void onTrackChanged() {
        super.onTrackChanged();

        refreshTrackInfo();
        updateAdapter();
        onPlaylistChanged();
    }

    /**
     * Called when the PlaybackServiceBroadcastReceiver received a Broadcast indicating that the
     * playlist has changed inside our PlaybackService
     */
    @Override
    public void onPlaylistChanged() {
        super.onPlaylistChanged();

        forceResolveVisibleItems(false);
        updateAdapter();

        //refreshRepeatButtonState();
        //refreshShuffleButtonState();
        if (mAlbumArtSwipeAdapter != null) {
            mAlbumArtSwipeAdapter.updatePlaylist();
        }
    }

    /**
     * Called when the PlaybackServiceBroadcastReceiver in PlaybackFragment received a Broadcast
     * indicating that the playState (playing or paused) has changed inside our PlaybackService
     */
    @Override
    public void onPlaystateChanged() {
        super.onPlaystateChanged();

        if (mAlbumArtSwipeAdapter != null) {
            mAlbumArtSwipeAdapter.updatePlaylist();
        }
    }

    /**
     * Update this {@link TomahawkFragment}'s {@link TomahawkListAdapter} content
     */
    @Override
    protected void updateAdapter() {
        if (!mIsResumed) {
            return;
        }

        PlaybackService playbackService =
                ((TomahawkMainActivity) getActivity()).getPlaybackService();
        if (playbackService != null) {
            List<Segment> segments = new ArrayList<>();
            List entries = new ArrayList();
            //entries.add(playbackService.getCurrentEntry());
            Segment segment = new Segment.Builder(entries).build();
            segment.setShowNumeration(true, 0);
            segments.add(segment);

            entries = new ArrayList();
            entries.addAll(playbackService.getQueue().getEntries());
            /*entries.remove(playbackService
                    .getCurrentEntry());*/ // don't show queue entry if currently playing
            segment = new Segment.Builder(entries).build();
            segment.setShowAsQueued(true);
            segments.add(segment);

            if (playbackService.getPlaylist().size() > 1) {
                int currentIndex;
                if (playbackService.getQueue().getIndexOfEntry(playbackService.getCurrentEntry())
                        > 0) {
                    currentIndex = playbackService.getQueueStartPos();
                } else {
                    currentIndex = playbackService.getPlaylist()
                            .getIndexOfEntry(playbackService.getCurrentEntry());
                }
                segment = new Segment.Builder(playbackService.getPlaylist())
                        //.offset(Math.max(1, 1))//currentIndex + 1))
                        .build();
                segment.setShowNumeration(true, 1);
                segments.add(segment);
            }
            fillAdapter(segments, mAlbumArtViewPager, null);
        }
    }

    private void setupAlbumArtAnimation() {
        if (mAlbumArtViewPager != null) {
            ViewUtils.afterViewGlobalLayout(new ViewUtils.ViewRunnable(mAlbumArtViewPager) {
                @Override
                public void run() {
                    if (mOriginalViewPagerHeight <= 0) {
                        mOriginalViewPagerHeight = mAlbumArtViewPager.getHeight();
                    }

                    // now calculate the animation goal and instantiate the animation
                    int playbackPanelHeight = TomahawkApp.getContext().getResources()
                            .getDimensionPixelSize(R.dimen.playback_panel_height);
                    ValueAnimator animator = ObjectAnimator
                            .ofFloat(getLayedOutView(), "y", playbackPanelHeight,
                                    getLayedOutView().getHeight() / -4)
                            .setDuration(10000);
                    animator.setInterpolator(new AccelerateDecelerateInterpolator());
                    addAnimator(ANIM_ALBUMART_ID, animator);

                    refreshAnimations();
                }
            });
        }
    }

    /**
     * Called when the shuffle button is clicked.
     */
    public void onShuffleClicked() {
        final PlaybackService playbackService = ((TomahawkMainActivity) getActivity())
                .getPlaybackService();
        if (playbackService != null) {
            playbackService.setShuffled(!playbackService.isShuffled());

            if (mToast != null) {
                mToast.cancel();
            }
            mToast = Toast.makeText(getActivity(), getString(playbackService.isShuffled()
                    ? R.string.shuffle_on_label
                    : R.string.shuffle_off_label), Toast.LENGTH_SHORT);
            mToast.show();
        }
    }

    /**
     * Called when the repeat button is clicked.
     */
    public void onRepeatClicked() {
        final PlaybackService playbackService = ((TomahawkMainActivity) getActivity())
                .getPlaybackService();
        if (playbackService != null) {
            int repeatMode = playbackService.getRepeatingMode();
            if (repeatMode == PlaybackService.NOT_REPEATING) {
                playbackService.setRepeatingMode(PlaybackService.REPEAT_ALL);
            } else if (repeatMode == PlaybackService.REPEAT_ALL) {
                playbackService.setRepeatingMode(PlaybackService.REPEAT_ONE);
            } else if (repeatMode == PlaybackService.REPEAT_ONE) {
                playbackService.setRepeatingMode(PlaybackService.NOT_REPEATING);
            }
        }
    }

    /**
     * Refresh the information in this fragment to reflect that of the current Track, if possible
     * (meaning mPlaybackService is not null).
     */
    protected void refreshTrackInfo() {
        final PlaybackService playbackService = ((TomahawkMainActivity) getActivity())
                .getPlaybackService();
        if (playbackService != null) {
            refreshTrackInfo(playbackService.getCurrentQuery());
        } else {
            refreshTrackInfo(null);
        }
    }

    /**
     * Refresh the information in this fragment to reflect that of the given Track.
     *
     * @param query the query to which the track info view stuff should be updated to
     */
    protected void refreshTrackInfo(final Query query) {
        if (getView() != null) {
            final TomahawkMainActivity activity = (TomahawkMainActivity) getActivity();
            final PlaybackService playbackService = activity.getPlaybackService();
            if (query != null && playbackService != null) {
                /*
                This logic makes sure, that if a track is being skipped by the user, it doesn't do this
                for eternity. Because a press of the next button would cause the AlbumArtSwipeAdapter
                to display a swipe to the next track, which would then cause another skipping to the
                next track. That's why we have to make a difference between a swipe by the user, and a
                programmatically called swipe.
                */
                mAlbumArtViewPager.setPlaybackService(playbackService);
                mAlbumArtSwipeAdapter.setPlaybackService(playbackService);

                // Make all buttons clickable
                getView().findViewById(R.id.imageButton_shuffle).setClickable(true);
                getView().findViewById(R.id.imageButton_repeat).setClickable(true);
                String name;
                if (playbackService.getCurrentQuery().getArtist()==null)
                    name = playbackService.getCurrentQuery().getGenre().getPrettyName();
                else
                    name =playbackService.getCurrentQuery().getArtist().getPrettyName();
                mArtistTextView.setText(name);
                mTrackTextView.setText(playbackService.getCurrentQuery().getPrettyName());
                ImageView bgImageView =
                        (ImageView) getView().findViewById(R.id.background);
                ImageUtils.loadBlurredImageIntoImageView(TomahawkApp.getContext(), bgImageView,
                        playbackService.getCurrentQuery().getImage(),
                        Image.getSmallImageSize(), R.color.playerview_default_bg);
                setUIColor(Color.DKGRAY);//ImageUtils.getColor());
            } else {
                // Make all buttons not clickable
                getView().findViewById(R.id.imageButton_shuffle).setClickable(false);
                getView().findViewById(R.id.imageButton_repeat).setClickable(false);
            }
        }
    }



    @Override
    public void animate(int position) {
        super.animate(position);
        TomahawkMainActivity activity = (TomahawkMainActivity) getActivity();
        if (activity.getSlidingOffset() > 0f) {
            activity.getPlaybackPanel().animate(position + 10000);
        }
    }
}
