package org.tomahawk.tomahawk_android.fragments;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.TextView;

import org.tomahawk.libtomahawk.infosystem.SensorItem;
import org.tomahawk.tomahawk_android.R;
import org.tomahawk.tomahawk_android.TomahawkApp;
import org.tomahawk.tomahawk_android.activities.TomahawkMainActivity;
import org.tomahawk.tomahawk_android.adapters.ActionSpinnerAdapter;
import org.tomahawk.tomahawk_android.adapters.SpinnerAdapter;
import org.tomahawk.tomahawk_android.sensor.AccelerometerDataAnalyzer;
import org.tomahawk.tomahawk_android.sensor.Action;
import org.tomahawk.tomahawk_android.sensor.IntentMapping;
import org.tomahawk.tomahawk_android.sensor.MyIntents;
import org.tomahawk.tomahawk_android.sensor.PrefsHelper;
import org.tomahawk.tomahawk_android.sensor.UserMappableAction;
import org.tomahawk.tomahawk_android.services.PlaybackService;
import org.tomahawk.tomahawk_android.views.SensorSeekBar;

import java.util.ArrayList;
import java.util.List;

import de.greenrobot.event.EventBus;

/**
 * Created by YGS on 9/26/2015.
 */
public class AccelerometerFragment extends ContentHeaderFragment implements View.OnClickListener,AdapterView.OnItemSelectedListener,SeekBar.OnSeekBarChangeListener{

    SpinnerAdapter mAdapter;
    List<SensorItem> mAdapterList = new ArrayList<>();
    int initialPage = -1;
    boolean initializing = true;
    Activity activity;
    TomahawkApp app;
    private SharedPreferences mSharedPreferences;
    AccelerometerDataAnalyzer accAnalyzer;
    ActionSpinnerAdapter actionspinneradapter;
    private SensorManager mSensorEventManager;
    private TextView accelerometerTextAction;
    private LinearLayout accelerometerLayout;
    private Spinner xAxisSpinner;
    private SensorSeekBar xAxisSeekBar;
    private Spinner yAxisSpinner;
    private SensorSeekBar yAxisSeekBar;
    private Spinner zAxisSpinner;
    private SensorSeekBar zAxisSeekBar;
    private Button applyBtn;
    SharedPreferences preferences = null;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        getActivity().setTitle("");
        return inflater.inflate(R.layout.accelerometer, container, false);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        this.activity = activity;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        app = (TomahawkApp)getActivity().getApplication();
        itemPosition = PreferenceManager.getDefaultSharedPreferences(getActivity()).getInt(TomahawkMainActivity.SENSOR_POSITION, -1);
        if (itemPosition == -1){
            itemPosition = 0;
        }
        ((TomahawkMainActivity)getActivity()).getSupportActionBarS().setSelectedNavigationItem(itemPosition);
        mSharedPreferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
        mSensorEventManager = (SensorManager)getActivity().getSystemService(Context.SENSOR_SERVICE);
        accAnalyzer = app.getSensorDataHelper().getAccAnalyzer();
        actionspinneradapter = new ActionSpinnerAdapter(getActivity(), R.layout.action_spinner_item, UserMappableAction.getMappableActions());

        android.content.SharedPreferences.Editor editor = mSharedPreferences.edit();
        PrefsHelper.updateSensorAvailability(editor, mSensorEventManager);
        accelerometerTextAction = (TextView)view.findViewById(R.id.textView_accelerometer);
        accelerometerLayout = (LinearLayout)view.findViewById(R.id.accelerometerLayout);
        xAxisSpinner = (Spinner)view.findViewById(R.id.xaxis_spinner);
        xAxisSpinner.setAdapter(actionspinneradapter);
        xAxisSpinner.setOnItemSelectedListener(this);

        xAxisSeekBar = (SensorSeekBar)view.findViewById(R.id.customXAxisSensitivitySeekbar);
        xAxisSeekBar.setParsedMaxValue(SensorPagerFragment.maxSensitivityValue);
        xAxisSeekBar.setOnSeekBarChangeListener(this);
        xAxisSeekBar.setVisualyDisabledWhenMax(true);
        int i = PrefsHelper.getCustomXSensitivity(app.getPrefs());
        xAxisSeekBar.setParsedProgress(i);
        AccelerometerDataAnalyzer accelerometerdataanalyzerx = app.getSensorDataHelper().getAccAnalyzer();
        accelerometerdataanalyzerx.updateXSensitivity(i);

        yAxisSpinner = (Spinner)view.findViewById(R.id.yaxis_spinner);
        yAxisSpinner.setAdapter(actionspinneradapter);
        yAxisSpinner.setOnItemSelectedListener(this);

        yAxisSeekBar = (SensorSeekBar)view.findViewById(R.id.customYAxisSensitivitySeekbar);
        yAxisSeekBar.setParsedMaxValue(SensorPagerFragment.maxSensitivityValue);
        yAxisSeekBar.setVisualyDisabledWhenMax(true);
        int iy = PrefsHelper.getCustomYSensitivity(app.getPrefs());
        yAxisSeekBar.setParsedProgress(iy);
        AccelerometerDataAnalyzer accelerometerdataanalyzery = app.getSensorDataHelper().getAccAnalyzer();
        accelerometerdataanalyzery.updateYSensitivity(iy);
        yAxisSeekBar.setOnSeekBarChangeListener(this);

        zAxisSpinner = (Spinner)view.findViewById(R.id.zaxis_spinner);
        zAxisSpinner.setAdapter(actionspinneradapter);
        zAxisSpinner.setOnItemSelectedListener(this);

        zAxisSeekBar = (SensorSeekBar)view.findViewById(R.id.customZAxisSensitivitySeekbar);
        zAxisSeekBar.setParsedMaxValue(SensorPagerFragment.maxSensitivityValue);
        zAxisSeekBar.setVisualyDisabledWhenMax(true);
        int iz = PrefsHelper.getCustomZSensitivity(app.getPrefs());
        zAxisSeekBar.setParsedProgress(iz);
        AccelerometerDataAnalyzer accelerometerdataanalyzer = app.getSensorDataHelper().getAccAnalyzer();
        accelerometerdataanalyzer.updateZSensitivity(iz);
        zAxisSeekBar.setOnSeekBarChangeListener(this);

        applyBtn = (Button)view.findViewById(R.id.okBtn);
        applyBtn.setOnClickListener(this);
        if (!PrefsHelper.isAccelerometerAvailable(mSharedPreferences))
        {
            accelerometerTextAction.setText(getActivity().getResources().getString(R.string.accelerometer_not_available));
            accelerometerLayout.setVisibility(View.GONE);
            applyBtn.setVisibility(View.GONE);
        }else {
            accelerometerTextAction.setText(getActivity().getResources().getString(R.string.preferences_customize_line1));
            accelerometerLayout.setVisibility(View.VISIBLE);
            applyBtn.setVisibility(View.VISIBLE);
        }

        app.getSensorDataHelper().setIntentBroadcastEnabled(false);
        PlaybackService.DisableAllSensorEvent event = new PlaybackService.DisableAllSensorEvent();
        EventBus.getDefault().post(event);

        PrefsHelper.setCustomAccEnabled(app.getPrefEditor(), true);
        PrefsHelper.setCustomProxEnabled(app.getPrefEditor(), false);
        app.getPrefEditor().apply();
        app.getSensorModeHelper().resetPreset(4);
        app.getSensorDataHelper().setIntentBroadcastEnabled(true);
        MyIntents.selectSensorMode(app, 4, true, true);
        Action action = IntentMapping.getActionForEvent(app, 100, 4,TomahawkApp.getContext());
        xAxisSpinner.setSelection(UserMappableAction.getActionPosition(action.id));

        Action action1 = IntentMapping.getActionForEvent(app, 111, 4,TomahawkApp.getContext());
        yAxisSpinner.setSelection(UserMappableAction.getActionPosition(action1.id));

        Action action2 = IntentMapping.getActionForEvent(app, 122, 4,TomahawkApp.getContext());
        zAxisSpinner.setSelection(UserMappableAction.getActionPosition(action2.id));

        setupNonScrollableSpacer(getView());
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
      if (parent.getId()==R.id.xaxis_spinner){
          if (view != null) {
              Object obj = view.getTag();
              if (obj != null) {
                  app.getPrefs().edit().putInt("custom_acc_xaxis", Integer.valueOf(obj.toString())).apply();
                  //IntentMapping.setCustomModeActionForEvent(app.getPrefs(), 100, Integer.valueOf(obj.toString()).intValue());
              }
          }
      }else if (parent.getId() == R.id.yaxis_spinner){
          if (view != null) {
              Object obj = view.getTag();
              if (obj != null) {
                  app.getPrefs().edit().putInt("custom_acc_yaxis", Integer.valueOf(obj.toString())).apply();
                  //IntentMapping.setCustomModeActionForEvent(app.getPrefs(), 111, Integer.valueOf(obj.toString()).intValue());
              }
          }
      }else if (parent.getId() == R.id.zaxis_spinner){
          if (view != null) {
              Object obj = view.getTag();
              if (obj != null) {
                  app.getPrefs().edit().putInt("custom_acc_zaxis", Integer.valueOf(obj.toString())).apply();
                  //IntentMapping.setCustomModeActionForEvent(app.getPrefs(), 122, Integer.valueOf(obj.toString()).intValue());
              }
          }
      }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

        if (seekBar.getId() == R.id.customXAxisSensitivitySeekbar){
            accAnalyzer.updateXSensitivity(xAxisSeekBar.getProgress());
            PrefsHelper.setCustomXSensitivity(app.getPrefEditor(), xAxisSeekBar.getParsedProgress());
            app.getPrefEditor().commit();
        }else if (seekBar.getId() == R.id.customYAxisSensitivitySeekbar){
            accAnalyzer.updateYSensitivity(yAxisSeekBar.getProgress());
            PrefsHelper.setCustomYSensitivity(app.getPrefEditor(), yAxisSeekBar.getParsedProgress());
            app.getPrefEditor().commit();
        }else if (seekBar.getId() == R.id.customZAxisSensitivitySeekbar){
            accAnalyzer.updateZSensitivity(zAxisSeekBar.getProgress());
            PrefsHelper.setCustomZSensitivity(app.getPrefEditor(), zAxisSeekBar.getParsedProgress());
            app.getPrefEditor().commit();
        }
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {

    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {

    }

    @Override
    public void onClick(View v) {
        app.getSensorDataHelper().setIntentBroadcastEnabled(false);
        PlaybackService.DisableAllSensorEvent event = new PlaybackService.DisableAllSensorEvent();
        EventBus.getDefault().post(event);
        IntentMapping.setCustomModeActionForEvent(app.getPrefs(), 100, app.getPrefs().getInt("custom_acc_xaxis",0));
        IntentMapping.setCustomModeActionForEvent(app.getPrefs(), 111, app.getPrefs().getInt("custom_acc_yaxis", 0));
        IntentMapping.setCustomModeActionForEvent(app.getPrefs(), 122, app.getPrefs().getInt("custom_acc_zaxis", 0));
        Action action = IntentMapping.getActionForEvent(app, 100, 4,TomahawkApp.getContext());
        //xAxisSpinner.setSelection(UserMappableAction.getActionPosition(action.id));

        Action action1 = IntentMapping.getActionForEvent(app, 111, 4,TomahawkApp.getContext());
        //yAxisSpinner.setSelection(UserMappableAction.getActionPosition(action1.id));

        Action action2 = IntentMapping.getActionForEvent(app, 122, 4,TomahawkApp.getContext());
        //zAxisSpinner.setSelection(UserMappableAction.getActionPosition(action2.id));

        PrefsHelper.setCustomAccEnabled(app.getPrefEditor(), true);
        PrefsHelper.setCustomProxEnabled(app.getPrefEditor(), false);
        app.getPrefEditor().commit();
        app.getSensorModeHelper().resetPreset(4);
        app.getSensorDataHelper().setIntentBroadcastEnabled(true);
        MyIntents.selectSensorMode(app, 4, true, true);
    }
}
