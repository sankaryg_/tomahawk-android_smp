package org.tomahawk.tomahawk_android.fragments;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v7.app.ActionBar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.TextView;

import org.tomahawk.libtomahawk.infosystem.SensorItem;
import org.tomahawk.tomahawk_android.R;
import org.tomahawk.tomahawk_android.TomahawkApp;
import org.tomahawk.tomahawk_android.activities.TomahawkMainActivity;
import org.tomahawk.tomahawk_android.adapters.ActionSpinnerAdapter;
import org.tomahawk.tomahawk_android.adapters.SpinnerAdapter;
import org.tomahawk.tomahawk_android.sensor.Action;
import org.tomahawk.tomahawk_android.sensor.IntentMapping;
import org.tomahawk.tomahawk_android.sensor.MyIntents;
import org.tomahawk.tomahawk_android.sensor.PrefsHelper;
import org.tomahawk.tomahawk_android.sensor.SensorModeHelper;
import org.tomahawk.tomahawk_android.sensor.UserMappableAction;
import org.tomahawk.tomahawk_android.services.PlaybackService;

import java.util.ArrayList;
import java.util.List;

import de.greenrobot.event.EventBus;

/**
 * Created by YGS on 9/26/2015.
 */
public class ProximityFragment extends ContentHeaderFragment implements View.OnClickListener,AdapterView.OnItemSelectedListener,SeekBar.OnSeekBarChangeListener{

    SpinnerAdapter mAdapter;
    List<SensorItem> mAdapterList = new ArrayList<>();
    int initialPage = -1;
    boolean initializing = true;
    Activity activity;
    TomahawkApp app;
    private SharedPreferences mSharedPreferences;

    private SensorManager mSensorEventManager;
    private TextView proximityTextAction;
    private LinearLayout proximityLayout;
    private Button applyBtn;
    private Spinner waveSpinner;
    private Spinner doubleWaveSpinner;
    private SeekBar doubleWaveSeekBar;
    private TextView doubleWaveTime;
    private Spinner longWaveSpinner;
    private SeekBar longWaveSeekBar;
    private TextView longWaveTime;
    int currentDoubleWaveSpeedValue;
    int currentLongStayDurationValue;
    private ActionSpinnerAdapter actionspinneradapter;
    SharedPreferences preferences;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setHasOptionsMenu(true);

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        getActivity().setTitle("");
        return inflater.inflate(R.layout.proximity, container, false);
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        this.activity = activity;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        app = (TomahawkApp)getActivity().getApplication();
        itemPosition = PreferenceManager.getDefaultSharedPreferences(getActivity()).getInt(TomahawkMainActivity.SENSOR_POSITION,-1);
        if (itemPosition == -1){
            itemPosition = 0;
        }
        ((TomahawkMainActivity)getActivity()).getSupportActionBarS().setSelectedNavigationItem(itemPosition);
        mSharedPreferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
        mSensorEventManager = (SensorManager)getActivity().getSystemService(Context.SENSOR_SERVICE);
        android.content.SharedPreferences.Editor editor = mSharedPreferences.edit();
        PrefsHelper.updateSensorAvailability(editor, mSensorEventManager);
        proximityTextAction = (TextView)view.findViewById(R.id.textview_proximity);
        proximityLayout = (LinearLayout)view.findViewById(R.id.proximityLayout);
        waveSpinner = (Spinner)view.findViewById(R.id.wave_spinner);
        doubleWaveSpinner = (Spinner)view.findViewById(R.id.double_wave_spinner);
        doubleWaveSeekBar = (SeekBar)view.findViewById(R.id.doubleWaveSpeedSB);
        doubleWaveTime = (TextView)view.findViewById(R.id.textview_double_wave_time);
        longWaveSpinner = (Spinner)view.findViewById(R.id.long_wave_spinner);
        longWaveSeekBar = (SeekBar)view.findViewById(R.id.longStayDurationSB);
        longWaveTime = (TextView)view.findViewById(R.id.textview_long_wave_time);
        actionspinneradapter = new ActionSpinnerAdapter(getActivity(), R.layout.action_spinner_item, UserMappableAction.getMappableActions());
        waveSpinner.setAdapter(actionspinneradapter);
        waveSpinner.setOnItemSelectedListener(this);
        doubleWaveSpinner.setAdapter(actionspinneradapter);
        doubleWaveSpinner.setOnItemSelectedListener(this);
        longWaveSpinner.setAdapter(actionspinneradapter);
        longWaveSpinner.setOnItemSelectedListener(this);
        doubleWaveSeekBar.setMax(100);
        currentDoubleWaveSpeedValue = PrefsHelper.getCustomProxDoubleWaveSpeed(app.getPrefs());
        int i1 = SensorModeHelper.fromDoubleWaveSpeedToRatio(currentDoubleWaveSpeedValue);
        doubleWaveSeekBar.setProgress(i1);
        doubleWaveSeekBar.setOnSeekBarChangeListener(this);
        float f = (float) Math.round(currentDoubleWaveSpeedValue / 10) / 100F;
        doubleWaveTime.setText((new StringBuffer(" (")).append(f).append("s) ").toString());

        longWaveSeekBar.setMax(100);
        currentLongStayDurationValue = PrefsHelper.getCustomProxLongStay(app.getPrefs());
        int l = SensorModeHelper.fromLongStayDurationToRatio(currentLongStayDurationValue);
        longWaveSeekBar.setProgress(l);
        longWaveSeekBar.setOnSeekBarChangeListener(this);
        float f1 = (float) Math.round(currentLongStayDurationValue / 10) / 100F;
        longWaveTime.setText((new StringBuilder(" (")).append(f1).append("s) ").toString());

        applyBtn = (Button)view.findViewById(R.id.okBtn);
        applyBtn.setOnClickListener(this);
        if (!PrefsHelper.isProxSensorAvailable(mSharedPreferences))
        {
            proximityTextAction.setText(getActivity().getResources().getString(R.string.proximity_not_available));
            proximityLayout.setVisibility(View.GONE);
            applyBtn.setVisibility(View.GONE);
        }else {
            proximityTextAction.setText(getActivity().getResources().getString(R.string.preferences_customize_line1));
            proximityLayout.setVisibility(View.VISIBLE);
            applyBtn.setVisibility(View.VISIBLE);
        }

        app.getSensorDataHelper().setIntentBroadcastEnabled(false);
        PlaybackService.DisableAllSensorEvent event = new PlaybackService.DisableAllSensorEvent();
        EventBus.getDefault().post(event);

        PrefsHelper.setCustomAccEnabled(app.getPrefEditor(), false);
        PrefsHelper.setCustomProxEnabled(app.getPrefEditor(), true);
        app.getPrefEditor().apply();
        app.getSensorModeHelper().resetPreset(4);
        app.getSensorDataHelper().setIntentBroadcastEnabled(true);
        MyIntents.selectSensorMode(app, 4, true, true);
        Action action3 = IntentMapping.getActionForEvent(app, 200, 4,TomahawkApp.getContext());
        waveSpinner.setSelection(UserMappableAction.getActionPosition(action3.id));

        Action action4 = IntentMapping.getActionForEvent(app, 222, 4,TomahawkApp.getContext());
        doubleWaveSpinner.setSelection(UserMappableAction.getActionPosition(action4.id));

        Action action5 = IntentMapping.getActionForEvent(app, 211, 4,TomahawkApp.getContext());
        longWaveSpinner.setSelection(UserMappableAction.getActionPosition(action5.id));
        setupNonScrollableSpacer(getView());
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        if (parent.getId() == R.id.wave_spinner){
            if (view != null) {
                Object obj = view.getTag();
                if (obj != null) {
                    app.getPrefs().edit().putInt("custom_prox_wave", Integer.valueOf(obj.toString())).apply();
                    //IntentMapping.setCustomModeActionForEvent(app.getPrefs(), 200, Integer.valueOf(obj.toString()).intValue());
                }
            }
        }else if (parent.getId() == R.id.double_wave_spinner){
            if (view != null) {
                Object obj = view.getTag();
                if (obj != null) {
                    app.getPrefs().edit().putInt("custom_prox_double", Integer.valueOf(obj.toString())).apply();
                    //IntentMapping.setCustomModeActionForEvent(app.getPrefs(), 222, Integer.valueOf(obj.toString()).intValue());
                }
            }
        }else if (parent.getId() == R.id.long_wave_spinner){
            if (view != null) {
                Object obj = view.getTag();
                if (obj != null) {
                    app.getPrefs().edit().putInt("custom_prox_long", Integer.valueOf(obj.toString())).apply();
                    //IntentMapping.setCustomModeActionForEvent(app.getPrefs(), 211, Integer.valueOf(obj.toString()).intValue());
                }
            }
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {

    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
        if (seekBar.getId() == R.id.doubleWaveSpeedSB){
            currentDoubleWaveSpeedValue = SensorModeHelper.fromRatioToDoubleWaveSpeed(progress);
            float f = (float) Math.round(currentDoubleWaveSpeedValue / 10) / 100F;
            doubleWaveTime.setText((new StringBuffer(" (")).append(f).append("s) ").toString());
            PrefsHelper.setCustomProxDoubleWaveSpeed(app.getPrefEditor(), currentDoubleWaveSpeedValue);
            app.getPrefEditor().commit();
        }else if (seekBar.getId() == R.id.longStayDurationSB){
            currentLongStayDurationValue = SensorModeHelper.fromRatioToLongStayDuration(progress);
            float f = (float) Math.round(currentLongStayDurationValue / 10) / 100F;
            longWaveTime.setText((new StringBuilder(" (")).append(f).append("s) ").toString());
            PrefsHelper.setCustomProxLongStay(app.getPrefEditor(), currentLongStayDurationValue);
            app.getPrefEditor().commit();
        }
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {

    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {

    }

    @Override
    public void onClick(View v) {
        app.getSensorDataHelper().setIntentBroadcastEnabled(false);
        PlaybackService.DisableAllSensorEvent event = new PlaybackService.DisableAllSensorEvent();
        EventBus.getDefault().post(event);
        IntentMapping.setCustomModeActionForEvent(app.getPrefs(), 200, app.getPrefs().getInt("custom_prox_wave",0));
        IntentMapping.setCustomModeActionForEvent(app.getPrefs(), 211, app.getPrefs().getInt("custom_prox_long", 0));
        IntentMapping.setCustomModeActionForEvent(app.getPrefs(), 222, app.getPrefs().getInt("custom_prox_double",0));
        Action action3 = IntentMapping.getActionForEvent(app, 200, 4,TomahawkApp.getContext());
        //waveSpinner.setSelection(UserMappableAction.getActionPosition(action3.id));

        Action action4 = IntentMapping.getActionForEvent(app, 222, 4,TomahawkApp.getContext());
        //doubleWaveSpinner.setSelection(UserMappableAction.getActionPosition(action4.id));

        Action action5 = IntentMapping.getActionForEvent(app, 211, 4,TomahawkApp.getContext());
        //longWaveSpinner.setSelection(UserMappableAction.getActionPosition(action5.id));
        Log.d("T1",action3.id+"_"+action4.id+"_"+action5.id);
        PrefsHelper.setCustomAccEnabled(app.getPrefEditor(), false);
        PrefsHelper.setCustomProxEnabled(app.getPrefEditor(), true);
        app.getPrefEditor().commit();
        app.getSensorModeHelper().resetPreset(4);
        app.getSensorDataHelper().setIntentBroadcastEnabled(true);
        MyIntents.selectSensorMode(app, 4, true, true);
    }
}
