package org.tomahawk.tomahawk_android.fragments;


import android.content.Context;
import android.content.SharedPreferences;
import android.hardware.SensorManager;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;
import android.widget.AdapterView;

import org.tomahawk.tomahawk_android.R;
import org.tomahawk.tomahawk_android.TomahawkApp;
import org.tomahawk.tomahawk_android.adapters.FakePreferencesAdapter;
import org.tomahawk.tomahawk_android.sensor.PrefsHelper;
import org.tomahawk.tomahawk_android.utils.FakePreferenceGroup;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by android on 24/9/15.
 */
public class ProximityFragment_  extends TomahawkListFragment
        implements AdapterView.OnItemClickListener, SharedPreferences.OnSharedPreferenceChangeListener {

private static final String TAG = PreferenceAdvancedFragment.class.getSimpleName();

public static final String FAKEPREFERENCEFRAGMENT_ID_PREFPROXIMITY = "pref_proximity";

public static final String FAKEPREFERENCEFRAGMENT_ID_PREFPROXIMITY_WAVE_OVER_SPINNER = "pref_proximity_wave_spinner";

public static final String FAKEPREFERENCEFRAGMENT_ID_PREFPROXIMITY_DOUBLE_WAVE_SPINNER = "pref_proximity_double_wave_spinner";

public static final String FAKEPREFERENCEFRAGMENT_ID_PREFPROXIMITY_DOUBLEWAVE_SEEKBAR = "pref_proximity_double_wave_seekbar";

public static final String FAKEPREFERENCEFRAGMENT_ID_PREFPROXIMITY_LONG_STAY_SPINNER = "pref_proximity_long_stay_spinner";

public static final String FAKEPREFERENCEFRAGMENT_ID_PREFPROXIMITY_LONG_STAY_SEEKBAR = "pref_proximity_long_stay_seekbar";

public static final String FAKEPREFERENCEFRAGMENT_ID_PREFPROXIMITY_WAVE_BUTTON = "pref_proximity_wave_btn";

public static final String FAKEPREFERENCEFRAGMENT_KEY_PREFPROXIMITY
        = "org.tomahawk.tomahawk_android.pref_proximity";

public static final String FAKEPREFERENCEFRAGMENT_KEY_PREFPROXIMITY_WAVE_OVER_SPINNER
        = "org.tomahawk.tomahawk_android.pref_proximity_wave_spinner";

public static final String FAKEPREFERENCEFRAGMENT_KEY_PREFPROXIMITY_DOUBLE_WAVE_SPINNER
        = "org.tomahawk.tomahawk_android.pref_proximity_double_wave_spinner";

public static final String FAKEPREFERENCEFRAGMENT_KEY_PREFPROXIMITY_DOUBLEWAVE_SEEKBAR
        = "org.tomahawk.tomahawk_android.pref_proximity_double_wave_seekbar";

public static final String FAKEPREFERENCEFRAGMENT_KEY_PREFPROXIMITY_LONG_STAY_SPINNER
        = "org.tomahawk.tomahawk_android.pref_proximity_long_stay_spinner";

public static final String FAKEPREFERENCEFRAGMENT_KEY_PREFPROXIMITY_LONG_STAY_SEEKBAR
        = "org.tomahawk.tomahawk_android.pref_proximity_long_stay_seekbar";

public static final String FAKEPREFERENCEFRAGMENT_KEY_PREFPROXIMITY_WAVE_BUTTON
        = "org.tomahawk.tomahawk_android.pref_proximity_wave_btn";





private SharedPreferences mSharedPreferences;

private SensorManager mSensorEventManager;

/**
 * Called, when this {@link PreferenceAdvancedFragment}'s {@link android.view.View} has been
 * created
 */
@Override
public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        // Fetch our SharedPreferences from the PreferenceManager
        mSharedPreferences = PreferenceManager.getDefaultSharedPreferences(getActivity());
        mSharedPreferences.registerOnSharedPreferenceChangeListener(this);

        mSensorEventManager = (SensorManager)getActivity().getSystemService(Context.SENSOR_SERVICE);



        // Set up the set of FakePreferences to be shown in this Fragment
        List<FakePreferenceGroup> fakePreferenceGroups = new ArrayList<>();
        FakePreferenceGroup prefGroup = new FakePreferenceGroup(
        getString(R.string.preferences_playback));
        android.content.SharedPreferences.Editor editor = mSharedPreferences.edit();
        PrefsHelper.updateSensorAvailability(editor, mSensorEventManager);
        if (!PrefsHelper.isProxSensorAvailable(mSharedPreferences))
        {
        prefGroup.addFakePreference(new FakePreferenceGroup.FakePreference(
        FakePreferenceGroup.FAKEPREFERENCE_TYPE_PLAIN,
        FAKEPREFERENCEFRAGMENT_ID_PREFPROXIMITY,
        FAKEPREFERENCEFRAGMENT_KEY_PREFPROXIMITY,
        getString(R.string.proximity),
        getString(R.string.proximity_not_available), false));
        }else {
        PrefsHelper.setCustomProxEnabled(mSharedPreferences.edit(), true);
            PrefsHelper.setCustomAccEnabled(mSharedPreferences.edit(), false);
        mSharedPreferences.edit().commit();
        prefGroup.addFakePreference(new FakePreferenceGroup.FakePreference(
        FakePreferenceGroup.FAKEPREFERENCE_TYPE_PLAIN,
        FAKEPREFERENCEFRAGMENT_ID_PREFPROXIMITY,
        FAKEPREFERENCEFRAGMENT_KEY_PREFPROXIMITY,
        getString(R.string.proximity),
        getString(R.string.preferences_customize_line1), false));

        prefGroup.addFakePreference(new FakePreferenceGroup.FakePreference(
        FakePreferenceGroup.FAKEPREFERENCE_TYPE_SPINNER,
        FAKEPREFERENCEFRAGMENT_ID_PREFPROXIMITY_WAVE_OVER_SPINNER,
        FAKEPREFERENCEFRAGMENT_KEY_PREFPROXIMITY_WAVE_OVER_SPINNER,
        getString(R.string.preferences_proximity),
        getString(R.string.wave_over), false));


        prefGroup.addFakePreference(new FakePreferenceGroup.FakePreference(
        FakePreferenceGroup.FAKEPREFERENCE_TYPE_SPINNER,
        FAKEPREFERENCEFRAGMENT_ID_PREFPROXIMITY_DOUBLE_WAVE_SPINNER,
        FAKEPREFERENCEFRAGMENT_KEY_PREFPROXIMITY_DOUBLE_WAVE_SPINNER,
        "",
        getString(R.string.preferences_double_wave), false));
            prefGroup.addFakePreference(new FakePreferenceGroup.FakePreference(
                    FakePreferenceGroup.FAKEPREFERENCE_TYPE_SEEKBAR,
                    FAKEPREFERENCEFRAGMENT_ID_PREFPROXIMITY_DOUBLEWAVE_SEEKBAR,
                    FAKEPREFERENCEFRAGMENT_KEY_PREFPROXIMITY_DOUBLEWAVE_SEEKBAR,
                    "",
                    getString(R.string.preferences_double_wave_action_line1),true));


        prefGroup.addFakePreference(new FakePreferenceGroup.FakePreference(
        FakePreferenceGroup.FAKEPREFERENCE_TYPE_SPINNER,
        FAKEPREFERENCEFRAGMENT_ID_PREFPROXIMITY_LONG_STAY_SPINNER,
        FAKEPREFERENCEFRAGMENT_KEY_PREFPROXIMITY_LONG_STAY_SPINNER,
        "",
        getString(R.string.preferences_long_wave), false));

            prefGroup.addFakePreference(new FakePreferenceGroup.FakePreference(
                    FakePreferenceGroup.FAKEPREFERENCE_TYPE_SEEKBAR,
                    FAKEPREFERENCEFRAGMENT_ID_PREFPROXIMITY_LONG_STAY_SEEKBAR,
                    FAKEPREFERENCEFRAGMENT_KEY_PREFPROXIMITY_LONG_STAY_SEEKBAR,
                    "",
                    getString(R.string.preferences_long_wave_action_line1),false));

        prefGroup.addFakePreference(new FakePreferenceGroup.FakePreference(
        FakePreferenceGroup.FAKEPREFERENCE_TYPE_BUTTON,
        FAKEPREFERENCEFRAGMENT_ID_PREFPROXIMITY_WAVE_BUTTON,
        FAKEPREFERENCEFRAGMENT_KEY_PREFPROXIMITY_WAVE_BUTTON,
        getString(R.string.wave_apply),
        "", false));
        }
        fakePreferenceGroups.add(prefGroup);
final TomahawkApp app = (TomahawkApp)getActivity().getApplication();
        // Now we can push the complete set of FakePreferences into our FakePreferencesAdapter,
        // so that it can provide our ListView with the correct Views.
        FakePreferencesAdapter fakePreferencesAdapter = new FakePreferencesAdapter(getActivity(),
        getActivity().getLayoutInflater(), fakePreferenceGroups,app);
        setListAdapter(fakePreferencesAdapter);

        getListView().setOnItemClickListener(this);
        setupNonScrollableSpacer(getListView());
        }

/**
 * Initialize
 */
@Override
public void onResume() {
        super.onResume();

        getListAdapter().notifyDataSetChanged();
        }

/**
 * Called every time an item inside the {@link se.emilsjolander.stickylistheaders.StickyListHeadersListView}
 * is clicked
 *
 * @param parent   The AdapterView where the click happened.
 * @param view     The view within the AdapterView that was clicked (this will be a view
 *                 provided by the adapter)
 * @param position The position of the view in the adapter.
 * @param id       The row id of the item that was clicked.
 */
@Override
public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

        }

@Override
public void onSharedPreferenceChanged(SharedPreferences sharedPreferences, String key) {
        getListAdapter().notifyDataSetChanged();
        }
}
