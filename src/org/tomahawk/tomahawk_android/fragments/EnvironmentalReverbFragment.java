package org.tomahawk.tomahawk_android.fragments;

import android.os.Bundle;
import android.support.v7.widget.SwitchCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CompoundButton;
import android.widget.SeekBar;
import android.widget.Spinner;

import org.tomahawk.tomahawk_android.R;
import org.tomahawk.tomahawk_android.events.EventDefs;
import org.tomahawk.tomahawk_android.framework.AppEvent;
import org.tomahawk.tomahawk_android.mediaplayers.StandardMediaPlayer;
import org.tomahawk.tomahawk_android.model.BaseAudioEffectStateStore;
import org.tomahawk.tomahawk_android.model.EnvironmentalReverbStateStore;

import java.util.ArrayList;

import de.greenrobot.event.EventBus;

/**
 * Created by android on 20/10/15.
 */
public class EnvironmentalReverbFragment extends ContentHeaderFragment implements AdapterView.OnItemSelectedListener,
        SeekBar.OnSeekBarChangeListener {

    // constants
    static final int SEEKBAR_MAX = 10000;


    private SeekBar mSeekBarDecayHFRatio;
    private SeekBar mSeekBarDecayTime;
    private SeekBar mSeekBarDensity;
    private SeekBar mSeekBarDiffusion;
    private SeekBar mSeekBarReflectionsDelay;
    private SeekBar mSeekBarReflectionsLevel;
    private SeekBar mSeekBarReverbDelay;
    private SeekBar mSeekBarReverbLevel;
    private SeekBar mSeekBarRoomHFLevel;
    private SeekBar mSeekBarRoomLevel;
    private Spinner mSpinnerPreset;
    private ArrayList<SeekBar> mParamSeekBars;
    private int mActiveTrackingSeekBarIndex = -1;
    private SwitchCompat mEnvironmentalReverbState;



    protected boolean onGetActionBarSwitchCheckedState() {
        return StandardMediaPlayer.get().getEnvironmentalReverbStateStore().isEnabled();
    }


    protected void onActionBarSwitchCheckedChanged(
            CompoundButton buttonView, boolean isChecked) {
        postAppEvent(EventDefs.EnvironmentalReverbReqEvents.SET_ENABLED, (isChecked ? 1 : 0), 0);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_environment_reverb, container, false);
        return rootView;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        obtainViewReferences(view);
        /*FrameLayout.LayoutParams params = (FrameLayout.LayoutParams) view.getLayoutParams();
        params.topMargin = getResources().getDimensionPixelSize(R.dimen.header_clear_space_nonscrollable)+getResources().getDimensionPixelSize(R.dimen.pager_indicator_height);
       */ setupViews();
        //showContentHeader(getView());
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    public void onEvent(AppEvent event){
        switch (event.category) {
            case EventDefs.Category.NOTIFY_ENVIRONMENTAL_REVERB:
                onReceiveEnvironmentalReverbNotifyEvents(event);
                break;
        }
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();

        mSeekBarDecayHFRatio = null;
        mSeekBarDecayTime = null;
        mSeekBarDensity = null;
        mSeekBarDiffusion = null;
        mSeekBarReflectionsDelay = null;
        mSeekBarReflectionsLevel = null;
        mSeekBarReverbDelay = null;
        mSeekBarReverbLevel = null;
        mSeekBarRoomHFLevel = null;
        mSeekBarRoomLevel = null;
        mSpinnerPreset = null;

        mParamSeekBars.clear();
        mParamSeekBars = null;
    }

    @Override
    public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
        switch (parent.getId()) {
            case R.id.spinner_environment_reverb_preset:
                postAppEvent(
                        EventDefs.EnvironmentalReverbReqEvents.SET_PRESET,
                        (position - 1), 0);
                break;
        }
    }

    @Override
    public void onNothingSelected(AdapterView<?> parent) {
    }

    @Override
    public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
        if (fromUser) {
            int paramIndex = mParamSeekBars.indexOf(seekBar);

            postAppEvent(
                    EventDefs.EnvironmentalReverbReqEvents.SET_PARAMETER,
                    paramIndex, seekBarProgressToFloat(progress));
        }
    }

    @Override
    public void onStartTrackingTouch(SeekBar seekBar) {
        mActiveTrackingSeekBarIndex = mParamSeekBars.indexOf(seekBar);
    }

    @Override
    public void onStopTrackingTouch(SeekBar seekBar) {
        mActiveTrackingSeekBarIndex = -1;
    }


    private void onReceiveEnvironmentalReverbNotifyEvents(AppEvent event) {
        switch (event.event) {
            case EventDefs.EnvironmentalReverbNotifyEvents.PRESET_UPDATED:
                // preset updated
                if (mSpinnerPreset != null) {
                    updatePresetSpinner();
                }
                break;
            case EventDefs.EnvironmentalReverbNotifyEvents.PARAMETER_UPDATED: {
                // parameter updated
                final int paramIndex = event.arg1;
                final boolean nowTracking = (mActiveTrackingSeekBarIndex == paramIndex);

                if (mParamSeekBars != null && !nowTracking) {
                    final EnvironmentalReverbStateStore state = getEnvironmentalReverbState();

                    if (paramIndex == EventDefs.EnvironmentalReverbReqEvents.PARAM_INDEX_ALL) {
                        for (int i = 0; i < mParamSeekBars.size(); i++) {
                            final float value = state.getNormalizedParameter(i);
                            mParamSeekBars.get(i).setProgress(floatToSeekbarProgress(value));
                        }
                    } else {
                        final float value = state.getNormalizedParameter(paramIndex);
                        mParamSeekBars.get(paramIndex).setProgress(
                                floatToSeekbarProgress(value));
                    }
                }
            }
            break;
        }
    }

    private void obtainViewReferences(View view) {
        mSeekBarDecayHFRatio =
                (SeekBar)view.findViewById(R.id.seekbar_environment_reverb_decay_hf_ratio);
        mSeekBarDecayTime =
                (SeekBar)view.findViewById(R.id.seekbar_environment_reverb_decay_time);
        mSeekBarDensity =
                (SeekBar)view.findViewById(R.id.seekbar_environment_reverb_density);
        mSeekBarDiffusion =
                (SeekBar)view.findViewById(R.id.seekbar_environment_reverb_diffusion);
        mSeekBarReflectionsDelay =
                (SeekBar)view.findViewById(R.id.seekbar_environment_reverb_reflections_delay);
        mSeekBarReflectionsLevel =
                (SeekBar)view.findViewById(R.id.seekbar_environment_reverb_reflections_level);
        mSeekBarReverbDelay =
                (SeekBar)view.findViewById(R.id.seekbar_environment_reverb_reverb_delay);
        mSeekBarReverbLevel =
                (SeekBar)view.findViewById(R.id.seekbar_environment_reverb_reverb_level);
        mSeekBarRoomHFLevel =
                (SeekBar)view.findViewById(R.id.seekbar_environment_reverb_room_hf_level);
        mSeekBarRoomLevel =
                (SeekBar)view.findViewById(R.id.seekbar_environment_reverb_room_level);
        mEnvironmentalReverbState = (SwitchCompat)view.findViewById(R.id.environmentalReverbState);

        mSpinnerPreset = (Spinner)view.findViewById(R.id.spinner_environment_reverb_preset);
        mSeekBarDecayHFRatio.setOnSeekBarChangeListener(this);
        mSeekBarDecayTime.setOnSeekBarChangeListener(this);
        mSeekBarDensity.setOnSeekBarChangeListener(this);
        mSeekBarDiffusion.setOnSeekBarChangeListener(this);
        mSeekBarReflectionsDelay.setOnSeekBarChangeListener(this);
        mSeekBarReflectionsLevel.setOnSeekBarChangeListener(this);
        mSeekBarReverbDelay.setOnSeekBarChangeListener(this);
        mSeekBarReverbLevel.setOnSeekBarChangeListener(this);
        mSeekBarRoomHFLevel.setOnSeekBarChangeListener(this);
        mSeekBarRoomLevel.setOnSeekBarChangeListener(this);
        mSpinnerPreset.setOnItemSelectedListener(this);
        mEnvironmentalReverbState.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {


                postAppEvent(EventDefs.EnvironmentalReverbReqEvents.SET_ENABLED, (isChecked ? 1 : 0), 0);

            }
        });

    }

    private BaseAudioEffectStateStore getAudioEfectStateStore(int index) {
        switch (index) {
            case EventDefs.NavigationDrawerReqEvents.SECTION_INDEX_BASSBOOST:
                return StandardMediaPlayer.get().getBassBoostStateStore();
            case EventDefs.NavigationDrawerReqEvents.SECTION_INDEX_VIRTUALIZER:
                return StandardMediaPlayer.get().getVirtualizerStateStore();
            case EventDefs.NavigationDrawerReqEvents.SECTION_INDEX_EQUALIZER:
                return StandardMediaPlayer.get().getEqualizerStateStore();
            case EventDefs.NavigationDrawerReqEvents.SECTION_INDEX_LOUDNESS_ENHANCER:
                return StandardMediaPlayer.get().getLoudnessEnhancerStateStore();
            case EventDefs.NavigationDrawerReqEvents.SECTION_INDEX_PRESET_REVERB:
                return StandardMediaPlayer.get().getPresetReverbStateStore();
            case EventDefs.NavigationDrawerReqEvents.SECTION_INDEX_ENVIRONMENTAL_REVERB:
                return StandardMediaPlayer.get().getEnvironmentalReverbStateStore();
            case EventDefs.NavigationDrawerReqEvents.SECTION_INDEX_VISUALIZER:
                return StandardMediaPlayer.get().getVisualizerStateStore();
            case EventDefs.NavigationDrawerReqEvents.SECTION_INDEX_HQ_EQUALIZER:
                return StandardMediaPlayer.get().getHQEqualizerStateStore();
            case EventDefs.NavigationDrawerReqEvents.SECTION_INDEX_HQ_VISUALIZER:
                return StandardMediaPlayer.get().getHQVisualizerStateStore();
            default:
                throw new IllegalArgumentException();
        }
    }

    private void setupViews() {
        mParamSeekBars = new ArrayList<SeekBar>();

        mParamSeekBars.add(mSeekBarDecayHFRatio);
        mParamSeekBars.add(mSeekBarDecayTime);
        mParamSeekBars.add(mSeekBarDensity);
        mParamSeekBars.add(mSeekBarDiffusion);
        mParamSeekBars.add(mSeekBarReflectionsDelay);
        mParamSeekBars.add(mSeekBarReflectionsLevel);
        mParamSeekBars.add(mSeekBarReverbDelay);
        mParamSeekBars.add(mSeekBarReverbLevel);
        mParamSeekBars.add(mSeekBarRoomHFLevel);
        mParamSeekBars.add(mSeekBarRoomLevel);
        mEnvironmentalReverbState.setChecked(getAudioEfectStateStore(EventDefs.NavigationDrawerReqEvents.SECTION_INDEX_ENVIRONMENTAL_REVERB).isEnabled());

        for (SeekBar seekbar : mParamSeekBars) {
            seekbar.setMax(SEEKBAR_MAX);
        }

        // disable unsupported parameter seekbars
        // (these parameters are not supported in current Android)
        mSeekBarReflectionsLevel.setEnabled(false);
        mSeekBarReflectionsDelay.setEnabled(false);
        mSeekBarReverbDelay.setEnabled(false);

        {
            final ArrayAdapter<CharSequence> adapter =
                    ArrayAdapter.createFromResource(
                            getActivity(), R.array.aux_env_reverb_preset_names,
                            android.R.layout.simple_spinner_item);

            adapter.setDropDownViewResource(
                    android.R.layout.simple_spinner_dropdown_item);
            mSpinnerPreset.setAdapter(adapter);
        }

        updatePresetSpinner();
    }

    private void updatePresetSpinner() {
        mSpinnerPreset.setSelection(getEnvironmentalReverbState().getPreset() + 1);
    }

    private EnvironmentalReverbStateStore getEnvironmentalReverbState() {
        return StandardMediaPlayer.get().getEnvironmentalReverbStateStore();
    }

    private void postAppEvent(int event, int arg1, int arg2) {
        EventBus.getDefault().post(new AppEvent(
                EventDefs.Category.ENVIRONMENTAL_REVERB, event, arg1, arg2));
    }

    private void postAppEvent(int event, int arg1, float arg2) {
        EventBus.getDefault().post(new AppEvent(
                EventDefs.Category.ENVIRONMENTAL_REVERB, event, arg1, arg2));
    }

    private static float seekBarProgressToFloat(int progress) {
        return progress * (1.0f / SEEKBAR_MAX);
    }

    private static int floatToSeekbarProgress(float value) {
        return (int) (value * SEEKBAR_MAX);
    }
}