package org.tomahawk.tomahawk_android.fragments;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.View;

import org.jdeferred.DoneCallback;
import org.tomahawk.libtomahawk.collection.Album;
import org.tomahawk.libtomahawk.collection.AlphaComparator;
import org.tomahawk.libtomahawk.collection.ArtistAlphaComparator;
import org.tomahawk.libtomahawk.collection.Collection;
import org.tomahawk.libtomahawk.collection.CollectionCursor;
import org.tomahawk.libtomahawk.collection.CollectionManager;
import org.tomahawk.libtomahawk.collection.Folder;
import org.tomahawk.libtomahawk.collection.LastModifiedComparator;
import org.tomahawk.libtomahawk.collection.Playlist;
import org.tomahawk.libtomahawk.collection.PlaylistEntry;
import org.tomahawk.libtomahawk.collection.UserCollection;
import org.tomahawk.tomahawk_android.R;
import org.tomahawk.tomahawk_android.TomahawkApp;
import org.tomahawk.tomahawk_android.activities.TomahawkMainActivity;
import org.tomahawk.tomahawk_android.adapters.Segment;
import org.tomahawk.tomahawk_android.services.PlaybackService;
import org.tomahawk.tomahawk_android.utils.FragmentUtils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


/**
 * Created by YGS on 9/5/2015.
 */
public class FoldersFragment extends TomahawkFragment {

    public static final String COLLECTION_FOLDER_SPINNER_POSITION
            = "sensitivemusicplayer.smartfoxsoft.com.sense.collection_folders_spinner_position";
    public static final String COLLECTION_FOLDER_SHOW_TYPE = "org.tomahawk.tomahawk_android.collection_folders_show_type";
    public static final String COLLECTION_FOLDER_EDIT = "org.tomahawk.tomahawk_android.collection_folder_edit";

    public static final String COLLECTION_FOLDER = "org.tomahawk.tomahawk_android.collection_folder";
    public static final int SHOW_MODE_STARREDARTISTS = 1;

    @Override
    public void onResume() {
        super.onResume();
        if (mContainerFragmentClass == null) {
            getActivity().setTitle("");
        }
        updateAdapter();
    }

    @Override
    public void onItemClick(View view, final Object item) {
        final TomahawkMainActivity activity = (TomahawkMainActivity) getActivity();
        if (item instanceof PlaylistEntry) {
            final PlaylistEntry entry = (PlaylistEntry) item;
            if (entry.getQuery().isPlayable()) {
                final PlaybackService playbackService = activity.getPlaybackService();
                if (playbackService != null) {
                    if (playbackService.getCurrentEntry() == entry) {
                        playbackService.playPause();
                    }
                }
            }
        } else if (item instanceof Folder) {
            Folder album = (Folder) item;
            mCollection.getFolderTracks(album).done(new DoneCallback<Playlist>() {
                @Override
                public void onDone(Playlist playlist) {
                    Bundle bundle = new Bundle();
                    bundle.putString(TomahawkFragment.FOLDER, ((Folder) item).getCacheKey());
                    if (playlist != null) {
                        bundle.putString(TomahawkFragment.COLLECTION_ID, mCollection.getId());
                    } else {
                        bundle.putString(
                                TomahawkFragment.COLLECTION_ID, TomahawkApp.PLUGINNAME_HATCHET);
                    }
                    bundle.putInt(CONTENT_HEADER_MODE,
                            ContentHeaderFragment.MODE_HEADER_DYNAMIC);
                    FragmentUtils.replace((TomahawkMainActivity) getActivity(),
                            PlaylistEntriesFragment.class, bundle);
                }
            });
        }
    }

    @Override
    protected void updateAdapter() {
        if (!mIsResumed) {
            return;
        }
        final SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(TomahawkApp.getContext());

        if (mArtist != null) {
            if (!TomahawkApp.PLUGINNAME_HATCHET.equals(mCollection.getId())) {
                mCollection.getArtistAlbums(mArtist)
                        .done(new DoneCallback<CollectionCursor<Album>>() {
                            @Override
                            public void onDone(CollectionCursor<Album> cursor) {
                                Segment segment = new Segment.Builder(cursor)
                                        .headerLayout(R.layout.single_line_list_header)
                                        .headerString(mCollection.getName() + " "
                                                + getString(R.string.albums))
                                        .showAsGrid(COLLECTION_FOLDER_SHOW_TYPE,R.integer.grid_column_count,
                                                R.dimen.padding_superlarge,
                                                R.dimen.padding_superlarge)
                                        .build();
                                fillAdapter(segment, mCollection);
                            }
                        });
            } else {

            }
        }
        else  if (mGenre != null){
            if (!TomahawkApp.PLUGINNAME_HATCHET.equals(mCollection.getId())) {
                mCollection.getGenreAlbums(mGenre)
                        .done(new DoneCallback<CollectionCursor<Album>>() {
                            @Override
                            public void onDone(CollectionCursor<Album> cursor) {
                                Segment segment = new Segment.Builder(cursor)
                                        .headerLayout(R.layout.single_line_list_header)
                                        .headerString(mCollection.getName() + " "
                                                + getString(R.string.albums))
                                        .showAsGrid(COLLECTION_FOLDER_SHOW_TYPE,R.integer.grid_column_count,
                                                R.dimen.padding_superlarge,
                                                R.dimen.padding_superlarge)
                                        .build();
                                fillAdapter(segment, mCollection);
                            }
                        });
            }
        }
        else if (mAlbumArray != null) {
            fillAdapter(new Segment.Builder(mAlbumArray).build());
        }else if (mGenreArray != null){
            fillAdapter(new Segment.Builder(mGenreArray).build());
        }
        else if (mUser != null) {
            Segment segment = new Segment.Builder(sortAlbums(mUser.getStarredAlbums()))
                    .headerLayout(R.layout.single_line_list_header)
                    .headerStrings(constructDropdownItems())
                    .spinner(constructDropdownListener(COLLECTION_FOLDER_SPINNER_POSITION),
                            getDropdownPos(COLLECTION_FOLDER_SPINNER_POSITION))
                    .showAsGrid(COLLECTION_FOLDER_SHOW_TYPE,R.integer.grid_column_count,
                            R.dimen.padding_superlarge,
                            R.dimen.padding_superlarge)
                    .build();
            fillAdapter(segment);
        } else {
            final List<Folder> starredAlbums = null;
            if (mCollection.getId().equals(TomahawkApp.PLUGINNAME_USERCOLLECTION)) {
               // starredAlbums = DatabaseHelper.get().getStarredAlbums();
            } else {
             //   starredAlbums = null;
            }

            mCollection.getFolders(getSortMode(),preferences.getString(COLLECTION_FOLDER_EDIT,"")).done(new DoneCallback<CollectionCursor<Folder>>() {
                @Override
                public void onDone(final CollectionCursor<Folder> cursor) {
                    new Thread(new Runnable() {
                        @Override
                        public void run() {
                            if (starredAlbums != null) {
                                cursor.mergeItems(getSortMode(), starredAlbums);
                            }
                            Segment segment;
                            if (preferences.getString(COLLECTION_FOLDER_SHOW_TYPE,"grid").equals("grid")) {
                                 segment = new Segment.Builder(cursor)
                                        .headerLayout(R.layout.dropdown_header)
                                         .headerStrings(constructDropdownItems())
                                         .showType(COLLECTION_FOLDER_SHOW_TYPE,selectShowTypeListener(COLLECTION_FOLDER_SHOW_TYPE), addTextListener(COLLECTION_FOLDER_EDIT), addEditorActionListener(),false)
                                         .spinner(constructDropdownListener(
                                                         COLLECTION_FOLDER_SPINNER_POSITION),
                                                 getDropdownPos(COLLECTION_FOLDER_SPINNER_POSITION))
                                        .showAsGrid(COLLECTION_FOLDER_SHOW_TYPE, R.integer.grid_column_count,
                                                R.dimen.padding_superlarge,
                                                R.dimen.padding_superlarge)
                                        .build();
                            }else {
                                segment = new Segment.Builder(cursor)
                                        .headerLayout(R.layout.dropdown_header)//single_line_list_header)
                                        .headerStrings(constructDropdownItems())
                                        .showType(COLLECTION_FOLDER_SHOW_TYPE, selectShowTypeListener(COLLECTION_FOLDER_SHOW_TYPE),addTextListener(COLLECTION_FOLDER_EDIT),addEditorActionListener(),false)
                                        .spinner(constructDropdownListener(
                                                        COLLECTION_FOLDER_SPINNER_POSITION),
                                                getDropdownPos(COLLECTION_FOLDER_SPINNER_POSITION))

                                        .build();
                            }
                            fillAdapter(segment, mCollection);
                        }
                    }).start();
                }
            });
        }
    }

    private List<Integer> constructDropdownItems() {
        List<Integer> dropDownItems = new ArrayList<>();
        dropDownItems.add(R.string.collection_dropdown_recently_added);
        dropDownItems.add(R.string.collection_dropdown_alpha);
        //dropDownItems.add(R.string.collection_dropdown_alpha_artists);
        return dropDownItems;
    }

    private int getSortMode() {
        switch (getDropdownPos(COLLECTION_FOLDER_SPINNER_POSITION)) {
            case 0:
                return Collection.SORT_LAST_MODIFIED;
            case 1:
                return Collection.SORT_ALPHA;
            case 2:
                return Collection.SORT_ARTIST_ALPHA;
            default:
                return Collection.SORT_NOT;
        }
    }

    private List<Album> sortAlbums(java.util.Collection<Album> albums) {
        List<Album> sortedAlbums;
        if (albums instanceof List) {
            sortedAlbums = (List<Album>) albums;
        } else {
            sortedAlbums = new ArrayList<>(albums);
        }
        switch (getDropdownPos(COLLECTION_FOLDER_SPINNER_POSITION)) {
            case 0:
                UserCollection userColl = (UserCollection) CollectionManager.get()
                        .getCollection(TomahawkApp.PLUGINNAME_USERCOLLECTION);
                Collections.sort(sortedAlbums,
                        new LastModifiedComparator<>(userColl.getAlbumTimeStamps()));
                break;
            case 1:
                Collections.sort(sortedAlbums, new AlphaComparator());
                break;
            case 2:
                Collections.sort(sortedAlbums, new ArtistAlphaComparator());
                break;
        }
        return sortedAlbums;
    }
    /*   @Override
    protected void updateAdapter() {
        if (!mIsResumed)
            return;
        LayoutInflater layoutInflater = getActivity().getLayoutInflater();
        List folders = new ArrayList<>();
        if (mShowMode == SHOW_MODE_STARREDARTISTS) {
            ArrayList<Artist> starredArtists = DatabaseHelper.getInstance().getStarredArtists();
            for (Artist artist : starredArtists) {
                ArrayList<String> requestIds = InfoSystem.getInstance().resolve(artist, false);
                for (String requestId : requestIds) {
                    mCorrespondingRequestIds.add(requestId);
                }
            }
            folders.addAll(starredArtists);
            if (getListAdapter() == null) {
                TomahawkListAdapter tomahawkListAdapter =
                        new TomahawkListAdapter((SensitiveMainActivity) getActivity(),
                                layoutInflater, new Segment(folders), getListView(), this);
                setListAdapter(tomahawkListAdapter);
            } else {
                getListAdapter().setSegments(new Segment(folders), getListView());
            }
        } else if (mArtistArray != null) {
            folders.addAll(mArtistArray);
            if (getListAdapter() == null) {
                TomahawkListAdapter tomahawkListAdapter =
                        new TomahawkListAdapter((SensitiveMainActivity) getActivity(),
                                layoutInflater, new Segment(folders), getListView(), this);
                setListAdapter(tomahawkListAdapter);
            } else {
                getListAdapter().setSegments(new Segment(folders), getListView());
            }
        } else {
            folders.addAll(CollectionManager.getInstance()
                    .getCollection(SensitiveApp.PLUGINNAME_USERCOLLECTION).getFolders());
            *//*for (Artist artist : DatabaseHelper.getInstance().getStarredArtists()) {
                if (!folders.contains(artist)) {
                    folders.add(artist);
                }
            }*//*
            SharedPreferences preferences =
                    PreferenceManager.getDefaultSharedPreferences(SensitiveApp.getContext());
            List<Integer> dropDownItems = new ArrayList<>();
            dropDownItems.add(R.string.collection_dropdown_recently_added);
            dropDownItems.add(R.string.collection_dropdown_alpha);
            AdapterView.OnItemSelectedListener spinnerClickListener
                    = new AdapterView.OnItemSelectedListener() {
                @Override
                public void onItemSelected(AdapterView<?> parent, View view, int position,
                                           long id) {
                    SharedPreferences preferences =
                            PreferenceManager.getDefaultSharedPreferences(SensitiveApp.getContext());
                    int initialPos = preferences.getInt(COLLECTION_ARTISTS_SPINNER_POSITION, 0);
                    if (initialPos != position) {
                        preferences.edit().putInt(COLLECTION_ARTISTS_SPINNER_POSITION, position)
                                .commit();
                        updateAdapter();
                    }
                }

                @Override
                public void onNothingSelected(AdapterView<?> parent) {
                }
            };
            int initialPos = preferences.getInt(COLLECTION_ARTISTS_SPINNER_POSITION, 0);
            if (initialPos == 0) {
                Collection userColl = CollectionManager.getInstance().getCollection(
                        SensitiveApp.PLUGINNAME_USERCOLLECTION);
                Collections.sort(folders, new TomahawkListItemComparator(
                        TomahawkListItemComparator.COMPARE_RECENTLY_ADDED,
                        userColl.getArtistAddedTimeStamps()));
            } else if (initialPos == 1) {
                Collections.sort(folders, new TomahawkListItemComparator(
                        TomahawkListItemComparator.COMPARE_ALPHA));
            }
            List<Segment> segments = new ArrayList<>();
            segments.add(new Segment(initialPos, dropDownItems, spinnerClickListener, folders,
                    R.integer.grid_column_count, R.dimen.padding_superlarge,
                    R.dimen.padding_superlarge));
            if (getListAdapter() == null) {
                TomahawkListAdapter tomahawkListAdapter =
                        new TomahawkListAdapter((SensitiveMainActivity) getActivity(),
                                layoutInflater, segments, getListView(), this);
                setListAdapter(tomahawkListAdapter);
            } else {
                getListAdapter().setSegments(segments, getListView());
            }
        }

        onUpdateAdapterFinished();

    }

    @Override
    public void onResume() {
        super.onResume();
        if (getArguments() != null) {
            if (getArguments().containsKey(SHOW_MODE)) {
                mShowMode = getArguments().getInt(SHOW_MODE);
            }
        }
        updateAdapter();
    }

    @Override
    public void onItemClick(View view, Object item) {
        SensitiveMainActivity activity = (SensitiveMainActivity) getActivity();
        if (item instanceof Query) {
            Query query = ((Query) item);
            if (query.isPlayable()) {
                PlaybackService playbackService = activity.getPlaybackService();
                if (playbackService != null
                        && playbackService.getCurrentQuery() == query) {
                    playbackService.playPause();
                } else {
                    Playlist playlist = Playlist.fromQueryList(
                            SensitiveMainActivity.getLifetimeUniqueStringId(), mShownQueries);
                    if (playbackService != null) {
                        playbackService.setPlaylist(playlist, playlist.getEntryWithQuery(query));
                        Class clss = mContainerFragmentClass != null ? mContainerFragmentClass
                                : ((Object) this).getClass();
                        playbackService.setReturnFragment(clss, getArguments());
                        playbackService.start();
                    }
                }
            }
        } else if (item instanceof Folder) {
            Bundle bundle = new Bundle();
            bundle.putString(TomahawkFragment.FOLDER, ((Folder) item).getCacheKey());
            if (mCollection != null
                    && ( mCollection.getFolderTracks((Folder) item, false).size() > 0)) {
                //mCollection instanceof ScriptResolverCollection
                //||
                bundle.putString(TomahawkFragment.COLLECTION_ID, mCollection.getId());
            }
            bundle.putInt(CONTENT_HEADER_MODE,
                    ContentHeaderFragment.MODE_HEADER_DYNAMIC);
            FragmentUtils
                    .replace((SensitiveMainActivity) getActivity(), TracksFragment.class, bundle);
        }
    }*/
}
