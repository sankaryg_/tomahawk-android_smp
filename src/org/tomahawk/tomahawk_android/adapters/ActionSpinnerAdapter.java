package org.tomahawk.tomahawk_android.adapters;

import android.content.Context;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import org.tomahawk.tomahawk_android.sensor.Action;

public class ActionSpinnerAdapter extends ArrayAdapter<Action>
{

    private Action actions[];
	private Context mContext;

    public ActionSpinnerAdapter(Context context, int i, Action aaction[])
    {
        super(context, i, aaction);
        this.mContext = context;
        actions = new Action[10];
        actions = aaction;
    }

   public View getDropDownView(int i, View view, ViewGroup viewgroup)
    {
        TextView textview = (TextView)view;
        if (textview == null)
        {
            textview = (TextView)((LayoutInflater)getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(android.R.layout.simple_spinner_item, null);
            textview.setHeight(Math.round(TypedValue.applyDimension(1, 50F, getContext().getResources().getDisplayMetrics())));
        }
        Action action = actions[i];
        if (action != null)
        {
            textview.setText(mContext.getString(action.labelID));
        }
        return textview;
    }

    public Action getItemData(int i)
    {
        return actions[i];
    }

   /* public volatile Object getItem(int i)
    {
        return getItem(i);
    }*/
    
    @Override
    public Action getItem(int position) {
    	// TODO Auto-generated method stub
    	return getItemData(position);//super.getItem(position);
    }

    public View getView(int i, View view, ViewGroup viewgroup)
    {
        TextView textview = (TextView)view;
        if (textview == null)
        {
            textview = (TextView)((LayoutInflater)getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE)).inflate(android.R.layout.simple_spinner_item, null);
        }
        Action action = actions[i];
        if (action != null)
        {
            textview.setText(mContext.getString(action.labelID));
            textview.setTag(Integer.valueOf(action.id));
        }
        return textview;
    }
}
