package org.tomahawk.tomahawk_android.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import org.tomahawk.libtomahawk.infosystem.SensorItem;
import org.tomahawk.tomahawk_android.R;

import java.util.List;

/**
 * Created by YGS on 9/24/2015.
 */
public class SpinnerAdapter extends ArrayAdapter<SensorItem> {

    // CUSTOM SPINNER ADAPTER
    private Context mContext;
    private int layout;
    private List<SensorItem> list;
    public SpinnerAdapter(Context context, int textViewResourceId,
                            List<SensorItem> objects) {
        super(context, textViewResourceId, objects);

        mContext = context;
        layout = textViewResourceId;
        list = objects;
        // TODO Auto-generated constructor stub
    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        return getCustomView(position, convertView, parent);
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        // TODO Auto-generated method stub
        return getCustomView(position, convertView, parent);
    }

    public View getCustomView(int position, View convertView,ViewGroup parent) {
// TODO Auto-generated method stub
// return super.getView(position, convertView, parent);


        LayoutInflater inflater =
                ( LayoutInflater)mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);


        ViewHolder holder;
        if (convertView == null) {
            convertView = inflater.inflate(layout, null);
            holder = new ViewHolder();
            //holder.txt01 = (TextView) convertView.findViewById(R.id.spinnerHeader);
            holder.txt02 = (TextView) convertView.findViewById(R.id.spinnerItem);
             convertView.setTag(holder);

        } else {

            holder = (ViewHolder) convertView.getTag();
        }

        //holder.txt01.setText(list.get(position).getHeaderItem());
        holder.txt02.setText(list.get(position).getSpinnerItem());

        return convertView;
    }

    class ViewHolder {
        TextView txt01;
        TextView txt02;
    }



}