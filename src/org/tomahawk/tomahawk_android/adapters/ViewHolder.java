/* == This file is part of Tomahawk Player - <http://tomahawk-player.org> ===
 *
 *   Copyright 2014, Enno Gottschalk <mrmaffen@googlemail.com>
 *
 *   Tomahawk is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Tomahawk is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Tomahawk. If not, see <http://www.gnu.org/licenses/>.
 */
package org.tomahawk.tomahawk_android.adapters;

import android.content.SharedPreferences;
import android.content.res.Resources;
import android.preference.PreferenceManager;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;

import org.jdeferred.DoneCallback;
import org.tomahawk.libtomahawk.collection.Album;
import org.tomahawk.libtomahawk.collection.Artist;
import org.tomahawk.libtomahawk.collection.Collection;
import org.tomahawk.libtomahawk.collection.CollectionManager;
import org.tomahawk.libtomahawk.collection.Folder;
import org.tomahawk.libtomahawk.collection.Genre;
import org.tomahawk.libtomahawk.collection.Image;
import org.tomahawk.libtomahawk.collection.Playlist;
import org.tomahawk.libtomahawk.infosystem.User;
import org.tomahawk.libtomahawk.resolver.Query;
import org.tomahawk.libtomahawk.resolver.Resolver;
import org.tomahawk.libtomahawk.utils.ImageUtils;
import org.tomahawk.libtomahawk.utils.ViewUtils;
import org.tomahawk.tomahawk_android.R;
import org.tomahawk.tomahawk_android.TomahawkApp;
import org.tomahawk.tomahawk_android.fragments.AlbumsFragment;
import org.tomahawk.tomahawk_android.fragments.ArtistsFragment;
import org.tomahawk.tomahawk_android.fragments.FoldersFragment;
import org.tomahawk.tomahawk_android.fragments.GenreFragment;
import org.tomahawk.tomahawk_android.fragments.PlaylistEntriesFragment;
import org.tomahawk.tomahawk_android.fragments.PlaylistsFragment;
import org.tomahawk.tomahawk_android.views.PlaybackPanel;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

public class ViewHolder {

    final int mLayoutId;

    private final View mRootView;

    private final Map<Integer, View> mCachedViews = new HashMap<>();

    public ViewHolder(View rootView, int layoutId) {
        mLayoutId = layoutId;
        mRootView = rootView;
    }

    public View ensureInflation(int stubResId, int inflatedId) {
        return ViewUtils.ensureInflation(mRootView, stubResId, inflatedId);
    }

    public View findViewById(int id) {
        if (mCachedViews.containsKey(id)) {
            return mCachedViews.get(id);
        } else {
            View view = mRootView.findViewById(id);
            if (view != null) {
                mCachedViews.put(id, view);
            }
            return view;
        }
    }

    public void setMainClickListener(ClickListener listener) {
        View mainClickArea = findViewById(R.id.mainclickarea);
        mainClickArea.setOnClickListener(listener);
        mainClickArea.setOnLongClickListener(listener);
    }

    public void fillEmptyView(){

    }

    public void fillView(Query query, String numerationString, boolean showAsPlaying,
            View.OnClickListener swipeMenuButton1Listener, boolean showAsQueued,boolean isPlaying,int currentHeaderState) {
        FrameLayout frameLayout = (FrameLayout)findViewById(R.id.swipelayout);
        frameLayout.setBackgroundColor(currentHeaderState);
        TextView trackNameTextView = (TextView) findViewById(R.id.track_textview);
        trackNameTextView.setText(query.getPrettyName());
        setTextViewEnabled(trackNameTextView, query.isPlayable(), false);
        ImageView imageView2 = (ImageView) findViewById(R.id.imageview2);
        if (imageView2!=null) {
            if (query.getBasicTrack().getmLike() == 0)
                imageView2.setVisibility(View.GONE);
            else
                imageView2.setVisibility(View.VISIBLE);
        }
        ImageView resolverImageView = (ImageView) ensureInflation(R.id.resolver_imageview_stub,
                R.id.resolver_imageview);
        TextView numerationTextView = (TextView) findViewById(R.id.numeration_textview);
        if (showAsQueued) {
            if (numerationTextView != null) {
                numerationTextView.setVisibility(View.GONE);
            }
            if (resolverImageView != null) {
                resolverImageView.setVisibility(View.VISIBLE);
                ImageUtils.loadDrawableIntoImageView(TomahawkApp.getContext(), resolverImageView,
                        R.drawable.ic_action_queue_red);
            }
        } else if (showAsPlaying) {
            if (numerationTextView != null) {
                numerationTextView.setVisibility(View.GONE);
            }
            if (resolverImageView != null) {
                resolverImageView.setVisibility(View.VISIBLE);
                if (query.getPreferredTrackResult() != null) {
                    Resolver resolver = query.getPreferredTrackResult().getResolvedBy();
                    //resolver.loadIcon(resolverImageView, false);
                    if (isPlaying)
                    resolver.loadAnimatedIcon(resolverImageView);
                }
            }
        } else if (numerationString != null) {
            if (resolverImageView != null) {
                resolverImageView.setVisibility(View.GONE);
            }
            if (numerationTextView != null) {
                numerationTextView.setVisibility(View.VISIBLE);
                numerationTextView.setText(numerationString);
                setTextViewEnabled(numerationTextView, query.isPlayable(), false);
            }
        }
        if (mLayoutId == R.layout.list_item_numeration_track_artist
                || mLayoutId == R.layout.list_item_track_artist) {
            TextView artistNameTextView = (TextView) findViewById(R.id.artist_textview);
            artistNameTextView.setText(query.getArtist().getPrettyName());
            setTextViewEnabled(artistNameTextView, query.isPlayable(), false);
        }
        if (mLayoutId == R.layout.list_item_numeration_track_duration || mLayoutId == R.layout.list_item_track_artist) {
            TextView durationTextView = (TextView) findViewById(R.id.duration_textview);
            if (query.getPreferredTrack().getDuration() > 0) {
                durationTextView.setText(ViewUtils.durationToString(
                        (query.getPreferredTrack().getDuration())));
            } else {
                durationTextView.setText(PlaybackPanel.COMPLETION_STRING_DEFAULT);
            }
            setTextViewEnabled(durationTextView, query.isPlayable(), false);
        }
        ImageView swipeMenuButton;
        if (showAsQueued) {
            swipeMenuButton = (ImageView) ensureInflation(R.id.swipe_menu_button_dequeue_stub,
                    R.id.swipe_menu_button_dequeue);
            swipeMenuButton.setVisibility(View.VISIBLE);
            swipeMenuButton.setImageResource(R.drawable.ic_player_exit_light);
            ImageUtils.setTint(swipeMenuButton.getDrawable(), R.color.tomahawk_red);
            /*ImageView swipeMenuButtonEnqueue =
                    (ImageView) findViewById(R.id.swipe_menu_button_enqueue);
            if (swipeMenuButtonEnqueue != null) {
                swipeMenuButtonEnqueue.setVisibility(View.GONE);
            }*/
        } else {
            swipeMenuButton = (ImageView) ensureInflation(R.id.swipe_menu_button_enqueue_stub,
                    R.id.swipe_menu_button_enqueue);
            swipeMenuButton.setVisibility(View.VISIBLE);
            /*ImageView swipeMenuButtonDequeue =
                    (ImageView) findViewById(R.id.swipe_menu_button_dequeue);
            if (swipeMenuButtonDequeue != null) {
                swipeMenuButtonDequeue.setVisibility(View.GONE);
            }*/
        }
        //swipeMenuButton.setOnClickListener(swipeMenuButton1Listener);
        final ImageView optionsImage = (ImageView)findViewById(R.id.optionImage);
        optionsImage.setOnClickListener(swipeMenuButton1Listener);
    }

    public void fillView(String string) {
        TextView textView1 = (TextView) findViewById(R.id.textview1);
        textView1.setText(string);
    }

    public void fillView(User user) {
        TextView textView1 = (TextView) findViewById(R.id.textview1);
        textView1.setText(user.getName());
        if (mLayoutId == R.layout.list_item_user) {
            TextView textView2 = (TextView) findViewById(R.id.textview2);
            if (user.getFollowersCount() >= 0 && user.getFollowCount() >= 0) {
                textView2.setText(TomahawkApp.getContext().getString(R.string.followers_count,
                        user.getFollowersCount(), user.getFollowCount()));
            }
        }
        TextView userTextView1 = (TextView) findViewById(R.id.usertextview1);
        ImageView userImageView1 = (ImageView) findViewById(R.id.userimageview1);
        ImageUtils.loadUserImageIntoImageView(TomahawkApp.getContext(),
                userImageView1, user, Image.getSmallImageSize(),
                userTextView1);
    }

    public void fillView(Artist artist,View.OnClickListener listener) {
        TextView textView1 = (TextView) findViewById(R.id.textview1);
        textView1.setText(artist.getPrettyName());
        ImageView imageView1 = (ImageView) findViewById(R.id.imageview1);
        ImageUtils.loadImageIntoImageView(TomahawkApp.getContext(), imageView1,
                artist.getImage(), Image.getSmallImageSize(), true);
        ImageView imageView2 = (ImageView) findViewById(R.id.imageview2);
        if (artist.getmLike()==0)
            imageView2.setVisibility(View.GONE);
        else
            imageView2.setVisibility(View.VISIBLE);
        final ImageView optionsImage = (ImageView)findViewById(R.id.optionImage);
        optionsImage.setOnClickListener(listener);
        LinearLayout emptyView = (LinearLayout)findViewById(R.id.emptyView);
        LinearLayout mainView = (LinearLayout)findViewById(R.id.mainclickarea);
        if (artist.getPrettyName().equalsIgnoreCase("Artist Empty")){
            emptyView.setVisibility(View.VISIBLE);
            mainView.setVisibility(View.GONE);
        }else {
            emptyView.setVisibility(View.GONE);
            mainView.setVisibility(View.VISIBLE);
        }
    }
 public void fillView(Genre artist,View.OnClickListener listener) {
        TextView textView1 = (TextView) findViewById(R.id.textview1);
        textView1.setText(artist.getPrettyName());
        ImageView imageView1 = (ImageView) findViewById(R.id.imageview1);
        	ImageUtils.loadImageIntoImageView(TomahawkApp.getContext(), imageView1,
                    artist.getImage(), Image.getSmallImageSize(), true);
     ImageView imageView2 = (ImageView) findViewById(R.id.imageview2);
     if (artist.getmLike()==0)
         imageView2.setVisibility(View.GONE);
     else
         imageView2.setVisibility(View.VISIBLE);
     final ImageView optionsImage = (ImageView)findViewById(R.id.optionImage);
     optionsImage.setOnClickListener(listener);
     LinearLayout emptyView = (LinearLayout)findViewById(R.id.emptyView);
     LinearLayout mainView = (LinearLayout)findViewById(R.id.mainclickarea);
     if (artist.getPrettyName().equalsIgnoreCase("Genre Empty")){
         emptyView.setVisibility(View.VISIBLE);
         mainView.setVisibility(View.GONE);
     }else {
         emptyView.setVisibility(View.GONE);
         mainView.setVisibility(View.VISIBLE);
     }

    }
    public void fillView(final Album album, Collection collection,View.OnClickListener listener) {
        /*if (collection == null) {
            collection =
                    CollectionManager.get().getCollection(TomahawkApp.PLUGINNAME_HATCHET);
        }*/
        TextView textView1 = (TextView) findViewById(R.id.textview1);
        textView1.setText(album.getPrettyName());
        TextView textView2 = (TextView) findViewById(R.id.textview2);
        if (album.getArtist() != null)
        textView2.setText(album.getArtist().getPrettyName());
        else
            textView2.setText(album.getGenre().getPrettyName());

        ImageView imageView1 = (ImageView) findViewById(R.id.imageview1);
        ImageUtils.loadImageIntoImageView(TomahawkApp.getContext(), imageView1,
                album.getImage(), Image.getSmallImageSize(), false);
        ImageView imageView2 = (ImageView) findViewById(R.id.imageview2);
        if (album.getmLike()==0)
            imageView2.setVisibility(View.GONE);
        else
            imageView2.setVisibility(View.VISIBLE);
        final TextView textView3 = (TextView) findViewById(R.id.textview3);
        textView3.setVisibility(View.INVISIBLE);
        if (album.getArtist()!=null) {
            collection.getAlbumTrackCount(album).done(new DoneCallback<Integer>() {
                @Override
                public void onDone(Integer trackCount) {
                    if (trackCount != null) {
                        textView3.setVisibility(View.VISIBLE);
                        textView3.setText(TomahawkApp.getContext().getResources().getQuantityString(
                                R.plurals.songs_with_count, trackCount, trackCount));
                    }
                }
            });
        }else {
            collection.getGenreAlbumTrackCount(album).done(new DoneCallback<Integer>() {
                @Override
                public void onDone(Integer trackCount) {
                    if (trackCount != null) {
                        textView3.setVisibility(View.VISIBLE);
                        textView3.setText(TomahawkApp.getContext().getResources().getQuantityString(
                                R.plurals.songs_with_count, trackCount, trackCount));
                    }
                }
            });
        }

        final ImageView optionsImage = (ImageView)findViewById(R.id.optionImage);
        optionsImage.setOnClickListener(listener);
        LinearLayout emptyView = (LinearLayout)findViewById(R.id.emptyView);
        LinearLayout mainView = (LinearLayout)findViewById(R.id.mainclickarea);
        if (album.getPrettyName().equalsIgnoreCase("Album Empty")){
            emptyView.setVisibility(View.VISIBLE);
            mainView.setVisibility(View.GONE);
        }else {
            emptyView.setVisibility(View.GONE);
            mainView.setVisibility(View.VISIBLE);
        }

    }
    public void fillView(Folder album, Collection collection,View.OnClickListener listener) {
        if (collection == null) {
            collection =
                    CollectionManager.get().getCollection(TomahawkApp.PLUGINNAME_HATCHET);
        }
        TextView textView1 = (TextView) findViewById(R.id.textview1);
        textView1.setText(album.getFolderName());
        TextView textView2 = (TextView) findViewById(R.id.textview2);
        textView2.setText(album.getFolderPath());
        ImageView imageView1 = (ImageView) findViewById(R.id.imageview1);
        ImageUtils.loadImageIntoImageView(TomahawkApp.getContext(), imageView1,
                album.getImage(), Image.getSmallImageSize(), false);
        ImageView imageView2 = (ImageView) findViewById(R.id.imageview2);
        if (album.getmLike()==0)
        imageView2.setVisibility(View.GONE);
        else
        imageView2.setVisibility(View.VISIBLE);

        final TextView textView3 = (TextView) findViewById(R.id.textview3);
        Log.d("T1",album.getFolderName()+"_"+album.getmLike());
        textView3.setVisibility(View.INVISIBLE);
        collection.getFolderTrackCount(album).done(new DoneCallback<Integer>() {
            @Override
            public void onDone(Integer trackCount) {
                if (trackCount != null) {
                    textView3.setVisibility(View.VISIBLE);
                    textView3.setText(TomahawkApp.getContext().getResources().getQuantityString(
                            R.plurals.songs_with_count, trackCount, trackCount));
                }
            }
        });
        final ImageView optionsImage = (ImageView)findViewById(R.id.optionImage);
        optionsImage.setOnClickListener(listener);
        LinearLayout emptyView = (LinearLayout)findViewById(R.id.emptyView);
        LinearLayout mainView = (LinearLayout)findViewById(R.id.mainclickarea);
        if (album.getPrettyName().equalsIgnoreCase("Folder Empty")){
            emptyView.setVisibility(View.VISIBLE);
            mainView.setVisibility(View.GONE);
        }else {
            emptyView.setVisibility(View.GONE);
            mainView.setVisibility(View.VISIBLE);
        }
        /*int songCount = CollectionManager.getInstance().getCollection(
                SensitiveApp.PLUGINNAME_USERCOLLECTION).getFolderTracks(album, false).size();

        TextView textView3 = (TextView) findViewById(R.id.textview3);
        if (songCount > 0) {
            textView3.setVisibility(View.VISIBLE);
            textView3.setText(SensitiveApp.getContext().getResources()
                    .getQuantityString(R.plurals.songs_with_count, songCount, songCount));
        }*/
    }
    public void fillView(Resolver resolver) {
        TextView textView1 = (TextView) findViewById(R.id.textview1);
        textView1.setText(resolver.getPrettyName());
        ImageView imageView1 = (ImageView) findViewById(R.id.imageview1);
        imageView1.clearColorFilter();
       /* if (!(resolver instanceof ScriptResolver) ||
                ((ScriptResolver) resolver).getScriptAccount().getMetaData()
                        .manifest.iconBackground != null) {
            resolver.loadIconBackground(imageView1, !resolver.isEnabled());
        } else*/ {
            if (resolver.isEnabled()) {
                imageView1.setBackgroundColor(TomahawkApp.getContext().getResources()
                        .getColor(android.R.color.black));
            } else {
                imageView1.setBackgroundColor(TomahawkApp.getContext().getResources()
                        .getColor(R.color.fallback_resolver_bg));
            }
        }
        ImageView imageView2 = (ImageView) findViewById(R.id.imageview2);
       /* if (!(resolver instanceof ScriptResolver) ||
                ((ScriptResolver) resolver).getScriptAccount().getMetaData()
                        .manifest.iconWhite != null) {
            resolver.loadIconWhite(imageView2);
        } else */{
            resolver.loadIcon(imageView2, !resolver.isEnabled());
        }
        View connectImageViewContainer = findViewById(R.id.connect_imageview);
        if (resolver.isEnabled()) {
            connectImageViewContainer.setVisibility(View.VISIBLE);
        } else {
            connectImageViewContainer.setVisibility(View.GONE);
        }
    }

    public void fillView(final Playlist playlist, View.OnClickListener listener) {
        /*if (findViewById(R.id.imageview_create_playlist) != null) {
            findViewById(R.id.imageview_create_playlist).setVisibility(View.GONE);
        }*/
        final ArrayList<Image> artistImages = new ArrayList<>();
        String topArtistsString = "";
        if (playlist.getTopArtistNames() == null
                || playlist.getTopArtistNames().length == 0) {
            playlist.updateTopArtistNames();
        }
        String[] artists = playlist.getTopArtistNames();
        if (artists != null) {
            for (int i = 0; i < artists.length && i < 5 && artistImages.size() < 3; i++) {
                Artist artist = Artist.get(artists[i]);
                topArtistsString += artists[i];
                if (i != artists.length - 1) {
                    topArtistsString += ", ";
                }
                if (artist.getImage() != null) {
                    artistImages.add(artist.getImage());
                }
            }
        }
        //if (this.mLayoutId != R.layout.list_item_playlist)
        //fillView(mRootView, artistImages, 0, false);
        ImageView imageView1 = (ImageView) findViewById(R.id.imageview1);
        ImageView imageView2 = (ImageView) findViewById(R.id.imageview2);
        imageView2.setVisibility(View.GONE);
        if (artistImages.size() > 0) {
            ImageUtils.loadImageIntoImageView(TomahawkApp.getContext(),
                    imageView1,
                    artistImages.get(0), Image.getLargeImageSize(), false);
        } else {
            ImageUtils.loadDrawableIntoImageView(TomahawkApp.getContext(),
                    imageView1,
                    R.drawable.album_placeholder_grid);
        }

        TextView textView1 = (TextView) findViewById(R.id.textview1);
        if (textView1 != null) {
            textView1.setText(playlist.getName());
        }
        final TextView textView2 = (TextView) findViewById(R.id.textview2);
        if (textView2 != null) {
            textView2.setText(topArtistsString);
            textView2.setVisibility(View.VISIBLE);
        }
        final TextView textView3 = (TextView) findViewById(R.id.textview3);
        if (textView3 != null && playlist.isFilled()) {
            textView3.setVisibility(View.VISIBLE);
            textView3.setText(TomahawkApp.getContext().getResources().getQuantityString(
                    R.plurals.songs_with_count, ((int) playlist.getCount()<0)?0:(int) playlist.getCount(), ((int) playlist.getCount()<0)?0:(int) playlist.getCount()));
        }
        final ImageView optionsImage = (ImageView)findViewById(R.id.optionImage);
        optionsImage.setOnClickListener(listener);
        LinearLayout emptyView = (LinearLayout)findViewById(R.id.emptyView);
        LinearLayout mainView = (LinearLayout)findViewById(R.id.mainclickarea);
        if (playlist.getName().equalsIgnoreCase("Playlist Empty")){
            emptyView.setVisibility(View.VISIBLE);
            mainView.setVisibility(View.GONE);
        }else {
            emptyView.setVisibility(View.GONE);
            mainView.setVisibility(View.VISIBLE);
        }
        /*ThreadManager.get()
                .execute(new TomahawkRunnable(TomahawkRunnable.PRIORITY_IS_INFOSYSTEM_LOW) {
                    @Override
                    public void run() {
                        if (playlist.getTopArtistNames() == null
                                || playlist.getTopArtistNames().length == 0) {
                            playlist.updateTopArtistNames();
                        } else {
                            for (int i = 0; i < playlist.getTopArtistNames().length
                                    && i < 5; i++) {
                                String artistName = playlist.getTopArtistNames()[i];
                                *//*if (mResolvingTopArtistNames.contains(artistName)) {
                                    mCorrespondingRequestIds.addAll(InfoSystem.get()
                                            .resolve(Artist.get(artistName), false));
                                    mResolvingTopArtistNames.add(artistName);
                                }*//*
                            }
                        }
                        if (textView2 != null) {
                            String[] artists = playlist.getTopArtistNames();
                            String topArtistsString11 = "";
                            if (artists != null) {
                                for (int i = 0; i < artists.length && i < 5 && artistImages.size() < 3; i++) {
                                    Artist artist = Artist.get(artists[i]);
                                    topArtistsString11 += artists[i];
                                    if (i != artists.length - 1) {
                                        topArtistsString11 += ", ";
                                    }
                                    if (artist.getImage() != null) {
                                        artistImages.add(artist.getImage());
                                    }
                                }
                            }
                            textView2.setText(topArtistsString11);
                            textView2.setVisibility(View.VISIBLE);
                        }
                        if (textView3 != null && playlist.isFilled()) {
                            textView3.setVisibility(View.VISIBLE);
                            textView3.setText(TomahawkApp.getContext().getResources().getQuantityString(
                                    R.plurals.songs_with_count, (int) playlist.getCount(), playlist.getCount()));
                        }
                    }
                });*/
    }

    public static void fillView(View view, Artist playlist, int height, boolean isPagerFragment) {
        ArrayList<Image> artistImages = new ArrayList<>();
        String artists = playlist.getName();//getTopArtistNames();
        if (artists != null) {
            Artist artist = Artist.get(artists);
            if (artist.getImage() != null) {
                artistImages.add(artist.getImage());
            }
        }
        fillView(view, artistImages, height, isPagerFragment);
    }

    public static void fillView(View view, Album playlist, int height, boolean isPagerFragment) {
        ArrayList<Image> artistImages = new ArrayList<>();
        String artists = playlist.getName();//getTopArtistNames();
        if (artists != null) {
                Album artist = Album.get(artists,playlist.getArtist());
                if (artist.getImage() != null) {
                    artistImages.add(artist.getImage());
                }
        }
        fillView(view, artistImages, height, isPagerFragment);
    }

    public static void fillView(View view, Genre playlist, int height, boolean isPagerFragment) {
        ArrayList<Image> artistImages = new ArrayList<>();
        String artists = playlist.getName();//getTopArtistNames();
        if (artists != null) {
                Genre artist = Genre.get(artists);
                if (artist.getImage() != null) {
                    artistImages.add(artist.getImage());
                }

        }
        fillView(view, artistImages, height, isPagerFragment);
    }

    public static void fillView(View view, Playlist playlist, int height, boolean isPagerFragment) {
        ArrayList<Image> artistImages = new ArrayList<>();
        String[] artists = playlist.getTopArtistNames();
        if (artists != null) {
            for (int i = 0; i < artists.length && i < 5 && artistImages.size() < 3; i++) {
                Artist artist = Artist.get(artists[i]);
                if (artist.getImage() != null) {
                    artistImages.add(artist.getImage());
                }
            }
        }
        fillView(view, artistImages, height, isPagerFragment);
    }

    private static void fillView(View view, List<Image> artistImages, int height,
            boolean isPagerFragment) {
        View v;
        int gridOneResId = isPagerFragment ? R.id.imageview_grid_one_pager
                : R.id.imageview_grid_one;
        int gridTwoResId = isPagerFragment ? R.id.imageview_grid_two_pager
                : R.id.imageview_grid_two;
        int gridThreeResId = isPagerFragment ? R.id.imageview_grid_three_pager
                : R.id.imageview_grid_three;
        int gridOneStubId = isPagerFragment ? R.id.imageview_grid_one_pager_stub
                : R.id.imageview_grid_one_stub;
        int gridTwoStubId = isPagerFragment ? R.id.imageview_grid_two_pager_stub
                : R.id.imageview_grid_two_stub;
        int gridThreeStubId = isPagerFragment ? R.id.imageview_grid_three_pager_stub
                : R.id.imageview_grid_three_stub;
        if (artistImages.size() > 2) {
            v = view.findViewById(gridOneResId);
            if (v != null) {
                v.setVisibility(View.GONE);
            }
            v = view.findViewById(gridTwoResId);
            if (v != null) {
                v.setVisibility(View.GONE);
            }
            v = ViewUtils.ensureInflation(view, gridThreeStubId, gridThreeResId);
            v.setVisibility(View.VISIBLE);
            ImageUtils.loadImageIntoImageView(TomahawkApp.getContext(),
                    (ImageView) v.findViewById(R.id.imageview1),
                    artistImages.get(0), Image.getLargeImageSize(), false);
            ImageUtils.loadImageIntoImageView(TomahawkApp.getContext(),
                    (ImageView) v.findViewById(R.id.imageview2),
                    artistImages.get(1), Image.getSmallImageSize(), false);
            ImageUtils.loadImageIntoImageView(TomahawkApp.getContext(),
                    (ImageView) v.findViewById(R.id.imageview3),
                    artistImages.get(2), Image.getSmallImageSize(), false);
        } else if (artistImages.size() > 1) {
            v = view.findViewById(gridOneResId);
            if (v != null) {
                v.setVisibility(View.GONE);
            }
            v = view.findViewById(gridThreeResId);
            if (v != null) {
                v.setVisibility(View.GONE);
            }
            v = ViewUtils.ensureInflation(view, gridTwoStubId, gridTwoResId);
            v.setVisibility(View.VISIBLE);
            ImageUtils.loadImageIntoImageView(TomahawkApp.getContext(),
                    (ImageView) v.findViewById(R.id.imageview1),
                    artistImages.get(0), Image.getLargeImageSize(), false);
            ImageUtils.loadImageIntoImageView(TomahawkApp.getContext(),
                    (ImageView) v.findViewById(R.id.imageview2),
                    artistImages.get(1), Image.getSmallImageSize(), false);
        } else {
            v = view.findViewById(gridTwoResId);
            if (v != null) {
                v.setVisibility(View.GONE);
            }
            v = view.findViewById(gridThreeResId);
            if (v != null) {
                v.setVisibility(View.GONE);
            }
            v = ViewUtils.ensureInflation(view, gridOneStubId, gridOneResId);
            if (v!=null)
            v.setVisibility(View.VISIBLE);
            if (artistImages.size() > 0) {
                ImageUtils.loadImageIntoImageView(TomahawkApp.getContext(),
                        (ImageView) v.findViewById(R.id.imageview1),
                        artistImages.get(0), Image.getLargeImageSize(), false);
            } else {
                ImageUtils.loadDrawableIntoImageView(TomahawkApp.getContext(),
                        (ImageView) v.findViewById(R.id.imageview1),
                        R.drawable.album_placeholder_grid);
            }
        }
        if (height > 0) {
            v.getLayoutParams().height = height;
        }
    }

    public void fillView(int id) {
        switch (id) {
            case PlaylistsFragment.CREATE_PLAYLIST_BUTTON_ID:
                View v = mRootView.findViewById(R.id.imageview_grid_one);
                if (v != null) {
                    v.setVisibility(View.GONE);
                }
                v = mRootView.findViewById(R.id.imageview_grid_two);
                if (v != null) {
                    v.setVisibility(View.GONE);
                }
                v = mRootView.findViewById(R.id.imageview_grid_three);
                if (v != null) {
                    v.setVisibility(View.GONE);
                }
                /*ViewUtils.ensureInflation(mRootView, R.id.imageview_create_playlist_stub,
                        R.id.imageview_create_playlist);
                findViewById(R.id.imageview_create_playlist).setVisibility(View.VISIBLE);*/
                TextView textView1 = (TextView) findViewById(R.id.textview1);
                textView1.setText(
                        TomahawkApp.getContext().getString(R.string.create_playlist).toUpperCase());
                TextView textView2 = (TextView) findViewById(R.id.textview2);
                textView2.setVisibility(View.GONE);
                TextView textView3 = (TextView) findViewById(R.id.textview3);
                textView3.setVisibility(View.GONE);
                break;
        }
    }

    public void fillHeaderView(ArrayList<CharSequence> spinnerItems,
            int initialSelection, AdapterView.OnItemSelectedListener listener,View.OnClickListener clickListener,TextWatcher textWatcher,TextView.OnEditorActionListener editorActionListener,String mPrefKey,boolean trackItem) {
        ArrayAdapter<CharSequence> adapter =
                new ArrayAdapter<>(TomahawkApp.getContext(),
                        R.layout.dropdown_header_textview, spinnerItems);
        adapter.setDropDownViewResource(R.layout.dropdown_header_dropdown_textview);
        Spinner spinner = (Spinner) findViewById(R.id.spinner1);
        spinner.setAdapter(adapter);
        spinner.setSelection(initialSelection);
        spinner.setOnItemSelectedListener(listener);
        ImageView changeViewState = (ImageView)findViewById(R.id.changeStateView);
        ImageView searchImage = (ImageView)findViewById(R.id.searchImgBtn);
        ImageView newPlaylistImage = (ImageView)findViewById(R.id.addImgBtn);
        ImageView searchClear = (ImageView)findViewById(R.id.searchClear);
        EditText searchEdit = (EditText)findViewById(R.id.searchText);
        LinearLayout mainLayout = (LinearLayout)findViewById(R.id.mainLayout);
        LinearLayout searchLayout = (LinearLayout)findViewById(R.id.searchLayout);
        SharedPreferences preferences = PreferenceManager
                .getDefaultSharedPreferences(TomahawkApp.getContext());
        if (preferences.getString(mPrefKey+"_layout","normal").equals("normal")){
            mainLayout.setVisibility(View.VISIBLE);
            searchLayout.setVisibility(View.GONE);
        }else {
            mainLayout.setVisibility(View.GONE);
            searchLayout.setVisibility(View.VISIBLE);
        }
        if (trackItem){
            newPlaylistImage.setVisibility(View.GONE);
            changeViewState.setVisibility(View.INVISIBLE);
        }else {
            newPlaylistImage.setVisibility(View.VISIBLE);
            changeViewState.setVisibility(View.VISIBLE);
        }
        if (preferences.getBoolean(PlaylistsFragment.COLLECTION_PLAYLISTS_ADD, false)){
            searchImage.setVisibility(View.GONE);
            newPlaylistImage.setVisibility(View.VISIBLE);
        }else {
            searchImage.setVisibility(View.VISIBLE);
            newPlaylistImage.setVisibility(View.GONE);
        }

        if (searchEdit!=null) {
            if (mPrefKey!=null) {
                if (mPrefKey.equals(AlbumsFragment.COLLECTION_ALBUMS_SHOW_TYPE))
                    searchEdit.setText(preferences.getString(AlbumsFragment.COLLECTION_ALBUMS_EDIT, ""));
                else if (mPrefKey.equals(FoldersFragment.COLLECTION_FOLDER_SHOW_TYPE))
                    searchEdit.setText(preferences.getString(FoldersFragment.COLLECTION_FOLDER_EDIT, ""));
                else if (mPrefKey.equals(ArtistsFragment.COLLECTION_ARTISTS_SHOW_TYPE))
                    searchEdit.setText(preferences.getString(ArtistsFragment.COLLECTION_ARTIST_EDIT, ""));
                else if (mPrefKey.equals(GenreFragment.COLLECTION_GENRES_SHOW_TYPE))
                    searchEdit.setText(preferences.getString(GenreFragment.COLLECTION_GENRE_EDIT, ""));
                else if (mPrefKey.equals(PlaylistEntriesFragment.COLLECTION_TRACKS_SHOW_TYPE))
                    searchEdit.setText(preferences.getString(PlaylistEntriesFragment.COLLECTION_TRACKS_EDIT, ""));
            }
            if (editorActionListener != null)
            searchEdit.setOnEditorActionListener(editorActionListener);
            if (textWatcher != null)
            searchEdit.addTextChangedListener(textWatcher);
        }
        newPlaylistImage.setOnClickListener(clickListener);
        searchImage.setOnClickListener(clickListener);
        searchClear.setOnClickListener(clickListener);
        changeViewState.setOnClickListener(clickListener);

    }

    public void fillHeaderView(String text) {
        TextView textView1 = (TextView) findViewById(R.id.textview1);
        textView1.setText(text);
    }



    private static String dateToString(Resources resources, Date date) {
        String s = "";
        if (date != null) {
            long diff = System.currentTimeMillis() - date.getTime();
            if (diff < 60000) {
                s += resources.getString(R.string.time_afewseconds);
            } else if (diff < 3600000) {
                long minutes = TimeUnit.MILLISECONDS.toMinutes(diff);
                s += resources.getQuantityString(R.plurals.time_minute, (int) minutes, minutes);
            } else if (diff < 86400000) {
                long hours = TimeUnit.MILLISECONDS.toHours(diff);
                s += resources.getQuantityString(R.plurals.time_hour, (int) hours, hours);
            } else {
                long days = TimeUnit.MILLISECONDS.toDays(diff);
                s += resources.getQuantityString(R.plurals.time_day, (int) days, days);
            }
        }
        return s;
    }

    private static void setTextViewEnabled(TextView textView, boolean enabled,
            boolean isSecondary) {
        if (textView != null && textView.getResources() != null) {
            int colorResId;
            if (enabled) {
                if (isSecondary) {
                    colorResId = R.color.secondary_textcolor;
                } else {
                    colorResId = R.color.primary_textcolor;
                }
            } else {
                colorResId = R.color.disabled;
            }
            textView.setTextColor(textView.getResources().getColor(colorResId));
        }
    }

}
