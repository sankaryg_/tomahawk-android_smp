/* == This file is part of Tomahawk Player - <http://tomahawk-player.org> ===
 *
 *   Copyright 2012, Enno Gottschalk <mrmaffen@googlemail.com>
 *
 *   Tomahawk is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Tomahawk is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Tomahawk. If not, see <http://www.gnu.org/licenses/>.
 */
package org.tomahawk.tomahawk_android.adapters;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.TextView;

import org.tomahawk.tomahawk_android.R;
import org.tomahawk.tomahawk_android.TomahawkApp;
import org.tomahawk.tomahawk_android.fragments.AccelerometerFragment_;
import org.tomahawk.tomahawk_android.fragments.HammerFragment_;
import org.tomahawk.tomahawk_android.fragments.PocketSensorFragment_;
import org.tomahawk.tomahawk_android.fragments.ProximityFragment_;
import org.tomahawk.tomahawk_android.fragments.SensorPagerFragment;
import org.tomahawk.tomahawk_android.fragments.WaveOverFragment_;
import org.tomahawk.tomahawk_android.sensor.AccelerometerDataAnalyzer;
import org.tomahawk.tomahawk_android.sensor.IntentMapping;
import org.tomahawk.tomahawk_android.sensor.MyIntents;
import org.tomahawk.tomahawk_android.sensor.PrefsHelper;
import org.tomahawk.tomahawk_android.sensor.SensorModeHelper;
import org.tomahawk.tomahawk_android.sensor.UserMappableAction;
import org.tomahawk.tomahawk_android.services.PlaybackService;
import org.tomahawk.tomahawk_android.utils.FakePreferenceGroup;
import org.tomahawk.tomahawk_android.views.SensorSeekBar;

import java.util.ArrayList;
import java.util.List;

import de.greenrobot.event.EventBus;

/**
 * Since {@link android.preference.PreferenceFragment} is not supported with the official support
 * library, and also not within ActionBarSherlock, we have to create our own Fragment with our own
 * {@link FakePreferencesAdapter_}
 */
public class FakePreferencesAdapter_ extends StickyBaseAdapter {

    private final LayoutInflater mLayoutInflater;

    private final SharedPreferences mSharedPreferences;

    private final List<FakePreferenceGroup> mFakePreferenceGroups;

    private Context mContext;

    final TomahawkApp smp;

    private static int waveOverProgress;

    private static int longStayOverProgress;

    SensorSeekBar seekBar1 = null;

    SensorSeekBar seekBarX;

    SensorSeekBar seekBarY;

    SensorSeekBar seekBarZ;

    SeekBar longStayDuration;

    SeekBar doubleWaveSpeed;

    Spinner xActionSpinner;

    Spinner yActionSpinner;

    Spinner zActionSpinner;

    Spinner longStayActionSpinner;

    Spinner doubleWaveActionSpinner;

    Spinner simpleWaveActionSpinner;

    AccelerometerDataAnalyzer accAnalyzer;

    int currentDoubleWaveSpeedValue;

    int currentLongStayDurationValue;

    ActionSpinnerAdapter actionspinneradapter;

    private class SpinnerListener implements AdapterView.OnItemSelectedListener {

        private final String mKey;

        public SpinnerListener(String key) {
            mKey = key;
        }

        @Override
        public void onItemSelected(AdapterView<?> parent, View view,
                int position, long id) {
            /*if (mSharedPreferences.getInt(mKey,
                    SpotifyMediaPlayer.SPOTIFY_PREF_BITRATE_MODE_MEDIUM) != position) {
                SharedPreferences.Editor editor = mSharedPreferences.edit();
                editor.putInt(mKey, position);
                editor.commit();
                SpotifyMediaPlayer.get().setBitRate(position);
            }*/
        }

        @Override
        public void onNothingSelected(AdapterView<?> parent) {
        }
    }

    @Override
    public void setPlayBackView(int currentState) {

    }

    private class seekBarListener implements SeekBar.OnSeekBarChangeListener
    {
        private final String mKey;
        private final TextView t1;
        private final TextView t2;
        private final SeekBar seekBar1;
        int i = PrefsHelper.getWaveOverDoubleWaveSpeed(smp.getPrefs());
        int j = PrefsHelper.getWaveOverLongStay(smp.getPrefs());

        public seekBarListener(TextView t1,TextView t2,String key,SeekBar seekBar){

            mKey = key;
            this.t1 = t1;
            this.t2 = t2;
            this.seekBar1 = seekBar;
            if (mKey.equals(WaveOverFragment_.FAKEPREFERENCEFRAGMENT_KEY_DOUBLEWAVE)){
                t1.setText(mContext.getResources().getString(R.string.preferences_double_wave_action));
                float f = (float)Math.round(i / 10) / 100F;
                t2.setText((new StringBuilder(" (")).append(String.format("%.2f", (f))).append("s) :").toString());
                seekBar1.setProgress(SensorModeHelper.fromDoubleWaveSpeedToRatio(i));
                //waveOverProgress = seekBar1.getProgress();
                //summary.setText(String.format(mContext.getString(R.string.preferences_double_wave_action_line1),t2.getText().toString()));

            }

            if(mKey.equals(WaveOverFragment_.FAKEPREFERENCEFRAGMENT_KEY_LONGOVER)){
                t1.setText(mContext.getResources().getString(R.string.preferences_long_wave_action));
                float f1 = (float)Math.round(j / 10) / 100F;
                t2.setText((new StringBuilder(" (")).append(String.format("%.2f", (f1))).append("s) :").toString());
                seekBar1.setProgress(SensorModeHelper.fromLongStayDurationToRatio(j));
                //longStayOverProgress = seekBar1.getProgress();
                //summary.setText(String.format(mContext.getString(R.string.preferences_long_wave_action_line1), t2.getText().toString()));
            }

        }
        @Override
        public void onProgressChanged(SeekBar seekBar, int k, boolean fromUser) {
            int temp = 0;

            if (mKey.equals(WaveOverFragment_.FAKEPREFERENCEFRAGMENT_KEY_DOUBLEWAVE)) {
                t2.setText((new StringBuilder(" (")).append(String.format("%.2f", (double) SensorModeHelper.fromRatioToDoubleWaveSpeed(k) / 1000D)).append("s)").toString());
                //summary.setText(String.format(mContext.getString(R.string.preferences_double_wave_action_line1), t2.getText().toString()));
                /*temp = SensorModeHelper.fromRatioToDoubleWaveSpeed(seekBar1.getProgress());
                PrefsHelper.setTempWaveOverDoubleWaveSpeed(smp.getPrefEditor(),temp);
                smp.getPrefEditor().commit();*/
                waveOverProgress = seekBar1.getProgress();
            }
            if(mKey.equals(WaveOverFragment_.FAKEPREFERENCEFRAGMENT_KEY_LONGOVER)){
                t2.setText((new StringBuilder(" (")).append( String.format("%.2f", (double)SensorModeHelper.fromRatioToLongStayDuration(k) / 1000D)).append("s)").toString());
                //summary.setText(String.format(mContext.getString(R.string.preferences_long_wave_action_line1), t2.getText().toString()));
                /*temp = SensorModeHelper.fromRatioToLongStayDuration(seekBar1.getProgress());
                PrefsHelper.setWaveOverLongStayTemp(smp.getPrefEditor(), temp);
                smp.getPrefEditor().commit();*/
                longStayOverProgress = seekBar1.getProgress();
            }
        }

        @Override
        public void onStartTrackingTouch(SeekBar seekBar) {

        }

        @Override
        public void onStopTrackingTouch(SeekBar seekBar) {

        }
    }
    /**
     * Constructs a new {@link org.tomahawk.tomahawk_android.adapters.FakePreferencesAdapter_}
     */
    public FakePreferencesAdapter_(Context context, LayoutInflater layoutInflater,
            List<FakePreferenceGroup> fakePreferenceGroups,TomahawkApp tomahawkApp) {
        mLayoutInflater = layoutInflater;
        mContext = context;
        mSharedPreferences = PreferenceManager.getDefaultSharedPreferences(context);
        mFakePreferenceGroups = fakePreferenceGroups;
        smp = tomahawkApp;
        waveOverProgress = SensorModeHelper.fromDoubleWaveSpeedToRatio(PrefsHelper.getWaveOverDoubleWaveSpeed(smp.getPrefs()));
        longStayOverProgress = SensorModeHelper.fromLongStayDurationToRatio(PrefsHelper.getWaveOverLongStay(smp.getPrefs()));
        accAnalyzer = smp.getSensorDataHelper().getAccAnalyzer();
        actionspinneradapter = new ActionSpinnerAdapter(mContext, R.layout.action_spinner_item, UserMappableAction.getMappableActions());

    }

    /**
     * @return the total amount of all {@link FakePreferenceGroup}s this {@link
     * FakePreferencesAdapter_} displays
     */
    @Override
    public int getCount() {
        int countSum = 0;
        for (FakePreferenceGroup fakePreferenceGroup : mFakePreferenceGroups) {
            countSum += fakePreferenceGroup.getFakePreferences().size();
        }
        return countSum;
    }

    /**
     * Get the correct {@link org.tomahawk.tomahawk_android.utils.FakePreferenceGroup.FakePreference}
     * for the given position
     */
    @Override
    public Object getItem(int position) {
        Object item = null;
        int offsetCounter = 0;
        for (FakePreferenceGroup fakePreferenceGroup : mFakePreferenceGroups) {
            if (position - offsetCounter < fakePreferenceGroup.getFakePreferences().size()) {
                item = fakePreferenceGroup.getFakePreferences().get(position - offsetCounter);
                break;
            }
            offsetCounter += fakePreferenceGroup.getFakePreferences().size();
        }
        return item;
    }

    /**
     * Get the id of the item with the given position (the returned id is equal to the position)
     */
    @Override
    public long getItemId(int position) {
        return position;
    }

    /**
     * Get the correct {@link View} for the given position. Recycle a convertView, if possible.
     *
     * @param position    The position for which to get the correct {@link View}
     * @param convertView The old {@link View}, which we might be able to recycle
     * @param parent      parental {@link ViewGroup}
     * @return the correct {@link View} for the given position.
     */
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = null;
        final FakePreferenceGroup.FakePreference item =
                (FakePreferenceGroup.FakePreference) getItem(position);
         //final TextView bottomSeekbarText = null;

        if (item != null) {
            ViewHolder viewHolder = null;
            if (convertView != null) {
                viewHolder = (ViewHolder) convertView.getTag();
                view = convertView;
            }
            int viewType = getViewType(item);
            if (viewHolder == null || viewHolder.mLayoutId != viewType) {
                // If the viewHolder is null or the old viewType is different than the new one,
                // we need to inflate a new view and construct a new viewHolder,
                // which we set as the view's tag
                view = mLayoutInflater.inflate(viewType, parent, false);
                viewHolder = new ViewHolder(view, viewType);
                view.setTag(viewHolder);
            } else {
                ImageView imageView = (ImageView) viewHolder.findViewById(R.id.imageview1);
                if (imageView != null) {
                    imageView.setVisibility(View.GONE);
                }
            }


            // After we've set up the correct view and viewHolder, we now can fill the View's
            // components with the correct data
            if (viewHolder.mLayoutId == R.layout.fake_preferences_checkbox) {
                boolean preferenceState = mSharedPreferences
                        .getBoolean(item.getStorageKey(), false);
                CheckBox checkBox = (CheckBox) viewHolder.findViewById(R.id.checkbox1);
                checkBox.setChecked(preferenceState);
            } else if (viewHolder.mLayoutId == R.layout.fake_preferences_spinner) {
                String key = item.getStorageKey();
                ArrayList<CharSequence> list = new ArrayList<>();
                if(key.equals(AccelerometerFragment_.FAKEPREFERENCEFRAGMENT_KEY_PREFACCELEROMETER_SHAKE_XAXIS)){
                    /*for (String headerString : TomahawkApp.getContext().getResources()
                            .getStringArray(R.array.fake_preferences_items_custom)) {
                        list.add(headerString.toUpperCase());
                    }*/
                    //ActionSpinnerAdapter actionspinneradapter = new ActionSpinnerAdapter(mContext, R.layout.action_spinner_item, UserMappableAction.getMappableActions());
                    Spinner spinner = (Spinner) viewHolder.findViewById(R.id.spinner1);
                    xActionSpinner = spinner;
                    xActionSpinner.setAdapter(actionspinneradapter);
                    xActionSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                            if (view != null) {
                                Object obj = view.getTag();
                                if (obj != null) {
                                    IntentMapping.setCustomModeActionForEvent(smp.getPrefs(), 100, Integer.valueOf(obj.toString()).intValue());
                                }
                            }
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> adapterView) {

                        }
                    });

                }else if(key.equals(AccelerometerFragment_.FAKEPREFERENCEFRAGMENT_KEY_PREFACCELEROMETER_SHAKE_YAXIS)){
                    /*for (String headerString : TomahawkApp.getContext().getResources()
                            .getStringArray(R.array.fake_preferences_items_custom)) {
                        list.add(headerString.toUpperCase());
                    }*/
                    //ActionSpinnerAdapter actionspinneradapter = new ActionSpinnerAdapter(mContext, R.layout.action_spinner_item, UserMappableAction.getMappableActions());
                    Spinner spinner = (Spinner) viewHolder.findViewById(R.id.spinner1);
                    yActionSpinner = spinner;
                    yActionSpinner.setAdapter(actionspinneradapter);
                    yActionSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                            if (view != null) {
                                Object obj = view.getTag();
                                if (obj != null) {
                                    IntentMapping.setCustomModeActionForEvent(smp.getPrefs(), 111, Integer.valueOf(obj.toString()).intValue());
                                }
                            }
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> adapterView) {

                        }
                    });

                }else if(key.equals(AccelerometerFragment_.FAKEPREFERENCEFRAGMENT_KEY_PREFACCELEROMETER_SHAKE_ZAXIS)){
                    /*for (String headerString : TomahawkApp.getContext().getResources()
                            .getStringArray(R.array.fake_preferences_items_custom)) {
                        list.add(headerString.toUpperCase());
                    }*/
                    //ActionSpinnerAdapter actionspinneradapter = new ActionSpinnerAdapter(mContext, R.layout.action_spinner_item, UserMappableAction.getMappableActions());
                    Spinner spinner = (Spinner) viewHolder.findViewById(R.id.spinner1);
                    zActionSpinner = spinner;
                    zActionSpinner.setAdapter(actionspinneradapter);
                    zActionSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                            if (view != null) {
                                Object obj = view.getTag();
                                if (obj != null) {
                                    IntentMapping.setCustomModeActionForEvent(smp.getPrefs(), 122, Integer.valueOf(obj.toString()).intValue());
                                }
                            }
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> adapterView) {

                        }
                    });

                }
                else if(key.equals(ProximityFragment_.FAKEPREFERENCEFRAGMENT_KEY_PREFPROXIMITY_WAVE_OVER_SPINNER)){
                    /*for (String headerString : TomahawkApp.getContext().getResources()
                            .getStringArray(R.array.fake_preferences_items_custom)) {
                        list.add(headerString.toUpperCase());
                    }*/
                    //ActionSpinnerAdapter actionspinneradapter = new ActionSpinnerAdapter(mContext, R.layout.action_spinner_item, UserMappableAction.getMappableActions());
                    Spinner spinner = (Spinner) viewHolder.findViewById(R.id.spinner1);
                    simpleWaveActionSpinner = spinner;
                    simpleWaveActionSpinner.setAdapter(actionspinneradapter);
                    simpleWaveActionSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                            if (view != null) {
                                Object obj = view.getTag();
                                if (obj != null) {
                                    IntentMapping.setCustomModeActionForEvent(smp.getPrefs(), 200, Integer.valueOf(obj.toString()).intValue());
                                }
                            }
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> adapterView) {

                        }
                    });

                }
                else if(key.equals(ProximityFragment_.FAKEPREFERENCEFRAGMENT_KEY_PREFPROXIMITY_DOUBLE_WAVE_SPINNER)){
                    /*for (String headerString : TomahawkApp.getContext().getResources()
                            .getStringArray(R.array.fake_preferences_items_custom)) {
                        list.add(headerString.toUpperCase());
                    }*/
                    //ActionSpinnerAdapter actionspinneradapter = new ActionSpinnerAdapter(mContext, R.layout.action_spinner_item, UserMappableAction.getMappableActions());
                    Spinner spinner = (Spinner) viewHolder.findViewById(R.id.spinner1);
                    doubleWaveActionSpinner = spinner;
                    doubleWaveActionSpinner.setAdapter(actionspinneradapter);
                    doubleWaveActionSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                            if (view != null) {
                                Object obj = view.getTag();
                                if (obj != null) {
                                    IntentMapping.setCustomModeActionForEvent(smp.getPrefs(), 222, Integer.valueOf(obj.toString()).intValue());
                                }
                            }
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> adapterView) {

                        }
                    });

                }
                else if(key.equals(ProximityFragment_.FAKEPREFERENCEFRAGMENT_KEY_PREFPROXIMITY_LONG_STAY_SPINNER)){
                    /*for (String headerString : TomahawkApp.getContext().getResources()
                            .getStringArray(R.array.fake_preferences_items_custom)) {
                        list.add(headerString.toUpperCase());
                    }*/
                    Spinner spinner = (Spinner) viewHolder.findViewById(R.id.spinner1);
                    longStayActionSpinner = spinner;
                    longStayActionSpinner.setAdapter(actionspinneradapter);
                    longStayActionSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                        @Override
                        public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                            if (view != null) {
                                Object obj = view.getTag();
                                if (obj != null) {
                                    IntentMapping.setCustomModeActionForEvent(smp.getPrefs(), 222, Integer.valueOf(obj.toString()).intValue());
                                }
                            }
                        }

                        @Override
                        public void onNothingSelected(AdapterView<?> adapterView) {

                        }
                    });


                }
                else {
                    for (String headerString : TomahawkApp.getContext().getResources()
                            .getStringArray(R.array.fake_preferences_items_bitrate)) {
                        list.add(headerString.toUpperCase());
                    }
                    ArrayAdapter<CharSequence> adapter =
                            new ArrayAdapter<>(TomahawkApp.getContext(),
                                    R.layout.spinner_textview, list);
                    adapter.setDropDownViewResource(R.layout.spinner_dropdown_textview);
                    Spinner spinner = (Spinner) viewHolder.findViewById(R.id.spinner1);
                    spinner.setAdapter(adapter);
                }

            }else if (viewHolder.mLayoutId == R.layout.fake_preferences_seekbar){
                TextView topSeekbarText = (TextView)viewHolder.findViewById(R.id.topText);
                final TextView bottomSeekbarText = (TextView)viewHolder.findViewById(R.id.bottomText);
                if (item.getStorageKey().equals(PocketSensorFragment_.FAKEPREFERENCEFRAGMENT_KEY_PREFPOCKET_SENSITIVITY)){
                    SensorSeekBar seekBar = (SensorSeekBar)viewHolder.findViewById(R.id.sensorSeekBarPreference);
                    seekBar1 = seekBar;
                    seekBar.setVisibility(View.VISIBLE);
                    topSeekbarText.setText(mContext.getResources().getString(R.string.preferences_pocket_low));
                    bottomSeekbarText.setText(mContext.getResources().getString(R.string.preferences_pocket_high));
                    seekBar1.setParsedMaxValue(2500);
                    seekBar1.setParsedMinValue(500);
                    seekBar1.setParsedProgress(PrefsHelper.getModePocketAccSensitivity(smp.getPrefs(), 1000));
                    smp.getSensorDataHelper().setAccZSeekbar(seekBar1);
                }else if (item.getStorageKey().equals(HammerFragment_.FAKEPREFERENCEFRAGMENT_KEY_PREFHAMMER_SENSITIVITY)){
                    SensorSeekBar seekBar = (SensorSeekBar)viewHolder.findViewById(R.id.sensorSeekBarPreference);
                    seekBar1 = seekBar;
                    seekBar.setVisibility(View.VISIBLE);
                    topSeekbarText.setText(mContext.getResources().getString(R.string.preferences_pocket_low));
                    bottomSeekbarText.setText(mContext.getResources().getString(R.string.preferences_pocket_high));
                    seekBar1.setParsedMaxValue(600);//800
                    seekBar1.setParsedMinValue(150);
                    seekBar1.setParsedProgress(PrefsHelper.getModeHammerSensitivity(smp.getPrefs(), 500));
                    smp.getSensorDataHelper().setAccZSeekbar(seekBar1);
                }else  if (item.getStorageKey().equals(AccelerometerFragment_.FAKEPREFERENCEFRAGMENT_KEY_PREFACCELEROMETER_SHAKE_XAXIS_SEEKBAR)){
                    SensorSeekBar seekBar = (SensorSeekBar)viewHolder.findViewById(R.id.sensorSeekBarPreference);
                    seekBarX = seekBar;
                    seekBar.setVisibility(View.VISIBLE);
                    topSeekbarText.setText(mContext.getResources().getString(R.string.preferences_pocket_low));
                    bottomSeekbarText.setText(mContext.getResources().getString(R.string.preferences_pocket_high));
                    seekBarX.setParsedMaxValue(SensorPagerFragment.maxSensitivityValue);
                    seekBarX.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                        @Override
                        public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                            accAnalyzer.updateXSensitivity(seekBar.getProgress());
                            PrefsHelper.setCustomXSensitivity(smp.getPrefEditor(), seekBarX.getParsedProgress());
                            smp.getPrefEditor().commit();
                        }

                        @Override
                        public void onStartTrackingTouch(SeekBar seekBar) {

                        }

                        @Override
                        public void onStopTrackingTouch(SeekBar seekBar) {

                        }
                    });
                    seekBarX.setVisualyDisabledWhenMax(true);
                    int i = PrefsHelper.getCustomXSensitivity(smp.getPrefs());
                    seekBarX.setParsedProgress(i);
                    AccelerometerDataAnalyzer accelerometerdataanalyzer = smp.getSensorDataHelper().getAccAnalyzer();
                    accelerometerdataanalyzer.updateXSensitivity(i);

                }else  if (item.getStorageKey().equals(AccelerometerFragment_.FAKEPREFERENCEFRAGMENT_KEY_PREFACCELEROMETER_SHAKE_YAXIS_SEEKBAR)){
                    SensorSeekBar seekBar = (SensorSeekBar)viewHolder.findViewById(R.id.sensorSeekBarPreference);
                    seekBarY = seekBar;
                    seekBar.setVisibility(View.VISIBLE);
                    topSeekbarText.setText(mContext.getResources().getString(R.string.preferences_pocket_low));
                    bottomSeekbarText.setText(mContext.getResources().getString(R.string.preferences_pocket_high));
                    seekBarY.setParsedMaxValue(SensorPagerFragment.maxSensitivityValue);
                    seekBarY.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                        @Override
                        public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                            accAnalyzer.updateYSensitivity(seekBar.getProgress());
                            PrefsHelper.setCustomYSensitivity(smp.getPrefEditor(), seekBarY.getParsedProgress());
                            smp.getPrefEditor().commit();
                        }

                        @Override
                        public void onStartTrackingTouch(SeekBar seekBar) {

                        }

                        @Override
                        public void onStopTrackingTouch(SeekBar seekBar) {

                        }
                    });
                    seekBarY.setVisualyDisabledWhenMax(true);
                    int i = PrefsHelper.getCustomYSensitivity(smp.getPrefs());
                    seekBarY.setParsedProgress(i);
                    AccelerometerDataAnalyzer accelerometerdataanalyzer = smp.getSensorDataHelper().getAccAnalyzer();
                    accelerometerdataanalyzer.updateYSensitivity(i);
                }else  if (item.getStorageKey().equals(AccelerometerFragment_.FAKEPREFERENCEFRAGMENT_KEY_PREFACCELEROMETER_SHAKE_ZAXIS_SEEKBAR)){
                    SensorSeekBar seekBar = (SensorSeekBar)viewHolder.findViewById(R.id.sensorSeekBarPreference);
                    seekBarZ = seekBar;
                    seekBar.setVisibility(View.VISIBLE);
                    topSeekbarText.setText(mContext.getResources().getString(R.string.preferences_pocket_low));
                    bottomSeekbarText.setText(mContext.getResources().getString(R.string.preferences_pocket_high));
                    seekBarZ.setParsedMaxValue(SensorPagerFragment.maxSensitivityValue);
                    seekBarZ.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                        @Override
                        public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                            accAnalyzer.updateZSensitivity(seekBar.getProgress());
                            PrefsHelper.setCustomZSensitivity(smp.getPrefEditor(), seekBarY.getParsedProgress());
                            smp.getPrefEditor().commit();
                        }

                        @Override
                        public void onStartTrackingTouch(SeekBar seekBar) {

                        }

                        @Override
                        public void onStopTrackingTouch(SeekBar seekBar) {

                        }
                    });
                    seekBarZ.setVisualyDisabledWhenMax(true);
                    int i = PrefsHelper.getCustomZSensitivity(smp.getPrefs());
                    seekBarZ.setParsedProgress(i);
                    AccelerometerDataAnalyzer accelerometerdataanalyzer = smp.getSensorDataHelper().getAccAnalyzer();
                    accelerometerdataanalyzer.updateZSensitivity(i);
                }
                else  if (item.getStorageKey().equals(ProximityFragment_.FAKEPREFERENCEFRAGMENT_KEY_PREFPROXIMITY_DOUBLEWAVE_SEEKBAR)){
                    SeekBar seekBar = (SeekBar)viewHolder.findViewById(R.id.seekBarPreference);
                    doubleWaveSpeed = seekBar;
                    seekBar.setVisibility(View.VISIBLE);
                    doubleWaveSpeed.setMax(100);
                    currentDoubleWaveSpeedValue = PrefsHelper.getCustomProxDoubleWaveSpeed(smp.getPrefs());
                    int i1 = SensorModeHelper.fromDoubleWaveSpeedToRatio(currentDoubleWaveSpeedValue);
                    doubleWaveSpeed.setProgress(i1);
                    doubleWaveSpeed.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {


                        @Override
                        public void onProgressChanged(SeekBar seekBar, int i, boolean b) {



                            currentDoubleWaveSpeedValue = SensorModeHelper.fromRatioToDoubleWaveSpeed(i);
                            float f = (float) Math.round(currentDoubleWaveSpeedValue / 10) / 100F;
                            bottomSeekbarText.setText((new StringBuffer(" (")).append(f).append("s) :").toString());
                            PrefsHelper.setCustomProxDoubleWaveSpeed(smp.getPrefEditor(), currentDoubleWaveSpeedValue);
                            smp.getPrefEditor().commit();
                        }

                        @Override
                        public void onStartTrackingTouch(SeekBar seekBar) {

                        }

                        @Override
                        public void onStopTrackingTouch(SeekBar seekBar) {

                        }
                    });

                }
                else  if (item.getStorageKey().equals(ProximityFragment_.FAKEPREFERENCEFRAGMENT_KEY_PREFPROXIMITY_LONG_STAY_SEEKBAR)){
                    SeekBar seekBar = (SeekBar)viewHolder.findViewById(R.id.seekBarPreference);
                    longStayDuration = seekBar;
                    seekBar.setVisibility(View.VISIBLE);
                    longStayDuration.setMax(100);
                    currentLongStayDurationValue = PrefsHelper.getCustomProxLongStay(smp.getPrefs());
                    int l = SensorModeHelper.fromLongStayDurationToRatio(currentLongStayDurationValue);
                    longStayDuration.setProgress(l);
                    longStayDuration.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                        @Override
                        public void onProgressChanged(SeekBar seekBar, int i, boolean b) {
                            currentLongStayDurationValue = SensorModeHelper.fromRatioToLongStayDuration(i);
                            float f = (float) Math.round(currentLongStayDurationValue / 10) / 100F;
                            bottomSeekbarText.setText((new StringBuilder(" (")).append(f).append("s) :").toString());
                            PrefsHelper.setCustomProxLongStay(smp.getPrefEditor(), currentLongStayDurationValue);
                            smp.getPrefEditor().commit();
                        }

                        @Override
                        public void onStartTrackingTouch(SeekBar seekBar) {

                        }

                        @Override
                        public void onStopTrackingTouch(SeekBar seekBar) {

                        }
                    });

                    if (topSeekbarText.getText().toString().isEmpty()) {
                        topSeekbarText.setVisibility(View.GONE);
                    }
                    if (bottomSeekbarText.getText().toString().isEmpty()) {
                        bottomSeekbarText.setVisibility(View.GONE);
                    }
                }
                else {
                    SeekBar seekBar = (SeekBar)viewHolder.findViewById(R.id.seekBarPreference);
                    seekBar.setMax(100);
                    seekBar.setVisibility(View.VISIBLE);
                    seekBar.setOnSeekBarChangeListener(new seekBarListener(topSeekbarText, bottomSeekbarText, item.getStorageKey(), seekBar));
                }

                }else if (viewHolder.mLayoutId == R.layout.fake_preferences_button){
                Button applyBtn = (Button)viewHolder.findViewById(R.id.button);
                applyBtn.setText(item.getTitle());
                applyBtn.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        smp.getSensorDataHelper().setIntentBroadcastEnabled(false);
                        PlaybackService.DisableAllSensorEvent event = new PlaybackService.DisableAllSensorEvent();
                        EventBus.getDefault().post(event);
                        if(item.getStorageKey().equals(WaveOverFragment_.FAKEPREFERENCEFRAGMENT_KEY_WAVE_BUTTON)) {
                            int k = SensorModeHelper.fromRatioToDoubleWaveSpeed(waveOverProgress);
                            PrefsHelper.setWaveOverDoubleWaveSpeed(smp.getPrefEditor(), k);
                            int l = SensorModeHelper.fromRatioToLongStayDuration(longStayOverProgress);
                            PrefsHelper.setWaveOverLongStay(smp.getPrefEditor(), l);
                            smp.getPrefEditor().commit();
                            smp.getSensorModeHelper().resetPreset(1);
                            MyIntents.selectSensorMode(smp, 1, true, true);
                            try {
                                smp.getSensorDataHelper().setIntentBroadcastEnabled(true);
                            } catch (Exception exception) {
                                exception.printStackTrace();
                            }
                        }else if (item.getStorageKey().equals(PocketSensorFragment_.FAKEPREFERENCEFRAGMENT_KEY_PREFPOCKET_BUTTON)){

                            int i = seekBar1.getParsedProgress();
                            PrefsHelper.setModePocketAccSensitivity(smp.getPrefEditor(), i);
                            smp.getPrefEditor().commit();
                            smp.getSensorModeHelper().resetPreset(2);
                            smp.getSensorDataHelper().setIntentBroadcastEnabled(true);
                            MyIntents.selectSensorMode(smp, 2, true, true);

                        }else if (item.getStorageKey().equals(HammerFragment_.FAKEPREFERENCEFRAGMENT_KEY_PREFHAMMER_BUTTON)){
                            int i = seekBar1.getParsedProgress();
                            PrefsHelper.setModeHammerSensitivity(smp.getPrefEditor(), i);
                            smp.getPrefEditor().commit();
                            smp.getSensorModeHelper().resetPreset(3);
                            smp.getSensorDataHelper().setIntentBroadcastEnabled(true);
                            MyIntents.selectSensorMode(smp, 3, true, true);
                        } else if (item.getStorageKey().equals(AccelerometerFragment_.FAKEPREFERENCEFRAGMENT_KEY_PREFACCELEROMETER_SHAKE_BUTTON)){
                            /*Action action = IntentMapping.getActionForEvent(smp, 100, 4);
                            xActionSpinner.setSelection(UserMappableAction.getActionPosition(action.id));

                            Action action1 = IntentMapping.getActionForEvent(smp, 111, 4);
                            yActionSpinner.setSelection(UserMappableAction.getActionPosition(action1.id));

                            Action action2 = IntentMapping.getActionForEvent(smp, 122, 4);
                            zActionSpinner.setSelection(UserMappableAction.getActionPosition(action2.id));*/

                            PrefsHelper.setCustomAccEnabled(smp.getPrefEditor(), true);
                            PrefsHelper.setCustomProxEnabled(smp.getPrefEditor(), false);
                            smp.getPrefEditor().commit();
                            smp.getSensorModeHelper().resetPreset(4);
                            smp.getSensorDataHelper().setIntentBroadcastEnabled(true);
                            MyIntents.selectSensorMode(smp, 4, true, true);
                        }else if (item.getStorageKey().equals(ProximityFragment_.FAKEPREFERENCEFRAGMENT_KEY_PREFPROXIMITY_WAVE_BUTTON)){
                            /*Action action3 = IntentMapping.getActionForEvent(smp, 200, 4);
                            simpleWaveActionSpinner.setSelection(UserMappableAction.getActionPosition(action3.id));

                            Action action4 = IntentMapping.getActionForEvent(smp, 222, 4);
                            doubleWaveActionSpinner.setSelection(UserMappableAction.getActionPosition(action4.id));

                            Action action5 = IntentMapping.getActionForEvent(smp, 211, 4);
                            longStayActionSpinner.setSelection(UserMappableAction.getActionPosition(action5.id));*/
                            PrefsHelper.setCustomAccEnabled(smp.getPrefEditor(), false);
                            PrefsHelper.setCustomProxEnabled(smp.getPrefEditor(), true);
                            smp.getPrefEditor().commit();
                            smp.getSensorModeHelper().resetPreset(4);
                            smp.getSensorDataHelper().setIntentBroadcastEnabled(true);
                            MyIntents.selectSensorMode(smp, 4, true, true);
                        }

                    }
                });
            }
            TextView textView1 = (TextView) viewHolder.findViewById(R.id.textview1);

                textView1.setText(item.getTitle());
                if (textView1.getText().toString().isEmpty()) {
                    textView1.setVisibility(View.GONE);
                }

            TextView textView2= (TextView) viewHolder.findViewById(R.id.textview2);
               textView2.setText(item.getSummary());
                if (textView2.getText().toString().isEmpty()) {
                    textView2.setVisibility(View.GONE);
                }

        }

        // Finally we can return the correct view
        return view;
    }

    /**
     * This method is being called by the StickyListHeaders library. Get the correct header {@link
     * View} for the given position.
     *
     * @param position    The position for which to get the correct {@link View}
     * @param convertView The old {@link View}, which we might be able to recycle
     * @param parent      parental {@link ViewGroup}
     * @return the correct header {@link View} for the given position.
     */
    @Override
    public View getHeaderView(int position, View convertView, ViewGroup parent) {
        return new View(TomahawkApp.getContext());
    }

    /**
     * This method is being called by the StickyListHeaders library. Returns the same value for each
     * item that should be grouped under the same header.
     *
     * @param position the position of the item for which to get the header id
     * @return the same value for each item that should be grouped under the same header.
     */
    @Override
    public long getHeaderId(int position) {
        return 0;
    }

    private int getViewType(FakePreferenceGroup.FakePreference item) {
        if (item.getType() == FakePreferenceGroup.FAKEPREFERENCE_TYPE_CHECKBOX) {
            return R.layout.fake_preferences_checkbox;
        } else if (item.getType() == FakePreferenceGroup.FAKEPREFERENCE_TYPE_SPINNER) {
            return R.layout.fake_preferences_spinner;
        } else if (item.getType() == FakePreferenceGroup.FAKEPREFERENCE_TYPE_SEEKBAR){
            return R.layout.fake_preferences_seekbar;
        } else if (item.getType() == FakePreferenceGroup.FAKEPREFERENCE_TYPE_BUTTON){
            return R.layout.fake_preferences_button;
        }
        return R.layout.fake_preferences_plain;
    }

}
