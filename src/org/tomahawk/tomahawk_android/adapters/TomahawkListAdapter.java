/* == This file is part of Tomahawk Player - <http://tomahawk-player.org> ===
 *
 *   Copyright 2012, Enno Gottschalk <mrmaffen@googlemail.com>
 *
 *   Tomahawk is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Tomahawk is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Tomahawk. If not, see <http://www.gnu.org/licenses/>.
 */
package org.tomahawk.tomahawk_android.adapters;

import android.content.Intent;
import android.content.res.Resources;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.Toast;

import org.jdeferred.DoneCallback;
import org.tomahawk.libtomahawk.collection.Album;
import org.tomahawk.libtomahawk.collection.Artist;
import org.tomahawk.libtomahawk.collection.Collection;
import org.tomahawk.libtomahawk.collection.CollectionManager;
import org.tomahawk.libtomahawk.collection.Folder;
import org.tomahawk.libtomahawk.collection.Genre;
import org.tomahawk.libtomahawk.collection.ListItemString;
import org.tomahawk.libtomahawk.collection.Playlist;
import org.tomahawk.libtomahawk.collection.PlaylistEntry;
import org.tomahawk.libtomahawk.collection.Track;
import org.tomahawk.libtomahawk.infosystem.User;
import org.tomahawk.libtomahawk.resolver.Query;
import org.tomahawk.libtomahawk.resolver.Resolver;
import org.tomahawk.libtomahawk.resolver.models.ActionItem;
import org.tomahawk.libtomahawk.utils.ViewUtils;
import org.tomahawk.tomahawk_android.R;
import org.tomahawk.tomahawk_android.TomahawkApp;
import org.tomahawk.tomahawk_android.activities.TomahawkMainActivity;
import org.tomahawk.tomahawk_android.dialogs.CreatePlaylistDialog;
import org.tomahawk.tomahawk_android.fragments.ContentHeaderFragment;
import org.tomahawk.tomahawk_android.fragments.PlaylistsFragment;
import org.tomahawk.tomahawk_android.fragments.TomahawkFragment;
import org.tomahawk.tomahawk_android.menuItem.QuickAction;
import org.tomahawk.tomahawk_android.services.PlaybackService;
import org.tomahawk.tomahawk_android.utils.MultiColumnClickListener;
import org.tomahawk.tomahawk_android.views.BiDirectionalFrame;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import de.greenrobot.event.EventBus;
import se.emilsjolander.stickylistheaders.StickyListHeadersListView;

/*import com.daimajia.swipe.SwipeLayout;
import com.daimajia.swipe.implments.SwipeItemAdapterMangerImpl;
import com.daimajia.swipe.interfaces.SwipeAdapterInterface;
import com.daimajia.swipe.interfaces.SwipeItemMangerInterface;
import com.daimajia.swipe.util.Attributes;*/

/**
 * This class is used to populate a {@link se.emilsjolander.stickylistheaders.StickyListHeadersListView}.
 * SwipeItemMangerInterface,
 SwipeAdapterInterface
 */
public class TomahawkListAdapter extends StickyBaseAdapter implements
        AdapterView.OnItemClickListener, AdapterView.OnItemLongClickListener
        {

    private static final String TAG = TomahawkListAdapter.class.getSimpleName();

    private final TomahawkMainActivity mActivity;

    private List<Segment> mSegments;

    private int mRowCount;

    private Collection mCollection;

    private final MultiColumnClickListener mClickListener;

    private final LayoutInflater mLayoutInflater;

    private boolean mShowPlaystate = false;

    private PlaylistEntry mHighlightedPlaylistEntry;

    private Query mHighlightedQuery;

    private boolean mHighlightedItemIsPlaying = false;

    private int mHeaderSpacerHeight = 0;
private int mCurrentHeaderState;
    private View mHeaderSpacerForwardView;

            private boolean recentlyAdded = false;


            private int mFooterSpacerHeight = 0;

    private ProgressBar mProgressBar;

            private final int ID_ENQUEE = 1;
            private final int ID_PLAY = 2;
            private final int ID_SHUFFLE = 3;
            private final int ID_ADD_TO_PLAYLIST = 4;
            private final int ID_DELETE = 5;
            private final int ID_LIKE = 6;
            private final int ID_RENAME = 7;
            private final int ID_RINGTONE = 8;
            private final int ID_SHARE = 9;

            boolean isAlbum = false;
            boolean isArtist = false;
            boolean isGenre = false;
            boolean isFolder = false;
            boolean isTrack = false;

    //For ALbum Context Menu
    ActionItem enqueeItem 		= new ActionItem(ID_ENQUEE, "Add to queue", TomahawkApp.getContext().getResources().getDrawable(R.drawable.ic_action_charts));
            ActionItem playItem 	= new ActionItem(ID_PLAY, "Play", TomahawkApp.getContext().getResources().getDrawable(R.drawable.ic_player_play_light));
            ActionItem shuffleItem 	= new ActionItem(ID_SHUFFLE, "Shuffle", TomahawkApp.getContext().getResources().getDrawable(R.drawable.ic_player_shuffle_light));
            ActionItem addPlaylistItem 	= new ActionItem(ID_ADD_TO_PLAYLIST, "Add to playlist", TomahawkApp.getContext().getResources().getDrawable(R.drawable.ic_action_more));
            ActionItem deleteItem 	= new ActionItem(ID_DELETE, "Delete", TomahawkApp.getContext().getResources().getDrawable(R.drawable.ic_player_exit_light));
            ActionItem likeItem 	= new ActionItem(ID_LIKE, "Like", TomahawkApp.getContext().getResources().getDrawable(R.drawable.ic_action_favorites));
            ActionItem renameItem 	= new ActionItem(ID_RENAME, "Rename", TomahawkApp.getContext().getResources().getDrawable(R.drawable.ic_action_favorites));
            ActionItem ringtoneItem 	= new ActionItem(ID_RINGTONE, "Ringtone", TomahawkApp.getContext().getResources().getDrawable(R.drawable.ic_action_favorites));
            ActionItem shareItem 	= new ActionItem(ID_SHARE, "Share", TomahawkApp.getContext().getResources().getDrawable(R.drawable.ic_action_favorites));


    private final QuickAction quickAction;
            private final QuickAction quickActionPlaylist;
            private final QuickAction quickActionTrack;
    //private final SwipeItemAdapterMangerImpl mItemManager = new SwipeItemAdapterMangerImpl(this);

            @Override
            public int getViewTypeCount() {
                return super.getViewTypeCount();
            }

            /**
     * Constructs a new {@link TomahawkListAdapter}.
     */
    public TomahawkListAdapter(TomahawkMainActivity activity, LayoutInflater layoutInflater,
            List<Segment> segments, Collection collection, StickyListHeadersListView listView,
            MultiColumnClickListener clickListener) {
        mActivity = activity;
        mLayoutInflater = layoutInflater;
        mClickListener = clickListener;
        setSegments(segments);
        updateFooterSpacerHeight(listView);
        quickAction = new QuickAction(mActivity);
        quickActionPlaylist = new QuickAction(mActivity);
        quickActionTrack = new QuickAction(mActivity);
        initQuickListener();
        initQuickPlaylistListener();
        initQuickTrackListener();
        //mItemManager.setMode(Attributes.Mode.Single);
        mCollection = collection;

    }

    /**
     * Constructs a new {@link TomahawkListAdapter}.
     */
    public TomahawkListAdapter(TomahawkMainActivity activity, LayoutInflater layoutInflater,
            List<Segment> segments, StickyListHeadersListView listView,
            MultiColumnClickListener clickListener) {
        mActivity = activity;
        mLayoutInflater = layoutInflater;
        mClickListener = clickListener;
        setSegments(segments);
        updateFooterSpacerHeight(listView);
        quickAction = new QuickAction(mActivity);
        quickActionPlaylist = new QuickAction(mActivity);
        quickActionTrack = new QuickAction(mActivity);
        initQuickListener();
        initQuickPlaylistListener();
        initQuickTrackListener();
        //mItemManager.setMode(Attributes.Mode.Single);
    }

    /**
     * Constructs a new {@link TomahawkListAdapter}.
     */
    public TomahawkListAdapter(TomahawkMainActivity activity, LayoutInflater layoutInflater,
                               Segment segment, StickyListHeadersListView listView,
            MultiColumnClickListener clickListener) {
        mActivity = activity;
        mLayoutInflater = layoutInflater;
        mClickListener = clickListener;
        List<Segment> segments = new ArrayList<>();
        segments.add(segment);
        setSegments(segments);
        updateFooterSpacerHeight(listView);
        quickAction = new QuickAction(mActivity);
        quickActionPlaylist = new QuickAction(mActivity);
        quickActionTrack = new QuickAction(mActivity);
        initQuickListener();
        initQuickPlaylistListener();
        initQuickTrackListener();
        //mItemManager.setMode(Attributes.Mode.Single);
    }

            public void initQuickPlaylistListener(){
                /*quickActionPlaylist.getActionItemList().clear();
                quickActionPlaylist.addActionItem(playItem);
                quickActionPlaylist.addActionItem(deleteItem);
                quickActionPlaylist.addActionItem(renameItem);*/
                quickActionPlaylist.setOnActionItemClickListener(new QuickAction.OnActionItemClickListener() {
                    @Override
                    public void onItemClick(QuickAction source, int pos, int actionId) {
                        ActionItem actionItem = quickActionPlaylist.getActionItem(pos);
                        switch (actionId) {
                            case ID_PLAY:
                                if (mCollection != null) {
                                    mCollection.getQueries(Collection.SORT_ALPHA, ((Playlist) quickActionPlaylist.getItem()).getId()).done(new DoneCallback<Playlist>() {
                                        @Override
                                        public void onDone(Playlist result) {
                                            final PlaybackService playbackService = mActivity.getPlaybackService();
                                            PlaylistEntry entry = (PlaylistEntry) result.getEntries().get(0);
                                            if (playbackService != null) {
                                                if (playbackService.getCurrentEntry() == entry) {
                                                    playbackService.playPause();
                                                } else {
                                                    playbackService.setPlaylist(result, entry);
                                                    playbackService.start();
                                                }
                                            }
                                        }
                                    });

                                }
                                break;
                            case ID_DELETE:

                                Bundle bundle = new Bundle();
                                if (mCollection != null) {
                                    bundle.putString(TomahawkFragment.COLLECTION_ID, mCollection.getId());
                                }
                                bundle.putString(TomahawkFragment.DE_TITLE,
                                        "Permanently delete " + ((((Playlist) quickActionPlaylist.getItem()).getName() != null) ? ((Playlist) quickActionPlaylist.getItem()).getName() : ""));
                                bundle.putString(TomahawkFragment.PLAYLIST, (((Playlist) quickActionPlaylist.getItem()).getId()));
                                bundle.putInt(TomahawkFragment.DE_DIALOG_TYPE,
                                        TomahawkFragment.DELETE_DIALOG);
                                //FragmentUtils.replace(activity, PlaylistsFragment.class, bundle);
                                CreatePlaylistDialog dialog = new CreatePlaylistDialog();
                                dialog.setArguments(bundle);
                                dialog.show(mActivity.getSupportFragmentManager(), null);


                                break;
                            case ID_RENAME:
                                if (((Playlist) quickActionPlaylist.getItem()).getId() != null) {
                                    CreatePlaylistDialog dialog1 = new CreatePlaylistDialog();
                                    Bundle b = new Bundle();
                                    b.putInt(TomahawkFragment.DE_DIALOG_TYPE,
                                            TomahawkFragment.RENAME_PLAYLIST_DIALOG);
                                    b.putString(TomahawkFragment.DE_TITLE,
                                            mActivity.getResources().getString(R.string.context_menu_rename));
                                    b.putString(TomahawkFragment.PLAYLIST, ((Playlist) quickActionPlaylist.getItem()).getId());
                                    dialog1.setArguments(b);
                                    dialog1.show(mActivity.getSupportFragmentManager(), null);


                                }
                                break;
                        }
                    }
                });
                quickActionPlaylist.setOnDismissListener(new QuickAction.OnDismissListener() {
                    @Override
                    public void onDismiss() {
                        Toast.makeText(TomahawkApp.getContext(), "Ups..dismissed", Toast.LENGTH_SHORT).show();
                    }
                });
            }

            public void initQuickListener() {
                /*quickAction.getActionItemList().clear();
                quickAction.addActionItem(enqueeItem);
                quickAction.addActionItem(playItem);
                //quickAction.addActionItem(shuffleItem);
                quickAction.addActionItem(addPlaylistItem);
                quickAction.addActionItem(deleteItem);
                quickAction.addActionItem(likeItem);*/

                //setup the action item click listener
                quickAction.setOnActionItemClickListener(new QuickAction.OnActionItemClickListener() {
                    @Override
                    public void onItemClick(QuickAction quickAction, int pos, int actionId) {
                        ActionItem actionItem = quickAction.getActionItem(pos);
                        isAlbum = false;
                        isArtist = false;
                        isGenre = false;
                        isFolder = false;
                        isTrack = false;
                        switch (actionId){
                            case ID_ENQUEE:
                               if (mCollection!= null) {
                                   if (quickAction.getItem() instanceof Album) {
                                       mCollection.getAlbumTracks((Album) quickAction.getItem()).done(new DoneCallback<Playlist>() {
                                           @Override
                                           public void onDone(Playlist playlist) {
                                               List<Query> queries = new ArrayList<>();
                                               ((TomahawkMainActivity) mActivity).getPlaybackService().addToPlayList((ArrayList<PlaylistEntry>) playlist.getEntries());
                                               }
                                       });
                                   }else if (quickAction.getItem() instanceof Folder) {
                                       mCollection.getFolderTracks((Folder) quickAction.getItem()).done(new DoneCallback<Playlist>() {
                                           @Override
                                           public void onDone(Playlist playlist) {
                                               List<Query> queries = new ArrayList<>();
                                               ((TomahawkMainActivity) mActivity).getPlaybackService().addToPlayList((ArrayList<PlaylistEntry>) playlist.getEntries());

                                           }
                                       });
                                   }else if (quickAction.getItem() instanceof Artist) {
                                       mCollection.getArtistTracks((Artist) quickAction.getItem()).done(new DoneCallback<Playlist>() {
                                           @Override
                                           public void onDone(Playlist playlist) {
                                               List<Query> queries = new ArrayList<>();
                                               ((TomahawkMainActivity) mActivity).getPlaybackService().addToPlayList((ArrayList<PlaylistEntry>) playlist.getEntries());

                                           }
                                       });
                                   }else if (quickAction.getItem() instanceof Genre) {
                                       mCollection.getGenreTracks((Genre) quickAction.getItem()).done(new DoneCallback<Playlist>() {
                                           @Override
                                           public void onDone(Playlist playlist) {
                                               List<Query> queries = new ArrayList<>();
                                               ((TomahawkMainActivity) mActivity).getPlaybackService().addToPlayList((ArrayList<PlaylistEntry>) playlist.getEntries());

                                           }
                                       });
                                   }
                               }
                                break;
                            case ID_PLAY:
                                if (mCollection != null){
                                    if (quickAction.getItem() instanceof Album) {
                                        mCollection.getAlbumTracks((Album) quickAction.getItem()).done(new DoneCallback<Playlist>() {
                                            @Override
                                            public void onDone(Playlist result) {
                                                final PlaybackService playbackService = mActivity.getPlaybackService();
                                                PlaylistEntry entry = (PlaylistEntry) result.getEntries().get(0);
                                                if (playbackService != null) {
                                                    if (playbackService.getCurrentEntry() == entry) {
                                                        playbackService.playPause();
                                                    } else {
                                                        playbackService.setPlaylist(result, entry);
                                                        playbackService.start();
                                                    }
                                                }
                                            }
                                        });
                                    }else if (quickAction.getItem() instanceof Folder) {
                                        mCollection.getFolderTracks((Folder) quickAction.getItem()).done(new DoneCallback<Playlist>() {
                                            @Override
                                            public void onDone(Playlist result) {
                                                final PlaybackService playbackService = mActivity.getPlaybackService();
                                                PlaylistEntry entry = (PlaylistEntry) result.getEntries().get(0);
                                                if (playbackService != null) {
                                                    if (playbackService.getCurrentEntry() == entry) {
                                                        playbackService.playPause();
                                                    } else {
                                                        playbackService.setPlaylist(result, entry);
                                                        playbackService.start();
                                                    }
                                                }
                                            }
                                        });
                                    }else if (quickAction.getItem() instanceof Artist) {
                                        mCollection.getArtistTracks((Artist) quickAction.getItem()).done(new DoneCallback<Playlist>() {
                                            @Override
                                            public void onDone(Playlist result) {
                                                final PlaybackService playbackService = mActivity.getPlaybackService();
                                                PlaylistEntry entry = (PlaylistEntry) result.getEntries().get(0);
                                                if (playbackService != null) {
                                                    if (playbackService.getCurrentEntry() == entry) {
                                                        playbackService.playPause();
                                                    } else {
                                                        playbackService.setPlaylist(result, entry);
                                                        playbackService.start();
                                                    }
                                                }
                                            }
                                        });
                                    }else if (quickAction.getItem() instanceof Genre) {
                                        mCollection.getGenreTracks((Genre) quickAction.getItem()).done(new DoneCallback<Playlist>() {
                                            @Override
                                            public void onDone(Playlist result) {
                                                final PlaybackService playbackService = mActivity.getPlaybackService();
                                                PlaylistEntry entry = (PlaylistEntry) result.getEntries().get(0);
                                                if (playbackService != null) {
                                                    if (playbackService.getCurrentEntry() == entry) {
                                                        playbackService.playPause();
                                                    } else {
                                                        playbackService.setPlaylist(result, entry);
                                                        playbackService.start();
                                                    }
                                                }
                                            }
                                        });
                                    }
                                }
                                break;
                            case ID_ADD_TO_PLAYLIST:
                                if (mCollection != null) {
                                    if (quickAction.getItem() instanceof Album) {
                                        mCollection.getAlbumTracks((Album) quickAction.getItem()).done(new DoneCallback<Playlist>() {
                                            @Override
                                            public void onDone(Playlist playlist) {
                                                List<PlaylistEntry> entries = playlist.getEntries();
                                                List<Query> queries = new ArrayList<>();
                                                for (PlaylistEntry entry : entries) {
                                                    queries.add(entry.getQuery());
                                                }
                                                showAddToPlaylist(mActivity, queries, TomahawkFragment.ADD_TO_PLAYLIST_DIALOG, null, entries);
                                            }
                                        });
                                    }else if (quickAction.getItem() instanceof Folder) {
                                        mCollection.getFolderTracks((Folder) quickAction.getItem()).done(new DoneCallback<Playlist>() {
                                            @Override
                                            public void onDone(Playlist result) {
                                                List<PlaylistEntry> entries = result.getEntries();
                                                List<Query> queries = new ArrayList<>();
                                                for (PlaylistEntry entry : entries) {
                                                    queries.add(entry.getQuery());
                                                }
                                                showAddToPlaylist(mActivity, queries, TomahawkFragment.ADD_TO_PLAYLIST_DIALOG, null, entries);

                                            }
                                        });
                                    }else if (quickAction.getItem() instanceof Artist) {
                                        mCollection.getArtistTracks((Artist) quickAction.getItem()).done(new DoneCallback<Playlist>() {
                                            @Override
                                            public void onDone(Playlist result) {
                                                List<PlaylistEntry> entries = result.getEntries();
                                                List<Query> queries = new ArrayList<>();
                                                for (PlaylistEntry entry : entries) {
                                                    queries.add(entry.getQuery());
                                                }
                                                showAddToPlaylist(mActivity, queries, TomahawkFragment.ADD_TO_PLAYLIST_DIALOG, null, entries);

                                            }
                                        });
                                    }else if (quickAction.getItem() instanceof Genre) {
                                        mCollection.getGenreTracks((Genre) quickAction.getItem()).done(new DoneCallback<Playlist>() {
                                            @Override
                                            public void onDone(Playlist result) {
                                                List<PlaylistEntry> entries = result.getEntries();
                                                List<Query> queries = new ArrayList<>();
                                                for (PlaylistEntry entry : entries) {
                                                    queries.add(entry.getQuery());
                                                }
                                                showAddToPlaylist(mActivity, queries, TomahawkFragment.ADD_TO_PLAYLIST_DIALOG, null, entries);

                                            }
                                        });
                                    }
                                }
                                break;
                            case ID_SHUFFLE:
                                break;
                            case ID_DELETE:
                                if (mCollection != null) {
                                    if (quickAction.getItem() instanceof Album) {
                                        final String name = ((Album) quickAction.getItem()).getName();
                                        if (quickAction.getItem() instanceof Album) {
                                            isAlbum = true;
                                        }
                                        mCollection.getAlbumTracks((Album) quickAction.getItem()).done(new DoneCallback<Playlist>() {
                                            @Override
                                            public void onDone(Playlist playlist) {
                                                List<PlaylistEntry> entries = playlist.getEntries();
                                                List<Query> queries = new ArrayList<>();
                                                for (PlaylistEntry entry : entries) {
                                                    queries.add(entry.getQuery());
                                                }
                                                showAddToPlaylist(mActivity, queries, TomahawkFragment.DELETE_DIALOG, name, entries);
                                            }
                                        });
                                    }else if (quickAction.getItem() instanceof Folder) {
                                        final String name = ((Folder) quickAction.getItem()).getName()+";"+((Folder) quickAction.getItem()).getFolderPath();
                                        if (quickAction.getItem() instanceof Folder) {
                                            isFolder = true;
                                        }
                                        mCollection.getFolderTracks((Folder) quickAction.getItem()).done(new DoneCallback<Playlist>() {
                                            @Override
                                            public void onDone(Playlist result) {
                                                List<PlaylistEntry> entries = result.getEntries();
                                                List<Query> queries = new ArrayList<>();
                                                for (PlaylistEntry entry : entries) {
                                                    queries.add(entry.getQuery());
                                                }
                                                showAddToPlaylist(mActivity, queries, TomahawkFragment.DELETE_DIALOG, name, entries);

                                            }
                                        });
                                    }else if (quickAction.getItem() instanceof Artist) {
                                        final String name = ((Artist) quickAction.getItem()).getName();
                                        if (quickAction.getItem() instanceof Artist) {
                                            isArtist = true;
                                        }
                                        mCollection.getArtistTracks((Artist) quickAction.getItem()).done(new DoneCallback<Playlist>() {
                                            @Override
                                            public void onDone(Playlist result) {
                                                List<PlaylistEntry> entries = result.getEntries();
                                                List<Query> queries = new ArrayList<>();
                                                for (PlaylistEntry entry : entries) {
                                                    queries.add(entry.getQuery());
                                                }
                                                showAddToPlaylist(mActivity, queries, TomahawkFragment.DELETE_DIALOG, name, entries);

                                            }
                                        });
                                    }else if (quickAction.getItem() instanceof Genre) {
                                        final String name = ((Genre) quickAction.getItem()).getName();
                                        if (quickAction.getItem() instanceof Genre) {
                                            isArtist = true;
                                        }
                                        mCollection.getGenreTracks((Genre) quickAction.getItem()).done(new DoneCallback<Playlist>() {
                                            @Override
                                            public void onDone(Playlist result) {
                                                List<PlaylistEntry> entries = result.getEntries();
                                                List<Query> queries = new ArrayList<>();
                                                for (PlaylistEntry entry : entries) {
                                                    queries.add(entry.getQuery());
                                                }
                                                showAddToPlaylist(mActivity, queries, TomahawkFragment.DELETE_DIALOG, name, entries);

                                            }
                                        });
                                    }
                                }
                                break;
                            case ID_LIKE:
                                if (quickAction.getItem() instanceof Album) {
                                    Album album = (Album)quickAction.getItem();
                                    if (album.getmLike() == 0){
                                        album.setmLike(1);
                                    }else {
                                        album.setmLike(0);
                                    }
                                    CollectionManager.get().toggleLovedItem(album);
                                }else if (quickAction.getItem() instanceof Artist) {
                                    Artist album = (Artist)quickAction.getItem();
                                    if (album.getmLike() == 0){
                                        album.setmLike(1);
                                    }else {
                                        album.setmLike(0);
                                    }
                                    CollectionManager.get().toggleLovedItem(album);
                                }else if (quickAction.getItem() instanceof Genre) {
                                    Genre album = (Genre)quickAction.getItem();
                                    if (album.getmLike() == 0){
                                        album.setmLike(1);
                                    }else {
                                        album.setmLike(0);
                                    }
                                    CollectionManager.get().toggleLovedItem(album);
                                }else if (quickAction.getItem() instanceof Folder) {
                                    Folder album = (Folder)quickAction.getItem();
                                    if (album.getmLike() == 0){
                                        album.setmLike(1);
                                    }else {
                                        album.setmLike(0);
                                    }
                                    CollectionManager.get().toggleLovedItem(album);
                                }
                                /*if ((Album)quickAction.getItem() != null) {
                                    CollectionManager.get().toggleLovedItem((Album) quickAction.getItem());
                                }*/
                                break;
                            case ID_RENAME:
                                break;
                        }

                    }
                });

                quickAction.setOnDismissListener(new QuickAction.OnDismissListener() {
                    @Override
                    public void onDismiss() {
                        Toast.makeText(TomahawkApp.getContext(), "Ups..dismissed", Toast.LENGTH_SHORT).show();
                    }
                });
            }


            public void initQuickTrackListener(){
                /*quickActionTrack.getActionItemList().clear();
                quickActionTrack.addActionItem(enqueeItem);
                quickActionTrack.addActionItem(playItem);
                //quickAction.addActionItem(shuffleItem);
                quickActionTrack.addActionItem(addPlaylistItem);
                quickActionTrack.addActionItem(deleteItem);
                quickActionTrack.addActionItem(likeItem);
                quickActionTrack.addActionItem(ringtoneItem);
                quickActionTrack.addActionItem(shareItem);*/


                //setup the action item click listener
                quickActionTrack.setOnActionItemClickListener(new QuickAction.OnActionItemClickListener() {
                    @Override
                    public void onItemClick(QuickAction quickAction, int pos, int actionId) {
                        ActionItem actionItem = quickAction.getActionItem(pos);
                        isAlbum = false;
                        isArtist = false;
                        isGenre = false;
                        isFolder = false;
                        isTrack = false;
                        switch (actionId) {
                            case ID_ENQUEE:
                                if (mCollection != null) {
                                    if (quickAction.getItem() instanceof Query) {
                                        Query query = (Query) quickAction.getItem();
                                        Playlist mPlaylist = ((TomahawkMainActivity) mActivity).getPlaybackService().getPlaylist();
                                        PlaylistEntry entry = PlaylistEntry.get(mPlaylist.getId(), query, query.getBasicTrack().getTrackId());
                                        ArrayList<PlaylistEntry> entries = new ArrayList<PlaylistEntry>();
                                        entries.add(entry);
                                        ((TomahawkMainActivity) mActivity).getPlaybackService().addToPlayList(entries);

                                    }
                                }
                                break;
                            case ID_PLAY:
                                if (mCollection != null) {
                                    if (quickAction.getItem() instanceof Query) {
                                        Query query = (Query) quickAction.getItem();
                                        final PlaybackService playbackService = mActivity.getPlaybackService();

                                        Playlist mPlaylist = ((TomahawkMainActivity) mActivity).getPlaybackService().getPlaylist();
                                        PlaylistEntry entry = PlaylistEntry.get(mPlaylist.getId(), query, query.getBasicTrack().getTrackId());
                                        if (playbackService != null) {
                                            if (playbackService.getCurrentEntry() == entry) {
                                                playbackService.playPause();
                                            } else {
                                                playbackService.setPlaylist(mPlaylist, entry);
                                                playbackService.start();
                                            }
                                        }
                                    }

                                }
                                break;
                            case ID_ADD_TO_PLAYLIST:
                                if (mCollection != null) {
                                    if (quickAction.getItem() instanceof Query) {
                                        List<Query> queries = new ArrayList<>();
                                        queries.add((Query) quickAction.getItem());

                                        Query query = (Query) quickAction.getItem();
                                        Playlist mPlaylist = ((TomahawkMainActivity) mActivity).getPlaybackService().getPlaylist();
                                        PlaylistEntry entry = PlaylistEntry.get(mPlaylist.getId(), query, query.getBasicTrack().getTrackId());
                                        ArrayList<PlaylistEntry> entries = new ArrayList<PlaylistEntry>();
                                        entries.add(entry);
                                        showAddToPlaylist(mActivity, queries, TomahawkFragment.ADD_TO_PLAYLIST_DIALOG, null, entries);


                                    }
                                }
                                break;
                            case ID_SHUFFLE:
                                break;
                            case ID_DELETE:
                                if (mCollection != null) {
                                    ActionItem actionItem1 = quickAction.getActionItem(pos);
                                    if (actionItem1.getTitle().equalsIgnoreCase("remove")) {
                                        if (quickAction.getItem() instanceof Query) {
                                            int which = quickAction.getCurrentPosition();
                                            final Query query =(Query)quickAction.getItem();
                                            final String mPlaylistId = ((TomahawkMainActivity) mActivity).getPlaybackService().getPlaylistSelectedId();

                                            Log.d("T1", "_playlistname"+mPlaylistId);
                                            mCollection.getQueries(which,mPlaylistId,"").done(new DoneCallback<Cursor>() {
                                                @Override
                                                public void onDone(Cursor result) {
                                                    if (result!=null){
                                                        PlaylistEntry entry = PlaylistEntry.get(mPlaylistId, query, query.getBasicTrack().getTrackId());
                                                        CollectionManager.get().deletePlaylistEntry(mPlaylistId,
                                                                entry.getId());
                                                        CollectionManager.UpdatedEvent event = new CollectionManager.UpdatedEvent();
                                                        event.mUpdatedItemIds = new HashSet<>();
                                                        event.mUpdatedItemIds.add(query.getCacheKey());
                                                        EventBus.getDefault().post(new CollectionManager.UpdatedEvent());
                                                    }
                                                }
                                            });
                                             /*{
                                                int colidx = mTrackCursor
                                                        .getColumnIndexOrThrow(MediaStore.Audio.Playlists.Members._ID);
                                                mTrackCursor.moveToPosition(which);
                                                long id = mTrackCursor.getLong(colidx);
                                                Uri uri = MediaStore.Audio.Playlists.Members.getContentUri(
                                                        "external", Long.valueOf(getmPlaylist()));
                                                getActivity().getContentResolver().delete(
                                                        ContentUris.withAppendedId(uri, id), null, null);
                                            }*/
                                        }
                                    } else {
                                        if (quickAction.getItem() instanceof Query) {
                                            final String name = ((Query) quickAction.getItem()).getName();
                                            if (quickAction.getItem() instanceof Query) {
                                                isTrack = true;
                                            }
                                            List<Query> queries = new ArrayList<>();
                                            queries.add((Query) quickAction.getItem());

                                            Query query = (Query) quickAction.getItem();
                                            Playlist mPlaylist = ((TomahawkMainActivity) mActivity).getPlaybackService().getPlaylist();
                                            PlaylistEntry entry = PlaylistEntry.get(mPlaylist.getId(), query, query.getBasicTrack().getTrackId());
                                            ArrayList<PlaylistEntry> entries = new ArrayList<PlaylistEntry>();
                                            entries.add(entry);
                                            showAddToPlaylist(mActivity, queries, TomahawkFragment.DELETE_DIALOG, name, entries);


                                        }
                                    }
                                }

                        break;
                        case ID_LIKE:
                                /*if ((Album)quickAction.getItem() != null) {
                                    CollectionManager.get().toggleLovedItem((Album) quickAction.getItem());
                                }*/
                        if (quickAction.getItem() instanceof Query) {
                            Query album = (Query) quickAction.getItem();
                            if (album.getBasicTrack().getmLike() == 0) {
                                album.getBasicTrack().setmLike(1);
                            } else {
                                album.getBasicTrack().setmLike(0);
                            }
                            CollectionManager.get().toggleLovedItem(album.getBasicTrack());
                        }
                        break;
                        case ID_RINGTONE:
                        if (quickAction.getItem() instanceof Query) {
                            final String name = ((Query) quickAction.getItem()).getName();
                            Query query = (Query) quickAction.getItem();

                            Bundle b = new Bundle();
                            String f;
                            if (android.os.Environment.isExternalStorageRemovable()) {
                                f = mActivity.getString(R.string.ringtone_song_desc);
                            } else {
                                f = mActivity.getString(R.string.ringtone_song_desc_nosdcard);
                            }
                            String desc = String.format(f, name);

                            b.putInt(TomahawkFragment.DE_DIALOG_TYPE, TomahawkFragment.RINGTONE_DIALOG);
                            b.putString(TomahawkFragment.DE_TITLE, "Ringtone");
                            b.putString(TomahawkFragment.DE_LINE_1, desc);
                            try {
                                b.putLong(TomahawkFragment.AUDIO_ID, Long.valueOf(query.getBasicTrack().getTrackId()));
                            } catch (Exception e1) {
                                // TODO Auto-generated catch block
                                e1.printStackTrace();
                            }
                            CreatePlaylistDialog dialog = new CreatePlaylistDialog();
                            dialog.setArguments(b);
                            dialog.show(mActivity.getSupportFragmentManager(), null);
                        }
                        break;
                        case ID_SHARE:
                        if (quickAction.getItem() instanceof Query) {
                            final String name = ((Query) quickAction.getItem()).getName();
                            Query query = (Query) quickAction.getItem();
                            String sharePath = query.getBasicTrack().getPath();
                            Uri uri = Uri.parse("file:///" + sharePath);
                            Intent share = new Intent(Intent.ACTION_SEND);
                            share.setType("audio/*");
                            share.putExtra(Intent.EXTRA_STREAM, uri);
                            mActivity.startActivity(Intent.createChooser(share, "Share Sound File"));
                        }
                        break;
                    }

                }
            });

                quickActionTrack.setOnDismissListener(new QuickAction.OnDismissListener() {
                    @Override
                    public void onDismiss() {
                        Toast.makeText(TomahawkApp.getContext(), "Ups..dismissed", Toast.LENGTH_SHORT).show();
                    }
                });
            }

            private void showAddToPlaylist(final TomahawkMainActivity activity, final List<Query> queries,final int type, final String name, final List<PlaylistEntry> entries) {
                User.getSelf().done(new DoneCallback<User>() {
                    @Override
                    public void onDone(User user) {
                        ArrayList<String> queryKeys = new ArrayList<>();
                        for (Query query : queries) {
                            queryKeys.add(query.getCacheKey());
                        }
                        ArrayList<String> entryKeys = new ArrayList<>();
                        for (PlaylistEntry query : entries) {
                            entryKeys.add(query.getCacheKey());
                        }
                        Bundle bundle = new Bundle();
                        if (mCollection != null) {
                            bundle.putString(TomahawkFragment.COLLECTION_ID, mCollection.getId());
                        }
                        if (type == TomahawkFragment.DELETE_DIALOG) {
                            if (name.contains(";")) {
                                bundle.putString(TomahawkFragment.DE_TITLE,
                                        "Permanently delete " + ((name != null) ? name.split(";")[1] : ""));
                            } else
                                bundle.putString(TomahawkFragment.DE_TITLE,
                                        "Permanently delete " + ((name != null) ? name : ""));
                            if (isAlbum)
                                bundle.putString(TomahawkFragment.ALBUM_NAME, name);
                            else if (isFolder) {
                                bundle.putString(TomahawkFragment.FOLDER_NAME, name.split(";")[0]);
                                bundle.putString(TomahawkFragment.FOLDER_PATH, name.split(";")[1]);
                            } else if (isArtist) {
                                bundle.putString(TomahawkFragment.ARTIST_NAME, name);
                            } else if (isGenre) {
                                bundle.putString(TomahawkFragment.GENRE_NAME, name);
                            }
                        }
                        bundle.putString(TomahawkFragment.USER, user.getId());
                        bundle.putStringArrayList(TomahawkFragment.QUERYARRAY, queryKeys);
                        bundle.putStringArrayList(TomahawkFragment.ENTRYARRAY, entryKeys);
                        bundle.putInt(TomahawkFragment.DE_DIALOG_TYPE,
                                type);
                        //FragmentUtils.replace(activity, PlaylistsFragment.class, bundle);
                        CreatePlaylistDialog dialog = new CreatePlaylistDialog();
                        dialog.setArguments(bundle);
                        dialog.show(mActivity.getSupportFragmentManager(), null);
                    }
                });
            }
    /**
     * Set the complete list of {@link Segment}
     */
    public void setSegments(List<Segment> segments, StickyListHeadersListView listView) {
        setSegments(segments);

        updateFooterSpacerHeight(listView);
        notifyDataSetChanged();
    }

    private void setSegments(List<Segment> segments) {
        closeSegments();
        mSegments = segments;
        mRowCount = 0;
        for (Segment segment : mSegments) {
            mRowCount += segment.getRowCount();
        }
    }

    public void closeSegments() {
        if (mSegments != null) {
            for (Segment segment : mSegments) {
                segment.close();
            }
        }
    }

    public void setShowContentHeaderSpacer(int headerSpacerHeight,
            StickyListHeadersListView listView, View headerSpacerForwardView) {
        mHeaderSpacerHeight = headerSpacerHeight;
        mHeaderSpacerForwardView = headerSpacerForwardView;
        updateFooterSpacerHeight(listView);
    }

    /**
     * Set whether or not to highlight the currently playing {@link Query} and show the play/pause
     * state
     */
    public void setShowPlaystate(boolean showPlaystate) {
        this.mShowPlaystate = showPlaystate;
    }

    public void setHighlightedItemIsPlaying(boolean highlightedItemIsPlaying) {
        mHighlightedItemIsPlaying = highlightedItemIsPlaying;
    }

            @Override
            public void setPlayBackView(int currentState) {
                this.mCurrentHeaderState = currentState;
            }

            /**
     * set the position of the item, which should be highlighted
     */
    public void setHighlightedQuery(Query query) {
        mHighlightedQuery = query;
    }

    /**
     * set the position of the item, which should be highlighted
     */
    public void setHighlightedEntry(PlaylistEntry entry) {
        mHighlightedPlaylistEntry = entry;
    }

    /**
     * Get the correct {@link View} for the given position.
     *
     * @param position    The position for which to get the correct {@link View}
     * @param convertView The old {@link View}, which we might be able to recycle
     * @param parent      parental {@link ViewGroup}
     * @return the correct {@link View} for the given position.
     */
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        View view = null;
        Object o = getItem(position);

        // Don't display the socialAction item directly, but rather the item that is its target

        boolean shouldBeHighlighted = false;
        if (o instanceof PlaylistEntry) {
            shouldBeHighlighted = mShowPlaystate && mHighlightedPlaylistEntry != null
                    && o == mHighlightedPlaylistEntry;
        } else if (o instanceof Query) {
            shouldBeHighlighted = mShowPlaystate && mHighlightedQuery != null
                    && o == mHighlightedQuery;
        }

        List<ViewHolder> viewHolders = new ArrayList<>();
        if (convertView != null) {
            viewHolders = (List<ViewHolder>) convertView.getTag();
            view = convertView;
        }
        int viewType = getViewType(o, getSegment(position),
                mHeaderSpacerHeight > 0 && position == 0, position == getCount() - 1);
        int expectedViewHoldersCount = 1;
        if (o instanceof List) {
            expectedViewHoldersCount = 0;
            for (Object object : (List) o) {
                if (object != null) {
                    expectedViewHoldersCount++;
                }
            }
        }
        if (viewHolders.size() != expectedViewHoldersCount
                || viewHolders.get(0).mLayoutId != viewType) {
            // If the viewHolder is null or the old viewType is different than the new one,
            // we need to inflate a new view and construct a new viewHolder,
            // which we set as the view's tag
            viewHolders = new ArrayList<>();
            if (o instanceof List) {
                LinearLayout rowContainer = (LinearLayout) mLayoutInflater
                        .inflate(R.layout.row_container, parent, false);
                rowContainer.setPadding(rowContainer.getPaddingLeft(),
                        getSegment(position).getVerticalPadding(), rowContainer.getPaddingRight(),
                        0);
                for (int i = 0; i < ((List) o).size(); i++) {
                    if (((List) o).get(i) != null) {
                        View gridItem = mLayoutInflater.inflate(viewType, rowContainer, false);
                        ViewHolder viewHolder = new ViewHolder(gridItem, viewType);
                        rowContainer.addView(gridItem);
                        viewHolders.add(viewHolder);
                        if (i < ((List) o).size() - 1) {
                            View spacer = new View(mLayoutInflater.getContext());
                            spacer.setLayoutParams(new ViewGroup.LayoutParams(
                                    getSegment(position).getHorizontalPadding(),
                                    ViewGroup.LayoutParams.MATCH_PARENT));
                            rowContainer.addView(spacer);
                        }
                    } else {
                        rowContainer.addView(mLayoutInflater
                                .inflate(R.layout.row_container_spacer, rowContainer, false));
                    }
                }
                view = rowContainer;
            } else {
                view = mLayoutInflater.inflate(viewType, parent, false);
                ViewHolder viewHolder = new ViewHolder(view, viewType);
                viewHolders.add(viewHolder);
                /*if (view instanceof SwipeLayout) {
                    mItemManager.initialize(view, position);
                    final SharedPreferences preferences =
                            PreferenceManager.getDefaultSharedPreferences(TomahawkApp.getContext());
                    if (!preferences.getBoolean(
                            TomahawkMainActivity.COACHMARK_SWIPELAYOUT_ENQUEUE_DISABLED, false)) {
                        ((SwipeLayout) view).addSwipeListener(new SwipeLayout.SwipeListener() {
                            @Override
                            public void onStartOpen(SwipeLayout swipeLayout) {
                            }

                            @Override
                            public void onOpen(SwipeLayout swipeLayout) {
                                if (!preferences.getBoolean(
                                        TomahawkMainActivity.COACHMARK_SWIPELAYOUT_ENQUEUE_DISABLED,
                                        false)) {
                                    final View coachMark = ViewUtils.ensureInflation(
                                            swipeLayout, R.id.swipelayout_enqueue_coachmark_stub,
                                            R.id.swipelayout_enqueue_coachmark);
                                    coachMark.setVisibility(View.VISIBLE);
                                    View closeButton = coachMark.findViewById(R.id.close_button);
                                    closeButton.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            preferences.edit().putBoolean(
                                                    TomahawkMainActivity.COACHMARK_SWIPELAYOUT_ENQUEUE_DISABLED,
                                                    true).apply();
                                            coachMark.setVisibility(View.GONE);
                                        }
                                    });
                                }
                            }

                            @Override
                            public void onStartClose(SwipeLayout swipeLayout) {
                            }

                            @Override
                            public void onClose(SwipeLayout swipeLayout) {
                                View coachMark = swipeLayout.findViewById(
                                        R.id.swipelayout_enqueue_coachmark);
                                if (coachMark != null) {
                                    coachMark.setVisibility(View.GONE);
                                }
                            }

                            @Override
                            public void onUpdate(SwipeLayout swipeLayout, int i, int i1) {
                            }

                            @Override
                            public void onHandRelease(SwipeLayout swipeLayout, float v, float v1) {
                            }
                        });
                    }
                }
            }*/
            // Set extra padding
            if (getSegment(position) != null && getSegment(position).getLeftExtraPadding() > 0) {
                /*if (view instanceof SwipeLayout) {
                    // if it's a SwipeLayout, we have to set the padding on the foreground
                    // layout within the SwipeLayout instead
                    View foreground = ((ViewGroup) view).getChildAt(1);
                    foreground.setPadding(foreground.getPaddingLeft() + getSegment(position)
                                    .getLeftExtraPadding(),
                            foreground.getPaddingTop(), foreground.getPaddingRight(),
                            foreground.getPaddingBottom());

                /*}else*/
                {
                    view.setPadding(
                            view.getPaddingLeft() + getSegment(position).getLeftExtraPadding(),
                            view.getPaddingTop(),
                            view.getPaddingRight(), view.getPaddingBottom());
                }
            }
            }
            view.setTag(viewHolders);
        } /*else {
            if (view instanceof SwipeLayout) {
                // We have a SwipeLayout
                mItemManager.updateConvertView(view, position);
            }
        }*/

        // After we've setup the correct view and viewHolder, we now can fill the View's
        // components with the correct data
        for (int i = 0; i < viewHolders.size(); i++) {
            ViewHolder viewHolder = viewHolders.get(i);
            final Object item = o instanceof List ? ((List) o).get(i) : o;
            if (item == null) {
                if (viewHolder.mLayoutId == R.layout.content_footer_spacer) {
                    view.setLayoutParams(
                            new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                                    mFooterSpacerHeight));
                } else if (viewHolder.mLayoutId == R.layout.content_header_spacer) {
                    view.setLayoutParams(
                            new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT,
                                    mHeaderSpacerHeight));
                    if (mHeaderSpacerForwardView != null) {
                        BiDirectionalFrame biDirectionalFrame = (BiDirectionalFrame) view;
                        biDirectionalFrame.setForwardView(mHeaderSpacerForwardView);
                    }
                }
            } else {
                // Don't display the socialAction item directly, but rather the item that is its target

                if (viewHolder.mLayoutId == R.layout.grid_item_artist
                        || viewHolder.mLayoutId == R.layout.list_item_artist) {
                    View.OnClickListener swipeButtonListener;
                    swipeButtonListener = new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (v.getId() == R.id.optionImage){
                                if (item instanceof Artist||item instanceof Genre){
                                    if (item instanceof Artist){
                                       Artist artist = (Artist)item;
                                        quickAction.getActionItemList().clear();
                                        quickAction.clearView();
                                        quickAction.addActionItem(enqueeItem);
                                        quickAction.addActionItem(playItem);
                                        //quickAction.addActionItem(shuffleItem);
                                        quickAction.addActionItem(addPlaylistItem);
                                        quickAction.addActionItem(deleteItem);
                                        if (artist.getmLike()==0){
                                            likeItem.setTitle("Like");
                                        }else {
                                            likeItem.setTitle("UnLike");
                                        }
                                        quickAction.addActionItem(likeItem);
                                    }else {
                                        Genre artist = (Genre)item;
                                        quickAction.getActionItemList().clear();
                                        quickAction.clearView();
                                        quickAction.addActionItem(enqueeItem);
                                        quickAction.addActionItem(playItem);
                                        //quickAction.addActionItem(shuffleItem);
                                        quickAction.addActionItem(addPlaylistItem);
                                        quickAction.addActionItem(deleteItem);
                                        if (artist.getmLike()==0){
                                            likeItem.setTitle("Like");
                                        }else {
                                            likeItem.setTitle("UnLike");
                                        }
                                        quickAction.addActionItem(likeItem);
                                    }
                                    quickAction.show(v,item,4,-1);
                                }
                            }
                        }
                    };
                	if(item instanceof Artist)
                    viewHolder.fillView((Artist) item,swipeButtonListener);
                	else
                		viewHolder.fillView((Genre) item,swipeButtonListener);
                } else if (viewHolder.mLayoutId == R.layout.grid_item_album
                        || viewHolder.mLayoutId == R.layout.list_item_album) {
                    View.OnClickListener swipeButtonListener;
                    swipeButtonListener = new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                           if (v.getId() == R.id.optionImage){
                               if (item instanceof Album||item instanceof Folder){
                                   if (item instanceof Album){
                                       Album artist = (Album)item;
                                       quickAction.getActionItemList().clear();
                                       quickAction.clearView();
                                       quickAction.addActionItem(enqueeItem);
                                       quickAction.addActionItem(playItem);
                                       //quickAction.addActionItem(shuffleItem);
                                       quickAction.addActionItem(addPlaylistItem);
                                       quickAction.addActionItem(deleteItem);
                                       if (artist.getmLike()==0){
                                           likeItem.setTitle("Like");
                                       }else {
                                           likeItem.setTitle("UnLike");
                                       }
                                       quickAction.addActionItem(likeItem);
                                   }else {
                                       Folder artist = (Folder)item;
                                       quickAction.getActionItemList().clear();
                                       quickAction.clearView();
                                       quickAction.addActionItem(enqueeItem);
                                       quickAction.addActionItem(playItem);
                                       //quickAction.addActionItem(shuffleItem);
                                       quickAction.addActionItem(addPlaylistItem);
                                       quickAction.addActionItem(deleteItem);
                                       if (artist.getmLike()==0){
                                           likeItem.setTitle("Like");
                                       }else {
                                           likeItem.setTitle("UnLike");
                                       }
                                       quickAction.addActionItem(likeItem);
                                   }
                                   quickAction.show(v,item,4,-1);
                               }
                           }
                        }
                    };
                    Log.d("tttt",viewHolder.mLayoutId+"_"+R.layout.grid_item_album+"_"+R.layout.list_item_album);
                    if(item instanceof Folder)
                        viewHolder.fillView((Folder) item,mCollection,swipeButtonListener);
                    else {

                        viewHolder.fillView((Album) item, mCollection, swipeButtonListener);

                    }
                } else if (viewHolder.mLayoutId == R.layout.grid_item_resolver) {
                    viewHolder.fillView((Resolver) item);
                } else if (viewHolder.mLayoutId == R.layout.grid_item_playlist
                        || viewHolder.mLayoutId == R.layout.list_item_playlist) {
                    View.OnClickListener swipeButtonListener;
                    swipeButtonListener = new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (v.getId() == R.id.optionImage){
                                if (item instanceof Playlist){
                                    quickActionPlaylist.getActionItemList().clear();
                                    quickActionPlaylist.clearView();
                                    quickActionPlaylist.addActionItem(playItem);
                                    quickActionPlaylist.addActionItem(deleteItem);
                                    quickActionPlaylist.addActionItem(renameItem);
                                    quickActionPlaylist.show(v,item,-1,-1);
                                }
                            }
                        }
                    };
                    if (item instanceof Playlist) {
                        viewHolder.fillView((Playlist) item,swipeButtonListener);
                    } else if (item instanceof Integer) {
                        viewHolder.fillView((int) item);
                    }
                } else if (viewHolder.mLayoutId == R.layout.single_line_list_item) {
                    viewHolder.fillView(((Playlist) item).getName());
                } else if (viewHolder.mLayoutId == R.layout.list_item_text
                        || viewHolder.mLayoutId == R.layout.list_item_text_highlighted) {
                    viewHolder.fillView(((ListItemString) item).getText());
                } else if (viewHolder.mLayoutId == R.layout.list_item_track_artist
                        || viewType == R.layout.list_item_numeration_track_artist
                        || viewType == R.layout.list_item_numeration_track_duration) {
                    View coachMark = viewHolder.findViewById(R.id.swipelayout_enqueue_coachmark);
                    if (coachMark != null) {
                        coachMark.setVisibility(View.GONE);
                    }
                    String numerationString = null;
                    if (!getSegment(position).isShowAsQueued() && getSegment(position)
                            .isShowNumeration()) {
                        numerationString = String.format("%02d", getPosInSegment(position)
                                + getSegment(position).getNumerationCorrection());
                    }
                    final Query query;
                    final PlaylistEntry entry;
                    if (item instanceof PlaylistEntry) {
                        query = ((PlaylistEntry) item).getQuery();
                        entry = (PlaylistEntry) item;
                    } else {
                        query = (Query) item;
                        entry = null;
                    }
                   // View.OnClickListener swipeButtonListener;
                    boolean isShowAsQueued = getSegment(position).isShowAsQueued();
                    if (isShowAsQueued) {
                        /*swipeButtonListener = new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                mActivity.getPlaybackService().deleteQueryInQueue(entry);
                                //closeAllItems();
                            }
                        };*/
                    } else {
                       /* swipeButtonListener = new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                mActivity.getPlaybackService().addQueryToQueue(query);
                                SharedPreferences preferences = PreferenceManager
                                        .getDefaultSharedPreferences(TomahawkApp.getContext());
                                preferences.edit().putBoolean(
                                        TomahawkMainActivity.COACHMARK_SWIPELAYOUT_ENQUEUE_DISABLED,
                                        true).apply();
                                //closeAllItems();
                            }
                        };*/
                    }
                     if (getSegment(position).getPlayListType() == Collection.RECENTLY_ADDED){
                        recentlyAdded = true;
                    }
                    View.OnClickListener swipeButtonListener;
                    swipeButtonListener = new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            if (v.getId() == R.id.optionImage){
                                //if (item instanceof Query){
                                if (recentlyAdded) {
                                    Track artist = query.getBasicTrack();
                                    quickActionTrack.getActionItemList().clear();
                                    quickActionTrack.clearView();
                                    quickActionTrack.addActionItem(enqueeItem);
                                    quickActionTrack.addActionItem(playItem);
                                    //quickAction.addActionItem(shuffleItem);
                                    quickActionTrack.addActionItem(addPlaylistItem);
                                    deleteItem.setTitle("Remove");
                                    quickActionTrack.addActionItem(deleteItem);
                                    if (artist.getmLike()==0){
                                        likeItem.setTitle("Like");
                                    }else {
                                        likeItem.setTitle("UnLike");
                                    }
                                    quickActionTrack.addActionItem(likeItem);
                                    quickActionTrack.addActionItem(ringtoneItem);
                                    quickActionTrack.addActionItem(shareItem);
                                }else {
                                    Track artist = query.getBasicTrack();
                                    quickActionTrack.getActionItemList().clear();
                                    quickActionTrack.clearView();
                                    quickActionTrack.addActionItem(enqueeItem);
                                    quickActionTrack.addActionItem(playItem);
                                    //quickAction.addActionItem(shuffleItem);
                                    quickActionTrack.addActionItem(addPlaylistItem);
                                    deleteItem.setTitle("Delete");
                                    quickActionTrack.addActionItem(deleteItem);
                                    if (artist.getmLike()==0){
                                        likeItem.setTitle("Like");
                                    }else {
                                        likeItem.setTitle("UnLike");
                                    }
                                    quickActionTrack.addActionItem(likeItem);
                                    quickActionTrack.addActionItem(ringtoneItem);
                                    quickActionTrack.addActionItem(shareItem);
                                }
                                if (recentlyAdded) {
                                     quickActionTrack.show(v, query, 4, position);
                                }else {
                                    quickActionTrack.show(v, query, 4, position);
                                }
                                //}
                            }
                        }
                    };
                    int mColor;
                    if (mCurrentHeaderState == ContentHeaderFragment.MODE_HEADER_PLAYBACK){
                        mColor = mActivity.getResources().getColor(R.color.full_transparent);
                    }else
                    {
                        mColor = mActivity.getResources().getColor(R.color.list_pressed_opaque);
                    }
                    viewHolder.fillView(query, numerationString,
                            mHighlightedItemIsPlaying && shouldBeHighlighted, swipeButtonListener,
                            isShowAsQueued,mActivity.getPlaybackService().isPlaying(),mColor);

                    FrameLayout progressBarContainer = (FrameLayout) viewHolder
                            .findViewById(R.id.progressbar_container);
                    if (mHighlightedItemIsPlaying && shouldBeHighlighted) {
                        if (mProgressBar == null) {
                            mProgressBar = (ProgressBar) mLayoutInflater
                                    .inflate(R.layout.progressbar,
                                            progressBarContainer, false);
                        }
                        if (mProgressBar.getParent() instanceof FrameLayout) {
                            ((FrameLayout) mProgressBar.getParent()).removeView(mProgressBar);
                        }
                        progressBarContainer.addView(mProgressBar);
                    } else {
                        progressBarContainer.removeAllViews();
                    }
                }

                //Set up the click listeners
                if (viewHolder.findViewById(R.id.mainclickarea) != null) {
                    viewHolder.setMainClickListener(new ClickListener(item, mClickListener));
                }
            }
        }
        return view;
    }

    public int dpToPx(){
        Resources r = TomahawkApp.getContext().getResources();
        int px = (int) TypedValue.applyDimension(
                TypedValue.COMPLEX_UNIT_DIP,
                R.dimen.padding_superlarge,
                r.getDisplayMetrics()
        );
        return px;
    }

    /**
     * @return the count of every item to display
     */
    @Override
    public int getCount() {
        return mRowCount + (mHeaderSpacerHeight > 0 ? 1 : 0) + 1;
    }

    /**
     * @return item for the given position
     */
    @Override
    public Object getItem(int position) {
        if (mHeaderSpacerHeight > 0) {
            if (position == 0) {
                return null;
            } else {
                position--;
            }
        }
        int counter = 0;
        int correctedPos = position;
        for (Segment segment : mSegments) {
            counter += segment.getRowCount();
            if (position < counter) {
                return segment.get(correctedPos);
            } else {
                correctedPos -= segment.getRowCount();
            }
        }
        return null;
    }

    public Segment getSegment(int position) {
        if (mHeaderSpacerHeight > 0) {
            if (position == 0) {
                return null;
            } else {
                position--;
            }
        }
        int counter = 0;
        for (Segment segment : mSegments) {
            counter += segment.getRowCount();
            if (position < counter) {
                return segment;
            }
        }
        return null;
    }

    public int getPosInSegment(int position) {
        if (mHeaderSpacerHeight > 0) {
            if (position == 0) {
                return 0;
            } else {
                position--;
            }
        }
        int counter = 0;
        int correctedPos = position;
        for (Segment segment : mSegments) {
            counter += segment.getRowCount();
            if (position < counter) {
                return correctedPos;
            } else {
                correctedPos -= segment.getRowCount();
            }
        }
        return 0;
    }

    /**
     * Get the id of the item for the given position. (Id is equal to given position)
     */
    @Override
    public long getItemId(int position) {
        return position;
    }

    /**
     * This method is being called by the StickyListHeaders library. Get the correct header {@link
     * View} for the given position.
     *
     * @param position    The position for which to get the correct {@link View}
     * @param convertView The old {@link View}, which we might be able to recycle
     * @param parent      parental {@link ViewGroup}
     * @return the correct header {@link View} for the given position.
     */
    @Override
    public View getHeaderView(int position, View convertView, ViewGroup parent) {
        Segment segment = getSegment(position);
        if (segment != null && segment.getHeaderLayoutId() > 0) {
            View view = null;
            ViewHolder viewHolder = null;
            if (convertView != null) {
                viewHolder = (ViewHolder) convertView.getTag();
                view = convertView;
            }
            int layoutId = getHeaderViewType(segment);
            if (viewHolder == null || viewHolder.mLayoutId != layoutId) {
                view = mLayoutInflater.inflate(layoutId, null);
                viewHolder = new ViewHolder(view, layoutId);
                view.setTag(viewHolder);
            }

            if (layoutId == R.layout.dropdown_header) {
                ArrayList<CharSequence> spinnerItems = new ArrayList<>();
                for (String headerString : segment.getHeaderStrings()) {
                    spinnerItems.add(headerString.toUpperCase());
                }
                viewHolder.fillHeaderView(spinnerItems, segment.getInitialPos(),
                        segment.getSpinnerClickListener(),segment.getViewClickListener(),segment.getTextWatcher(),segment.getEditorAction(),segment.getPrefKey(),segment.isTrackItem());
            } else if (layoutId == R.layout.single_line_list_header) {
                viewHolder.fillHeaderView(segment.getHeaderString().toUpperCase());
            } else if (layoutId == R.layout.list_header_socialaction_fake) {
                viewHolder.fillHeaderView(segment.getHeaderString());
            }
            return view;
        } else {
            return new View(mActivity);
        }
    }

    /**
     * This method is being called by the StickyListHeaders library. Returns the same value for each
     * item that should be grouped under the same header.
     *
     * @param position the position of the item for which to get the header id
     * @return the same value for each item that should be grouped under the same header.
     */
    @Override
    public long getHeaderId(int position) {
        Segment segment = getSegment(position);
        if (segment != null) {
            return mSegments.indexOf(segment);
        } else {
            return -1;
        }
    }

    private int getViewType(Object item, Segment segment, boolean isContentHeaderItem,
            boolean isFooter) {
        if (item instanceof List) {
            // We have a grid item
            // Don't display the socialAction item directly, but rather the item that is its target
            if (!((List) item).isEmpty()) {
                Object firstItem = ((List) item).get(0);

                if (firstItem instanceof User) {
                    return R.layout.grid_item_user;
                } else if (firstItem instanceof Resolver) {
                    return R.layout.grid_item_resolver;
                } else if (firstItem instanceof Artist) {
                    return R.layout.grid_item_artist;
                }else if (firstItem instanceof Genre) {
                    return R.layout.grid_item_artist;
                } 
                else if (firstItem instanceof Album || firstItem instanceof Folder) {
                    return R.layout.grid_item_album;
                } else if (firstItem instanceof Playlist) {
                    return R.layout.grid_item_playlist;
                } else if (firstItem instanceof Integer) {
                    switch ((Integer) firstItem) {
                        case PlaylistsFragment.CREATE_PLAYLIST_BUTTON_ID:
                            return R.layout.grid_item_playlist;
                    }
                } else {
                    Log.e(TAG, "getViewType - Couldn't find appropriate viewType!");
                    return R.layout.empty_view;
                }
            }
        }

        if (isContentHeaderItem) {
            return R.layout.content_header_spacer;
        } else if (isFooter) {
            return R.layout.content_footer_spacer;
        } else if (item instanceof Playlist) {
            return R.layout.list_item_playlist;//single_line_list_item;
        } else if (item instanceof ListItemString) {
            if (((ListItemString) item).isHighlighted()) {
                return R.layout.list_item_text_highlighted;
            } else {
                return R.layout.list_item_text;
            }
        } else if (item instanceof Album || item instanceof Folder) {
            return R.layout.list_item_album;
        } else if (item instanceof Artist || item instanceof Genre) {
            return R.layout.list_item_artist;
        } else if (item instanceof User) {
            return R.layout.list_item_user;
        } else if (segment!=null && segment.isHideArtistName()) {
            return R.layout.list_item_numeration_track_duration;
        } else if (segment!=null && (segment.isShowNumeration() || segment.isShowAsQueued())) {
            return R.layout.list_item_numeration_track_artist;
        } else {
            return R.layout.list_item_track_artist;
        }
    }

    private int getHeaderViewType(Segment segment) {
        return segment.getHeaderLayoutId();
    }

    private void updateFooterSpacerHeight(final StickyListHeadersListView listView) {
        if (mHeaderSpacerHeight > 0) {
            ViewUtils.afterViewGlobalLayout(new ViewUtils.ViewRunnable(listView) {
                @Override
                public void run() {
                    mFooterSpacerHeight = calculateFooterSpacerHeight(listView);
                    notifyDataSetChanged();
                }
            });
        }
    }

    private int calculateFooterSpacerHeight(StickyListHeadersListView listView) {
        int footerSpacerHeight = listView.getWrappedList().getHeight();
        long headerId = getHeaderId(0);
        for (int i = 1; i < getCount(); i++) {
            View view = getView(i, null, listView.getWrappedList());
            if (view != null) {
                view.measure(View.MeasureSpec.makeMeasureSpec(0,
                                View.MeasureSpec.UNSPECIFIED),
                        View.MeasureSpec.makeMeasureSpec(0,
                                View.MeasureSpec.UNSPECIFIED));
                footerSpacerHeight -= view.getMeasuredHeight();
            }
            if (headerId != getHeaderId(i)) {
                headerId = getHeaderId(i);
                View headerView = getHeaderView(i, null, listView.getWrappedList());
                if (headerView != null) {
                    headerView.measure(View.MeasureSpec.makeMeasureSpec(0,
                                    View.MeasureSpec.UNSPECIFIED),
                            View.MeasureSpec.makeMeasureSpec(0,
                                    View.MeasureSpec.UNSPECIFIED));
                    footerSpacerHeight -= headerView.getMeasuredHeight();
                }
            }
            if (footerSpacerHeight <= 0) {
                footerSpacerHeight = 0;
                break;
            }
        }
        return footerSpacerHeight;
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Object o = getItem(position);
        if (!(o instanceof List)) {
            mClickListener.onItemClick(view, o);
        }
    }

    @Override
    public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
        Object o = getItem(position);
        if (!(o instanceof List)) {
            return mClickListener.onItemLongClick(view, o);
        }
        return false;
    }

   /* @Override
    public int getSwipeLayoutResourceId(int position) {
        return R.id.swipelayout;
    }

    @Override
    public void openItem(int position) {
        mItemManager.openItem(position);
    }

    @Override
    public void closeItem(int position) {
        mItemManager.closeItem(position);
    }

    @Override
    public void closeAllExcept(SwipeLayout layout) {
        mItemManager.closeAllExcept(layout);
    }

    @Override
    public void closeAllItems() {
        mItemManager.closeAllItems();
    }

    @Override
    public List<Integer> getOpenItems() {
        return mItemManager.getOpenItems();
    }

    @Override
    public List<SwipeLayout> getOpenLayouts() {
        return mItemManager.getOpenLayouts();
    }

    @Override
    public void removeShownLayouts(SwipeLayout layout) {
        mItemManager.removeShownLayouts(layout);
    }

    @Override
    public boolean isOpen(int position) {
        return mItemManager.isOpen(position);
    }

    @Override
    public Attributes.Mode getMode() {
        return mItemManager.getMode();
    }

    @Override
    public void setMode(Attributes.Mode mode) {
        mItemManager.setMode(mode);
    }
*/
    public void onPlayPositionChanged(long duration, int currentPosition) {
        if (mProgressBar != null) {
            mProgressBar.setProgress(
                    (int) ((float) currentPosition / duration * mProgressBar.getMax()));
        }
    }
}
