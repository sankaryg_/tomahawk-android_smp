/* == This file is part of Tomahawk Player - <http://tomahawk-player.org> ===
 *
 *   Copyright 2012, Christopher Reichert <creichert07@gmail.com>
 *   Copyright 2013, Enno Gottschalk <mrmaffen@googlemail.com>
 *
 *   Tomahawk is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Tomahawk is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Tomahawk. If not, see <http://www.gnu.org/licenses/>.
 */
package org.tomahawk.tomahawk_android;

import android.app.AlarmManager;
import android.app.Application;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.Environment;
import android.os.StrictMode;
import android.preference.PreferenceManager;
import android.util.Log;

import org.tomahawk.tomahawk_android.activities.ErrorProgress;
import org.tomahawk.tomahawk_android.sensor.PrefsHelper;
import org.tomahawk.tomahawk_android.sensor.SensorDataHelper;
import org.tomahawk.tomahawk_android.sensor.SensorModeHelper;
import org.tomahawk.tomahawk_android.sensor.SensorServiceRunning;
import org.tomahawk.tomahawk_android.sensor.SensorServiceStopped;
import org.tomahawk.tomahawk_android.services.PlaybackService;
import org.tomahawk.tomahawk_android.utils.MediaUtils;

import java.io.File;
import java.io.PrintWriter;
import java.io.StringWriter;

/**
 * This class represents the Application core.
 */

public class TomahawkApp extends Application {

    private static final String TAG = TomahawkApp.class.getSimpleName();

    public final static String PLUGINNAME_HATCHET = "hatchet";

    public final static String PLUGINNAME_USERCOLLECTION = "usercollection";

    public final static String PLUGINNAME_SPOTIFY = "spotify";

    public final static String PLUGINNAME_DEEZER = "deezer";

    public final static String PLUGINNAME_BEATSMUSIC = "beatsmusic";

    public final static String PLUGINNAME_RDIO = "rdio";

    public final static String PLUGINNAME_JAMENDO = "jamendo";

    public final static String PLUGINNAME_OFFICIALFM = "officialfm";

    public final static String PLUGINNAME_SOUNDCLOUD = "soundcloud";

    public final static String PLUGINNAME_GMUSIC = "gmusic";

    private static Context sApplicationContext;

    private final String LINE_SEPARATOR = "\n";

    public SensorDataHelper sensorDataHelper;

    private SharedPreferences prefs;

    private SensorModeHelper sensorModeHelper;

    private SharedPreferences.Editor prefEditor;

    private SensorServiceRunning sensorServiceRunningState;

    private SensorServiceStopped sensorServiceStoppedState;

    private MediaUtils mUtils;




    private Thread.UncaughtExceptionHandler unCaughtExceptionHandler = new Thread.UncaughtExceptionHandler() {
        @Override
        public void uncaughtException(Thread thread, Throwable exception) {

            // TODO remove the following code to restart the application when
            // its crashed.

            exception.printStackTrace();
            Log.e("Exception", exception.getMessage());
            StringWriter stackTrace = new StringWriter();
            exception.printStackTrace(new PrintWriter(stackTrace));
            StringBuilder errorReport = new StringBuilder();
            errorReport.append("************ CAUSE OF ERROR ************\n\n");
            errorReport.append(stackTrace.toString());

            errorReport.append("\n************ DEVICE INFORMATION ***********\n");
            errorReport.append("Brand: ");
            errorReport.append(Build.BRAND);
            errorReport.append(LINE_SEPARATOR);
            errorReport.append("Device: ");
            errorReport.append(Build.DEVICE);
            errorReport.append(LINE_SEPARATOR);
            errorReport.append("Model: ");
            errorReport.append(Build.MODEL);
            errorReport.append(LINE_SEPARATOR);
            errorReport.append("Id: ");
            errorReport.append(Build.ID);
            errorReport.append(LINE_SEPARATOR);
            errorReport.append("Product: ");
            errorReport.append(Build.PRODUCT);
            errorReport.append(LINE_SEPARATOR);
            errorReport.append("\n************ FIRMWARE ************\n");
            errorReport.append("SDK: ");
            errorReport.append(Build.VERSION.SDK);
            errorReport.append(LINE_SEPARATOR);
            errorReport.append("Release: ");
            errorReport.append(Build.VERSION.RELEASE);
            errorReport.append(LINE_SEPARATOR);
            errorReport.append("Incremental: ");
            errorReport.append(Build.VERSION.INCREMENTAL);
            errorReport.append(LINE_SEPARATOR);

             restartApp(errorReport.toString());

        }
    };



    private void restartApp(String string) {
        Intent intent = new Intent(getApplicationContext(), ErrorProgress.class);
        intent.putExtra("error", string);
        PendingIntent myActivity = PendingIntent.getActivity(getApplicationContext(),
                192837, intent,
                PendingIntent.FLAG_ONE_SHOT);

        AlarmManager mgr = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        mgr.set(AlarmManager.RTC, System.currentTimeMillis() + 2000, myActivity);

        System.exit(0);
    }


    @Override
    public void onCreate() {
        /*ACRA.init(this);
        ACRA.getErrorReporter().setReportSender(
                new TomahawkHttpSender(ACRA.getConfig().httpMethod(), ACRA.getConfig().reportType(),
                        null));*/
        Thread.setDefaultUncaughtExceptionHandler(unCaughtExceptionHandler);

        StrictMode.setThreadPolicy(
                new StrictMode.ThreadPolicy.Builder().detectCustomSlowCalls().detectDiskReads()
                        .detectDiskWrites().detectNetwork().penaltyLog().build());
        try {
            StrictMode.setVmPolicy(new StrictMode.VmPolicy.Builder().detectLeakedSqlLiteObjects()
                    .detectLeakedClosableObjects()
                    .setClassInstanceLimit(Class.forName(PlaybackService.class.getName()), 1)
                    .penaltyLog().build());
        } catch (ClassNotFoundException e) {
            Log.e(TAG, e.toString());
        }

        super.onCreate();
        mUtils = new MediaUtils(this);
        //mController = new GlobalAppController(this, null);
        sApplicationContext = getApplicationContext();
        sensorDataHelper = new SensorDataHelper(this);
        sensorServiceRunningState = new SensorServiceRunning(this);
        sensorServiceStoppedState = new SensorServiceStopped(this);
        }



    public int getCurrentPresetID()
    {
        return PrefsHelper.getPreviousSensorModeId(this);
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
        mUtils = null;
    }

    public MediaUtils getMediaUtils() {

                return mUtils;

    }
    public SensorDataHelper getSensorDataHelper() {
        return sensorDataHelper;
    }

    public SensorModeHelper getSensorModeHelper()
    {
        if (sensorModeHelper == null)
        {
            sensorModeHelper = new SensorModeHelper(this);
        }
        return sensorModeHelper;
    }

    public SharedPreferences.Editor getPrefEditor()
    {
        if (prefEditor == null)
        {
            prefEditor = getPrefs().edit();
        }
        return prefEditor;
    }

    public SharedPreferences getPrefs()
    {
        if (prefs == null)
        {
            prefs = PreferenceManager.getDefaultSharedPreferences(getContext());
        }
        return prefs;
    }
    public File getWritableCacheDir()
    {
        File file = getCacheDir();
        if (file != null && file.isDirectory() && file.canWrite())
        {
            return file;
        }
        if (Build.VERSION.SDK_INT >= 8)
        {
            File file4 = getExternalCacheDir();
            if (file4 != null && file4.isDirectory() && file4.canWrite())
            {
                return file4;
            }
        }
        File file1 = Environment.getDownloadCacheDirectory();
        if (file1 != null && file1.isDirectory() && file1.canWrite())
        {
            return file1;
        }
        File file2 = Environment.getDataDirectory();
        if (file2 != null && file2.isDirectory() && file2.canWrite())
        {
            return file2;
        }
        File file3 = Environment.getExternalStorageDirectory();
        if (file3 != null && file3.isDirectory() && file3.canWrite())
        {
            return file3;
        } else
        {
            return null;
        }
    }
    public void loadSensorMode(int i)
    {
        if (!PrefsHelper.isSensorDisabledByStop(this))
        {
            PrefsHelper.setCurrentSensorModeId(this, i);
            getPrefEditor().commit();
        }
        sensorDataHelper.loadSensorMode(i, true);

    }

    public static Context getContext() {
        return sApplicationContext;
    }




}
