package org.tomahawk.tomahawk_android.utils;

/**
 * Created by YGS on 10/13/2015.
 */
import android.content.ComponentName;
import android.content.Context;
import android.content.ContextWrapper;
import android.content.Intent;
import android.content.ServiceConnection;
import android.util.Log;

import org.tomahawk.aidl.IMediaPlaybackInterface;
import org.tomahawk.tomahawk_android.services.PlaybackService;

import java.util.HashMap;

/**
 * Created by YGS on 9/30/2015.
 */
public class MediaUtils {

    private static IMediaPlaybackInterface mService = null;

    private ServiceToken mToken = null;

    private static Context mContext;

    private static HashMap<Context, ServiceBinder> mConnectionMap = new HashMap<Context, ServiceBinder>();


    public MediaUtils(Context context) {
        mContext = context;
    }

    public static class ServiceToken {
        public ContextWrapper mWrappedContext;
        public ServiceToken(ContextWrapper context) {
            mWrappedContext = context;
        }
    }


    public void unbindFromService(ServiceToken token) {

        if (token == null) {
            Log.e("LOGTAG_MUSICUTILS", "Trying to unbind with null token");
            return;
        }
        ContextWrapper wrapper = token.mWrappedContext;
        ServiceBinder binder = mConnectionMap.remove(wrapper);
        if (binder == null) {
            Log.e("LOGTAG_MUSICUTILS", "Trying to unbind for unknown Context");
            return;
        }
        wrapper.unbindService(binder);
        if (mConnectionMap.isEmpty()) {
            // presumably there is nobody interested in the service at this
            // point,
            // so don't hang on to the ServiceConnection
            mService = null;
        }
    }

    public ServiceToken bindToService(ServiceConnection callback) {

        ContextWrapper cw = new ContextWrapper(mContext);
        cw.startService(new Intent(cw, PlaybackService.class));
        ServiceBinder sb = new ServiceBinder(callback);
        if (cw.bindService(new Intent(cw, PlaybackService.class), sb, 0)) {
            mConnectionMap.put(cw, sb);
            mToken = new ServiceToken(cw);
            return mToken;
        }
        Log.e("Music", "Failed to bind to service");
        return mToken;
    }


    private class ServiceBinder implements ServiceConnection {

        ServiceConnection mCallback;

        ServiceBinder(ServiceConnection callback) {

            mCallback = callback;
        }

        @Override
        public void onServiceConnected(ComponentName className, android.os.IBinder service) {

            mService = IMediaPlaybackInterface.Stub.asInterface(service);

            if (mCallback != null) {
                mCallback.onServiceConnected(className, service);
            }
        }

        @Override
        public void onServiceDisconnected(ComponentName className) {

            if (mCallback != null) {
                mCallback.onServiceDisconnected(className);
            }
            mService = null;
        }
    }

}