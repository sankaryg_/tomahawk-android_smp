/*   
 * Copyright (C) 2011 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package org.tomahawk.tomahawk_android.utils;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.media.AudioManager;
import android.os.Handler;
import android.os.Message;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.KeyEvent;

import org.tomahawk.tomahawk_android.fragments.PreferenceAdvancedFragment;
import org.tomahawk.tomahawk_android.services.PlaybackService;

/**
 * Receives broadcasted intents. In particular, we are interested in the
 * android.media.AUDIO_BECOMING_NOISY and android.intent.action.MEDIA_BUTTON intents, which is
 * broadcast, for example, when the user disconnects the headphones. This class works because we
 * are
 * declaring it in a &lt;receiver&gt; tag in AndroidManifest.xml.
 */
public class MediaButtonReceiver extends BroadcastReceiver {

    private static final String TAG = MediaButtonReceiver.class.getSimpleName();
    private static final int MSG_LONGPRESS_TIMEOUT = 1;
    private static final int LONG_PRESS_DELAY = 1000;

    private static long mLastClickTime = 0;
    private static boolean mDown = false;
    private static boolean mLaunched = false;

    private static Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case MSG_LONGPRESS_TIMEOUT:
                    if (!mLaunched) {
                        Context context = (Context)msg.obj;
                        Intent serviceIntent = new Intent(PlaybackService.ACTION_NEXT, null, context,
                                PlaybackService.class);
                        if (serviceIntent != null) {
                            context.startService(serviceIntent);
                        }
                        mLaunched = true;
                    }
                    break;
            }
        }
    };

    @Override
    public void onReceive(Context context, Intent intent) {
        if (AudioManager.ACTION_AUDIO_BECOMING_NOISY.equals(intent.getAction())) {
            // AudioManager tells us that the sound will be played through the speaker
            Log.d(TAG, "Action audio becoming noisy, pausing ...");
            // So we stop playback, if needed
            SharedPreferences prefs =
                    PreferenceManager.getDefaultSharedPreferences(context);
            boolean playbackPauseOnHeadsetInsert = prefs.getBoolean(
                    PreferenceAdvancedFragment.FAKEPREFERENCEFRAGMENT_KEY_HEADSET_PAUSE, false);
            if (playbackPauseOnHeadsetInsert)
            context.startService(new Intent(PlaybackService.ACTION_PAUSE, null, context,
                    PlaybackService.class));
        } else if (Intent.ACTION_MEDIA_BUTTON.equals(intent.getAction())) {
            KeyEvent keyEvent = (KeyEvent) intent.getExtras().get(Intent.EXTRA_KEY_EVENT);
            Log.d(TAG, "Mediabutton pressed ... keyCode: " + keyEvent.getKeyCode());
            if (keyEvent.getAction() != KeyEvent.ACTION_DOWN) {
                return;
            }
            int keycode = keyEvent.getKeyCode();
            int action = keyEvent.getAction();
            long eventtime = keyEvent.getEventTime();
            Intent serviceIntent = null;
            String command = null;
            switch (keyEvent.getKeyCode()) {
                case KeyEvent.KEYCODE_HEADSETHOOK:
                case KeyEvent.KEYCODE_MEDIA_PLAY_PAUSE:
                    command = PlaybackService.ACTION_PLAYPAUSE;
                    serviceIntent = new Intent(PlaybackService.ACTION_PLAYPAUSE, null, context,
                            PlaybackService.class);
                    break;
                case KeyEvent.KEYCODE_MEDIA_PLAY:
                    command = PlaybackService.ACTION_PLAY;
                    serviceIntent = new Intent(PlaybackService.ACTION_PLAY, null, context,
                            PlaybackService.class);
                    break;
                case KeyEvent.KEYCODE_MEDIA_PAUSE:
                    command = PlaybackService.ACTION_PAUSE;
                    serviceIntent = new Intent(PlaybackService.ACTION_PAUSE, null, context,
                            PlaybackService.class);
                    break;
                case KeyEvent.KEYCODE_MEDIA_STOP:
                    command = PlaybackService.ACTION_PAUSE;
                    serviceIntent = new Intent(PlaybackService.ACTION_PAUSE, null, context,
                            PlaybackService.class);
                    break;
                case KeyEvent.KEYCODE_MEDIA_NEXT:
                    command = PlaybackService.ACTION_NEXT;
                    serviceIntent = new Intent(PlaybackService.ACTION_NEXT, null, context,
                            PlaybackService.class);
                    break;
                case KeyEvent.KEYCODE_MEDIA_PREVIOUS:
                    command = PlaybackService.ACTION_PREVIOUS;
                    serviceIntent = new Intent(PlaybackService.ACTION_PREVIOUS, null, context,
                            PlaybackService.class);
                    break;
            }
            if (command != null) {
                if (action == KeyEvent.ACTION_DOWN) {
                    if (mDown) {
                        if ((PlaybackService.ACTION_PAUSE.equals(command) ||
                                PlaybackService.ACTION_PLAY.equals(command))
                                && mLastClickTime != 0
                                && eventtime - mLastClickTime > LONG_PRESS_DELAY) {
                            mHandler.sendMessage(
                                    mHandler.obtainMessage(MSG_LONGPRESS_TIMEOUT, context));
                        }
                    } else if (keyEvent.getRepeatCount() == 0) {
                        // only consider the first event in a sequence, not the repeat events,
                        // so that we don't trigger in cases where the first event went to
                        // a different app (e.g. when the user ends a phone call by
                        // long pressing the headset button)

                        // The service may or may not be running, but we need to send it
                        // a command.

                        if (keycode == KeyEvent.KEYCODE_HEADSETHOOK &&
                                eventtime - mLastClickTime < 300) {
                            serviceIntent = new Intent(PlaybackService.ACTION_NEXT, null, context,
                                    PlaybackService.class);
                            mLastClickTime = 0;
                        } else {
                            serviceIntent = new Intent(command, null, context,
                                    PlaybackService.class);
                            mLastClickTime = eventtime;
                        }

                        mLaunched = false;
                        mDown = true;
                    }
                } else {
                    mHandler.removeMessages(MSG_LONGPRESS_TIMEOUT);
                    mDown = false;
                }
                if (isOrderedBroadcast()) {
                    abortBroadcast();
                }
            }

            if (serviceIntent != null) {
                context.startService(serviceIntent);
            }


        }
    }
}
