package org.tomahawk.tomahawk_android.sensor;


public class Action
{

    public int id;
    public String intentAction;
    public int labelID;
    public int repeatDelay;

    public Action(int i, String s, int j)
    {
        intentAction = "";
        labelID = 0;
        id = -1;
        repeatDelay = 0;
        id = i;
        intentAction = s;
        labelID = j;
    }

    public Action(int i, String s, int j, int k)
    {
        intentAction = "";
        labelID = 0;
        id = -1;
        repeatDelay = 0;
        id = i;
        intentAction = s;
        labelID = j;
        repeatDelay = k;
    }

    public Boolean isRepeatable()
    {
        if (repeatDelay > 0)
        {
            return Boolean.valueOf(true);
        } else
        {
            return Boolean.valueOf(false);
        }
    }

    public String toString()
    {
        return intentAction;
    }
}
