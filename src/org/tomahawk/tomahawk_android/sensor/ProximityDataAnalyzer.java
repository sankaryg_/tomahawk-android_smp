package org.tomahawk.tomahawk_android.sensor;

import android.os.Handler;
import android.util.Log;

import org.tomahawk.tomahawk_android.TomahawkApp;

import java.util.Date;

public class ProximityDataAnalyzer
{
    public static interface MyProximityEventListener
    {

        public abstract void onCovered();

        public abstract void onDoubleWaveOver();

        public abstract void onLongStayOver();

        public abstract void onUncovered();

        public abstract void onWaveOver();
    }


    public static final int STATUS_COVERED = 1;
    public static final int STATUS_NOT_SURE = 0;
    public static final int STATUS_UNCOVERED = 2;
    private SensorMode currentPreset;
    private int currentStatus;
    private Runnable doubleWaveOverRunnable = new Runnable() {

        public void run()
        {
            listener.onDoubleWaveOver();
        }

            
    };
    private long lastCoveredTime;
    private long lastUncoveredTime;
    private boolean lastWasDouble;
    private MyProximityEventListener listener;
    private Runnable longStayRunnable = new Runnable() {


        public void run()
        {
            listener.onLongStayOver();
        }

    };
    private Handler lsHandler;
    private Runnable simpleWaveOverRunnable = new Runnable() {


        public void run()
        {
            listener.onWaveOver();
        }

    };
    private TomahawkApp smp;
    private float unCoveredProximityValue;
    private boolean wasCovered;

    public ProximityDataAnalyzer(TomahawkApp tomahawkApp)
    {
        this(tomahawkApp, new SensorMode(1), new MyProximityEventListener() {

            public void onCovered()
            {
            }

            public void onDoubleWaveOver()
            {
            }

            public void onLongStayOver()
            {
            }

            public void onUncovered()
            {
            }

            public void onWaveOver()
            {
            }

        });
    }

    public ProximityDataAnalyzer(TomahawkApp  tomahawkApp, SensorMode sensormode, MyProximityEventListener myproximityeventlistener)
    {
        wasCovered = false;
        lastUncoveredTime = 0L;
        lastCoveredTime = 0L;
        lastWasDouble = false;
        currentStatus = 0;
        listener = myproximityeventlistener;
        smp = tomahawkApp;
        currentPreset = sensormode;
        lsHandler = new Handler();
        unCoveredProximityValue = PrefsHelper.getModeProxUncoveredValue(tomahawkApp.getPrefs());
    }

    public void addValues(float f)
    {
    	 currentStatus = 0;
         if (f > unCoveredProximityValue)
         {
             unCoveredProximityValue = f;
             PrefsHelper.setModeProxUncoveredValue(smp.getPrefEditor(), f);
             smp.getPrefEditor().commit();
         }
         if (f >= unCoveredProximityValue) {
         	wasCovered = true;
             if (!wasCovered) return; 
             else{
             currentStatus = 2;
             listener.onUncovered();
             lsHandler.removeCallbacks(longStayRunnable);
             lastUncoveredTime = (new Date()).getTime();
             wasCovered = false;
             if (currentPreset != null && !lastWasDouble && lastUncoveredTime - lastCoveredTime < (long)currentPreset.getLongStayDurationProx() && currentPreset.isProxEnabled())
             {
                 lsHandler.postDelayed(simpleWaveOverRunnable, currentPreset.getDoubleWaveSpeed());
             }
             lastWasDouble = false;
             IntentBroadcastHelper.stopAllBroadcastsFromRequester("proximity");
             return;
             }
                  } else {
         currentStatus = 1;
         listener.onCovered();
         lastCoveredTime = (new Date()).getTime();
         lsHandler.removeCallbacks(longStayRunnable);
         lsHandler.removeCallbacks(simpleWaveOverRunnable);
         if (currentPreset == null || lastCoveredTime - lastUncoveredTime >= (long)currentPreset.getDoubleWaveSpeed()) {
             if (currentPreset != null && currentPreset.isProxEnabled())
             {
                 lsHandler.postDelayed(longStayRunnable, currentPreset.getLongStayDurationProx());
             }
            // continue; /* Loop/switch isn't completed */

         }
         else{
         	if (currentPreset.isProxEnabled())
             {
                 doubleWaveOverRunnable.run();
             }
             lastWasDouble = true;
         }
         }
    }

    public int getStatus()
    {
        Log.d("", (new StringBuilder("status : ")).append(currentStatus).toString());
        return currentStatus;
    }

    public void setPreset(SensorMode sensormode)
    {
        currentPreset = sensormode;
    }

}
