package org.tomahawk.tomahawk_android.sensor;

import android.content.Context;
import android.media.MediaRecorder;
import android.util.Log;

import org.tomahawk.tomahawk_android.TomahawkApp;

import java.io.File;

public class SoundMeter
{

    private static final double EMA_FILTER = 0.59999999999999998D;
    private int failedAttempt;
    private Context mContext;
    private double mEMA;
    private MediaRecorder mRecorder;
    private int maxAttempt;
    private String samplePath;
    private TomahawkApp smp;

    public SoundMeter(Context context)
    {
        mRecorder = null;
        mEMA = 0.0D;
        failedAttempt = 0;
        maxAttempt = 9;
        samplePath = "";
        mContext = context;
    }

    public int getAmplitude()
    {
        if (mRecorder != null)
        {
            return mRecorder.getMaxAmplitude();
        } else
        {
            return -1;
        }
    }

    public double getAmplitudeEMA()
    {
        mEMA = 0.59999999999999998D * (double)getAmplitude() + 0.40000000000000002D * mEMA;
        return mEMA;
    }

    public void start()
    {
        File file;
        stop();
        smp = (TomahawkApp)mContext.getApplicationContext();
        mRecorder = new MediaRecorder();
        Log.d("SOUND_METER", "setAudioSource");
        try
        {
            mRecorder.setAudioSource(5);
        }
        catch (RuntimeException runtimeexception)
        {
            Log.d("SOUND_METER", "setAudioSource failed");
            mRecorder = null;
            return;
        }

for(failedAttempt = 0;failedAttempt < maxAttempt;failedAttempt++){ 
   try{
        mRecorder.setOutputFormat(0);
        file = smp.getWritableCacheDir();
        if (file != null)         
		{
		samplePath = (new StringBuilder(String.valueOf(file.getAbsolutePath()))).append("/sample").toString();
        mRecorder.setOutputFile(samplePath);
        mRecorder.setAudioEncoder(1);
        mRecorder.prepare();
        mEMA = 0.0D;
        mRecorder.start();
        failedAttempt = 0;
        break;
		}
   }catch(Exception e){
        failedAttempt = 1 + failedAttempt;
        if (failedAttempt > maxAttempt)
        {
         break;
		 //   throw new Error(exception.getMessage());
        }
        }
}        
    }

    public void stop()
    {
        File file;
        try
        {
            if (mRecorder != null)
            {
                mRecorder.stop();
                mRecorder.reset();
                mRecorder.release();
                mRecorder = null;
            }
        }
        catch (Exception exception) { }
        file = new File(samplePath);
        if (file.exists())
        {
            file.delete();
        }
    }
}
