
package org.tomahawk.tomahawk_android.sensor;


import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import org.tomahawk.tomahawk_android.services.PlaybackService;

import de.greenrobot.event.EventBus;

public class SensorMode
    implements Comparable
{

    private int accActiveAnalysisDuration;
    private boolean accEnabled;
    private int actionIDForAccX;
    private int actionIDForAccY;
    private int actionIDForAccZ;
    private int actionIDForProxDouble;
    private int actionIDForProxLong;
    private int actionIDForProxSimple;
    private int doubleWaveSpeed;
    private int drawableID;
    private int helpTextID;
    private int id;
    private boolean isAvailable;
    private int labelID;
    private int longStayDurationProx;
    private boolean proxEnabled;
    private boolean soundEnabled;
    private int soundSensitivity;
    private int xSensitivity;
    private int ySensitivity;
    private int zSensitivity;

    public SensorMode(int i)
    {
        drawableID = -1;
        id = -1;
        labelID = -1;
        helpTextID = -1;
        id = i;
        boolean flag = setSoundEnabled(false);
        proxEnabled = flag;
        accEnabled = flag;
        isAvailable = true;
        zSensitivity = 9999;
        ySensitivity = 9999;
        xSensitivity = 9999;
        soundSensitivity = 0;
        actionIDForProxSimple = 0;
        actionIDForAccZ = 0;
        actionIDForAccY = 0;
        actionIDForAccX = 0;
        actionIDForProxDouble = 1;
        actionIDForProxLong = 2;
        accActiveAnalysisDuration = 60;

    }

    public int compareTo(SensorMode sensormode)
    {
        return id - sensormode.id;
    }

    public int compareTo(Object obj)
    {
        return compareTo((SensorMode)obj);
    }

    public int getAccActiveAnalysisDuration()
    {
        return accActiveAnalysisDuration;
    }

    public int getActionIDForEventID(int i,Context mContext)
    {
        SharedPreferences sharedpreferences = PreferenceManager.getDefaultSharedPreferences(mContext);
        switch (i)
        {
        case 13: // '\r'
        case 14: // '\016'
        case 15: // '\017'
        case 16: // '\020'
        case 17: // '\021'
        case 18: // '\022'
        case 19: // '\023'
        default:
            return -1;

        case 100: // '\n'
            return actionIDForAccX;
            //return sharedpreferences.getInt("event_100", actionIDForAccX);

        case 111: // '\013'
            //return sharedpreferences.getInt("event_111", actionIDForAccY);
            return actionIDForAccY;

        case 122: // '\f'
            //return sharedpreferences.getInt("event_122", actionIDForAccZ);
            return actionIDForAccZ;

        case 200: // '\024'
            //return sharedpreferences.getInt("event_200", actionIDForProxSimple);
            return actionIDForProxSimple;

        case 222: // '\026'
            //return sharedpreferences.getInt("event_222", actionIDForProxDouble);
            return actionIDForProxDouble;

        case 211: // '\025'
            //return sharedpreferences.getInt("event_211", actionIDForProxLong);
            return actionIDForProxLong;
        }
    }

    public int getDoubleWaveSpeed()
    {
        return doubleWaveSpeed;
    }

    public int getDrawableID()
    {
        return drawableID;
    }

    public int getHelpTextID()
    {
        return helpTextID;
    }

    public int getId()
    {
        return id;
    }
    public int getLabelID()
    {
        return labelID;
    }

    public int getLongStayDurationProx()
    {
        return longStayDurationProx;
    }

    public String getReadableName()
    {
        switch (id)
        {
        default:
            return null;

        case 1: // '\001'
            return "wave_over";

        case 2: // '\002'
            return "pocket";

        case 3: // '\003'
            return "hammer";

        case 4: // '\004'
            return "custom";
        }
    }

    public int getSoundSensitivity()
    {
        return soundSensitivity;
    }

    public int getxSensitivity()
    {
        return xSensitivity;
    }

    public int getySensitivity()
    {
        return ySensitivity;
    }

    public int getzSensitivity()
    {
        return zSensitivity;
    }

    public boolean isAccEnabled()
    {
        return accEnabled;
    }

    public boolean isAvailable()
    {
        return isAvailable;
    }

    public boolean isProxEnabled()
    {
        return proxEnabled;
    }

    public boolean isSoundEnabled()
    {
        return soundEnabled;
    }

    public void setAccActiveAnalysisDuration(int i)
    {
        accActiveAnalysisDuration = i;
    }

    public void setAccEnabled(boolean flag)
    {
        accEnabled = flag;
    }

    public void setActionIDForAccX(int i)
    {
        if (i != -1)
        {
            actionIDForAccX = i;
        }
    }

    public void setActionIDForAccY(int i)
    {
        if (i != -1)
        {
            actionIDForAccY = i;
        }
    }

    public void setActionIDForAccZ(int i)
    {
        if (i != -1)
        {
            actionIDForAccZ = i;
        }
    }

    public void setActionIDForProxDouble(int i)
    {
        if (i != -1)
        {
            actionIDForProxDouble = i;
        }
    }

    public void setActionIDForProxLong(int i)
    {
        if (i != -1)
        {
            actionIDForProxLong = i;
        }
    }

    public void setActionIDForProxSimple(int i)
    {
        if (i != -1)
        {
            actionIDForProxSimple = i;
        }
    }

    public void setAvailable(boolean flag)
    {
        isAvailable = flag;
    }

    public void setDoubleWaveSpeed(int i)
    {
        doubleWaveSpeed = i;
    }

    public void setDrawableID(int i)
    {
        drawableID = i;
    }

    public void setHelpTextID(int i)
    {
        helpTextID = i;
    }

    public void setLabelID(int i)
    {
        labelID = i;
    }

    public void setLongStayDurationProx(int i)
    {
        longStayDurationProx = i;
    }

    public void setProxEnabled(boolean flag)
    {
        proxEnabled = flag;
    }

    public boolean setSoundEnabled(boolean flag)
    {
        soundEnabled = flag;
        return flag;
    }

    public void setSoundSensitivity(int i)
    {
        soundSensitivity = i;
    }

    public void setxSensitivity(int i)
    {
        xSensitivity = i;
    }

    public void setySensitivity(int i)
    {
        ySensitivity = i;
    }

    public void setzSensitivity(int i)
    {
        zSensitivity = i;
    }

    public String toString()
    {
        return (new StringBuilder("id:")).append(id).append(", prox(").append(proxEnabled).append("), acc(").append(accEnabled).append("):{").append(xSensitivity).append(";").append(ySensitivity).append(";").append(zSensitivity).append(";").append(soundSensitivity).append("}").toString();
    }
}
