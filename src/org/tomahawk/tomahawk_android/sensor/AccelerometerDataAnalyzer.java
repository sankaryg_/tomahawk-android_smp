package org.tomahawk.tomahawk_android.sensor;

import java.util.ArrayList;
import java.util.Date;


public class AccelerometerDataAnalyzer
{
    public static interface MyAccelerometerEventListener
    {

        public abstract void onXTresholdReached();

        public abstract void onYTresholdReached();

        public abstract void onZTresholdReached(int i, int j);
    }


    public static final int ACC_DISABLED_AXIS_SENSITIVITY_VALUE = 9999;
    private int activeAnalysisDuration;
    private MyAccelerometerEventListener listener;
    private int previousSoundValue;
    private int rateValueCalculationSize;
    private int refreshDelay;
    private int soundSensitivity;
    private DatedPooler soundValuePooler;
    private ArrayList<Long> times;
    private long waitUntil;
    private int xSensitivity;
    private DatedPooler xValuePooler;
    private int ySensitivity;
    private DatedPooler yValuePooler;
    private int zSensitivity;
    private DatedPooler zValuePooler;

    public AccelerometerDataAnalyzer(MyAccelerometerEventListener myaccelerometereventlistener)
    {
        activeAnalysisDuration = 0;
        refreshDelay = -1;
        times = new ArrayList<Long>();
        rateValueCalculationSize = 8;
        previousSoundValue = 0;
        listener = myaccelerometereventlistener;
    }

    private void initRateAndPoolers()
    {
label0:
        {
            long l = (new Date()).getTime();
            int i = times.size();
            if (i == 0 || l - ((Long)times.get(i - 1)).longValue() > 8L)
            {
                times.add(Long.valueOf(l));
                i++;
            }
            if (i == rateValueCalculationSize)
            {
                long l1 = ((Long)times.get(0)).longValue();
                refreshDelay = Math.round((((Long)times.get(-1 + rateValueCalculationSize)).longValue() - l1) / (long)(-1 + rateValueCalculationSize));
                if (refreshDelay <= 0)
                {
                    break label0;
                }
                int j = Math.min((int)Math.max(2D, Math.ceil(activeAnalysisDuration / refreshDelay)), PrefsHelper.SENSOR_DATEDPOOLER_MAX_SIZE);
                xValuePooler = new DatedPooler(j, activeAnalysisDuration);
                yValuePooler = new DatedPooler(j, activeAnalysisDuration);
                zValuePooler = new DatedPooler(j, activeAnalysisDuration);
                soundValuePooler = new DatedPooler(j, activeAnalysisDuration);
            }
            return;
        }
        times.clear();
        refreshDelay = -1;
    }

    public int[] addValues(float af[], int i)
    {

        int ai[] = {
            0, 0, 0, 0
        };
        if (refreshDelay != -1) {
        	label0:
        	{
                int ai1[];
                boolean flag;
                int j;
                boolean flag1;
                int k;
                int l;
                int i1;
                int j1;
                int k1;
                int l1;
                int i2;
                int j2;
                if (i <= 0)
                {
                    i = previousSoundValue;
                } else
                {
                    previousSoundValue = i;
                }
                soundValuePooler.queue(i);
                ai1 = soundValuePooler.getExtremes();
                if (ai1 != null)
                {
                    ai[3] = ai1[1];
                }
                if (i != -1)
                {
                    flag = false;
                    if (ai1 == null)
                    {
                        break label0;
                    }
                    int k2 = ai[3];
                    int l2 = soundSensitivity;
                    flag = false;
                    if (k2 < l2)
                    {
                        //break label0;
                    }
                }
                flag = true;
            
            j = xSensitivity;
            flag1 = false;
            if (j < 9999)
            {
                k1 = Math.round(100F * af[0]);
                xValuePooler.queue(k1);
                l1 = xValuePooler.getTotalAmplitude();
                i2 = xSensitivity;
                flag1 = false;
                if (l1 > i2)
                {
                    flag1 = false;
                    if (flag)
                    {
                        j2 = (System.currentTimeMillis() != waitUntil)?1:0;
                        flag1 = false;
                        if (j2 > 0)
                        {
                            xValuePooler.reset();
                            soundValuePooler.reset();
                            listener.onXTresholdReached();
                            flag1 = true;
                        }
                    }
                }
                ai[0] = l1;
            }
            if (ySensitivity < 9999)
            {
                i1 = Math.round(100F * af[1]);
                yValuePooler.queue(i1);
                j1 = yValuePooler.getTotalAmplitude();
                if (j1 > ySensitivity && flag && System.currentTimeMillis() > waitUntil)
                {
                    listener.onYTresholdReached();
                    yValuePooler.reset();
                    soundValuePooler.reset();
                    flag1 = true;
                }
                ai[1] = j1;
            }
            if (zSensitivity < 9999)
            {
                k = Math.round(100F * af[2]);
                zValuePooler.queue(k);
                l = zValuePooler.getTotalAmplitude();
                if (l > zSensitivity && flag && System.currentTimeMillis() > waitUntil)
                {
                    zValuePooler.reset();
                    soundValuePooler.reset();
                    listener.onZTresholdReached(l, ai[3]);
                    flag1 = true;
                }
                ai[2] = l;
            }
            
            if (flag1)
            {
                waitUntil = System.currentTimeMillis() + (long)1300;
                return ai;
            }
            }
        } else{
        initRateAndPoolers();
        }
        return ai;

        
    
    }

    public void setPreset(SensorMode sensormode)
    {
        activeAnalysisDuration = sensormode.getAccActiveAnalysisDuration();
        xSensitivity = sensormode.getxSensitivity();
        ySensitivity = sensormode.getySensitivity();
        zSensitivity = sensormode.getzSensitivity();
        soundSensitivity = sensormode.getSoundSensitivity();
    }

    public void updateXSensitivity(int i)
    {
        xSensitivity = i;
    }

    public void updateYSensitivity(int i)
    {
        ySensitivity = i;
    }

    public void updateZSensitivity(int i)
    {
        zSensitivity = i;
    }
}
