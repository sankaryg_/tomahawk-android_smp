package org.tomahawk.tomahawk_android.sensor;


import android.util.Log;

import org.tomahawk.tomahawk_android.R;
import org.tomahawk.tomahawk_android.services.PlaybackService;

public class UserMappableAction {

	private static final Action DO_LOWER_MUSIC_VOLUME = new Action(5,
			"do lower stream volume", 0, 300);
	public static final int DO_LOWER_MUSIC_VOLUME_ID = 5;
	private static final Action DO_NEXT_TRACK = new Action(0, PlaybackService.ACTION_NEXT,
			R.string.preferences_action_next);
	public static final int DO_NEXT_TRACK_ID = 0;
	private static final Action DO_NOTHING = new Action(3, "", R.string.preferences_action_none);
	public static final int DO_NOTHING_ID = 3;
	private static final Action DO_PLAY_PAUSE = new Action(2,
			PlaybackService.ACTION_PLAYPAUSE, R.string.preferences_action_playpause);
	public static final int DO_PLAY_PAUSE_ID = 2;
	private static final Action DO_PREV_TRACK = new Action(1,
			PlaybackService.ACTION_PREVIOUS, R.string.preferences_action_previous);
	public static final int DO_PREV_TRACK_ID = 1;
	private static final Action DO_RAISE_MUSIC_VOLUME = new Action(4,
			"do raise stream volume", 0, 300);
	public static final int DO_RAISE_MUSIC_VOLUME_ID = 4;
	private static final int TOTAL_NUMBER_OF_ACTION = 6;
	private static Action actions[];

	public UserMappableAction() {
	}

	public static Action getActionById(int i) {
		Action action;
		try {
			Log.d("T1","action_id "+i);
			action = getMappableActions()[i];
		} catch (IndexOutOfBoundsException indexoutofboundsexception) {
			throw new Error((new StringBuilder("invalid action ID : ")).append(
					i).toString());
		}
		return action;
	}

	public static Action getActionByIntent(String s) {
		Action aaction[] = getMappableActions();
		int i = 0;
		do {
			if (i >= aaction.length) {
				return null;
			}
			if (aaction[i].intentAction.equals(s)) {
				return aaction[i];
			}
			i++;
		} while (true);
	}

	public static int getActionPosition(int i) {
		int j = 0;
		for(;j<getMappableActions().length;j++){
		if (getMappableActions()[j].id == i)
			return j;
		}
		return j;
	}

	public static Action[] getMappableActions() {
		if (actions == null) {
			actions = new Action[4];
			insertAction(DO_NEXT_TRACK);
			insertAction(DO_PREV_TRACK);
			insertAction(DO_PLAY_PAUSE);
			/*insertAction(DO_LOWER_MUSIC_VOLUME);
			insertAction(DO_RAISE_MUSIC_VOLUME);*/
			insertAction(DO_NOTHING);
		}
		return actions;
	}

	private static void insertAction(Action action) {
		actions[action.id] = action;
	}

}
