package org.tomahawk.tomahawk_android.sensor;

import java.util.Vector;

public class Pooler {

    public static final int INVALID_VALUE = -9999;
    private int insertions;
    private int size;
    private Vector<Integer> values;

    public Pooler(int i)
    {
        insertions = 0;
        if (i < 2)
        {
            i = 2;
        }
        size = i;
        values = new Vector<Integer>(i);
    }

    public int get(int i)
    {
        int j;
        try
        {
            j = ((Integer)values.get(i)).intValue();
        }
        catch (Exception exception)
        {
            return getAverage();
        }
        return j;
    }

    public int getAverage()
    {
        while (size == 0 || values.size() < size) 
        {
            return -9999;
        }
        int i = 0;
        for (int j = 0; j < size; j++)
        {
            i += get(j);
        }

        return i / size;
    }

    public int[] getExtremes()
    {
        int i;
        int j;
        i = ((Integer)values.get(0)).intValue();
        j = ((Integer)values.get(0)).intValue();
        int k = 1;
        int l = values.size();
        
        while(k<l){
        	if (values.size() < size)
            {
                break;// MISSING_BLOCK_LABEL_140;
            }
            
        	int i1 = ((Integer)values.get(k)).intValue();
        if (i1 > j)
        {
            j = i1;
        } else
        if (i1 < i)
        {
            i = i1;
        }
        k++;
}
        int ai[] = {
            i, j
        };
        return ai;
        
    }

    public int getHighestAmplitudeFromNeutral(int i)
    {
        if (i == -9999)
        {
            return 0;
        } else
        {
            int ai[] = getExtremes();
            return Math.max(getPositiveAmplitude(ai, i), getNegativeAmplitude(ai, i));
        }
    }

    public int getNegativeAmplitude(int ai[], int i)
    {
        int j = 0;
        if (ai != null)
        {
            j = 0;
            if (i != -9999)
            {
                j = Math.abs(i - ai[0]);
            }
        }
        return j;
    }

    public int getPositiveAmplitude(int ai[], int i)
    {
        if (ai != null && i != -9999)
        {
            return Math.abs(i - ai[1]);
        } else
        {
            return 0;
        }
    }

    public int getSize()
    {
        return values.size();
    }

    public int getTotalAmplitude()
    {
        int ai[] = getExtremes();
        int i = 0;
        if (ai != null)
        {
            i = Math.abs(ai[1] - ai[0]);
        }
        return i;
    }

    public boolean isInitialized()
    {
        return insertions >= size;
    }

    public int queue(int i)
    {
        insertions = 1 + insertions;
        int j = -9999;
        if (size == 0)
        {
            return j;
        }
        if (i != -9999)
        {
            values.add(Integer.valueOf(i));
        }
        if (values.size() > size)
        {
            j = ((Integer)values.remove(0)).intValue();
        }
        return j;
    }

    public void reset()
    {
        values.clear();
        insertions = 0;
    }

    public String toString()
    {
        return values.elements().toString();
    }

    public void updateSize(int i)
    {
        if (i < 2)
        {
            i = 2;
        }
        size = i;
        for (; values.size() > i; values.remove(0)) { }
    }
}
