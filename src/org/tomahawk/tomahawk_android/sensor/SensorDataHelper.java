

package org.tomahawk.tomahawk_android.sensor;

import android.util.Log;
import android.widget.Toast;

import org.tomahawk.libtomahawk.infosystem.CalibrationEventListener;
import org.tomahawk.libtomahawk.infosystem.SensorServiceState;
import org.tomahawk.tomahawk_android.TomahawkApp;
import org.tomahawk.tomahawk_android.views.SensorSeekBar;

public class SensorDataHelper
{

    private AccelerometerDataAnalyzer accAnalyzer;
    private int accCalibrationValue;
    private SensorSeekBar accXSeekbar;
    private SensorSeekBar accYSeekbar;
    private SensorSeekBar accZSeekbar;
    private CalibrationEventListener calibrationListener;
    private boolean calibrationThresholdReached;
    private SensorMode currentPreset;
    private int currentPresetId;
    private boolean intentBroadcastEnabled;
    private ProximityDataAnalyzer proxAnalyzer;
    private SensorServiceState sensorServiceState;
    private TomahawkApp smp;
    private int soundCalibrationValue;
    private SensorSeekBar soundSeekbar;

    public SensorDataHelper(TomahawkApp sensormusicplayer)
    {
        intentBroadcastEnabled = true;
        calibrationListener = null;
        accCalibrationValue = 0;
        soundCalibrationValue = 0;
        calibrationThresholdReached = false;
        smp = (TomahawkApp)sensormusicplayer;
       
        sensorServiceState = new SensorServiceStopped(smp);
        accAnalyzer = new AccelerometerDataAnalyzer(new AccelerometerDataAnalyzer.MyAccelerometerEventListener() {


            public void onXTresholdReached()
            {
                if (intentBroadcastEnabled)
                {
                    Toast.makeText(smp, "XThreshold-Accelerometer", Toast.LENGTH_SHORT).show();
                    IntentBroadcastHelper.startIntentBroadcast(IntentMapping.getActionForEvent(smp, 100, smp.getCurrentPresetID(),TomahawkApp.getContext()), smp, "shake");
                }
               //
            }

            public void onYTresholdReached()
            {
                if (intentBroadcastEnabled)
                {
                    Toast.makeText(smp, "YThreshold-Accelerometer", Toast.LENGTH_SHORT).show();
                    IntentBroadcastHelper.startIntentBroadcast(IntentMapping.getActionForEvent(smp, 111, smp.getCurrentPresetID(),TomahawkApp.getContext()), smp, "shake");
                }
                //
            }

            public void onZTresholdReached(int i, int j)
            {
            	if (intentBroadcastEnabled)
                {
            		Toast.makeText(smp, "ZThreshold-Accelerometer", Toast.LENGTH_SHORT).show();
                    IntentBroadcastHelper.startIntentBroadcast(IntentMapping.getActionForEvent(smp, 122, smp.getCurrentPresetID(),TomahawkApp.getContext()), smp, "shake");
                } else
                if (calibrationListener != null && !calibrationThresholdReached)
                {
                	Toast.makeText(smp, "ZThreshold-Accelerometer-Calibration "+i+"_"+j, Toast.LENGTH_SHORT).show();
                    
                    calibrationThresholdReached = true;
                    accCalibrationValue = i;
                    soundCalibrationValue = j;
                    return;
                }
            }


        });
        proxAnalyzer = new ProximityDataAnalyzer(smp, currentPreset, new ProximityDataAnalyzer.MyProximityEventListener() {

             public void onCovered()
            {
            	
            	//Toast.makeText(smp, "Covered-Proximity", Toast.LENGTH_SHORT).show();
            	
            }

            public void onDoubleWaveOver()
            {
                if (intentBroadcastEnabled)
                {
                    Toast.makeText(smp, "DoubleWaveOver-Proximity", Toast.LENGTH_SHORT).show();
                    IntentBroadcastHelper.startIntentBroadcast(IntentMapping.getActionForEvent(smp, 222, smp.getCurrentPresetID(),TomahawkApp.getContext()), smp, "proximity");
                }

            }

            public void onLongStayOver()
            {
                if (intentBroadcastEnabled)
                {
                    Toast.makeText(smp, "LongStayOver-Proximity", Toast.LENGTH_SHORT).show();
                    IntentBroadcastHelper.startIntentBroadcast(IntentMapping.getActionForEvent(smp, 211, smp.getCurrentPresetID(),TomahawkApp.getContext()), smp, "proximity");
                }
                //
            }

            public void onUncovered()
            {
            	//Toast.makeText(smp, "UnCovered-Proximity", Toast.LENGTH_SHORT).show();
            }

            public void onWaveOver()
            {
                if (intentBroadcastEnabled)
                {
                    Toast.makeText(smp, "WaveOver-Proximity", Toast.LENGTH_SHORT).show();
                    IntentBroadcastHelper.startIntentBroadcast(IntentMapping.getActionForEvent(smp, 200, smp.getCurrentPresetID(),TomahawkApp.getContext()), smp, "proximity");
                }
                //
            }

            

        });
    }


    public void addRawAccValues(float af[], int i)
    {
        if (af.length == 3)
        {
            int ai[] = accAnalyzer.addValues(af, i);
            if (calibrationThresholdReached)
            {
                if (ai[2] > accCalibrationValue)
                {
                    accCalibrationValue = ai[2];
                    calibrationThresholdReached = true;
                }
                if (ai[3] > soundCalibrationValue)
                {
                    soundCalibrationValue = ai[3];
                    calibrationThresholdReached = true;
                }
                if (true && calibrationListener != null)
                {
                    calibrationListener.onCalibrationSet(accCalibrationValue, soundCalibrationValue);
                    accCalibrationValue = 0;
                    soundCalibrationValue = 0;
                    calibrationThresholdReached = false;
                }
            }
            if (accXSeekbar != null)
            {
                accXSeekbar.setParsedSecondaryProgress(Math.round(ai[0]));
            }
            if (accYSeekbar != null)
            {
                accYSeekbar.setParsedSecondaryProgress(Math.round(ai[1]));
            }
            if (accZSeekbar != null)
            {
                accZSeekbar.setParsedSecondaryProgress(Math.round(ai[2]));
            }
            if (soundSeekbar != null)
            {
                soundSeekbar.setParsedSecondaryProgress(Math.round(i));
            }
            return;
        } else
        {
            Log.e("Sensor Data", "weird accelerometer datas");
            return;
        }
    }

    public void addRawProxValue(float f)
    {
        proxAnalyzer.addValues(f);
    }

    public void disableSensorService()
    {
        sensorServiceState.disableSensorService();
        //smp.sendBroadcast(new Intent(Constants.New_Mode_Loaded));
    }

    public void enableSensorService()
    {
        sensorServiceState.enableSensorService();
        if (currentPreset != null)
        {
            loadPreset(currentPreset);
        }
    }

    public AccelerometerDataAnalyzer getAccAnalyzer()
    {
        return accAnalyzer;
    }

    public SensorServiceState getSensorServiceState()
    {
        return sensorServiceState;
    }

    public boolean isIntentBroadcastEnabled()
    {
        return intentBroadcastEnabled;
    }

    public void loadPreset(int i)
    {
        loadSensorMode(i, false);
    }

    public void loadPreset(SensorMode sensormode)
    {
        Log.d("SensorDataHelper", (new StringBuilder("load ->")).append(sensormode).toString());
        currentPreset = sensormode;
        proxAnalyzer.setPreset(sensormode);
        accAnalyzer.setPreset(sensormode);
        sensorServiceState.loadPreset(sensormode);
    }

    public void loadSensorMode(int i, boolean flag)
    {
        if (flag || i != currentPresetId)
        {
            SensorMode sensormode = smp.getSensorModeHelper().getPresetByID(i);
            currentPresetId = i;
            loadPreset(sensormode);
        }
    }

    public void removeAllSeekbars()
    {
        accXSeekbar = null;
        accYSeekbar = null;
        accZSeekbar = null;
        soundSeekbar = null;
    }

    public void setAccXSeekbar(SensorSeekBar zguiseekbar)
    {
        accXSeekbar = zguiseekbar;
    }

    public void setAccYSeekbar(SensorSeekBar zguiseekbar)
    {
        accYSeekbar = zguiseekbar;
    }

    public void setAccZSeekbar(SensorSeekBar zguiseekbar)
    {
        accZSeekbar = zguiseekbar;
    }

    public void setIntentBroadcastEnabled(boolean flag)
    {
        intentBroadcastEnabled = flag;
    }

    public void setListenerForPocketCalibration(CalibrationEventListener calibrationeventlistener)
    {
        calibrationListener = calibrationeventlistener;
        SensorMode sensormode = new SensorMode(-99);
        sensormode.setAccEnabled(true);
        sensormode.setzSensitivity(400);
        sensormode.setSoundEnabled(true);
        sensormode.setSoundSensitivity(15000);
        loadPreset(sensormode);
    }

    public void setSensorServiceState(SensorServiceState sensorservicestate)
    {
        sensorServiceState = sensorservicestate;
    }

    public void setSoundSeekbar(SensorSeekBar zguiseekbar)
    {
        soundSeekbar = zguiseekbar;
    }

    public void stopCalibration()
    {
        calibrationListener = null;
    }







}
