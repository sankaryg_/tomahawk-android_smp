package org.tomahawk.tomahawk_android.sensor;

import android.content.Context;
import android.content.Intent;

import org.tomahawk.tomahawk_android.services.PlaybackService;

import de.greenrobot.event.EventBus;

public class MyIntents
{

    public static final String DO_BUY_FEATURE = "buy feature";
    public static final String DO_DISPLAY_LIB_FRAG = "display lib frag";
    private static final String DO_PLAY_TRACK_AT = "play track at";
    public static final String DO_PLAY_TRACK_FILE = "play track file";
    public static final String DO_POPUP_BUY_PROCESS = "popup buy process";
    public static final String DO_POPUP_BUY_SUCCESS = "popup buy success";
    public static final String DO_POPUP_SOMETHING = "popup smtg";
    private static final String EXTRA_ALBUMKEY = "albumkey";
    private static final String EXTRA_ARTISTKEY = "artistkey";
    private static final String EXTRA_DRAWABLE_ID = "drawable id";
    private static final String EXTRA_FORCE_SET = "forceSet";
    private static final String EXTRA_MESSAGE_RES_ID = "msg res id";
    private static final String EXTRA_MESSAGE_STRING = "msg res string";
    private static final String EXTRA_NO_BUYING_POPUP = "forceSet";
    private static final String EXTRA_PATH = "path";
    private static final String EXTRA_POSITION = "track pos";
    private static final String EXTRA_PRODUCT_ID = "product id";
    public static final String MODE_ID = "modeId";
    private static final String SPEECH_KEY = "speech";
    private static final String TOAST_LENGTH_KEY = "toast_duration";
    private static final String TOAST_TEXT_RESID_KEY = "toast_resid";

    public MyIntents()
    {
    }
    public static int getModeIdFromIntent(Intent intent)
    {
        return intent.getIntExtra("modeId", -1);
    }

    public static void selectSensorMode(Context context, int i, boolean flag, boolean flag1)
    {
        /*Intent intent = new Intent(Constants.Do_Select_Sensor);
        intent.putExtra("modeId", i);
        intent.putExtra("forceSet", flag);
        intent.putExtra("forceSet", flag1);
        context.sendBroadcast(intent);*/
        PlaybackService.SelectSensorEvent event = new PlaybackService.SelectSensorEvent();
        event.sensorMode = i;
        EventBus.getDefault().post(event);
    }


}
