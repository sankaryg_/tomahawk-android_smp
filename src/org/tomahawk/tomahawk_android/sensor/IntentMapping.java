package org.tomahawk.tomahawk_android.sensor;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;

import org.tomahawk.tomahawk_android.TomahawkApp;


public class IntentMapping
{

    public static final int PROXIMITY_LONG_STAY = 211;
    public static final int PROXIMITY_WAVE_OVER_DOUBLE = 222;
    public static final int PROXIMITY_WAVE_OVER_SIMPLE = 200;
    public static final int SHAKE_TRESHOLD_X_REACHED = 100;
    public static final int SHAKE_TRESHOLD_Y_REACHED = 111;
    public static final int SHAKE_TRESHOLD_Z_REACHED = 122;

    public IntentMapping()
    {
    }

    public static Action getActionForEvent(TomahawkApp tomahawkApp, int i, int j,Context mContext)//i=200, j=4
    {
        return UserMappableAction.getActionById(tomahawkApp.getSensorModeHelper().getPresetByID(j).getActionIDForEventID(i,mContext));
    }

    private static Action getDefaultActionForEvent(int i)
    {
        switch (i)
        {
        case 13: // '\r'
        case 14: // '\016'
        case 15: // '\017'
        case 16: // '\020'
        case 17: // '\021'
        case 18: // '\022'
        case 19: // '\023'
        default:
            return null;

        case 100: // '\n'
            return UserMappableAction.getActionById(0);

        case 111: // '\013'
            return UserMappableAction.getActionById(0);

        case 122: // '\f'
            return UserMappableAction.getActionById(0);

        case 200: // '\024'
            return UserMappableAction.getActionById(0);

        case 211: // '\025'
            return UserMappableAction.getActionById(2);

        case 222: // '\026'
            return UserMappableAction.getActionById(1);
        }
    }

    public static void setCustomModeActionForEvent(SharedPreferences sharedpreferences, int i, int j)
    {
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.putInt((new StringBuilder("event_")).append(i).toString(), j);
        Log.d("T1","event_"+i+","+j);
        editor.apply();
    }
}
