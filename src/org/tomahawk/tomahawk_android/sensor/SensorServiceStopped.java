
package org.tomahawk.tomahawk_android.sensor;

import android.content.Context;
import android.os.Handler;
import android.util.Log;

import org.tomahawk.libtomahawk.infosystem.SensorServiceState;
import org.tomahawk.tomahawk_android.services.PlaybackService;

import de.greenrobot.event.EventBus;


public class SensorServiceStopped
    implements SensorServiceState
{

    private Handler loadPresetHandler;
    private Runnable loadPresetRunnable;
    private Context mContext;
    private final int timeBeforeRetryLoad = 200;

    public SensorServiceStopped(Context context)
    {
        Log.d("", "entering SensorServiceStopped state");
        mContext = context;
    }

    public void disableSensorService()
    {

    }

    public void enableSensorService()
    {
        /*Intent intent = new Intent(mContext,SensorService.class);//"com.smartfoxsoft.sensitivemusicplayer.sensor.SensorService");
        mContext.startService(intent);*/
    }

    public boolean isServiceRunning()
    {
        return false;
    }

    public void loadPreset(final SensorMode preset)
    {
        if (preset.getId() != 0)
        {
            loadPresetHandler = new Handler();
            /*loadPresetRunnable = new Runnable() {

               
                public void run()
                {
                    ((TomahawkApp)mContext.getApplicationContext()).getSensorDataHelper().loadPreset(preset);
                }

            
            };*/
            Log.d("", "try to load preset while service is stopped");
            //enableSensorService();
            toggleSensorsFromPreset(preset);
            //loadPresetHandler.post(loadPresetRunnable);
            //loadPresetHandler.postDelayed(loadPresetRunnable, 200L);
        }
    }

    private void toggleSensorsFromPreset(SensorMode sensormode)
    {
        if (sensormode != null)
        {
            Log.d("", "entering SensorService toggele sensor state");
            PlaybackService.EnableDisableEvent event = new PlaybackService.EnableDisableEvent();

            if (sensormode.isAccEnabled())
            {
                event.enableAccelerometer=true;

                // smp.sendBroadcast(new Intent(Constants.Enable_Accelerometer));
            } else
            {
                event.enableAccelerometer=false;
            }
            if (sensormode.isProxEnabled())
            {
                // smp.sendBroadcast(new Intent(Constants.Enable_Proximity));
                event.enableProximity=true;

            } else
            {
                event.enableProximity=false;
                // smp.sendBroadcast(new Intent(Constants.Disable_Proximity));
            }
            if (sensormode.isSoundEnabled())
            {
                event.enableSoundMeter=true;
                //smp.sendBroadcast(new Intent(Constants.Enable_SoundMeter));
                //return;
            }else {
                event.enableSoundMeter = false;
            }
            EventBus.getDefault().post(event);
        } else
        {
            Log.e("NOT GOOD", "preset not loaded yet");
            return;
        }
    }

}
