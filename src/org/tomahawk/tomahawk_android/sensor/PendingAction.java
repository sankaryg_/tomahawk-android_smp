package org.tomahawk.tomahawk_android.sensor;

import android.os.Handler;

public class PendingAction
{

    private Action action;
    private Handler handler;
    private String requester;
    private Runnable runnable;

    public PendingAction(Action action1, Handler handler1, Runnable runnable1, String s)
    {
        action = action1;
        handler = handler1;
        runnable = runnable1;
        requester = s;
    }

    public Action getAction()
    {
        return action;
    }

    public Handler getHandler()
    {
        return handler;
    }

    public String getRequester()
    {
        return requester;
    }

    public Runnable getRunnable()
    {
        return runnable;
    }
}
