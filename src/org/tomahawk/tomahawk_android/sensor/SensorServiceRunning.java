package org.tomahawk.tomahawk_android.sensor;

import android.util.Log;

import org.tomahawk.libtomahawk.infosystem.SensorServiceState;
import org.tomahawk.tomahawk_android.TomahawkApp;
import org.tomahawk.tomahawk_android.services.PlaybackService;

import de.greenrobot.event.EventBus;

public class SensorServiceRunning
    implements SensorServiceState
{

    private TomahawkApp smp;

    public SensorServiceRunning(TomahawkApp sensormusicplayer)
    {
        Log.d("", "entering SensorServiceRunning state");
        smp = sensormusicplayer;
    }

    private void toggleSensorsFromPreset(SensorMode sensormode)
    {
        if (sensormode != null)
        {
            Log.d("", "entering SensorService toggele sensor state");
            PlaybackService.EnableDisableEvent event = new PlaybackService.EnableDisableEvent();

            if (sensormode.isAccEnabled())
            {
                event.enableAccelerometer=true;

               // smp.sendBroadcast(new Intent(Constants.Enable_Accelerometer));
            } else
            {
                event.enableAccelerometer=false;
            }
            if (sensormode.isProxEnabled())
            {
               // smp.sendBroadcast(new Intent(Constants.Enable_Proximity));
                event.enableProximity=true;

            } else
            {
                event.enableProximity=false;
               // smp.sendBroadcast(new Intent(Constants.Disable_Proximity));
            }
            if (sensormode.isSoundEnabled())
            {
                event.enableSoundMeter=true;
                //smp.sendBroadcast(new Intent(Constants.Enable_SoundMeter));
                //return;
            }else {
                event.enableSoundMeter = false;
            }
            EventBus.getDefault().post(event);

        } else
        {
            Log.e("NOT GOOD", "preset not loaded yet");
            return;
        }
    }

    public void disableSensorService()
    {
    	/*Intent intent = new Intent(smp.getApplicationContext(),SensorService.class);
        smp.stopService(intent);*///new Intent("com.smartfoxsoft.sensitivemusicplayer.sensor.SensorService"));
    }

    public void enableSensorService()
    {
    }

    public boolean isServiceRunning()
    {
        return true;
    }

    public void loadPreset(SensorMode sensormode)
    {
        if (!sensormode.isAvailable())
        {
            sensormode = smp.getSensorModeHelper().getPresetByID(smp.getCurrentPresetID());//(3)
        }
        toggleSensorsFromPreset(sensormode);
    }
}
