package org.tomahawk.tomahawk_android.sensor;

import android.content.Context;
import android.content.SharedPreferences;
import android.hardware.Sensor;
import android.hardware.SensorManager;
import android.media.MediaRecorder;
import android.preference.PreferenceManager;
import android.util.Log;

import org.tomahawk.tomahawk_android.TomahawkApp;

import java.util.ArrayList;
import java.util.List;

public class PrefsHelper
{

    public static final String ACCELEROMETER_AVAILABILITY_KEY = "accelerometer available";
    public static final String MIC_AVAILABILITY_KEY = "mic available";
    public static final String ORIENTATION_AVAILABILITY_KEY = "orientation available";
    public static String PREF_VALUES_SEPARATOR = ";";
    public static final String PROXIMITY_AVAILABILITY_KEY = "proximity available";
    public static int POCKET_SENSITIVITY_WHEN_NO_MIC = 1500;
    public static int SENSOR_DATEDPOOLER_MAX_SIZE = 30;

    public PrefsHelper()
    {
    }

   public static float getAccelerometerMaxMesuredValue(SharedPreferences sharedpreferences)
    {
        return (float)sharedpreferences.getInt("acc max value", 2000);
    }

    public static short getBassBoostStrength(SharedPreferences sharedpreferences)
    {
        return (short)sharedpreferences.getInt("bassBoostLevel", 0);
    }

    public static boolean getCustomAccEnabled(SharedPreferences sharedpreferences)
    {
        return sharedpreferences.getBoolean("accelerometerEnabled", false);
    }

    public static int getCustomProxDoubleWaveSpeed(SharedPreferences sharedpreferences)
    {
        return sharedpreferences.getInt("customModeDoubleWaveSpeed", 600);
    }

    public static boolean getCustomProxEnabled(SharedPreferences sharedpreferences)
    {
        return sharedpreferences.getBoolean("proximityEnabled", false);
    }

    public static int getCustomProxLongStay(SharedPreferences sharedpreferences)
    {
        return sharedpreferences.getInt("proxLSDuration", 1000);
    }

    public static int getCustomXSensitivity(SharedPreferences sharedpreferences)
    {
        return sharedpreferences.getInt("sensitivityValueX", 2000);
    }

    public static int getCustomYSensitivity(SharedPreferences sharedpreferences)
    {
        return sharedpreferences.getInt("sensitivityValueY", 2000);
    }

    public static int getCustomZSensitivity(SharedPreferences sharedpreferences)
    {
        return sharedpreferences.getInt("sensitivityValueZ", 2000);
    }

       public static int getModeHammerSensitivity(SharedPreferences sharedpreferences, int i)
    {
        return sharedpreferences.getInt("hammerPresetSensitivty", i);
    }

    public static int getModePocketAccSensitivity(SharedPreferences sharedpreferences, int i)
    {
        return sharedpreferences.getInt("pocketPresetAccSensitivty", i);
    }

    public static float getModeProxUncoveredValue(SharedPreferences sharedpreferences)
    {
        return (float)(sharedpreferences.getInt("proximity uncovered value", 10) / 100);
    }


    public static int getPreviousSensorModeId(TomahawkApp tomahawkApp)
    {
        return tomahawkApp.getPrefs().getInt("selectedPresetID", 1);
    }



    public static int getWaveOverDoubleWaveSpeed(SharedPreferences sharedpreferences)
    {
        return sharedpreferences.getInt("waveoverDoubleWaveSpeed", 600);
    }

    public static int getWaveOverLongStay(SharedPreferences sharedpreferences)
    {
        return sharedpreferences.getInt("waveoverLSDuration", 500);
    }

    public static int getWaveOverDoubleWaveSpeedTemp(SharedPreferences sharedpreferences)
    {
        return sharedpreferences.getInt("waveoverDoubleWaveSpeedTemp", 600);
    }

    public static int getWaveOverLongStayTemp(SharedPreferences sharedpreferences)
    {
        return sharedpreferences.getInt("waveoverLSDurationTemp", 500);
    }

    public static boolean isAccelerometerAvailable(SharedPreferences sharedpreferences)
    {
        return sharedpreferences.getBoolean("accelerometer available", true);
    }

    public static boolean isAutoSensorMode(TomahawkApp tomahawkApp)
    {
        return tomahawkApp.getPrefs().getBoolean("auto sensor mode", false);
    }


    public static boolean isMicAvailable(SharedPreferences sharedpreferences)
    {
        return sharedpreferences.getBoolean("mic available", true);
    }

    
    public static boolean isOrientationSensorAvailable(SharedPreferences sharedpreferences)
    {
        return sharedpreferences.getBoolean("orientation available", true);
    }

   
    public static boolean isPocketModeCompat(Context context)
    {
        return PreferenceManager.getDefaultSharedPreferences(context).getBoolean("pocket_mode_compat", false);
    }

    public static boolean isProxSensorAvailable(SharedPreferences sharedpreferences)
    {
        return sharedpreferences.getBoolean("proximity available", true);
    }

    public static boolean isRdmPlayMode(TomahawkApp tomahawkApp)
    {
        return tomahawkApp.getPrefs().getBoolean("rdm play mode", false);
    }

    public static boolean isSensorDisabledByStop(TomahawkApp tomahawkApp)
    {
        return tomahawkApp.getPrefs().getBoolean("sensor_disabled_by_stop", false);
    }



    public static void resetCustomModeValues(SharedPreferences.Editor editor)
    {
        editor.remove("accelerometerEnabled");
        //editor.remove("proximityEnabled");
        //editor.remove("customModeDoubleWaveSpeed");
        //editor.remove("proxLSDuration");
        editor.remove("sensitivityValueX");
        editor.remove("sensitivityValueY");
        editor.remove("sensitivityValueZ");
        //editor.remove("event_21");
        //editor.remove("event_20");
        editor.remove("event_100");
        editor.remove("event_111");
        editor.remove("event_122");
    }
    
    public static void resetCustomModeProxValues(SharedPreferences.Editor editor)
    {
        //editor.remove("accelerometerEnabled");
        editor.remove("proximityEnabled");
        editor.remove("customModeDoubleWaveSpeed");
        editor.remove("proxLSDuration");
        //editor.remove("sensitivityValueX");
        //editor.remove("sensitivityValueY");
        //editor.remove("sensitivityValueZ");
        editor.remove("event_211");
        editor.remove("event_200");
        //editor.remove("event_10");
        //editor.remove("event_11");
        //editor.remove("event_12");
    }



    public static void setAccelerometerMaxMesuredValue(SharedPreferences.Editor editor, float f)
    {
        editor.putFloat("acc max value", f);
    }

    public static void setAutoSensorMode(TomahawkApp tomahawkApp, boolean flag)
    {
        tomahawkApp.getPrefEditor().putBoolean("auto sensor mode", flag);
    }


    public static void setCurrentSensorModeId(TomahawkApp tomahawkApp, int i)
    {
        tomahawkApp.getPrefEditor().putInt("selectedPresetID", i);
    }

    public static void setCustomAccEnabled(SharedPreferences.Editor editor, boolean flag)
    {
        editor.putBoolean("accelerometerEnabled", flag);
    }

    public static void setCustomProxDoubleWaveSpeed(SharedPreferences.Editor editor, int i)
    {
        editor.putInt("customModeDoubleWaveSpeed", i);
    }

    public static void setCustomProxEnabled(SharedPreferences.Editor editor, boolean flag)
    {
        editor.putBoolean("proximityEnabled", flag);
    }

    public static void setCustomProxLongStay(SharedPreferences.Editor editor, int i)
    {
        editor.putInt("proxLSDuration", i);
    }

    public static void setCustomXSensitivity(SharedPreferences.Editor editor, int i)
    {
        editor.putInt("sensitivityValueX", i);
    }

    public static void setCustomYSensitivity(SharedPreferences.Editor editor, int i)
    {
        editor.putInt("sensitivityValueY", i);
    }

    public static void setCustomZSensitivity(SharedPreferences.Editor editor, int i)
    {
        editor.putInt("sensitivityValueZ", i);
    }



    public static void setModeHammerSensitivity(SharedPreferences.Editor editor, int i)
    {
        editor.putInt("hammerPresetSensitivty", i);
    }

    public static void setModePocketAccSensitivity(SharedPreferences.Editor editor, int i)
    {
        editor.putInt("pocketPresetAccSensitivty", i);
    }

    public static void setModeProxUncoveredValue(SharedPreferences.Editor editor, float f)
    {
        editor.putInt("proximity uncovered value", Math.round(100F * f));
    }


    public static void setPocketModeCompat(SharedPreferences.Editor editor, boolean flag)
    {
        editor.putBoolean("pocket_mode_compat", flag);
    }



    public static void setSensorDisabledByStop(TomahawkApp tomahawkApp, boolean flag)
    {
        tomahawkApp.getPrefEditor().putBoolean("sensor_disabled_by_stop", flag);
    }


    public static void setWaveOverDoubleWaveSpeed(SharedPreferences.Editor editor, int i)
    {
        editor.putInt("waveoverDoubleWaveSpeed", i);
    }

    public static void setTempWaveOverDoubleWaveSpeed(SharedPreferences.Editor editor, int i)
    {
        editor.putInt("waveoverDoubleWaveSpeedTemp", i);
    }

    public static void setWaveOverLongStay(SharedPreferences.Editor editor, int i)
    {
        editor.putInt("waveoverLSDuration", i);
    }

    public static void setWaveOverLongStayTemp(SharedPreferences.Editor editor, int i)
    {
        editor.putInt("waveoverLSDurationTemp", i);
    }

    public static void updateSensorAvailability(SharedPreferences.Editor editor, SensorManager sensormanager)
    {
        List<Sensor> list = sensormanager.getSensorList(Sensor.TYPE_ALL);
        ArrayList<Integer> arraylist = new ArrayList<Integer>();
        int i = 0;
        do
        {
            if (i >= list.size())
            {
                try
                {
                    (new MediaRecorder()).setAudioSource(MediaRecorder.AudioSource.MIC);
                    editor.putBoolean("mic available", true);
                }
                catch (Exception exception)
                {
                    Log.d("SOUND_METER", "setAudioSource failed");
                    editor.putBoolean("mic available", false);
                    editor.putInt("pocketPresetAccSensitivty", POCKET_SENSITIVITY_WHEN_NO_MIC);
                }
                editor.putBoolean("accelerometer available", arraylist.contains(Integer.valueOf(Sensor.TYPE_ACCELEROMETER)));
                editor.putBoolean("proximity available", arraylist.contains(Integer.valueOf(Sensor.TYPE_PROXIMITY)));
                editor.putBoolean("orientation available", arraylist.contains(Integer.valueOf(Sensor.TYPE_ORIENTATION)));
                editor.commit();
                return;
            }
            arraylist.add(Integer.valueOf(((Sensor)list.get(i)).getType()));
            i++;
        } while (true);
    }

}
