
package org.tomahawk.tomahawk_android.sensor;

import java.util.Date;
import java.util.Vector;


public class DatedPooler extends Pooler
{

    private Vector<Long> dates;
    private int duration;

    public DatedPooler(int i, int j)
    {
        super(i);
        dates = new Vector<Long>(i);
        duration = j;
    }

    public int getDuration()
    {
        return duration;
    }

    public int queue(int i)
    {
        Long long1 = Long.valueOf((new Date()).getTime());
        int j = super.queue(i);
        int k = super.getSize();
        dates.add(long1);
        dates.size();
        for (; k > dates.size(); dates.remove(0)) { }
        if (k > 1)
        {
            Long long2 = Long.valueOf(((Long)dates.get(k - 1)).longValue() - ((Long)dates.get(0)).longValue());
            if (long2.longValue() > (long)(duration + duration / k))
            {
                super.updateSize(k - 1);
            } else
            if (long2.longValue() < (long)(duration - duration / k))
            {
                super.updateSize(k + 1);
                return j;
            }
        }
        return j;
      
    }

    public void setDuration(int i)
    {
        duration = i;
    }
}
