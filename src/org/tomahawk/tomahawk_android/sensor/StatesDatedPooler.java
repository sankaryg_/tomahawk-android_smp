package org.tomahawk.tomahawk_android.sensor;


public class StatesDatedPooler extends DatedPooler
{

    public StatesDatedPooler(int i, int j)
    {
        super(i, j);
    }

    public int getState()
    {
        if (super.isInitialized() && super.getTotalAmplitude() == 0)
        {
            return super.get(0);
        } else
        {
            return -1;
        }
    }
}
