package org.tomahawk.tomahawk_android.sensor;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.preference.PreferenceManager;
import android.util.Log;


public class SensorModeHelper
{

    public static final int DEFAULT_SELECTED_PRESET_ID = 1;
    public static final int POCKET_ACC_DEFAULT_VALUE = 1000;
    public static final int POCKET_ACC_MAX_MANUAL_VALUE = 2500;
    public static final int POCKET_ACC_MIN_MANUAL_VALUE = 500;
    public static final int POCKET_SOUND_MANUAL_MAX_VALUE = 30000;
    public static final int SENSOR_MODE_COUNT = 5;
    public static final int SENSOR_MODE_CUSTOM_ID = 4;
    public static final int SENSOR_MODE_HAMMER_ID = 3;
    public static final int SENSOR_MODE_NOSENSOR_ID = 0;
    public static final int SENSOR_MODE_POCKET_ID = 2;
    public static final int SENSOR_MODE_WAVEOVER_ID = 1;
    public static final int WAVEOVER_DEFAULT_DOUBLE_WAVE_SPEED = 600;
    public static final int WAVEOVER_DEFAULT_LS_DURATION = 500;
    public static final int WAVEOVER_MAX_DOUBLE_WAVE_SPEED = 1200;
    public static final int WAVEOVER_MAX_LS_DURATION = 2000;
    public static final int WAVEOVER_MIN_DOUBLE_WAVE_SPEED = 100;
    public static final int WAVEOVER_MIN_LS_DURATION = 200;
    private Context mContext;
    private SensorMode presets[];

    public SensorModeHelper(Context context)
    {
        presets = new SensorMode[5];
        mContext = context;
    }

    public static int fromDoubleWaveSpeedToRatio(int i)
    {
        return Math.round(100F * (((float)i - 200F) / 1100F));
    }

    public static int fromLongStayDurationToRatio(int i)
    {
        return Math.round(100F * (((float)i - 200F) / (float)1800));
    }

    public static int fromRatioToDoubleWaveSpeed(int i)
    {
        return Math.round(100F + 1100F * ((float)i / 100F));
    }

    public static int fromRatioToLongStayDuration(int i)
    {
        return Math.round(200F + 1800F * ((float)i / 100F));
    }

    private SensorMode generatePresetByID(int i)
    {
        SharedPreferences sharedpreferences = PreferenceManager.getDefaultSharedPreferences(mContext);
        switch (i)
        {
        default:
            return null;

        case 4: // '\004'
            SensorMode sensormode4 = getCustomSensorMode();
            presets[4] = sensormode4;
            return sensormode4;

        case 2: // '\002'
            SensorMode sensormode3 = new SensorMode(2);
            sensormode3.setAccEnabled(true);
            sensormode3.setzSensitivity(PrefsHelper.getModePocketAccSensitivity(sharedpreferences, 1000));
            sensormode3.setAccActiveAnalysisDuration(60);
            sensormode3.setSoundEnabled(true);
            sensormode3.setSoundSensitivity(30000);
            /*sensormode3.setDrawableID(0x7f02017c);
            sensormode3.setLabelID(0x7f0d00c2);
            sensormode3.setHelpTextID(0x7f0d00be);*/
            presets[2] = sensormode3;
            return sensormode3;

        case 1: // '\001'
            SensorMode sensormode2 = new SensorMode(1);
            sensormode2.setProxEnabled(true);
            sensormode2.setLongStayDurationProx(PrefsHelper.getWaveOverLongStay(sharedpreferences));
            sensormode2.setDoubleWaveSpeed(PrefsHelper.getWaveOverDoubleWaveSpeed(sharedpreferences));
            /*sensormode2.setDrawableID(0x7f020179);
            sensormode2.setLabelID(0x7f0d00c0);
            sensormode2.setHelpTextID(0x7f0d00ba);*/
            presets[1] = sensormode2;
            return sensormode2;

        case 3: // '\003'
            SensorMode sensormode1 = new SensorMode(3);
            sensormode1.setAccEnabled(true);
            sensormode1.setzSensitivity(PrefsHelper.getModeHammerSensitivity(sharedpreferences, 220));
            sensormode1.setAccActiveAnalysisDuration(60);
            /*sensormode1.setDrawableID(0x7f02017a);
            sensormode1.setLabelID(0x7f0d00c1);
            sensormode1.setHelpTextID(0x7f0d00bb);*/
            presets[3] = sensormode1;
            return sensormode1;

        case 0: // '\0'
            SensorMode sensormode = new SensorMode(0);
            /*sensormode.setDrawableID(0x7f02017b);
            sensormode.setLabelID(0x7f0d00c4);
            sensormode.setHelpTextID(0x7f0d00bf);*/
            presets[0] = sensormode;
            return sensormode;
        }
    }

    public SensorMode getCustomSensorMode()
    {
        SensorMode sensormode = new SensorMode(4);
        SharedPreferences sharedpreferences = PreferenceManager.getDefaultSharedPreferences(mContext);
        if (PrefsHelper.getCustomAccEnabled(sharedpreferences))
        {
            sensormode.setAccEnabled(true);
            sensormode.setxSensitivity(PrefsHelper.getCustomXSensitivity(sharedpreferences));
            sensormode.setySensitivity(PrefsHelper.getCustomYSensitivity(sharedpreferences));
            sensormode.setzSensitivity(PrefsHelper.getCustomZSensitivity(sharedpreferences));
            sensormode.setActionIDForAccX(sharedpreferences.getInt("event_100", -1));
            sensormode.setActionIDForAccY(sharedpreferences.getInt("event_111", -1));
            sensormode.setActionIDForAccZ(sharedpreferences.getInt("event_122", -1));
            sensormode.setAccActiveAnalysisDuration(60);
        }
        if (PrefsHelper.getCustomProxEnabled(sharedpreferences))
        {
            sensormode.setProxEnabled(true);
            sensormode.setLongStayDurationProx(PrefsHelper.getCustomProxLongStay(sharedpreferences));
            sensormode.setDoubleWaveSpeed(PrefsHelper.getCustomProxDoubleWaveSpeed(sharedpreferences));
            sensormode.setActionIDForProxSimple(sharedpreferences.getInt("event_200", -1));
            sensormode.setActionIDForProxDouble(sharedpreferences.getInt("event_222", -1));
            sensormode.setActionIDForProxLong(sharedpreferences.getInt("event_211", -1));
        }
        Resources resources = mContext.getResources();
        int i = resources.getIdentifier("custom_help_text", "string", mContext.getPackageName());
        int j = resources.getIdentifier("custom_label", "string", mContext.getPackageName());
        sensormode.setHelpTextID(i);
        sensormode.setLabelID(j);
        //sensormode.setDrawableID(0x7f020178);
        return sensormode;
    }

    public SensorMode[] getAllPresets()
    {
        int i = 0;
        do
        {
            if (i >= 5)
            {
                return presets;
            }
            getPresetByID(i);
            i++;
        } while (true);
    }

    public SensorMode getPresetByID(int i)
    {
        SensorMode sensormode;
        try
        {
            sensormode = presets[i];
        }
        catch (IndexOutOfBoundsException indexoutofboundsexception)
        {
            Log.e("MY ERROR", "unable to get preset (PresetHelper)");
            sensormode = null;
        }
        if (sensormode == null)
        {
            sensormode = generatePresetByID(i);
        }
        return sensormode;
    }

    public void resetPreset(int i)
    {
        try
        {
            presets[i] = null;
            return;
        }
        catch (IndexOutOfBoundsException indexoutofboundsexception)
        {
            Log.e("MY ERROR", "unable to reset preset (PresetHelper)");
        }
    }
}
