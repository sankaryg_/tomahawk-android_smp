package org.tomahawk.tomahawk_android.sensor;

import android.content.Context;
import android.os.Handler;

import org.tomahawk.tomahawk_android.services.PlaybackService;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map.Entry;

import de.greenrobot.event.EventBus;


public class IntentBroadcastHelper
{
    public static class _cls1IntentBroadcastTask
        implements Runnable
    {

        Action action;
        Context c;
        int delay;
        Handler mHandler;

        public void run()
        {
            mHandler.removeCallbacks(this);
            mHandler.postDelayed(this, delay);
            //c.sendBroadcast(new Intent(action.intentAction));
            PlaybackService.SensorEventAction event = new PlaybackService.SensorEventAction();
            event.intentAction = action.intentAction;
            EventBus.getDefault().post(event);
        }

        public _cls1IntentBroadcastTask(Handler handler, Action action1, Context context, int i)
        {
            c = context;
            action = action1;
            delay = i;
            mHandler = handler;
            run();
        }
    }


    public static final String OTHER_REQUESTER = "other";
    public static final String PROXIMITY_REQUESTER = "proximity";
    public static final String SHAKE_REQUESTER = "shake";
    private static final String TAG = "IntentBroadcastHelper";
    private static HashMap<Action,PendingAction> pendingActionMap = new HashMap<Action,PendingAction>();

    public IntentBroadcastHelper()
    {
    }

    public static PendingAction startIntentBroadcast(Action action, Context context, String s)
    {
    	
        if (action.isRepeatable().booleanValue() && pendingActionMap.get(action) == null && !s.equals("shake"))
        {
            Handler handler = new Handler();
            _cls1IntentBroadcastTask run = new _cls1IntentBroadcastTask(handler, action, context, action.repeatDelay);
            PendingAction pendingaction = new PendingAction(action, handler,run, s);
            pendingActionMap.put(action, pendingaction);
            return pendingaction;
        	//return null;
        } else
        {
            PlaybackService.SensorEventAction event = new PlaybackService.SensorEventAction();
            event.intentAction = action.intentAction;
            EventBus.getDefault().post(event);
            //context.sendBroadcast(new Intent(action.intentAction));
            return null;
        }
    }

    public static void stopAllBroadcasts()
    {
       if (pendingActionMap.isEmpty()) return;
       
        Iterator<Entry<Action, PendingAction>> iterator = pendingActionMap.entrySet().iterator();
       while (iterator.hasNext()) {
        	stopIntentBroadcast((Action)(iterator.next()).getKey());
        }

    }

    public static void stopAllBroadcastsFromRequester(String s)
    {
       
        if (pendingActionMap.isEmpty())
        {
           return;
        }
       Iterator<Entry<Action, PendingAction>> iterator = pendingActionMap.entrySet().iterator();

        if (!iterator.hasNext())
        {
            return;
        }
        while(iterator.hasNext()){
        Entry<Action, PendingAction> entry = iterator.next();
        PendingAction pendingaction = (PendingAction)pendingActionMap.get(entry.getKey());
        if (pendingaction.getRequester().equals(s))
        {
            Runnable runnable = pendingaction.getRunnable();
            pendingaction.getHandler().removeCallbacks(runnable);
            pendingActionMap.remove(pendingaction.getAction());
        }
        }

    }

    public static void stopIntentBroadcast(Action action)
    {
        PendingAction pendingaction = (PendingAction)pendingActionMap.get(action);
        if (pendingaction != null)
        {
            Runnable runnable = pendingaction.getRunnable();
            pendingaction.getHandler().removeCallbacks(runnable);
            pendingActionMap.remove(action);
        }
    }

}
