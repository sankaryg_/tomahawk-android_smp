/* == This file is part of Tomahawk Player - <http://tomahawk-player.org> ===
 *
 *   Copyright 2012, Christopher Reichert <creichert07@gmail.com>
 *   Copyright 2013, Enno Gottschalk <mrmaffen@googlemail.com>
 *
 *   Tomahawk is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Tomahawk is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Tomahawk. If not, see <http://www.gnu.org/licenses/>.
 */
package org.tomahawk.tomahawk_android.services;

import android.annotation.TargetApi;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.ServiceConnection;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.media.AudioManager;
import android.media.MediaMetadataRetriever;
import android.media.RemoteControlClient;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.BatteryManager;
import android.os.Binder;
import android.os.Build;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.os.Message;
import android.os.PowerManager;
import android.preference.PreferenceManager;
import android.speech.tts.TextToSpeech;
import android.speech.tts.UtteranceProgressListener;
import android.support.v4.app.NotificationCompat;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;
import android.text.TextUtils;
import android.util.Log;
import android.widget.RemoteViews;
import android.widget.Toast;

import com.squareup.picasso.Picasso;
import com.squareup.picasso.Target;

import org.tomahawk.libtomahawk.collection.Collection;
import org.tomahawk.libtomahawk.collection.CollectionManager;
import org.tomahawk.libtomahawk.collection.Image;
import org.tomahawk.libtomahawk.collection.Playlist;
import org.tomahawk.libtomahawk.collection.PlaylistEntry;
import org.tomahawk.libtomahawk.collection.Track;
import org.tomahawk.libtomahawk.infosystem.InfoSystem;
import org.tomahawk.libtomahawk.resolver.PipeLine;
import org.tomahawk.libtomahawk.resolver.Query;
import org.tomahawk.libtomahawk.utils.ImageUtils;
import org.tomahawk.libtomahawk.utils.ViewUtils;
import org.tomahawk.tomahawk_android.R;
import org.tomahawk.tomahawk_android.TomahawkApp;
import org.tomahawk.tomahawk_android.activities.TomahawkMainActivity;
import org.tomahawk.tomahawk_android.dialogs.BatteryStatusDialog;
import org.tomahawk.tomahawk_android.dialogs.TimeSayStatusDialog;
import org.tomahawk.tomahawk_android.events.BatteryRemainingEvent;
import org.tomahawk.tomahawk_android.events.ChargingEvent;
import org.tomahawk.tomahawk_android.events.CountDownTimeEvent;
import org.tomahawk.tomahawk_android.events.SleepEvent;
import org.tomahawk.tomahawk_android.events.SleepRemainingEvent;
import org.tomahawk.tomahawk_android.events.VoiceEvent;
import org.tomahawk.tomahawk_android.fragments.PreferenceAdvancedFragment;
import org.tomahawk.tomahawk_android.mediaplayers.StandardMediaPlayer;
import org.tomahawk.tomahawk_android.mediaplayers.TomahawkMediaPlayer;
import org.tomahawk.tomahawk_android.mediaplayers.TomahawkMediaPlayerCallback;
import org.tomahawk.tomahawk_android.sensor.PrefsHelper;
import org.tomahawk.tomahawk_android.sensor.SensorDataHelper;
import org.tomahawk.tomahawk_android.sensor.SoundMeter;
import org.tomahawk.tomahawk_android.utils.AudioFocusHelper;
import org.tomahawk.tomahawk_android.utils.MediaButtonHelper;
import org.tomahawk.tomahawk_android.utils.MediaButtonReceiver;
import org.tomahawk.tomahawk_android.utils.MusicFocusable;
import org.tomahawk.tomahawk_android.utils.RemoteControlClientCompat;
import org.tomahawk.tomahawk_android.utils.RemoteControlHelper;
import org.tomahawk.tomahawk_android.utils.ThreadManager;
import org.tomahawk.tomahawk_android.utils.TomahawkRunnable;
import org.tomahawk.tomahawk_android.utils.WeakReferenceHandler;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.Set;
import java.util.Timer;
import java.util.TimerTask;
import java.util.Vector;
import java.util.concurrent.ConcurrentHashMap;

import de.greenrobot.event.EventBus;

//import org.tomahawk.tomahawk_android.mediaplayers.VLCMediaPlayer;

/**
 * This {@link Service} handles all playback related processes.
 */
public class PlaybackService extends Service implements MusicFocusable,SensorEventListener,TextToSpeech.OnInitListener {

    private static final String TAG = PlaybackService.class.getSimpleName();

    public static final String ACTION_PLAYPAUSE
            = "org.tomahawk.tomahawk_android.ACTION_PLAYPAUSE";

    public static final String ACTION_PLAY
            = "org.tomahawk.tomahawk_android.ACTION_PLAY";

    public static final String ACTION_PAUSE
            = "org.tomahawk.tomahawk_android.ACTION_PAUSE";

    public static final String ACTION_NEXT
            = "org.tomahawk.tomahawk_android.ACTION_NEXT";

    public static final String ACTION_PREVIOUS
            = "org.tomahawk.tomahawk_android.ACTION_PREVIOUS";

    public static final String ACTION_EXIT
            = "org.tomahawk.tomahawk_android.ACTION_EXIT";

    public static final String ACTION_FAVORITE
            = "org.tomahawk.tomahawk_android.ACTION_FAVORITE";

    public static final String MERGED_PLAYLIST_ID = "merged_playlist_id";

    public static final String SHUFFLED_PLAYLIST_ID = "shuffled_playlist_id";


    public static final int NOT_REPEATING = 0;

    public static final int REPEAT_ALL = 1;

    public static final int REPEAT_ONE = 2;

    public static final int SHUFFLE_NONE = 0;
    public static final int SHUFFLE_NORMAL = 1;
    public static final int SHUFFLE_AUTO = 2;


    private int mShuffleMode = SHUFFLE_NONE;
    private int mRepeatMode = NOT_REPEATING;

    private boolean mQueueIsSaveable = true;

    public static final int PLAYBACKSERVICE_PLAYSTATE_PLAYING = 0;

    public static final int PLAYBACKSERVICE_PLAYSTATE_PAUSED = 1;

    public int mPlayState = PLAYBACKSERVICE_PLAYSTATE_PLAYING;

    private static final int PLAYBACKSERVICE_NOTIFICATION_ID = 1;

    private static final int DELAY_TO_KILL = 300000;

    private int mPlayListLen = 0;

    private TextToSpeech tts;

    HashMap<String, String> myHashAlarm;

    static int checkQuarter = 1;

    public static class SelectSensorEvent{
        public int sensorMode;
    }

    public static class SensorEventAction{
        public String intentAction;
    }
    public static class EnableDisableEvent{
        public boolean enableAccelerometer;
        public boolean enableProximity;
        public boolean enableSoundMeter;
    }

    public static class DisableAllSensorEvent{

    }

    public static class PlayingTrackChangedEvent {

    }

    public static class ShareEvent{
        public String shareText;
    }

    public static class PlayingPlaylistChangedEvent {

    }

    public static class PlayStateChangedEvent {

    }

    public static class PlayPositionChangedEvent {

        public long duration;

        public int currentPosition;

    }

    public static class ReadyEvent {

    }

    public static class ReloadEvent {
        public Collection mCollection;
    }

    public static class RequestServiceBindingEvent {

        //private PluginMediaPlayer mRequestingPlayer;

        private String mServicePackageName;

        /*public RequestServiceBindingEvent(PluginMediaPlayer requestingPlayer,
                String servicePackageName) {
            mRequestingPlayer = requestingPlayer;
            mServicePackageName = servicePackageName;
        }*/

    }

    private boolean mShowingNotification;

    protected final Set<Query> mCorrespondingQueries
            = Collections.newSetFromMap(new ConcurrentHashMap<Query, Boolean>());

    protected final ConcurrentHashMap<String, String> mCorrespondingRequestIds
            = new ConcurrentHashMap<>();

    private Playlist mPlaylist;

    private Playlist mQueue;

    private List<Integer> mShuffledIndex = new ArrayList<>();

    private int mQueueStartPos = -1;

    private PlaylistEntry mCurrentEntry;

    private int mCurrentIndex;

    private TomahawkMediaPlayer mCurrentMediaPlayer;

    private Notification mNotification;

    private RemoteViews mLargeNotificationView;

    private RemoteViews mSmallNotificationView;

    private PowerManager.WakeLock mWakeLock;

    private boolean mShuffled;

    private int mRepeatingMode = NOT_REPEATING;

    // our RemoteControlClient object, which will use remote control APIs available in
    // SDK level >= 14, if they're available.
    RemoteControlClientCompat mRemoteControlClientCompat;

    AudioManager mAudioManager;

    // The component name of PlaybackServiceBroadcastReceiver, for use with media button and
    // remote control APIs
    ComponentName mMediaButtonReceiverComponent;

    // our AudioFocusHelper object, if it's available (it's available on SDK level >= 8)
    // If not available, this will be null. Always check for null before using!
    AudioFocusHelper mAudioFocusHelper = null;

    TomahawkApp smp;

    private int result=0;
    private Sensor accSensor;
    private long accWhileScreenOffCount;
    private boolean accelerometerEnabled;
    private SensorManager mSensorEventManager;
    private BroadcastReceiver screenReceiver;
    private PowerManager pm;
    private long proxWhileScreenOffCount;
    private boolean proximityEnabled;
    private Sensor proximitySensor;
    private boolean screenOn;
    private SensorDataHelper sensorDataHelper;
    private boolean soundMeterEnabled;
    private SoundMeter soundSensor;
    private static boolean playing = false;

    private int iVolume;

    private final static int INT_VOLUME_MAX = 100;
    private final static int INT_VOLUME_MIN = 0;
    private final static float FLOAT_VOLUME_MAX = 1;
    private final static float FLOAT_VOLUME_MIN = 0;

    // do we have audio focus?
    enum AudioFocus {
        NoFocusNoDuck,    // we don't have audio focus, and can't duck
        NoFocusCanDuck,   // we don't have focus, but can play at a low volume ("ducking")
        Focused           // we have full audio focus
    }

    AudioFocus mAudioFocus = AudioFocus.NoFocusNoDuck;

    private final List<TomahawkMediaPlayer> mMediaPlayers = new ArrayList<>();

    private final Target mLockscreenTarget = new Target() {
        @Override
        public void onBitmapLoaded(final Bitmap bitmap, Picasso.LoadedFrom loadedFrom) {
            updateAlbumArt(bitmap);
        }

        @Override
        public void onBitmapFailed(Drawable drawable) {
            updateAlbumArt(BitmapFactory
                    .decodeResource(getResources(), R.drawable.album_placeholder_grid));
        }

        @Override
        public void onPrepareLoad(Drawable drawable) {
            updateAlbumArt(BitmapFactory
                    .decodeResource(getResources(), R.drawable.album_placeholder_grid));
        }

        private void updateAlbumArt(final Bitmap bitmap) {
            new Runnable() {
                @Override
                public void run() {
                    synchronized (PlaybackService.this) {
                        RemoteControlClientCompat.MetadataEditorCompat editor =
                                mRemoteControlClientCompat.editMetadata(false);
                        editor.putBitmap(
                                RemoteControlClientCompat.MetadataEditorCompat.METADATA_KEY_ARTWORK,
                                bitmap.copy(bitmap.getConfig(), false));
                        editor.apply();
                        Log.d(TAG, "Setting lockscreen bitmap");
                    }
                }
            }.run();
        }
    };

    /**
     * The static {@link ServiceConnection} which calls methods in {@link
     * PlaybackServiceConnectionListener} to let every depending object know, if the {@link
     * PlaybackService} connects or disconnects.
     */
    public static class PlaybackServiceConnection implements ServiceConnection {

        private final PlaybackServiceConnectionListener mPlaybackServiceConnectionListener;

        public interface PlaybackServiceConnectionListener {

            void setPlaybackService(PlaybackService ps);

            void onPlaybackServiceReady();
        }

        public PlaybackServiceConnection(
                PlaybackServiceConnectionListener playbackServiceConnectedListener) {
            mPlaybackServiceConnectionListener = playbackServiceConnectedListener;
        }

        @Override
        public void onServiceConnected(ComponentName className, IBinder service) {

            /*IMediaPlaybackInterface mService = IMediaPlaybackInterface.Stub.asInterface(service);
            PlaybackServiceBinder binder = (PlaybackServiceBinder) mService;*/
            PlaybackServiceBinder binder = (PlaybackServiceBinder) service;
            mPlaybackServiceConnectionListener.setPlaybackService(binder.getService());//binder.getService());
            mPlaybackServiceConnectionListener.onPlaybackServiceReady();
        }

        @Override
        public void onServiceDisconnected(ComponentName arg0) {
            mPlaybackServiceConnectionListener.setPlaybackService(null);
        }
    }

     ;


    private void updateVolume(int change)
    {
        //increment or decrement depending on type of fade
        iVolume = iVolume + change;

        //ensure iVolume within boundaries
        if (iVolume < INT_VOLUME_MIN)
            iVolume = INT_VOLUME_MIN;
        else if (iVolume > INT_VOLUME_MAX)
            iVolume = INT_VOLUME_MAX;

        //convert to float value
        float fVolume = 1 - ((float) Math.log(INT_VOLUME_MAX - iVolume) / (float) Math.log(INT_VOLUME_MAX));

        //ensure fVolume within boundaries
        if (fVolume < FLOAT_VOLUME_MIN)
            fVolume = FLOAT_VOLUME_MIN;
        else if (fVolume > FLOAT_VOLUME_MAX)
            fVolume = FLOAT_VOLUME_MAX;

        StandardMediaPlayer.get().getLibVlcInstance().setVolume(fVolume, fVolume);
    }

    public boolean timeEventStarted = false;
    public long timeInterval;
    public String currentTimeInMillis;
    public String playlistSelectedId;

    public static final String META_CHANGED = "com.smartfoxsoft.sensitivemusicplayer.pro.metachanged";
    public static final String QUEUE_CHANGED = "com.smartfoxsoft.sensitivemusicplayer.pro.queuechanged";

    public void onEvent(VoiceEvent event){
       timeEventStarted = event.getBattertEventStarted();
        if (timeEventStarted){
            timeInterval = event.getRemainingBattery();
        }
    }

    public void onEvent(CountDownTimeEvent event){
        currentTimeInMillis = event.getCurrentTimeLevel();
        SharedPreferences.Editor editor = PreferenceManager.getDefaultSharedPreferences(TomahawkApp.getContext()).edit();
        editor.putString("timer", currentTimeInMillis).apply();
    }
    private int level;
    public void onEvent(ChargingEvent event){
        level = event.getBatteryLevel();
        SharedPreferences.Editor editor = PreferenceManager.getDefaultSharedPreferences(TomahawkApp.getContext()).edit();
        editor.putInt("battery",level).apply();
    }
    private boolean batteryStarted;
    private int remainigLevel;
    public void onEvent(BatteryRemainingEvent event){
        batteryStarted = event.getBattertEventStarted();
        if(batteryStarted) {
            if (event.getRemainingBattery() != -1) {
               remainigLevel = event.getRemainingBattery();
            }
        }
    }
    private long mCurrentTimestamp,mStopTimestamp;
    public void onEvent(SleepEvent event){
        Calendar now = Calendar.getInstance();
        mCurrentTimestamp = now.getTimeInMillis();
        mStopTimestamp = mCurrentTimestamp + event.getSleepLevel();

        mSleepTimerHandler.sendEmptyMessageDelayed(START_SLEEP_TIMER,
                event.getSleepLevel());

    }

    public String getPlaylistSelectedId() {
        return playlistSelectedId;
    }

    private static final int START_SLEEP_TIMER = 1;
    private static final int STOP_SLEEP_TIMER = 2;
    private boolean mGentleSleepTimer,mSleepTimerTimedUp,voiceIsPlaying;
    private BroadcastReceiver batteryReceiver;
    private BroadcastReceiver voiceReceiver;
    private Handler mSleepTimerHandler = new Handler() {

        @Override
        public void handleMessage(Message msg) {

            switch (msg.what) {
                case START_SLEEP_TIMER:
                    mSleepTimerHandler.removeMessages(START_SLEEP_TIMER, null);
                    if (mGentleSleepTimer) {
                        if (isPlaying()) {

                            //stopSleepTimer();
                            mSleepTimerTimedUp = true;
                            //stopSelf();

                        } else {
                            pause(true);
                            // mNotificationManager.cancel(ID_NOTIFICATION_SLEEPTIMER);
                        }
                    } else {
                        pause(true);
                        // mNotificationManager.cancel(ID_NOTIFICATION_SLEEPTIMER);
                    }
                    mStopTimestamp = 0;
                    break;
                case STOP_SLEEP_TIMER:
                    mStopTimestamp = 0;
                    mSleepTimerHandler.removeMessages(START_SLEEP_TIMER, null);
                    // mNotificationManager.cancel(ID_NOTIFICATION_SLEEPTIMER);
                    break;
            }
        }
    };

    public long sleepRemainingEvent(){
        Calendar now = Calendar.getInstance();
        long mCurrentTimestamp = now.getTimeInMillis();
        if (mStopTimestamp != 0)
            return mStopTimestamp - mCurrentTimestamp;
        else
            return 0;
    }
    public void onEvent(SleepRemainingEvent event){
        if(event.getRemainingSleep()!= -1) {


        }else {
            mSleepTimerHandler.sendEmptyMessage(STOP_SLEEP_TIMER);
        }
    }

    public PlaybackService(){
        accelerometerEnabled = false;
        proximityEnabled = false;
        soundMeterEnabled = false;
        screenOn = true;
        accWhileScreenOffCount = 0L;
        proxWhileScreenOffCount = 0L;
        screenReceiver = new BroadcastReceiver(){
            private long SCREEN_OFF_RECEIVER_DELAY = 750L;
            @Override
            public void onReceive(Context context, Intent intent) {
                String s = intent.getAction();
                if (intent.getAction().equals("android.intent.action.SCREEN_OFF"))
                {
                    screenOn = false;
                    Runnable runnable = new Runnable() {

                       public void run()
                        {
                            restartSensors();
                        }



                    };
                    (new Handler()).postDelayed(runnable, SCREEN_OFF_RECEIVER_DELAY);
                    return;
                }
                if (intent.getAction().equals("android.intent.action.SCREEN_ON"))
                {
                    screenOn = true;
                    if (accelerometerEnabled && accWhileScreenOffCount == 0L)
                    {
                        //sendBroadcast(new Intent(Constants.No_Acc_While_Screen_Off));
                    }
                    accWhileScreenOffCount = 0L;
                    return;
                }
            }
        };
    }
    private void disableAccelerometer()
    {
        if (accelerometerEnabled)
        {
            accelerometerEnabled = false;
            mSensorEventManager.unregisterListener(this, accSensor);
            Log.d("", "analizer is now null");
        }
    }

    private void disableAllSensors()
    {
        disableAccelerometer();
        disableProximity();
        disableSoundMeter();
    }

    private void disableProximity()
    {
        if (proximityEnabled)
        {
            proximityEnabled = false;
            mSensorEventManager = (SensorManager)getSystemService(Context.SENSOR_SERVICE);
            mSensorEventManager.unregisterListener(this, proximitySensor);
        }
    }

    private void disableSoundMeter()
    {
        if (soundMeterEnabled)
        {
            if (soundSensor!=null)
            soundSensor.stop();
            soundMeterEnabled = false;
        }
    }

    private void enableAccelerometer()
    {
        if (!accelerometerEnabled)
        {
            accelerometerEnabled = true;
            accSensor = mSensorEventManager.getDefaultSensor(1);
            mSensorEventManager.registerListener(this, accSensor, 1);
        }
    }

    private void enableProximity()
    {
        if (!proximityEnabled)
        {
            proximityEnabled = true;
            proximitySensor = mSensorEventManager.getDefaultSensor(8);
            mSensorEventManager.registerListener(this, proximitySensor, 1);
        }
    }

    private void enableSoundMeter()
    {
        if (!soundMeterEnabled && !PrefsHelper.isPocketModeCompat(smp))
        {
            soundSensor = new SoundMeter(this);
            soundSensor.start();
            (new SoundMeter(this)).start();
            soundMeterEnabled = true;
        }
    }

    private void restartSensors()
    {
        if (accelerometerEnabled)
        {
            disableAccelerometer();
            enableAccelerometer();
        }
        if (proximityEnabled )
        {
            disableProximity();
            enableProximity();
        }
        if (soundMeterEnabled)
        {
            disableSoundMeter();
            enableSoundMeter();
        }
    }

    public  void setPlaylistSelectedId(String playlistSelectedId){
        this.playlistSelectedId = playlistSelectedId;
    }
    private PhoneCallListener mPhoneCallListener = new PhoneCallListener();

    /**
     * Listens for incoming phone calls and handles playback.
     */
    private class PhoneCallListener extends PhoneStateListener {

        private long mStartCallTime = 0L;

        @Override
        public void onCallStateChanged(int state, String incomingNumber) {
            switch (state) {
                case TelephonyManager.CALL_STATE_RINGING:
                    if (isPlaying()) {
                        mStartCallTime = System.currentTimeMillis();
                        pause();
                    }
                    break;

                case TelephonyManager.CALL_STATE_IDLE:
                    if (mStartCallTime > 0 && (System.currentTimeMillis() - mStartCallTime
                            < 30000)) {
                        start();
                    }

                    mStartCallTime = 0L;
                    break;
            }
        }
    }


    public class PlaybackServiceBinder extends Binder {



        public PlaybackService getService() {
            return PlaybackService.this;
        }

    }

    /**
     * Called when we receive a ACTION_MEDIA_EJECT notification.
     *
     * @param storagePath
     *            path to mount point for the removed media
     */
    public void closeExternalStorageFiles(String storagePath) {
        // stop playback and clean up if the SD card is going to be unmounted.
        pause(true);
        notifyChange(QUEUE_CHANGED);
        notifyChange(META_CHANGED);
    }
    private BroadcastReceiver mUnmountReceiver = null;
    private int mMediaMountedCount = 0;
    public int getMediaMountedCount() {
        return mMediaMountedCount;
    }
    public void registerExternalStorageListener() {
        if (mUnmountReceiver == null) {
            mUnmountReceiver = new BroadcastReceiver() {
                @Override
                public void onReceive(Context context, Intent intent) {
                    String action = intent.getAction();
                    if (action.equals(Intent.ACTION_MEDIA_EJECT)) {
                        saveQueue(true);
                        mQueueIsSaveable = false;
                        closeExternalStorageFiles(intent.getData().getPath());
                    } else if (action.equals(Intent.ACTION_MEDIA_MOUNTED)) {
                        mMediaMountedCount++;
                        mCardId = ViewUtils
                                .getCardId(PlaybackService.this);
                        reloadQueue();
                        mQueueIsSaveable = true;
                        notifyChange(QUEUE_CHANGED);
                        notifyChange(META_CHANGED);
                    }
                }
            };
            IntentFilter iFilter = new IntentFilter();
            iFilter.addAction(Intent.ACTION_MEDIA_EJECT);
            iFilter.addAction(Intent.ACTION_MEDIA_MOUNTED);
            iFilter.addDataScheme("file");
            registerReceiver(mUnmountReceiver, iFilter);
        }
    }

    private void notifyChange(String what) {

        Intent i = new Intent(what);
        i.putExtra("id", Long.valueOf(getCurrentEntry().getId()));//getAudioId()));
        i.putExtra("artist", getCurrentEntry().getArtist().getName());
        i.putExtra("album", getCurrentEntry().getAlbum().getName());
        i.putExtra("track", getCurrentEntry().getName());
        i.putExtra("playing", isPlaying());
        sendStickyBroadcast(i);

        if (what.equals(QUEUE_CHANGED)) {
            saveQueue(true);
        } else {
            saveQueue(false);
        }


    }
 /*   public class PlaybackServiceBinder extends IMediaPlaybackInterface.Stub {


        WeakReference<PlaybackService> mService;


        PlaybackServiceBinder(PlaybackService service) {

            mService = new WeakReference<PlaybackService>(service);

        }


        public PlaybackService getService() {

            return mService.get();

        }


        @Override

        public long[] getQueue() throws RemoteException {

            return new long[0];

        }


        public void openFile(String path) {

            //mService.get().open(path);

        }


        public void open(long[] list, int position) {

            //mService.get().open(list, position);

        }


        public int getQueuePosition() {

            return 0;//mService.get().getQueuePosition();

        }


        public void setQueuePosition(int index) {

            //mService.get().setQueuePosition(index);

        }


        public boolean isPlaying() {

            return mService.get().isPlaying();

        }


        public void stop() {

            //mService.get().stop();

        }


        public void pause() {

            mService.get().pause(true);

        }


        public void play() {

            //mService.get().play();

        }


        public void prev() {

            mService.get().previous();

        }


        public void next() {

            mService.get().next();//gotoNext(true);

        }


        public String getTrackName() {

            return "";//mService.get().getTrackName();

        }


        public String getAlbumName() {

            return "";//mService.get().getAlbumName();

        }


        public long getAlbumId() {

            return 0;//mService.get().getAlbumId();

        }


        public String getArtistName() {

            return "";//mService.get().getArtistName();

        }


        public long getArtistId() {

            return 0;//mService.get().getArtistId();

        }


        public void enqueue(long[] list, int action) {

            //mService.get().enqueue(list, action);

        }



        public void moveQueueItem(int from, int to) {

            //mService.get().moveQueueItem(from, to);

        }


        public String getPath() {

            return "";//mService.get().getPath();

        }


        public long getAudioId() {

            return 0;//mService.get().getAudioId();

        }


        public long position() {

            return 0;//mService.get().position();

        }


        public long duration() {

            return 0;//mService.get().duration();

        }


        public long seek(long pos) {

            return 0;//mService.get().seek(pos);

        }


        public void setShuffleMode(int shufflemode) {

            //mService.get().setShuffleMode(shufflemode);

        }


        public int getShuffleMode() {

            return 0;//mService.get().getShuffleMode();

        }


        public int removeTracks(int first, int last) {

            return 0;//mService.get().removeTracks(first, last);

        }


        public int removeTrack(long id) {

            return 0;//mService.get().removeTrack(id);

        }


        public void setRepeatMode(int repeatmode) {

            //mService.get().setRepeatMode(repeatmode);

        }


        public int getRepeatMode() {

            return 0;//mService.get().getRepeatMode();

        }


        public int getMediaMountedCount() {

            return 0;//mService.get().getMediaMountedCount();

        }


        public int getAudioSessionId() {

            return 0;//mService.get().getAudioSessionId();

        }


        public String getMediaUri() {

            return "";//mService.get().getMediaUri();

        }


        @Override

        public void toggleRepeat() throws RemoteException {

            // TODO Auto-generated method stub

            //mService.get().toggleRepeat();

        }


        @Override

        public void toggleShuffle() throws RemoteException {

            // TODO Auto-generated method stub

            //mService.get().toggleShuffle();

        }


        @Override

        public String getMediaPath() throws RemoteException {

            // TODO Auto-generated method stub

            return "";//mService.get().getMediaPath();

        }


        @Override

        public void setQueueId(long id) throws RemoteException {

            // TODO Auto-generated method stub

            //mService.get().setQueueId(id);

        }


        @Override

        public Uri getArtworkUri() throws RemoteException {

            // TODO Auto-generated method stub

            return null;//mService.get().getArtworkUri();

        }



        @Override

        public boolean togglePause() throws RemoteException {

            // TODO Auto-generated method stub

            return false;//mService.get().togglePause();

        }





        @Override

        public void plugHeadSet(boolean plug) throws RemoteException {

            // TODO Auto-generated method stub

            //mService.get().plugHeadSet(plug);

        }


        @Override

        public void unPlugHeadSet(boolean unPlug) throws RemoteException {

            // TODO Auto-generated method stub

            //mService.get().unPlugHeadSet(unPlug);

        }


        @Override

        public long getGenreId() throws RemoteException {

            // TODO Auto-generated method stub

            return 0;//mService.get().getGenreId();

        }

    }


*/
    // Stops this service if it doesn't have any bound services
    private KillTimerHandler mKillTimerHandler = new KillTimerHandler(this);

    private class KillTimerHandler extends WeakReferenceHandler<PlaybackService> {

        public KillTimerHandler(PlaybackService referencedObject) {
            super(referencedObject);
        }

        @Override
        public void handleMessage(Message msg) {
            PlaybackService service = getReferencedObject();
            if (service != null) {
                saveQueue(true);
                if (service.isPlaying()) {
                    removeCallbacksAndMessages(null);
                    Message msgx = obtainMessage();
                    sendMessageDelayed(msgx, DELAY_TO_KILL);
                    Log.d(TAG, "Killtimer checked if I should die, but I survived *cheer*");
                } else {
                    Log.d(TAG, "Killtimer called stopSelf() on me");
                    service.stopSelf();
                }

            }
        }
    }

    @Override
    public void onAccuracyChanged(Sensor sensor, int accuracy) {

    }

    @Override
    public void onSensorChanged(SensorEvent sensorevent) {
        if (sensorevent.sensor.getType() == 1)
        {
            if (accelerometerEnabled)
            {
                int i = 35000;
                if (soundMeterEnabled)
                {
                    i = soundSensor.getAmplitude();
                }
                sensorDataHelper.addRawAccValues(sensorevent.values, i);
                if (!screenOn)
                {
                    accWhileScreenOffCount = 1L + accWhileScreenOffCount;
                }
            }
        } else
        if (sensorevent.sensor.getType() == 8)
        {
            sensorDataHelper.addRawProxValue(sensorevent.values[0]);
            if (!screenOn)
            {
                proxWhileScreenOffCount = 1L + proxWhileScreenOffCount;
                return;
            }
        }
    }

    private TomahawkMediaPlayerCallback mMediaPlayerCallback = new TomahawkMediaPlayerCallback() {
        @Override
        public void onPrepared(Query query) {
            if (query == getCurrentQuery()) {
                Log.d(TAG, "MediaPlayer successfully prepared the track '"
                        + getCurrentQuery().getName() + "' by '"
                        + getCurrentQuery().getArtist().getName()
                        + "' resolved by Resolver " + getCurrentQuery()
                        .getPreferredTrackResult().getResolvedBy().getId());
                boolean allPlayersReleased = true;
                for (TomahawkMediaPlayer mediaPlayer : mMediaPlayers) {
                    if (!mediaPlayer.isPrepared(getCurrentQuery())) {
                        mediaPlayer.release();
                    } else {
                        allPlayersReleased = false;
                    }
                }
                if (allPlayersReleased) {
                    prepareCurrentQuery();
                } else if (isPlaying()) {
                   /* InfoSystem.get().sendNowPlayingPostStruct(
                            AuthenticatorManager.get().getAuthenticatorUtils(
                                    TomahawkApp.PLUGINNAME_HATCHET),
                            getCurrentQuery()
                    );*/
                }
                handlePlayState();
            } else {
                Log.e(TAG, "onPrepared received for an unexpected Query: "
                        + query.getName() + "' by '" + query.getArtist().getName()
                        + "' resolved by Resolver "
                        + query.getPreferredTrackResult().getResolvedBy().getId());
            }
        }

        @Override
        public void onCompletion(Query query) {
            if (query == getCurrentQuery()) {
                Log.d(TAG, "onCompletion");
                if (hasNextEntry()) {
                    next();
                } else {
                    pause();
                }
            }
        }

        @Override
        public void onError(String message) {
            Log.e(TAG, "onError - " + message);
            giveUpAudioFocus();
            if (hasNextEntry()) {
                next();
            } else {
                pause();
            }
        }
    };

    @SuppressWarnings("unused")
    public void onEventMainThread(PipeLine.ResultsEvent event) {
        if (getCurrentQuery() != null && getCurrentQuery() == event.mQuery) {
            updateNotification();
            updateLockscreenControls();
            EventBus.getDefault().post(new PlayingTrackChangedEvent());
            if (mCurrentMediaPlayer == null
                    || !(mCurrentMediaPlayer.isPrepared(getCurrentQuery())
                    || mCurrentMediaPlayer.isPreparing(getCurrentQuery()))) {
                prepareCurrentQuery();
            }
        }
    }

    @SuppressWarnings("unused")
    public void onEventMainThread(InfoSystem.ResultsEvent event) {
        if (getCurrentEntry() != null && getCurrentQuery().getCacheKey()
                .equals(mCorrespondingRequestIds.get(event.mInfoRequestData.getRequestId()))) {
            updateNotification();
            updateLockscreenControls();
            EventBus.getDefault().post(new PlayingTrackChangedEvent());
        }
    }

    @SuppressWarnings("unused")
    public void onEvent(CollectionManager.UpdatedEvent event) {
        if (event.mUpdatedItemIds != null && getCurrentQuery() != null
                && event.mUpdatedItemIds.contains(getCurrentQuery().getCacheKey())) {
            updateNotification();
        }
    }

    @SuppressWarnings("unused")
    public void onEvent(RequestServiceBindingEvent event) {
        /*Intent intent = new Intent(IPluginService.class.getName());
        intent.setPackage(event.mServicePackageName);*/
        /*bindService(intent, new PluginServiceConnection(event.mRequestingPlayer),
                Context.BIND_AUTO_CREATE);*/
    }

    public void onEvent(SelectSensorEvent event){
        smp.loadSensorMode(event.sensorMode);
    }

    public void onEvent(DisableAllSensorEvent event){
        disableAllSensors();
    }

    public void onEvent(EnableDisableEvent event){
        /*proximityEnabled = event.enableProximity;
        accelerometerEnabled = event.enableAccelerometer;
        soundMeterEnabled = event.enableSoundMeter;*/
        if (event.enableProximity)
            enableProximity();
        if (event.enableAccelerometer)
            enableAccelerometer();
        if (event.enableSoundMeter)
            enableSoundMeter();
    }
    @Override
    public void onCreate() {
        super.onCreate();

        EventBus.getDefault().register(this);
        smp = (TomahawkApp)getApplication();
        mSensorEventManager = (SensorManager)getSystemService(Context.SENSOR_SERVICE);
        android.content.SharedPreferences.Editor editor = PreferenceManager.getDefaultSharedPreferences(this).edit();
        PrefsHelper.updateSensorAvailability(editor, mSensorEventManager);
        editor.commit();
        registerExternalStorageListener();
        mCardId = ViewUtils.getCardId(this);
        mMediaPlayers.add(StandardMediaPlayer.get());
        tts = new TextToSpeech(this, this);
        voiceIsPlaying = PreferenceManager.getDefaultSharedPreferences(TomahawkApp.getContext()).getBoolean(TimeSayStatusDialog.TIME_WHEN_PLAYING,false);
        /*mMediaPlayers.add(DeezerMediaPlayer.get());
        mMediaPlayers.add(SpotifyMediaPlayer.get());
        mMediaPlayers.add(RdioMediaPlayer.get());
*/
        startService(new Intent(this, MicroService.class));
        sensorDataHelper = smp.getSensorDataHelper();
        IntentFilter intentfilter = new IntentFilter();
        intentfilter.addAction("android.intent.action.SCREEN_OFF");
        intentfilter.addAction("android.intent.action.SCREEN_ON");
        registerReceiver(screenReceiver, intentfilter);
        ServiceConnection connection = new ServiceConnection() {
            @Override
            public void onServiceConnected(ComponentName name, IBinder service) {
            }

            @Override
            public void onServiceDisconnected(ComponentName name) {
            }
        };

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            bindService(new Intent(this, RemoteControllerService.class), connection,
                    Context.BIND_AUTO_CREATE);
        }

        mAudioManager = (AudioManager) getSystemService(AUDIO_SERVICE);

        // Initialize PhoneCallListener
        TelephonyManager telephonyManager =
                (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        telephonyManager.listen(mPhoneCallListener, PhoneStateListener.LISTEN_CALL_STATE);

        // Initialize WakeLock
        PowerManager pm = (PowerManager) getSystemService(Context.POWER_SERVICE);
        mWakeLock = pm.newWakeLock(PowerManager.PARTIAL_WAKE_LOCK, TAG);

        mMediaButtonReceiverComponent = new ComponentName(this, MediaButtonReceiver.class);
        mAudioManager.registerMediaButtonEventReceiver(mMediaButtonReceiverComponent);
        mAudioFocusHelper = new AudioFocusHelper(getApplicationContext(), this);

        // Initialize killtime handler (watchdog style)
        mKillTimerHandler.removeCallbacksAndMessages(null);
        Message msg = mKillTimerHandler.obtainMessage();
        mKillTimerHandler.sendMessageDelayed(msg, DELAY_TO_KILL);

        mPlaylist = Playlist.fromEmptyList(TomahawkMainActivity.getLifetimeUniqueStringId(), "");
        mQueue = Playlist.fromEmptyList(TomahawkMainActivity.getLifetimeUniqueStringId(), "");
        Log.d(TAG, "PlaybackService has been created");
        reloadQueue();
        batteryReceiver = new BroadcastReceiver() {

            @Override
            public void onReceive(Context arg0, Intent intent) {
                // TODO Auto-generated method stub
                level = intent.getIntExtra(BatteryManager.EXTRA_LEVEL, 0);
                /*Intent intentBattery = new Intent("battery timer");
                intentBattery.putExtra("level", level);
                sendBroadcast(intentBattery);*/
                ChargingEvent event = new ChargingEvent();
                event.setBatteryLevel(intent.getIntExtra(BatteryManager.EXTRA_LEVEL, 0));
                EventBus.getDefault().post(event);
                if (batteryStarted) {
                    if (level < remainigLevel) {

                        pause(true);
                        PreferenceManager.getDefaultSharedPreferences(TomahawkApp.getContext()).edit().putBoolean(BatteryStatusDialog.BATTERY_STARTED,false).apply();
                    }
                }

            }
        };
        IntentFilter intent = new IntentFilter();
        intent.addAction("battery timer");
        registerReceiver(batteryReceiver, new IntentFilter(
                Intent.ACTION_BATTERY_CHANGED));
        voiceReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
            Calendar time = Calendar.getInstance();
            Toast.makeText(context,"test = "+time.getTime(),Toast.LENGTH_SHORT).show();
                int hour = time.get(Calendar.HOUR_OF_DAY);
                int minute = time.get(Calendar.MINUTE);
                int second = time.get(Calendar.SECOND);
                Calendar cal = Calendar.getInstance();
                String str = "";
                String am_pm = "";
                if(hour >= 0 && hour < 12){
                    Toast.makeText(context, "Good Morning", Toast.LENGTH_SHORT).show();
                    str = "Good Morning";
                    am_pm = "AM";
                }else if(hour >= 12 && hour < 16){
                    Toast.makeText(context, "Good Afternoon", Toast.LENGTH_SHORT).show();
                    str = "Good Afternoon";
                    am_pm = "PM";
                }else if(hour >= 16 && hour < 21){
                    Toast.makeText(context, "Good Evening", Toast.LENGTH_SHORT).show();
                    str = "Good Evening";
                    am_pm = "PM";
                }else if(hour >= 21 && hour < 24){
                    Toast.makeText(context, "Good Night", Toast.LENGTH_SHORT).show();
                    str = "Good Night";
                    am_pm = "PM";
                }
                if (hour > 12){
                    hour = hour - 12;
                }
                CountDownTimeEvent event = new CountDownTimeEvent();
                event.setCurrentTimeLevel(((hour>=10)?hour:("0"+hour)) + ":" + ((minute>=10)?minute:("0"+minute)) + " " + am_pm);//time.getTimeInMillis());
                EventBus.getDefault().post(event);
                if(timeEventStarted) {

                    long delay = 0;
                    switch ((int)timeInterval / 60) {
                        case 60:

                            try {
                                if (hour > 0 || minute > 0 || second > 0) {
                                    cal.set(Calendar.HOUR_OF_DAY, ++hour);
                                    cal.set(Calendar.MINUTE, 0);
                                    cal.set(Calendar.SECOND, 0);
                                    if(minute%60 == 0) {

                                        String text = str + " time is " + hour + " hours " + minute + " minutes" +" "+am_pm;
                                        {
                                            if (isPlaying()) {
                                                playing = true;
                                                pause();
                                            }else {
                                                playing = false;
                                            }
                                            if (Build.VERSION.RELEASE.startsWith("5")) {
                                                if (voiceIsPlaying ) {
                                                    if (isPlaying())
                                                        tts.speak(text, TextToSpeech.QUEUE_FLUSH, null, null);
                                                }else {
                                                    tts.speak(text, TextToSpeech.QUEUE_FLUSH, null, null);
                                                }
                                            }else {
                                                if (voiceIsPlaying) {
                                                    if (isPlaying())
                                                        tts.speak(text, TextToSpeech.QUEUE_FLUSH, myHashAlarm);
                                                } else {
                                                    tts.speak(text, TextToSpeech.QUEUE_FLUSH, myHashAlarm);
                                                }
                                            }
                                        }
                                    }


                                }
                            } catch (Exception e) {
                                // TODO Auto-generated catch block
                                e.printStackTrace();
                            }
                            break;
                        case 30:
                            if (hour > 0 || minute > 0 || second > 0) {
                                int minute1 = 30;
                                String text = str + " time is "+hour+" hours "+minute+ " minutes"+" "+am_pm;

                                Log.d("half", hour + "_" + minute + "_" + second);
                                if (minute%30==0){
                                    if (isPlaying()) {
                                        playing = true;
                                        pause();
                                    }else {
                                        playing = false;
                                    }
                                    if (Build.VERSION.RELEASE.startsWith("5")) {
                                        if (voiceIsPlaying ) {
                                            if (isPlaying())
                                            tts.speak(text, TextToSpeech.QUEUE_FLUSH, null, null);
                                        }else {
                                            tts.speak(text, TextToSpeech.QUEUE_FLUSH, null, null);
                                        }
                                    }
                                    else {
                                        if (voiceIsPlaying) {
                                            if (isPlaying())
                                            tts.speak(text, TextToSpeech.QUEUE_FLUSH, myHashAlarm);
                                        }else {
                                            tts.speak(text, TextToSpeech.QUEUE_FLUSH, myHashAlarm);
                                        }
                                    }
                                }

                                }


                            break;

                        case 15:
                            if (hour > 0 && minute > 0) {
                                String text = str + " time is "+hour+" hours "+minute+ " minutes"+" "+am_pm;

                                int minute2 = 15;
                                if (minute % 15 == 0) {
                                    minute2 = ++checkQuarter * 15;
                                    if (isPlaying()) {
                                        playing = true;
                                        pause();
                                    }else {
                                        playing = false;
                                    }
                                    if (Build.VERSION.RELEASE.startsWith("5")) {
                                        if (voiceIsPlaying ) {
                                            if (isPlaying())
                                            tts.speak(text, TextToSpeech.QUEUE_FLUSH, null, null);
                                        }else {
                                            tts.speak(text, TextToSpeech.QUEUE_FLUSH, null, null);
                                        }
                                    }
                                    else {
                                        if (voiceIsPlaying ) {
                                            if (isPlaying())
                                            tts.speak(text, TextToSpeech.QUEUE_FLUSH, myHashAlarm);
                                        }else {
                                            tts.speak(text, TextToSpeech.QUEUE_FLUSH, myHashAlarm);
                                        }
                                    }
                                }

                                if (checkQuarter == 4)
                                    checkQuarter = 0;
                            }
                            break;
                        default:
                            break;
                    }

                    }
            }
        };
        IntentFilter timerIntent = new IntentFilter();
        timerIntent.addAction("voice timer");
        registerReceiver(voiceReceiver,new IntentFilter(Intent.ACTION_TIME_TICK));

    }
    @Override
    public void onInit(int status) {
        // TODO Auto-generated method stub
        //check status for TTS is initialized or not
        if (status == TextToSpeech.SUCCESS) {
            //if TTS initialized than set language
            result = tts.setLanguage(Locale.ENGLISH);

            // tts.setPitch(5); // you can set pitch level
            // tts.setSpeechRate(2); //you can set speech speed rate
            myHashAlarm = new HashMap<String, String>();
            myHashAlarm.put(TextToSpeech.Engine.KEY_PARAM_UTTERANCE_ID, "alarm_id");

            //check language is supported or not
            //check language data is available or not
            if (result == TextToSpeech.LANG_MISSING_DATA || result == TextToSpeech.LANG_NOT_SUPPORTED) {
                Toast.makeText(this, "Missing data", Toast.LENGTH_LONG).show();
                //disable button
                //btnSpeak.setEnabled(false);
            } else {
                //if all is good than enable button convert text to speech
                //btnSpeak.setEnabled(true);
            }
            if (Build.VERSION.SDK_INT >= 15) {
                tts.setOnUtteranceProgressListener(new UtteranceProgressListener() {
                    @Override
                    public void onStart(String utteranceId) {

                    }

                    @Override
                    public void onDone(String utteranceId) {
                        if (playing)
                        playPause();
                    }

                    @Override
                    public void onError(String utteranceId) {

                    }
                });
            }else {
                tts.setOnUtteranceCompletedListener(new TextToSpeech.OnUtteranceCompletedListener() {
                    @Override
                    public void onUtteranceCompleted(String utteranceId) {
                        if (playing)
                        playPause();
                    }
                });
            }
        } else {
            Log.e("TTS", "Initilization Failed");
        }
    }
    public void onEventMainThread(SensorEventAction eventAction){
        if (eventAction.intentAction.equals(ACTION_PREVIOUS)) {
            previous();
        } else if (eventAction.intentAction.equals(ACTION_PLAYPAUSE)) {
            playPause();
        } else if (eventAction.intentAction.equals(ACTION_PLAY)) {
            start();
        } else if (eventAction.intentAction.equals(ACTION_PAUSE)) {
            pause();
        } else if (eventAction.intentAction.equals(ACTION_NEXT)) {
            next();
        } else if (eventAction.intentAction.equals(ACTION_FAVORITE)) {
            CollectionManager.get().toggleLovedItem(getCurrentQuery());
        } else if (eventAction.intentAction.equals(ACTION_EXIT)) {
            pause(true);
        }
    }
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        if (intent != null && intent.getAction() != null) {
            if (intent.getAction().equals(ACTION_PREVIOUS)) {
                previous();
            } else if (intent.getAction().equals(ACTION_PLAYPAUSE)) {
                playPause();
            } else if (intent.getAction().equals(ACTION_PLAY)) {
                start();
            } else if (intent.getAction().equals(ACTION_PAUSE)) {
                pause();
            } else if (intent.getAction().equals(ACTION_NEXT)) {
                next();
            } else if (intent.getAction().equals(ACTION_FAVORITE)) {
                CollectionManager.get().toggleLovedItem(getCurrentQuery());
            } else if (intent.getAction().equals(ACTION_EXIT)) {
                pause(true);
            }
        }
        return START_STICKY;
    }

    @Override
    public IBinder onBind(Intent intent) {
        Log.d(TAG, "Client has been bound to PlaybackService");
        return new PlaybackServiceBinder();
    }

    @Override
    public boolean onUnbind(Intent intent) {
        Log.d(TAG, "Client has been unbound from PlaybackService");
        //saveQueue(true);
        return false;
    }

    @Override
    public void onDestroy() {
        if (tts != null) {
            tts.stop();
            tts.shutdown();
        }
        super.onDestroy();

        EventBus.getDefault().unregister(this);

        pause(true);
        releaseAllPlayers();
        disableAllSensors();
        if (mWakeLock.isHeld()) {
            mWakeLock.release();
        }
        mWakeLock = null;
        TelephonyManager telephonyManager =
                (TelephonyManager) getSystemService(Context.TELEPHONY_SERVICE);
        telephonyManager.listen(mPhoneCallListener, PhoneStateListener.LISTEN_NONE);
        mPhoneCallListener = null;
        mKillTimerHandler.removeCallbacksAndMessages(null);
        mKillTimerHandler = null;
        try
        {
            unregisterReceiver(screenReceiver);
            unregisterReceiver(batteryReceiver);
            unregisterReceiver(voiceReceiver);
            if (mUnmountReceiver != null) {
                unregisterReceiver(mUnmountReceiver);
                mUnmountReceiver = null;
            }
        }
        catch (IllegalArgumentException illegalargumentexception) { }

        Log.d(TAG, "PlaybackService has been destroyed");
    }

    /**
     * Start or pause playback (Doesn't dismiss notification on pause)
     */
    public void playPause() {
        playPause(false);
    }

    /**
     * Start or pause playback.
     *
     * @param dismissNotificationOnPause if true, dismiss notification on pause, otherwise don't
     */
    public void playPause(boolean dismissNotificationOnPause) {
        SharedPreferences mPreferences =
                PreferenceManager.getDefaultSharedPreferences(TomahawkApp.getContext());
        mPreferences.edit().putInt("playstate",mPlayState).apply();
        if (mPlayState == PLAYBACKSERVICE_PLAYSTATE_PLAYING) {
            pause(dismissNotificationOnPause);
        } else if (mPlayState == PLAYBACKSERVICE_PLAYSTATE_PAUSED) {
            start();
        }
    }

    /**
     * Initial start of playback. Acquires wakelock and creates a notification
     */
    public void start() {
        Log.d(TAG, "start");
        if (getCurrentQuery() != null) {
            mPlayState = PLAYBACKSERVICE_PLAYSTATE_PLAYING;


            SharedPreferences prefs =
                    PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
            boolean fadeDuration = prefs.getBoolean(
                    PreferenceAdvancedFragment.FAKEPREFERENCEFRAGMENT_KEY_FADEINOUT, false);
            prefs.edit().putInt("playstate",mPlayState).apply();
            if (fadeDuration)
                iVolume = INT_VOLUME_MIN;
            else
                iVolume = INT_VOLUME_MAX;

            updateVolume(0);

            //Play music
            EventBus.getDefault().post(new PlayStateChangedEvent());
            saveQueue(true);
            handlePlayState();

            mShowingNotification = true;
            updateNotification();
            tryToGetAudioFocus();
            updateLockscreenPlayState();

            //Start increasing volume in increments
            if(fadeDuration)
            {
                final Timer timer = new Timer(true);
                TimerTask timerTask = new TimerTask()
                {
                    @Override
                    public void run()
                    {
                        updateVolume(1);
                        if (iVolume == INT_VOLUME_MAX)
                        {
                            timer.cancel();
                            timer.purge();
                        }
                    }
                };

                // calculate delay, cannot be zero, set to 1 if zero
                int delay = 2000/INT_VOLUME_MAX;
                if (delay == 0) delay = 1;

                timer.schedule(timerTask, delay, delay);
            }
        }else {
            EventBus.getDefault().post(new PlayStateChangedEvent());
            handlePlayState();
            saveQueue(true);
            mShowingNotification = true;
            updateNotification();
            tryToGetAudioFocus();
            updateLockscreenPlayState();
        }
    }

    /**
     * Pause playback. (Doesn't dismiss notification on pause)
     */
    public void pause() {
        pause(false);
    }

    /**
     * Pause playback.
     *
     * @param dismissNotificationOnPause if true, dismiss notification on pause, otherwise don't
     */
    public void pause(final boolean dismissNotificationOnPause) {
        Log.d(TAG, "pause, dismissing Notification:" + dismissNotificationOnPause);
        mPlayState = PLAYBACKSERVICE_PLAYSTATE_PAUSED;
        SharedPreferences prefs =
                PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        boolean fadeDuration = prefs.getBoolean(
                PreferenceAdvancedFragment.FAKEPREFERENCEFRAGMENT_KEY_FADEINOUT, false);
         prefs.edit().putInt("playstate",mPlayState).apply();
        if (fadeDuration)
            iVolume = INT_VOLUME_MAX;
        else
            iVolume = INT_VOLUME_MIN;

        updateVolume(0);

        //Start increasing volume in increments
        if(fadeDuration)
        {

            EventBus.getDefault().post(new PlayStateChangedEvent());
            //handlePlayState();
            final Timer timer = new Timer(true);
            TimerTask timerTask = new TimerTask()
            {
                @Override
                public void run()
                {
                    updateVolume(-1);
                    if (iVolume == INT_VOLUME_MIN)
                    {
                        //Pause music
                        //if (mediaPlayer.isPlaying()) mediaPlayer.pause();

                        EventBus.getDefault().post(new PlayStateChangedEvent());
                        handlePlayState();
                        if (dismissNotificationOnPause) {
                            mShowingNotification = false;
                            stopForeground(true);
                            giveUpAudioFocus();
                            NotificationManager notificationManager = (NotificationManager) TomahawkApp.getContext()
                                    .getSystemService(Context.NOTIFICATION_SERVICE);
                            notificationManager.cancel(PLAYBACKSERVICE_NOTIFICATION_ID);
                        } else {
                            updateNotification();
                        }
                        updateLockscreenPlayState();

                        timer.cancel();
                        timer.purge();
                    }
                }
            };

            // calculate delay, cannot be zero, set to 1 if zero
            int delay = 2000/INT_VOLUME_MAX;
            if (delay == 0) delay = 1;

            timer.schedule(timerTask, delay, delay);
        }else {

        EventBus.getDefault().post(new PlayStateChangedEvent());
        handlePlayState();
        if (dismissNotificationOnPause) {
            mShowingNotification = false;
            stopForeground(true);
            giveUpAudioFocus();
            NotificationManager notificationManager = (NotificationManager) TomahawkApp.getContext()
                    .getSystemService(Context.NOTIFICATION_SERVICE);
            notificationManager.cancel(PLAYBACKSERVICE_NOTIFICATION_ID);
        } else {
            updateNotification();
        }
        updateLockscreenPlayState();
        }
    }
    /**
     * Update the TomahawkMediaPlayer so that it reflects the current playState
     */
    public void handlePlayState() {
        Log.d(TAG, "handlePlayState");
        SharedPreferences mPreferences =
                PreferenceManager.getDefaultSharedPreferences(TomahawkApp.getContext());
       int state = mPreferences.getInt("playstate",-1);
        int seekpos = mPreferences.getInt("seekpos",0);
        if (state!=-1)
            mPlayState = state;
        if (!isPreparing() && getCurrentQuery() != null
                && getCurrentQuery().getMediaPlayerInterface() != null) {
            try {

                switch (mPlayState) {
                    case PLAYBACKSERVICE_PLAYSTATE_PLAYING:
                        if (mWakeLock != null && mWakeLock.isHeld()) {
                            mWakeLock.acquire();
                        }
                        if (getCurrentQuery().getMediaPlayerInterface()
                                .isPrepared(getCurrentQuery())) {
                            if (!getCurrentQuery().getMediaPlayerInterface()
                                    .isPlaying(getCurrentQuery())) {

                                getCurrentQuery().getMediaPlayerInterface().start();
                                if (seekpos>0){
                                    seekTo(seekpos);
                                }
                            }
                        } else if (!isPreparing()) {
                            prepareCurrentQuery();
                        }
                        break;
                    case PLAYBACKSERVICE_PLAYSTATE_PAUSED:
                        if (getCurrentQuery().getMediaPlayerInterface().isPlaying(getCurrentQuery())
                                && getCurrentQuery().getMediaPlayerInterface()
                                .isPrepared(getCurrentQuery())) {
                            /*InfoSystem.get().sendPlaybackEntryPostStruct(
                                    AuthenticatorManager.get().getAuthenticatorUtils(
                                            TomahawkApp.PLUGINNAME_HATCHET)
                            );*/
                            getCurrentQuery().getMediaPlayerInterface().pause();
                        }
                        if (mWakeLock != null && mWakeLock.isHeld()) {
                            mWakeLock.release();
                        }
                        break;
                }
            } catch (IllegalStateException e1) {
                Log.e(TAG,
                        "handlePlayState IllegalStateException, msg:" + e1.getLocalizedMessage()
                                + " , preparing=" + isPreparing()
                );
            }
            if (mKillTimerHandler != null) {
                mKillTimerHandler.removeCallbacksAndMessages(null);
                Message msg = mKillTimerHandler.obtainMessage();
                mKillTimerHandler.sendMessageDelayed(msg, DELAY_TO_KILL);
            }
        } else {
            Log.d(TAG, "handlePlayState couldn't do anything, isPreparing" + isPreparing());
        }
    }

    /**
     * Start playing the next Track.
     */
    public void next() {
        Log.d(TAG, "next");
        int counter = 0;
        PlaylistEntry entry = getNextEntry();
        while (entry != null && counter++ < getPlaybackListSize()) {
            setCurrentEntry(entry);
            if (entry.getQuery().isPlayable()) {
                break;
            }
            entry = getNextEntry(entry);
        }
    }

    /**
     * Play the previous track.
     */
    public void previous() {
        Log.d(TAG, "previous");
        int counter = 0;
        PlaylistEntry entry = getPreviousEntry();
        while (entry != null && counter++ < getPlaybackListSize()) {
            if (entry.getQuery().isPlayable()) {
                setCurrentEntry(entry);
                break;
            }
            entry = getPreviousEntry(entry);
        }
    }

    public boolean isShuffled() {
        return mShuffled;
    }

    /**
     * Set whether or not to enable shuffle mode on the current playlist.
     */
    public void setShuffled(boolean shuffled) {
        Log.d(TAG, "setShuffled from " + mShuffled + " to " + shuffled);
        if (mShuffled != shuffled) {
            mShuffled = shuffled;
            if (shuffled) {
                fillShuffledIndex();
            }
            if (getCurrentEntry() != null) {
                resolveQueriesFromTo(mCurrentIndex, mCurrentIndex + 10);
            }

            EventBus.getDefault().post(new PlayingPlaylistChangedEvent());
        }
    }

    /**
     * Shuffles the list of tracks in the current playlist and fills the shuffled index accordingly.
     * The shuffle method ensures that the shuffled list does only contain a minimum amount of
     * tracks by the same artist in sequence.
     */
    private void fillShuffledIndex() {
        mShuffledIndex.clear();
        Map<String, List<Integer>> artistMap = new HashMap<>();
        List<String> artistNames = new ArrayList<>();
        int shuffledTracksCount = 0;
        boolean isPlayingFromQueue = mQueue.getIndexOfEntry(mCurrentEntry) >= 0;
        int currentIndex = -1;
        if (isPlayingFromQueue) {
            currentIndex = mPlaylist.getIndexOfEntry(mCurrentEntry);
        }
        for (int i = 0; i < mPlaylist.size(); i++) {
            if (isPlayingFromQueue || i != currentIndex) {
                String artistName = mPlaylist.getArtistName(i);
                if (artistMap.get(artistName) == null) {
                    artistMap.put(artistName, new ArrayList<Integer>());
                    artistNames.add(artistName);
                }
                artistMap.get(artistName).add(i);
                shuffledTracksCount++;
            }
        }
        if (!isPlayingFromQueue) {
            mShuffledIndex.add(mCurrentIndex);
        }
        String lastArtistName = null;
        while (shuffledTracksCount >= 0) {
            // Get a random artistName out of all available ones
            String artistName = null;
            int tryCount = 0;
            while (tryCount++ < 3 && (artistName == null || artistName.equals(lastArtistName))) {
                // We try 3 times to get an artistName that is different from the one we picked
                // previously
                int randomPos = (int) (Math.random() * artistNames.size());
                artistName = artistNames.get(randomPos);
            }
            // Now we can get the list of track indexes
            List<Integer> indexes = artistMap.get(artistName);
            int randomPos = (int) (Math.random() * indexes.size());
            // Add the randomly picked track index to our shuffled index
            mShuffledIndex.add(indexes.get(randomPos));
            shuffledTracksCount--;
            lastArtistName = artistName;
        }
    }

    public int getRepeatingMode() {
        return mRepeatingMode;
    }

    /**
     * Set the repeat mode on the current playlist.
     */
    public void setRepeatingMode(int repeatingMode) {
        Log.d(TAG, "setRepeatingMode to " + repeatingMode);
        mRepeatingMode = repeatingMode;
        EventBus.getDefault().post(new PlayingPlaylistChangedEvent());
    }

    /**
     * Returns whether this PlaybackService is currently playing media.
     */
    public boolean isPlaying() {
        return mPlayState == PLAYBACKSERVICE_PLAYSTATE_PLAYING;
    }

    /**
     * @return Whether or not the mediaPlayer currently prepares a track
     */
    public boolean isPreparing() {
        return getCurrentQuery() != null && getCurrentQuery().getMediaPlayerInterface() != null
                && getCurrentQuery().getMediaPlayerInterface().isPreparing(getCurrentQuery());
    }

    /**
     * Get the current PlaylistEntry
     */
    public PlaylistEntry getCurrentEntry() {
        return mCurrentEntry;
    }

    public PlaylistEntry getNextEntry() {
        return getNextEntry(mCurrentEntry);
    }

    public PlaylistEntry getNextEntry(PlaylistEntry entry) {
        if (mRepeatingMode == REPEAT_ONE) {
            return entry;
        }
        int index = getPlaybackListIndex(entry);
        PlaylistEntry nextEntry = getPlaybackListEntry(index + 1);
        if (nextEntry == null && mRepeatingMode == REPEAT_ALL) {
            nextEntry = getPlaybackListEntry(0);
        }
        return nextEntry;
    }

    public boolean hasNextEntry() {
        return hasNextEntry(mCurrentEntry);
    }

    public boolean hasNextEntry(PlaylistEntry entry) {
        return mRepeatingMode == REPEAT_ONE || getNextEntry(entry) != null;
    }

    public PlaylistEntry getPreviousEntry() {
        return getPreviousEntry(mCurrentEntry);
    }

    public PlaylistEntry getPreviousEntry(PlaylistEntry entry) {
        if (mRepeatingMode == REPEAT_ONE) {
            return entry;
        }
        int index = getPlaybackListIndex(entry);
        PlaylistEntry previousEntry = getPlaybackListEntry(index - 1);
        if (previousEntry == null && mRepeatingMode == REPEAT_ALL) {
            previousEntry = getPlaybackListEntry(getPlaybackListSize() - 1);
        }
        return previousEntry;
    }

    public void deleteCurrentPlaylistEntry(ArrayList<PlaylistEntry> mEntryArray){
        for (PlaylistEntry entry:mEntryArray) {
            if(mPlaylist.deleteEntry(entry)){
                if (mPlaylist.getEntries().size() == 0){
                    pause(true);
                }else {
                    EventBus.getDefault().post(new PlayingPlaylistChangedEvent());
                    onTrackChanged();
                    saveQueue(true);
                }
            }
        }
    }

    public boolean hasPreviousEntry() {
        return hasPreviousEntry(mCurrentEntry);
    }

    public boolean hasPreviousEntry(PlaylistEntry entry) {
        return mRepeatingMode == REPEAT_ONE || getPreviousEntry(entry) != null;
    }

    /**
     * Get the current Query
     */
    public Query getCurrentQuery() {
        if (mCurrentEntry != null) {
            return mCurrentEntry.getQuery();
        }
        return null;
    }

    /**
     * @return the currently playing Track item (contains all necessary meta-data)
     */
    public Track getCurrentTrack() {
        if (getCurrentQuery() != null) {
            return getCurrentQuery().getPreferredTrack();
        }
        return null;
    }

    /**
     * This method sets the current track and prepares it for playback.
     */
    private void prepareCurrentQuery() {
        Log.d(TAG, "prepareCurrentQuery");
        if (getCurrentQuery() != null) {
            if (getCurrentQuery().isPlayable()) {
                mKillTimerHandler.removeCallbacksAndMessages(null);
                Message msg = mKillTimerHandler.obtainMessage();
                mKillTimerHandler.sendMessageDelayed(msg, DELAY_TO_KILL);

                if (getCurrentQuery().getImage() == null) {
                    ArrayList<String> requestIds = InfoSystem.get().resolve(
                            getCurrentQuery().getArtist(), false);
                    for (String requestId : requestIds) {
                        mCorrespondingRequestIds.put(requestId, getCurrentQuery().getCacheKey());
                    }
                    String requestId =
                            InfoSystem.get().resolve(getCurrentQuery().getAlbum());
                    if (requestId != null) {
                        mCorrespondingRequestIds.put(requestId, getCurrentQuery().getCacheKey());
                    }
                }

                TomahawkRunnable r = new TomahawkRunnable(TomahawkRunnable.PRIORITY_IS_PLAYBACK) {
                    @Override
                    public void run() {
                        if (isPlaying() && getCurrentQuery().getMediaPlayerInterface() != null) {
                            if (getCurrentQuery().getMediaPlayerInterface().prepare(
                                    getApplication(), getCurrentQuery(), mMediaPlayerCallback)
                                    == null) {
                                boolean isNetworkAvailable = isNetworkAvailable();
                                if (isNetworkAvailable
                                        && getCurrentQuery().getPreferredTrackResult() != null) {
                                    getCurrentQuery().blacklistTrackResult(
                                            getCurrentQuery().getPreferredTrackResult());
                                    EventBus.getDefault().post(new PlayingPlaylistChangedEvent());
                                }
                                if (!isNetworkAvailable
                                        || getCurrentQuery().getPreferredTrackResult() == null) {
                                    Log.e(TAG,
                                            "MediaPlayer was unable to prepare the track, jumping to next track");
                                    next();
                                } else {
                                    Log.d(TAG,
                                            "MediaPlayer blacklisted a result and tries to prepare again");
                                    prepareCurrentQuery();
                                }
                            } else {
                                mCurrentMediaPlayer = getCurrentQuery().getMediaPlayerInterface();
                            }
                        }
                    }
                };
                ThreadManager.get().executePlayback(r);
            } else {
                next();
            }
        }
    }
    private final char hexdigits[] = new char[] { '0', '1', '2', '3', '4', '5',
            '6', '7', '8', '9', 'a', 'b', 'c', 'd', 'e', 'f' };
    private int mCardId;
    private static final int MAX_HISTORY_SIZE = 100;
    private Vector<String> mHistory = new Vector<String>(MAX_HISTORY_SIZE);

    private  void saveQueue(boolean full) {
        if (!mQueueIsSaveable) {
            return;
        }
        SharedPreferences mPreferences =
                PreferenceManager.getDefaultSharedPreferences(TomahawkApp.getContext());
        SharedPreferences.Editor ed = mPreferences.edit();
        // long start = System.currentTimeMillis();
        if (full) {

            int length= mPreferences.getInt("queue_len",-1);
            if (length!=-1){
                for (int j=1;j<=length;j++){
                    ed.remove("queue_"+j);
                    ed.apply();
                }
            }
            List<PlaylistEntry> mEntries = mPlaylist.getEntries();
            ed.putInt("queue_len",mEntries.size());
            ed.apply();
            int i=1;
            for (PlaylistEntry entry:mEntries){
                Log.d("T1","queue_"+i+"_"+entry.getId());
                ed.putString("queue_"+i,entry.getId());
                ed.apply();
                i++;
            }

            ed.putInt("cardid", mCardId);
            if (mShuffleMode != SHUFFLE_NONE) {
                // In shuffle mode we need to save the history too
                length = mHistory.size();

                if (length!=-1){
                    for (int j=1;j<=length;j++){
                        ed.remove("history_"+j);
                        ed.apply();
                    }
                }
                i=1;
                for (String entry:mHistory){
                    ed.putString("history_"+i,entry);
                    ed.apply();
                    i++;
                }

            }
        }
        ed.putInt("curpos",mPlaylist.getIndexOfEntry(getCurrentEntry()));// mPlayPos);
        ed.putInt("seekpos", getPosition());
        ed.putInt("playstate",mPlayState);
        ed.putInt("repeatmode", mRepeatMode);
        ed.putInt("shufflemode", mShuffleMode);
        ed.apply();

        // Log.i("@@@@ service", "saved state in " + (System.currentTimeMillis()
        // - start) + " ms");
    }

    public void reloadQueue() {
        SharedPreferences mPreferences =
                PreferenceManager.getDefaultSharedPreferences(TomahawkApp.getContext());
        EventBus.getDefault().post(new PlaybackService.ReloadEvent());

      /*  String q = null;
        int qlen=-1;
        boolean newstyle = false;
        int id = mCardId;
        if (mPreferences.contains("cardid")) {
            newstyle = true;
            id = mPreferences.getInt("cardid", ~mCardId);
        }
        if (id == mCardId) {
            // Only restore the saved playlist if the card is still
            // the same one as when the playlist was saved
            qlen = mPreferences.getInt("queue_len", -1);
        }

        if (qlen!=-1) {
            List<PlaylistEntry> entries = new ArrayList<>();
            getPlaylist().clear();
            for (int i=1;i<=qlen;i++) {
                Log.d("T1","retreive_"+i+"_"+mPreferences.getString("queue_" + i, ""));
            PlaylistEntry entry = PlaylistEntry.getByKey(mPreferences.getString("queue_"+i, ""));
            if (entry!=null) {
            entries.add(entry);
            }

        }
            if(entries!=null&&entries.size()>0){
                getPlaylist().setEntries(entries);
                mPlayListLen = qlen;

                int pos = mPreferences.getInt("curpos", 0);
                if (pos < 0 || pos >= mPlayListLen) {
                    // The saved playlist is bogus, discard it
                    mPlayListLen = 0;
                    return;
                }
                PlaylistEntry currentEntry = getPlaylist().getEntryAtPos(pos);
                setPlaylist(getPlaylist(), currentEntry);
            }
        }

        if ((NormalMediaPlayer.get().getLibVlcInstance()==null)) {
        return;
        }


            long seekpos = mPreferences.getLong("seekpos", 0);
            //seek(seekpos >= 0 && seekpos < duration() ? seekpos : 0);
            seekTo((int)seekpos);*/
           /* Log.d(LOGTAG, "restored queue, currently at position " + position()
                    + "/" + duration() + " (requested " + seekpos + ")");*/

            int repmode = mPreferences.getInt("repeatmode", NOT_REPEATING);
            if (repmode != REPEAT_ALL && repmode != REPEAT_ONE) {
                repmode = NOT_REPEATING;
            }
            mRepeatMode = repmode;

            int shufmode = mPreferences.getInt("shufflemode", SHUFFLE_NONE);
            if (shufmode != SHUFFLE_AUTO && shufmode != SHUFFLE_NORMAL) {
                shufmode = SHUFFLE_NONE;
            }
            /*if (shufmode != SHUFFLE_NONE) {
                // in shuffle mode we need to restore the history too
                q = mPreferences.getString("history", "");
                qlen = q != null ? q.length() : 0;
                if (qlen > 1) {
                    plen = 0;
                    n = 0;
                    shift = 0;
                    mHistory.clear();
                    for (int i = 0; i < qlen; i++) {
                        char c = q.charAt(i);
                        if (c == ';') {
                            if (n >= mPlayListLen) {
                                // bogus history data
                                mHistory.clear();
                                break;
                            }
                            mHistory.add(n);
                            n = 0;
                            shift = 0;
                        } else {
                            if (c >= '0' && c <= '9') {
                                n += ((c - '0') << shift);
                            } else if (c >= 'a' && c <= 'f') {
                                n += ((10 + c - 'a') << shift);
                            } else {
                                // bogus history data
                                mHistory.clear();
                                break;
                            }
                            shift += 4;
                        }
                    }
                }
            }
            if (shufmode == SHUFFLE_AUTO) {
                if (!makeAutoShuffleList()) {
                    shufmode = SHUFFLE_NONE;
                }
            }
            mShuffleMode = shufmode;*/

    }


    public void setCurrentEntry(PlaylistEntry entry) {
        if(entry != null) {
            Log.d(TAG, "setCurrentEntry to " + entry.getId());
            releaseAllPlayers();
            if (mCurrentEntry != null) {
                deleteQueryInQueue(mCurrentEntry);
            }
            mCurrentEntry = entry;
            mCurrentIndex = getPlaybackListIndex(mCurrentEntry);
            handlePlayState();
            EventBus.getDefault().post(new PlayingPlaylistChangedEvent());
            onTrackChanged();
        }
    }

    private void onTrackChanged() {
        Log.d(TAG, "onTrackChanged");
        EventBus.getDefault().post(new PlayingTrackChangedEvent());
        saveQueue(true);
        if (getCurrentEntry() != null) {
            resolveQueriesFromTo(mCurrentIndex, mCurrentIndex - 2 + 10);
            updateNotification();
            updateLockscreenControls();
        }
    }

    /**
     * @return whether or not wi-fi is available
     */
    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(
                Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnectedOrConnecting();
    }

    /**
     * Get the current Playlist
     */
    public Playlist getPlaylist() {
        return mPlaylist;
    }

    /**
     * Get the current Queue
     */
    public Playlist getQueue() {
        return mQueue;
    }

    /**
     * Set the current Playlist to playlist and set the current Track to the Playlist's current
     * Track.
     */
    public void setPlaylist(Playlist playlist, PlaylistEntry currentEntry) {
        Log.d(TAG, "setPlaylist");
        releaseAllPlayers();
        mShuffled = false;
        mRepeatingMode = NOT_REPEATING;
        mPlaylist = playlist;
        mQueueStartPos = -1;
        setCurrentEntry(currentEntry);
    }

    public int getQueueStartPos() {
        return mQueueStartPos;
    }

    /**
     * @param position int containing the position in the current playback list
     * @return the {@link PlaylistEntry} which has been found at the given position
     */
    public PlaylistEntry getPlaybackListEntry(int position) {
        if (position < mQueueStartPos) {
            // The requested entry is positioned before the queue
            return getPlaylistEntry(position);
        } else {
            if (position < mQueueStartPos + mQueue.size()) {
                // Getting the entry from the queue
                return mQueue.getEntryAtPos(position - mQueueStartPos);
            } else {
                // The requested entry is positioned after the queue
                return getPlaylistEntry(position - mQueue.size());
            }
        }
    }

    /**
     * Private helper method that makes sure that the shuffled playlist is being used if needed.
     */
    private PlaylistEntry getPlaylistEntry(int position) {
        if (mShuffled) {
            int newPos = mShuffledIndex.get(position);
            return mPlaylist.getEntryAtPos(newPos);
        } else {
            return mPlaylist.getEntryAtPos(position);
        }
    }


    public void addToPlayList(ArrayList<PlaylistEntry> list) {
        int addlen = list.size();


        ArrayList<PlaylistEntry> tempEntry = (ArrayList<PlaylistEntry>)mPlaylist.getEntries();

        // copy list into playlist
        for (int i = 0; i < addlen; i++) {
            //tempEntry.add(list.get(i));
            mPlaylist.addQuery(tempEntry.size(),list.get(i).getQuery(),list.get(i).getId());
        }
        //mPlaylist.setEntries(tempEntry);
        EventBus.getDefault().post(new PlayingPlaylistChangedEvent());
        onTrackChanged();
    }

    /**
     * @param entry The {@link PlaylistEntry} to get the index for
     * @return an int containing the index of the given {@link PlaylistEntry} inside the current
     * playback list
     */
    public int getPlaybackListIndex(PlaylistEntry entry) {
        int index = mQueue.getIndexOfEntry(entry);
        if (index >= 0) {
            // Found entry in queue
            return index + mQueueStartPos;
        } else {
            index = mPlaylist.getIndexOfEntry(entry);
            if (index < 0) {
                Log.e(TAG,
                        "getPlaybackListIndex - Couldn't find given entry in mQueue or mPlaylist.");
                return -1;
            }
            if (index < mQueueStartPos) {
                // Found entry and its positioned before the queue
                return index;
            } else {
                // Found entry and its positioned after the queue
                return index + mQueue.size();
            }
        }
    }

    public int getPlaybackListSize() {
        return mQueue.size() + mPlaylist.size();
    }

    /**
     * Add given {@link org.tomahawk.libtomahawk.resolver.Query} to the Queue
     */
    public void addQueryToQueue(Query query) {
        Log.d(TAG, "addQueryToQueue " + query.getName());
        if (mQueue.size() == 0) {
            mQueueStartPos = mCurrentIndex + 1;
        }
        mQueue.addQuery(0, query,TomahawkMainActivity.getLifetimeUniqueStringId());
        EventBus.getDefault().post(new PlayingPlaylistChangedEvent());
        onTrackChanged();
    }


    /**
     * Add given {@link List} of {@link org.tomahawk.libtomahawk.resolver.Query}s to the Queue
     */
    public void addQueriesToQueue(List<Query> queries) {
        Log.d(TAG, "addQueriesToQueue count: " + queries.size());
        for (Query query : queries) {
            addQueryToQueue(query);
        }
    }

    public void deleteQueryInQueue(PlaylistEntry entry) {
        Log.d(TAG, "deleteQueryInQueue");
        if (mQueue.deleteEntry(entry)) {
            EventBus.getDefault().post(new PlayingPlaylistChangedEvent());
            onTrackChanged();
        }
    }

    /**
     * Returns the position of playback in the current Track.
     */
    public int getPosition() {
        int position = 0;
        if (getCurrentQuery() != null && getCurrentQuery().getMediaPlayerInterface() != null) {
            try {
                position = getCurrentQuery().getMediaPlayerInterface().getPosition();
            } catch (IllegalStateException e) {
                Log.e(TAG, "getPosition: " + e.getClass() + ": " + e.getLocalizedMessage());
            }
        }
        return position;
    }

    /**
     * Seeks to position msec
     */
    public void seekTo(int msec) {
        Log.d(TAG, "seekTo " + msec);
        if (getCurrentQuery() != null && getCurrentQuery().getMediaPlayerInterface() != null
                && getCurrentQuery().getMediaPlayerInterface().isPrepared(getCurrentQuery())) {
            if (msec < 0)
                msec = 0;
            if (msec > getCurrentTrack().getDuration())
                msec = (int)(long)getCurrentTrack().getDuration();
            getCurrentQuery().getMediaPlayerInterface().seekTo(msec);
        }
    }

    /**
     * Create or update an ongoing notification
     */
    public void updateNotification() {
        if (mShowingNotification && getCurrentQuery()!=null) {
            Log.d(TAG, "updateNotification");

            String albumName = "";
            String artistName = "";
            if (getCurrentQuery().getAlbum() != null) {
                albumName = getCurrentQuery().getAlbum().getName();
            }
            if (getCurrentQuery().getArtist() != null) {
                artistName = getCurrentQuery().getArtist().getName();
            }

            Intent intent = new Intent(ACTION_PREVIOUS, null, PlaybackService.this,
                    PlaybackService.class);
            PendingIntent previousPendingIntent = PendingIntent
                    .getService(PlaybackService.this, 0, intent,
                            PendingIntent.FLAG_UPDATE_CURRENT);
            intent = new Intent(ACTION_PLAYPAUSE, null, PlaybackService.this,
                    PlaybackService.class);
            PendingIntent playPausePendingIntent = PendingIntent
                    .getService(PlaybackService.this, 0, intent,
                            PendingIntent.FLAG_UPDATE_CURRENT);
            intent = new Intent(ACTION_NEXT, null, PlaybackService.this, PlaybackService.class);
            PendingIntent nextPendingIntent = PendingIntent
                    .getService(PlaybackService.this, 0, intent,
                            PendingIntent.FLAG_UPDATE_CURRENT);
            intent = new Intent(ACTION_FAVORITE, null, PlaybackService.this, PlaybackService.class);
            PendingIntent favoritePendingIntent = PendingIntent
                    .getService(PlaybackService.this, 0, intent,
                            PendingIntent.FLAG_UPDATE_CURRENT);
            intent = new Intent(ACTION_EXIT, null, PlaybackService.this, PlaybackService.class);
            PendingIntent exitPendingIntent = PendingIntent
                    .getService(PlaybackService.this, 0, intent,
                            PendingIntent.FLAG_UPDATE_CURRENT);

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                mSmallNotificationView = new RemoteViews(getPackageName(),
                        R.layout.notification_small);
            } else {
                mSmallNotificationView = new RemoteViews(getPackageName(),
                        R.layout.notification_small_compat);
            }
            mSmallNotificationView
                    .setTextViewText(R.id.notification_small_textview, getCurrentQuery().getName());
            if (TextUtils.isEmpty(albumName)) {
                mSmallNotificationView
                        .setTextViewText(R.id.notification_small_textview2, artistName);
            } else {
                mSmallNotificationView.setTextViewText(R.id.notification_small_textview2,
                        artistName + " - " + albumName);
            }
            if (isPlaying()) {
                mSmallNotificationView
                        .setImageViewResource(R.id.notification_small_imageview_playpause,
                                R.drawable.ic_player_pause_light);
            } else {
                mSmallNotificationView
                        .setImageViewResource(R.id.notification_small_imageview_playpause,
                                R.drawable.ic_player_play_light);
            }
            mSmallNotificationView
                    .setOnClickPendingIntent(R.id.notification_small_imageview_playpause,
                            playPausePendingIntent);
            mSmallNotificationView
                    .setOnClickPendingIntent(R.id.notification_small_imageview_next,
                            nextPendingIntent);
            mSmallNotificationView
                    .setOnClickPendingIntent(R.id.notification_small_imageview_exit,
                            exitPendingIntent);

            NotificationCompat.Builder builder = new NotificationCompat.Builder(
                    PlaybackService.this)
                    .setSmallIcon(R.drawable.ic_launcher).setContentTitle(artistName)
                    .setContentText(getCurrentQuery().getName()).setOngoing(true).setPriority(
                            NotificationCompat.PRIORITY_MAX).setContent(mSmallNotificationView);

            Intent notificationIntent = new Intent(PlaybackService.this,
                    TomahawkMainActivity.class);
            intent.setAction(TomahawkMainActivity.SHOW_PLAYBACKFRAGMENT_ON_STARTUP);
            intent.addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP);
            PendingIntent resultPendingIntent = PendingIntent
                    .getActivity(PlaybackService.this, 0, notificationIntent,
                            PendingIntent.FLAG_UPDATE_CURRENT);
            builder.setContentIntent(resultPendingIntent);

            mNotification = builder.build();

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                mLargeNotificationView = new RemoteViews(getPackageName(),
                        R.layout.notification_large);
                mLargeNotificationView.setTextViewText(R.id.notification_large_textview,
                        getCurrentQuery().getName());
                mLargeNotificationView
                        .setTextViewText(R.id.notification_large_textview2, artistName);
                mLargeNotificationView
                        .setTextViewText(R.id.notification_large_textview3, albumName);
                if (isPlaying()) {
                    mLargeNotificationView
                            .setImageViewResource(R.id.notification_large_imageview_playpause,
                                    R.drawable.ic_player_pause_light);
                } else {
                    mLargeNotificationView
                            .setImageViewResource(R.id.notification_large_imageview_playpause,
                                    R.drawable.ic_player_play_light);
                }
                /*if (DatabaseHelper.get().isItemLoved(getCurrentQuery())) {
                    mLargeNotificationView
                            .setImageViewResource(R.id.notification_large_imageview_favorite,
                                    R.drawable.ic_action_favorites_underlined);
                } else {
                    mLargeNotificationView
                            .setImageViewResource(R.id.notification_large_imageview_favorite,
                                    R.drawable.ic_action_favorites);
                }*/
                mLargeNotificationView
                        .setOnClickPendingIntent(R.id.notification_large_imageview_previous,
                                previousPendingIntent);
                mLargeNotificationView
                        .setOnClickPendingIntent(R.id.notification_large_imageview_playpause,
                                playPausePendingIntent);
                mLargeNotificationView
                        .setOnClickPendingIntent(R.id.notification_large_imageview_next,
                                nextPendingIntent);
                /*mLargeNotificationView
                        .setOnClickPendingIntent(R.id.notification_large_imageview_favorite,
                                favoritePendingIntent);*/
                mLargeNotificationView
                        .setOnClickPendingIntent(R.id.notification_large_imageview_exit,
                                exitPendingIntent);
                mNotification.bigContentView = mLargeNotificationView;

                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    @Override
                    public void run() {
                        ImageUtils.loadImageIntoNotification(TomahawkApp.getContext(),
                                getCurrentQuery().getImage(), mSmallNotificationView,
                                R.id.notification_small_imageview_albumart,
                                PLAYBACKSERVICE_NOTIFICATION_ID,
                                mNotification, Image.getSmallImageSize(),
                                getCurrentQuery().hasArtistImage());
                        ImageUtils.loadImageIntoNotification(TomahawkApp.getContext(),
                                getCurrentQuery().getImage(), mLargeNotificationView,
                                R.id.notification_large_imageview_albumart,
                                PLAYBACKSERVICE_NOTIFICATION_ID,
                                mNotification, Image.getSmallImageSize(),
                                getCurrentQuery().hasArtistImage());
                    }
                });
            }
            startForeground(PLAYBACKSERVICE_NOTIFICATION_ID, mNotification);
        }
    }

    /**
     * Update the playback controls/views which are being shown on the lockscreen
     */
    @TargetApi(Build.VERSION_CODES.ICE_CREAM_SANDWICH)
    private void updateLockscreenControls() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.ICE_CREAM_SANDWICH) {
            Log.d(TAG, "updateLockscreenControls()");
            // Use the media button APIs (if available) to register ourselves for media button
            // events
            MediaButtonHelper.registerMediaButtonEventReceiverCompat(
                    mAudioManager, mMediaButtonReceiverComponent);

            if (mRemoteControlClientCompat == null) {
                Intent intent = new Intent(Intent.ACTION_MEDIA_BUTTON);
                intent.setComponent(mMediaButtonReceiverComponent);
                mRemoteControlClientCompat = new RemoteControlClientCompat(
                        PendingIntent.getBroadcast(PlaybackService.this /*context*/,
                                0 /*requestCode, ignored*/, intent /*intent*/, 0 /*flags*/)
                );
                RemoteControlHelper.registerRemoteControlClient(mAudioManager,
                        mRemoteControlClientCompat);
            }

            // Use the remote control APIs (if available) to set the playback state
            if (isPlaying()) {
                mRemoteControlClientCompat
                        .setPlaybackState(RemoteControlClient.PLAYSTATE_PLAYING);
            } else {
                mRemoteControlClientCompat
                        .setPlaybackState(RemoteControlClient.PLAYSTATE_STOPPED);
            }

            int flags = RemoteControlClient.FLAG_KEY_MEDIA_PLAY |
                    RemoteControlClient.FLAG_KEY_MEDIA_PAUSE;
            if (hasNextEntry()) {
                flags |= RemoteControlClient.FLAG_KEY_MEDIA_NEXT;
            }
            if (hasPreviousEntry()) {
                flags |= RemoteControlClient.FLAG_KEY_MEDIA_PREVIOUS;
            }
            mRemoteControlClientCompat.setTransportControlFlags(flags);

            // Update the remote controls
            synchronized (this) {
                RemoteControlClientCompat.MetadataEditorCompat editor =
                        mRemoteControlClientCompat.editMetadata(true);
                editor.putString(MediaMetadataRetriever.METADATA_KEY_ALBUMARTIST,
                        getCurrentQuery().getArtist().getPrettyName())
                        .putString(MediaMetadataRetriever.METADATA_KEY_ARTIST,
                                getCurrentQuery().getArtist().getPrettyName())
                        .putString(MediaMetadataRetriever.METADATA_KEY_TITLE,
                                getCurrentQuery().getPrettyName())
                        .putLong(MediaMetadataRetriever.METADATA_KEY_DURATION,
                                getCurrentQuery().getPreferredTrack().getDuration());
                if (!TextUtils.isEmpty(getCurrentQuery().getAlbum().getPrettyName())) {
                    editor.putString(MediaMetadataRetriever.METADATA_KEY_ALBUM,
                            getCurrentQuery().getAlbum().getPrettyName());
                }
                editor.apply();
                Log.d(TAG, "Setting lockscreen metadata to: "
                        + getCurrentQuery().getArtist().getPrettyName() + ", "
                        + getCurrentQuery().getPrettyName());
            }

            Picasso.with(TomahawkApp.getContext()).cancelRequest(mLockscreenTarget);
            ImageUtils.loadImageIntoBitmap(TomahawkApp.getContext(),
                    getCurrentQuery().getImage(), mLockscreenTarget,
                    Image.getLargeImageSize(), getCurrentQuery().hasArtistImage());
        }
    }

    /**
     * Create or update an ongoing notification
     */
    @TargetApi(Build.VERSION_CODES.ICE_CREAM_SANDWICH)
    public void updateLockscreenPlayState() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.ICE_CREAM_SANDWICH
                && getCurrentQuery() != null) {
            Log.d(TAG, "updateLockscreenPlayState()");
            if (isPlaying()) {
                mRemoteControlClientCompat
                        .setPlaybackState(RemoteControlClient.PLAYSTATE_PLAYING);
            } else {
                mRemoteControlClientCompat
                        .setPlaybackState(RemoteControlClient.PLAYSTATE_STOPPED);
            }
        }
    }

    @Override
    public void onGainedAudioFocus(int gain) {
        switch (gain){
            case AudioManager.AUDIOFOCUS_GAIN:
                StandardMediaPlayer.get().getLibVlcInstance().setVolume(1f, 1f);
                start();
                break;
            default:
                break;
        }
    }

    @Override
    public void onLostAudioFocus(boolean canDuck,int loss) {
        switch (loss){
            case AudioManager.AUDIOFOCUS_LOSS_TRANSIENT_CAN_DUCK :
                // Lower the volume while ducking.
                StandardMediaPlayer.get().getLibVlcInstance().setVolume(0.2f, 0.2f);
                break;
            case (AudioManager.AUDIOFOCUS_LOSS) :
            case AudioManager.AUDIOFOCUS_LOSS_TRANSIENT:
                pause();
                break;
           default:
                break;
        }
    }

    void tryToGetAudioFocus() {
        if (mAudioFocus != AudioFocus.Focused && mAudioFocusHelper != null
                && mAudioFocusHelper.requestFocus()) {
            mAudioFocus = AudioFocus.Focused;
        }
    }

    void giveUpAudioFocus() {
        if (mAudioFocus == AudioFocus.Focused && mAudioFocusHelper != null
                && mAudioFocusHelper.abandonFocus()) {
            mAudioFocus = AudioFocus.NoFocusNoDuck;
        }
    }

    private void resolveQueriesFromTo(int start, int end) {
        Set<Query> qs = new HashSet<>();
        for (int i = start; i < end; i++) {
            if (i >= 0 && i < getPlaybackListSize()) {
                Query q = getPlaybackListEntry(i).getQuery();
                if (!mCorrespondingQueries.contains(q)) {
                    qs.add(q);
                }
            }
        }
        if (!qs.isEmpty()) {
            HashSet<Query> queries = PipeLine.get().resolve(qs);
            mCorrespondingQueries.addAll(queries);
        }
    }
    @Override
    public void onTaskRemoved(Intent rootIntent) {
        Toast.makeText(getApplicationContext(), "AS onTaskRemoved called", Toast.LENGTH_LONG).show();
        super.onTaskRemoved(rootIntent);
    }

    private void releaseAllPlayers() {
        //VLCMediaPlayer.get().release();
        StandardMediaPlayer.get().release();
        /*SpotifyMediaPlayer.get().release();
        RdioMediaPlayer.get().release();
        DeezerMediaPlayer.get().release();*/
    }

   /* private class MultiPlayer {
        private CompatMediaPlayer mCurrentMediaPlayer = new CompatMediaPlayer();
        private CompatMediaPlayer mNextMediaPlayer;
        private Handler mHandler;
        private boolean mIsInitialized = false;
        private Context context;

        public MultiPlayer(Context context) {
            this.context = context;
            mCurrentMediaPlayer.setWakeMode(context,
                    PowerManager.PARTIAL_WAKE_LOCK);
        }

        public void setDataSource(String path) {
            mIsInitialized = setDataSourceImpl(mCurrentMediaPlayer, path);
            if (mIsInitialized) {
                setNextDataSource(null);
            }
        }

        private boolean setDataSourceImpl(MediaPlayer player, String path) {
            try {
                player.reset();
                player.setOnPreparedListener(null);
                if (path.startsWith("content://")) {
                    player.setDataSource(context,
                            Uri.parse(path));
                } else {
                    player.setDataSource(path);
                }
                player.setAudioStreamType(AudioManager.STREAM_MUSIC);
                player.prepare();
            } catch (IOException ex) {
                // TODO: notify the user why the file couldn't be opened
                return false;
            } catch (IllegalArgumentException ex) {
                // TODO: notify the user why the file couldn't be opened
                return false;
            }
            player.setOnCompletionListener(listener);
            player.setOnErrorListener(errorListener);
            Intent i = new Intent(
                    AudioEffect.ACTION_OPEN_AUDIO_EFFECT_CONTROL_SESSION);
            i.putExtra(AudioEffect.EXTRA_AUDIO_SESSION, getAudioSessionId());
            i.putExtra(AudioEffect.EXTRA_PACKAGE_NAME, context.getPackageName());
            context.sendBroadcast(i);
            return true;
        }

        @SuppressLint("NewApi")
        public void setNextDataSource(String path) {
            mCurrentMediaPlayer.setNextMediaPlayer(null);
            if (mNextMediaPlayer != null) {
                mNextMediaPlayer.release();
                mNextMediaPlayer = null;
            }
            if (path == null) {
                return;
            }
            mNextMediaPlayer = new CompatMediaPlayer();
            mNextMediaPlayer.setWakeMode(context,
                    PowerManager.PARTIAL_WAKE_LOCK);
            mNextMediaPlayer.setAudioSessionId(getAudioSessionId());
            if (setDataSourceImpl(mNextMediaPlayer, path)) {
                mCurrentMediaPlayer.setNextMediaPlayer(mNextMediaPlayer);
            } else {
                // failed to open next, we'll transition the old fashioned way,
                // which will skip over the faulty file
                mNextMediaPlayer.release();
                mNextMediaPlayer = null;
            }
        }

        public boolean isInitialized() {
            return mIsInitialized;
        }

        public void start() {
            mCurrentMediaPlayer.start();
        }

        public void stop() {
            mCurrentMediaPlayer.reset();
            mIsInitialized = false;
        }

        *//**
         * You CANNOT use this player anymore after calling release()
         *//*
        public void release() {
            stop();
            mCurrentMediaPlayer.release();
        }

        public void pause() {
            mCurrentMediaPlayer.pause();
        }

        public void setHandler(Handler handler) {
            mHandler = handler;
        }

        MediaPlayer.OnCompletionListener listener = new MediaPlayer.OnCompletionListener() {
            public void onCompletion(MediaPlayer mp) {
                if (mp == mCurrentMediaPlayer && mNextMediaPlayer != null) {
                    mCurrentMediaPlayer.release();
                    mCurrentMediaPlayer = mNextMediaPlayer;
                    mNextMediaPlayer = null;
                    mHandler.sendEmptyMessage(TRACK_WENT_TO_NEXT);
                } else {
                    // Acquire a temporary wakelock, since when we return from
                    // this callback the MediaPlayer will release its wakelock
                    // and allow the device to go to sleep.
                    // This temporary wakelock is released when the
                    // RELEASE_WAKELOCK
                    // message is processed, but just in case, put a timeout on
                    // it.
                    mWakeLock.acquire(30000);
                    mHandler.sendEmptyMessage(TRACK_ENDED);
                    mHandler.sendEmptyMessage(RELEASE_WAKELOCK);
                }
            }
        };

        MediaPlayer.OnErrorListener errorListener = new MediaPlayer.OnErrorListener() {
            public boolean onError(MediaPlayer mp, int what, int extra) {
                switch (what) {
                    case MediaPlayer.MEDIA_ERROR_SERVER_DIED:
                        mIsInitialized = false;
                        mCurrentMediaPlayer.release();
                        // Creating a new MediaPlayer and settings its wakemode does
                        // not
                        // require the media service, so it's OK to do this now,
                        // while the
                        // service is still being restarted
                        mCurrentMediaPlayer = new CompatMediaPlayer();
                        mCurrentMediaPlayer.setWakeMode(context,
                                PowerManager.PARTIAL_WAKE_LOCK);
                        mHandler.sendMessageDelayed(
                                mHandler.obtainMessage(SERVER_DIED), 2000);
                        return true;
                    default:
                        Log.d("MultiPlayer", "Error: " + what + "," + extra);
                        break;
                }
                return false;
            }
        };

        public long duration() {
            return mCurrentMediaPlayer.getDuration();
        }

        public long position() {
            return mCurrentMediaPlayer.getCurrentPosition();
        }

        public long seek(long whereto) {
            mCurrentMediaPlayer.seekTo((int) whereto);
            return whereto;
        }

        public void setVolume(float vol) {
            mCurrentMediaPlayer.setVolume(vol, vol);
        }

        @SuppressLint("NewApi")
        public void setAudioSessionId(int sessionId) {
            mCurrentMediaPlayer.setAudioSessionId(sessionId);
        }

        @SuppressLint("NewApi")
        public int getAudioSessionId() {
            return mCurrentMediaPlayer.getAudioSessionId();
        }
    }

    class CompatMediaPlayer extends MediaPlayer implements
            MediaPlayer.OnCompletionListener {

        private boolean mCompatMode = true;
        private MediaPlayer mNextPlayer;
        private OnCompletionListener mCompletion;

        public CompatMediaPlayer() {
            try {
                MediaPlayer.class.getMethod("setNextMediaPlayer",
                        MediaPlayer.class);
                mCompatMode = false;
            } catch (NoSuchMethodException e) {
                mCompatMode = true;
                super.setOnCompletionListener(this);
            }
        }

        @SuppressLint("NewApi")
        public void setNextMediaPlayer(MediaPlayer next) {
            if (mCompatMode) {
                mNextPlayer = next;
            } else {
                super.setNextMediaPlayer(next);
            }
        }

        @Override
        public void setOnCompletionListener(OnCompletionListener listener) {
            if (mCompatMode) {
                mCompletion = listener;
            } else {
                super.setOnCompletionListener(listener);
            }
        }

        @Override
        public void onCompletion(MediaPlayer mp) {
            if (mNextPlayer != null) {
                // as it turns out, starting a new MediaPlayer on the completion
                // of a previous player ends up slightly overlapping the two
                // playbacks, so slightly delaying the start of the next player
                // gives a better user experience
                SystemClock.sleep(50);

                mNextPlayer.start();
            }
            mCompletion.onCompletion(this);
        }
    }
*/

}
