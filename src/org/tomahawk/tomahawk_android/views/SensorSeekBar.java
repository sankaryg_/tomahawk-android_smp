package org.tomahawk.tomahawk_android.views;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.util.AttributeSet;
import android.widget.SeekBar;

public class SensorSeekBar extends SeekBar
{

    private Paint bgPaint;
    private boolean isDisabled;
    private int maxParsedValue;
    private int minParsedValue;
    private int periodToMaxSecondaryValue;
    private long previousMaxSecondaryValueSetTime;
    private Paint txtPaint;
    private boolean visualyDisabledWhenMax;
    private boolean visualyDisabledWhenZero;

    public SensorSeekBar(Context context)
    {
        super(context);
        visualyDisabledWhenMax = false;
        visualyDisabledWhenZero = false;
        isDisabled = false;
        minParsedValue = 0;
        maxParsedValue = 99;
        periodToMaxSecondaryValue = 500;
        previousMaxSecondaryValueSetTime = 0L;
        bgPaint = new Paint();
        txtPaint = new Paint();
        init();
    }

    public SensorSeekBar(Context context, AttributeSet attributeset)
    {
        super(context, attributeset);
        visualyDisabledWhenMax = false;
        visualyDisabledWhenZero = false;
        isDisabled = false;
        minParsedValue = 0;
        maxParsedValue = 99;
        periodToMaxSecondaryValue = 500;
        previousMaxSecondaryValueSetTime = 0L;
        bgPaint = new Paint();
        txtPaint = new Paint();
        init();
    }

    public SensorSeekBar(Context context, AttributeSet attributeset, int i)
    {
        super(context, attributeset, i);
        visualyDisabledWhenMax = false;
        visualyDisabledWhenZero = false;
        isDisabled = false;
        minParsedValue = 0;
        maxParsedValue = 99;
        periodToMaxSecondaryValue = 500;
        previousMaxSecondaryValueSetTime = 0L;
        bgPaint = new Paint();
        txtPaint = new Paint();
    }

    private void init()
    {
        bgPaint = new Paint();
        txtPaint = new Paint();
        setMax(99);
    }

    private int realValueToSeekBarValue(int i)
    {
        float f = maxParsedValue - minParsedValue;
        if (i >= 9999)
        {
            return 99;
        } else
        {
            return Math.round(100F * ((float)(i - minParsedValue) / f));
        }
    }

    private int seekbarValueToRealValue(int i)
    {
        return Math.round(((float)(maxParsedValue - minParsedValue) * (float)i) / 100F) + minParsedValue;
    }

    public int getParsedMaxValue()
    {
        return maxParsedValue;
    }

    public int getParsedMinValue()
    {
        return minParsedValue;
    }

    public int getParsedProgress()
    {
        int i = seekbarValueToRealValue(super.getProgress());
        return i;
      }

    public int getParsedSecondaryProgress()
    {
         int i = seekbarValueToRealValue(super.getSecondaryProgress());
        return i;
        
    }

    public boolean isDisabled()
    {
        return isDisabled;
    }

    protected void onDraw(Canvas canvas)
    {
        /*this;
        JVM INSTR monitorenter ;
        */
    	super.onDraw(canvas);
        if ((!visualyDisabledWhenMax || getProgress() < getMax()) && (!visualyDisabledWhenZero || getProgress() > 0)) isDisabled = false; else{
        txtPaint.setColor(Color.parseColor("#88ffff"));
        txtPaint.setTextSize(18F);
        canvas.drawText(getContext().getResources().getString(0x7f0d00d2), 50F, 20F, txtPaint);
        isDisabled = true;
        }
    }

    public void setParsedMaxValue(int i)
    {
        maxParsedValue = i;
    }

    public void setParsedMinValue(int i)
    {
        minParsedValue = i;
    }

    public void setParsedProgress(int i)
    {
        super.setProgress(realValueToSeekBarValue(i));
        return;
        
    }

    public void setParsedSecondaryProgress(int i)
    {
        setSecondaryProgress(realValueToSeekBarValue(i));
        return;
        
    }

    public void setSecondaryProgress(int i)
    {
        long l;
        if (periodToMaxSecondaryValue <= 0)
        {
            //break MISSING_BLOCK_LABEL_62;
        }
        l = System.currentTimeMillis();
        if (i <= super.getSecondaryProgress()) {
        	if (l <= previousMaxSecondaryValueSetTime + (long)periodToMaxSecondaryValue) 
            	return; 
            else {
            super.setSecondaryProgress(i);
             }
        } else{
        super.setSecondaryProgress(i);
        previousMaxSecondaryValueSetTime = l;
        }
        
    }

    public void setVisualyDisabledWhenMax(boolean flag)
    {
        visualyDisabledWhenMax = flag;
    }

    public void setVisualyDisabledWhenZero(boolean flag)
    {
        visualyDisabledWhenZero = flag;
    }
}
