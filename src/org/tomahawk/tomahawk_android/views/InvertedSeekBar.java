package org.tomahawk.tomahawk_android.views;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.view.MotionEvent;

public class InvertedSeekBar extends SensorSeekBar
{

    public InvertedSeekBar(Context context)
    {
        super(context);
    }

    public InvertedSeekBar(Context context, AttributeSet attributeset)
    {
        super(context, attributeset);
    }

    public InvertedSeekBar(Context context, AttributeSet attributeset, int i)
    {
        super(context, attributeset, i);
    }

    protected void onDraw(Canvas canvas)
    {
        Rect rect = canvas.getClipBounds();
        canvas.rotate(180F);
        canvas.translate(-1 * rect.width(), -1 * rect.height());
        super.onDraw(canvas);
        return;
       
    }

    public boolean onTouchEvent(MotionEvent motionevent)
    {
        float f = motionevent.getX();
        motionevent.setLocation((float)getWidth() - f, motionevent.getY());
        return super.onTouchEvent(motionevent);
    }

    public void setSecondaryProgress(int i)
    {
        super.setSecondaryProgress(i);
        invalidate();
        return;
       
    }
}
