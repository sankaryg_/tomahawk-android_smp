package org.tomahawk.tomahawk_android.views;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.widget.SeekBar;

/**
 * Created by YGS on 9/15/2015.
 */
public class SensitiveSeekBar extends SeekBar{

    public SensitiveSeekBar(Context context) {
        super(context);
    }
    public SensitiveSeekBar(Context context, AttributeSet attrs) {
        super(context, attrs);
    }
    public SensitiveSeekBar(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
    }

    Drawable mThumb;
    @Override
    public void setThumb(Drawable thumb) {
        super.setThumb(thumb);
        mThumb = thumb;
    }
    public Drawable getSeekBarThumb() {
        return mThumb;
    }
}
