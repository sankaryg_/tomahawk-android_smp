package org.tomahawk.tomahawk_android.views;

import android.util.Log;
import android.view.MotionEvent;
import android.view.View;

import org.tomahawk.libtomahawk.infosystem.SwipeInterface;

/**
 * Created by YGS on 11/16/2015.
 */
public class PlaybackSwipeDetector implements View.OnTouchListener {

    private SwipeInterface activity;
    static final int MIN_DISTANCE = 100;
    private float downX, downY, upX, upY;

    public PlaybackSwipeDetector(SwipeInterface activity){
        this.activity = activity;
    }

    public void onTopToBottomSwipe(View v){
        Log.i("PlaybackSwipeDetector", "onTopToBottomSwipe!");
        activity.top2bottom(v);
    }


    @Override
    public boolean onTouch(View v, MotionEvent event) {
        switch(event.getAction()){
            case MotionEvent.ACTION_DOWN: {
                downX = event.getX();
                downY = event.getY();
                return true;
            }
            case MotionEvent.ACTION_UP: {
                upX = event.getX();
                upY = event.getY();

                float deltaX = downX - upX;
                float deltaY = downY - upY;

                // swipe horizontal?
               /* if(Math.abs(deltaX) > MIN_DISTANCE){
                    // left or right
                    if(deltaX < 0) { this.onLeftToRightSwipe(v); return true; }
                    if(deltaX > 0) { this.onRightToLeftSwipe(v); return true; }
                }
                else {
                    Log.i(logTag, "Swipe was only " + Math.abs(deltaX) + " long, need at least " + MIN_DISTANCE);
                }*/

                // swipe vertical?
                if(Math.abs(deltaY) > MIN_DISTANCE){
                    // top or down
                    if(deltaY < 0) { this.onTopToBottomSwipe(v); return true; }
                    //if(deltaY > 0) { this.onBottomToTopSwipe(v); return true; }
                }
                else {
                    Log.i("PlaybackSwipeDetector", "Swipe was only " + Math.abs(deltaX) + " long, need at least " + MIN_DISTANCE);
                    v.performClick();
                }
            }
        }
        return false;
    }
}
