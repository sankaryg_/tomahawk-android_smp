/* == This file is part of Tomahawk Player - <http://tomahawk-player.org> ===
 *
 *   Copyright 2014, Enno Gottschalk <mrmaffen@googlemail.com>
 *
 *   Tomahawk is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Tomahawk is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Tomahawk. If not, see <http://www.gnu.org/licenses/>.
 */
package org.tomahawk.tomahawk_android.views;

import android.content.Context;
import android.content.res.Resources;
import android.graphics.drawable.TransitionDrawable;
import android.media.AudioManager;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.annotation.NonNull;
import android.util.AttributeSet;
import android.util.Log;
import android.view.MotionEvent;
import android.view.View;
import android.view.animation.LinearInterpolator;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import com.nineoldandroids.animation.Keyframe;
import com.nineoldandroids.animation.ObjectAnimator;
import com.nineoldandroids.animation.PropertyValuesHolder;
import com.nineoldandroids.animation.ValueAnimator;

import org.tomahawk.libtomahawk.collection.Image;
import org.tomahawk.libtomahawk.resolver.Resolver;
import org.tomahawk.libtomahawk.utils.ImageUtils;
import org.tomahawk.libtomahawk.utils.ViewUtils;
import org.tomahawk.tomahawk_android.R;
import org.tomahawk.tomahawk_android.TomahawkApp;
import org.tomahawk.tomahawk_android.activities.TomahawkMainActivity;
import org.tomahawk.tomahawk_android.dialogs.BatteryStatusDialog;
import org.tomahawk.tomahawk_android.dialogs.SleepTimerDialog;
import org.tomahawk.tomahawk_android.dialogs.VolumeStatusDialog;
import org.tomahawk.tomahawk_android.fragments.ContentHeaderFragment;
import org.tomahawk.tomahawk_android.fragments.EffectsPagerFragment;
import org.tomahawk.tomahawk_android.fragments.TomahawkFragment;
import org.tomahawk.tomahawk_android.services.PlaybackService;
import org.tomahawk.tomahawk_android.utils.AnimationUtils;
import org.tomahawk.tomahawk_android.utils.FragmentUtils;

import java.util.HashSet;
import java.util.Set;

import de.greenrobot.event.EventBus;

public class PlaybackPanel extends FrameLayout {

   public enum PlaybackState{
        MINI,
        MAIN,
       START,
       END
    }

    private PlaybackService mPlaybackService;

    public static final String COMPLETION_STRING_DEFAULT = "-:--";

    private FrameLayout mTextViewContainer;

    private FrameLayout mOptionsContainer;

    //private View mPanelContainer;

    private View mMainPanelContainer;

    private FrameLayout mArtistNameButton;

    private TextView mArtistTextView;

    private TextView mTrackTextView;

    private TextView mCompletionTimeTextView;

    private TextView mCurrentTimeTextView;

    private TextView mSeekTimeTextView;

    //private ImageView mResolverImageView;

    private ImageView mPlayButton;

    private ImageView mPauseButton;

   // private ProgressBar mProgressBar;

    //private View mProgressBarThumb;

    private float mLastThumbPosition = -1f;

    private boolean mAbortSeeking;

    private FrameLayout mCircularProgressBarContainer;

    //private HoloCircularProgressBar mCircularProgressBar;

    private final Set<ValueAnimator> mAnimators = new HashSet<>();

    private int mLastPlayTime = 0;

    private boolean mInitialized = false;

    private PlaybackState state = PlaybackState.MAIN;

    private LinearLayout mainPlayerFooter;

    private ImageView mSleepTimer;

    private ImageView mEqualizer;

    private ImageView mBatteryStatus;

    private ImageView mFavouriteBtn;

    private ImageView mVolumeBtn;

    private ImageView mShareBtn;

    private TextView mCurrentPosition;

    private TextView mDuration;

    private SeekBar mSeekbar;

    private ImageButton mShuffleBtn;

    private ImageButton mPrevBtn;

    private ImageButton mPlayPauseBtn;

    private ImageButton mNextBtn;

    private ImageButton mRepeatBtn;

    private Toast mToast;

    private TextView batteryLevel;

    private TomahawkMainActivity mActivity;

    private ImageView mSmallImageView;

    //RelativeLayout miniPlayback;
    RelativeLayout mainPlayback;

    public PlaybackPanel(Context context) {
        super(context);
        inflate(getContext(), R.layout.playback_panel, this);
        init();
    }

    public PlaybackPanel(Context context, AttributeSet attrs) {
        super(context, attrs);
        inflate(getContext(), R.layout.playback_panel, this);
        init();
    }

    public void setActivity(TomahawkMainActivity mActivity){
        this.mActivity = mActivity;
    }
    private void init() {

        //miniPlayback = (RelativeLayout)findViewById(R.id.miniPlayBack);
        mTextViewContainer = (FrameLayout) findViewById(R.id.textview_container);
        //mPanelContainer = findViewById(R.id.panel_container);
        mMainPanelContainer = findViewById(R.id.mainPanelContainer);
        mArtistNameButton = (FrameLayout) mTextViewContainer.findViewById(R.id.artist_name_button);
        mArtistTextView = (TextView) mArtistNameButton.findViewById(R.id.artist_textview);
        mTrackTextView = (TextView) mTextViewContainer.findViewById(R.id.track_textview);
        mCompletionTimeTextView = (TextView) mTextViewContainer.findViewById(R.id.completiontime_textview);
        mCurrentTimeTextView = (TextView) mTextViewContainer.findViewById(R.id.currenttime_textview);
        mSmallImageView = (ImageView) mTextViewContainer.findViewById(R.id.imageview1);
        //mSeekTimeTextView = (TextView) findViewById(R.id.seektime_textview);
        //mResolverImageView = (ImageView) findViewById(R.id.resolver_imageview);
        mPlayButton = (ImageView) mTextViewContainer.findViewById(R.id.play_button);
        mPauseButton = (ImageView) mTextViewContainer.findViewById(R.id.pause_button);
        //mProgressBar = (ProgressBar) findViewById(R.id.progressbar);
        //mProgressBarThumb = findViewById(R.id.progressbar_thumb);
        mCircularProgressBarContainer =
                (FrameLayout)mTextViewContainer.findViewById(R.id.circularprogressbar_container);
       /* mCircularProgressBar = (HoloCircularProgressBar)
                mCircularProgressBarContainer.findViewById(R.id.circularprogressbar);*/

        //mainPlayback = (RelativeLayout)findViewById(R.id.main_player_controller);
        mOptionsContainer = (FrameLayout)findViewById(R.id.mainOptionContainer);
        mainPlayerFooter = (LinearLayout)findViewById(R.id.playerFooter);
        mSleepTimer = (ImageView)findViewById(R.id.sleepTimerBtn);
        mEqualizer = (ImageView)findViewById(R.id.equalizerBtn);
        mBatteryStatus = (ImageView)findViewById(R.id.batteryStatusBtn);
        batteryLevel = (TextView)findViewById(R.id.batteryLevel);
        mFavouriteBtn = (ImageView) findViewById(R.id.favourite_button);
        mVolumeBtn = (ImageView) findViewById(R.id.volumeBtn);
        mShareBtn = (ImageView) findViewById(R.id.shareBtn);
        mCurrentPosition = (TextView) findViewById(R.id.position_text);
        mDuration = (TextView)findViewById(R.id.duration_text);
        mSeekbar = (SeekBar) findViewById(R.id.seek_bar);
        //mSeekbar.getThumb().mutate().setAlpha(0);
        mShuffleBtn = (ImageButton)mMainPanelContainer.findViewById(R.id.shuffle_button);
        mPrevBtn = (ImageButton) mMainPanelContainer.findViewById(R.id.previous_button);
        mPlayPauseBtn = (ImageButton)mMainPanelContainer.findViewById(R.id.play_pause_button);
        mNextBtn = (ImageButton)mMainPanelContainer.findViewById(R.id.next_button);
        mRepeatBtn = (ImageButton)mMainPanelContainer.findViewById(R.id.repeat_button);
        mSeekbar.setMax(1000);
        showPlaybackPanel(PlaybackState.MAIN);
    }

    public void showPlaybackPanel(PlaybackState state){
        this.state = state;
        switch (state){
            case START:
                mOptionsContainer.setVisibility(INVISIBLE);
                mTextViewContainer.setVisibility(INVISIBLE);
                mMainPanelContainer.setVisibility(INVISIBLE);
                break;
            case END:

                break;
            case MINI:
                mOptionsContainer.setVisibility(VISIBLE);
                mTextViewContainer.setVisibility(INVISIBLE);
                mMainPanelContainer.setVisibility(VISIBLE);
                //mPanelContainer.setVisibility(INVISIBLE);
                break;
            case MAIN:
                mOptionsContainer.setVisibility(INVISIBLE);
                mTextViewContainer.setVisibility(VISIBLE);
                mMainPanelContainer.setVisibility(INVISIBLE);
                //mPanelContainer.setVisibility(VISIBLE);
                break;
        }

    }
  /*  private Point p;
    @Override
    public void onWindowFocusChanged(boolean hasWindowFocus) {
        super.onWindowFocusChanged(hasWindowFocus);
        int[] location = new int[2];
        mVolumeBtn = (ImageView) findViewById(R.id.volumeBtn);

        // Get the x, y location and store it in the location[] array
        // location[0] = x, location[1] = y.
        mVolumeBtn.getLocationOnScreen(location);

        // Initialize the Point with x, and y positions
        p = new Point();
        p.x = location[0];
        p.y = location[1];

    }*/

    /*private void showPopup(final Activity context, Point p) {

        // Inflate the popup_layout.xml
        LinearLayout viewGroup = (LinearLayout) context
                .findViewById(R.id.popup);
        LayoutInflater layoutInflater = (LayoutInflater) context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View layout = layoutInflater.inflate(R.layout.volume_settings,
                viewGroup);

        // Creating the PopupWindow
        final PopupWindow popup = new PopupWindow(context);
        popup.setContentView(layout);
        popup.setFocusable(true);
        popup.setWidth(WindowManager.LayoutParams.WRAP_CONTENT);
        popup.setHeight(WindowManager.LayoutParams.WRAP_CONTENT);

        // Some offset to align the popup a bit to the right, and a bit down,
        // relative to button's position.
        int OFFSET_X = 138;
        int OFFSET_Y = 30;

        // Clear the default translucent background
        popup.setBackgroundDrawable(new BitmapDrawable());

        // Displaying the popup at the specified location, + offsets.
        popup.showAtLocation(layout, Gravity.NO_GRAVITY, OFFSET_X,   OFFSET_Y);

        final TextView volumeNumber = (TextView) layout
                .findViewById(R.id.volumeNumberText);
        ImageView defaultEqualizer = (ImageView) layout
                .findViewById(R.id.defaultEqualizerBtn);
        SeekBar volumeBar = (SeekBar) layout
                .findViewById(R.id.volume_bar);
        if (Build.VERSION.SDK_INT >= 9) {
            defaultEqualizer.setImageResource(R.drawable.eq);
        } else {
            defaultEqualizer.setImageResource(R.drawable.unmute);
        }
        volumeNumber.setText(String.valueOf(VolumeHelper
                .getMediaVolume(context)));

        defaultEqualizer.setOnClickListener(new OnClickListener() {

            @SuppressLint("InlinedApi")
            @Override
            public void onClick(View v) {
                // TODO Auto-generated method stub

                Bundle bundle = new Bundle();
                bundle.putInt(TomahawkFragment.CONTENT_HEADER_MODE,
                        ContentHeaderFragment.MODE_ACTIONBAR_FILLED);
                FragmentUtils.replace((TomahawkMainActivity)getContext(), EqualizerFragment_.class,
                        bundle);
                popup.dismiss();

            }
        });
        volumeBar.setMax(audioManager
                .getStreamMaxVolume(AudioManager.STREAM_MUSIC));
        volumeBar.setProgress(audioManager
                .getStreamVolume(AudioManager.STREAM_MUSIC));

        volumeBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onStopTrackingTouch(SeekBar arg0) {
            }

            @Override
            public void onStartTrackingTouch(SeekBar arg0) {
            }

            @Override
            public void onProgressChanged(SeekBar arg0, int progress,
                                          boolean arg2) {
                audioManager.setStreamVolume(AudioManager.STREAM_MUSIC,
                        progress, 0);
                volumeNumber.setText(String.valueOf(VolumeHelper
                        .getMediaVolume(context)));
            }
        });
    }*/

    public void updateBatteryStatus(int level){
        if(level > 0){
            batteryLevel.setText(String.valueOf(level));
        }else
            batteryLevel.setText("");
    }

    @Override
    public boolean onTouchEvent(@NonNull MotionEvent event) {
        return false;
    }
    private long mLastSeekEventTime;
    private int mPosOverride = -1;
    private boolean mFromTouch = false;
    private AudioManager audioManager;
    private int currentVolume;
    public void setup(final boolean isPanelExpanded) {
        mInitialized = true;
        audioManager = (AudioManager) getContext().getSystemService(Context.AUDIO_SERVICE);

        currentVolume = audioManager.getStreamVolume(AudioManager.STREAM_MUSIC);
        if (currentVolume > 0) {
            mVolumeBtn.setImageResource(R.drawable.unmute);
        } else {
            mVolumeBtn.setImageResource(R.drawable.mute);
        }

        mVolumeBtn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                /*if (p != null)
                    showPopup((TomahawkMainActivity)getContext(), p);*/
                android.support.v4.app.FragmentManager fragmentManager = mActivity.getSupportFragmentManager();
                android.support.v4.app.Fragment fragment = fragmentManager.findFragmentByTag("volume");
                if (fragment!= null){
                    fragmentManager.beginTransaction().remove(fragment).commit();
                }
                VolumeStatusDialog dialog = new VolumeStatusDialog();
                dialog.setActivity(mActivity);
                dialog.show(fragmentManager,"volume");
            }
        });
        mCircularProgressBarContainer.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mPlaybackService != null) {
                    mPlaybackService.playPause(true);
                }
            }
        });
        mPlayPauseBtn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mPlaybackService != null) {
                    mPlaybackService.playPause(true);
                }
            }
        });
        mPrevBtn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mPlaybackService != null) {
                    mPlaybackService.previous();
                }
            }
        });
        mNextBtn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mPlaybackService != null) {
                    mPlaybackService.next();
                }
            }
        });

        mBatteryStatus.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                android.support.v4.app.FragmentManager fragmentManager = mActivity.getSupportFragmentManager();
                android.support.v4.app.Fragment fragment = fragmentManager.findFragmentByTag("battery_timer");
                if (fragment!= null){
                    fragmentManager.beginTransaction().remove(fragment).commit();
                }
                BatteryStatusDialog dialog = new BatteryStatusDialog();
                dialog.show(fragmentManager, "battery_timer");
            }
        });
        mSleepTimer.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                android.support.v4.app.FragmentManager fragmentManager = mActivity.getSupportFragmentManager();
                android.support.v4.app.Fragment fragment = fragmentManager.findFragmentByTag("sleep_timer");
                if (fragment!= null){
                    fragmentManager.beginTransaction().remove(fragment).commit();
                }
                SleepTimerDialog dialog = new SleepTimerDialog();
                dialog.setPlaybackService(mPlaybackService);
                dialog.show(fragmentManager, "sleep_timer");
            }
        });
        mShuffleBtn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mPlaybackService != null) {
                    mPlaybackService.setShuffled(!mPlaybackService.isShuffled());

                    if (mToast != null) {
                        mToast.cancel();
                    }
                    mToast = Toast.makeText(getContext(), getContext().getString(mPlaybackService.isShuffled()
                            ? R.string.shuffle_on_label
                            : R.string.shuffle_off_label), Toast.LENGTH_SHORT);
                    mToast.show();
                }
            }
        });
        mRepeatBtn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mPlaybackService != null) {
                    int repeatMode = mPlaybackService.getRepeatingMode();
                    if (repeatMode == PlaybackService.NOT_REPEATING) {
                        mPlaybackService.setRepeatingMode(PlaybackService.REPEAT_ALL);
                    } else if (repeatMode == PlaybackService.REPEAT_ALL) {
                        mPlaybackService.setRepeatingMode(PlaybackService.REPEAT_ONE);
                    } else if (repeatMode == PlaybackService.REPEAT_ONE) {
                        mPlaybackService.setRepeatingMode(PlaybackService.NOT_REPEATING);
                    }
                }
            }
        });
        mShareBtn.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                String shareTxt = "Listening ";
                if (mPlaybackService != null) {
                    try {
                        shareTxt = shareTxt + mPlaybackService.getCurrentQuery().getName() + " by "
                                + mPlaybackService.getCurrentQuery().getArtist().getName();
                    } catch (Exception e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                    }
                }


                PlaybackService.ShareEvent event = new PlaybackService.ShareEvent();
                event.shareText = shareTxt + " via SensitiveMusicPlayer";
                EventBus.getDefault().post(event);
                setupAnimations();
                setupAnimationsDown();

            }
        });
        mSeekbar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                if (!fromUser || (mPlaybackService == null))
                    return;
                long now = SystemClock.elapsedRealtime();
                if ((now - mLastSeekEventTime) > 250) {
                    mLastSeekEventTime = now;
                    mPosOverride = (int) (long) mPlaybackService.getCurrentTrack().getDuration() * progress / 1000;
                    mPlaybackService.seekTo(mPosOverride);

                    // trackball event, allow progress updates
                    if (!mFromTouch) {
                        refreshNow();
                        mPosOverride = -1;
                    }
                }
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                mLastSeekEventTime = 0;
                mFromTouch = true;
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                mPosOverride = -1;
                mFromTouch = false;
            }
        });
        mEqualizer.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                Bundle bundle = new Bundle();
                bundle.putInt(TomahawkFragment.CONTENT_HEADER_MODE,
                        ContentHeaderFragment.MODE_HEADER_STATIC);
                FragmentUtils.replace((TomahawkMainActivity)getContext(), EffectsPagerFragment.class,
                        bundle);
            }
        });
       /* mCircularProgressBar.setOnLongClickListener(new OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                PreferenceManager.getDefaultSharedPreferences(getContext()).edit()
                        .putBoolean(TomahawkMainActivity.COACHMARK_SEEK_DISABLED, true)
                        .apply();
                View coachMark = ViewUtils.ensureInflation(PlaybackPanel.this,
                        R.id.playbackpanel_seek_coachmark_stub, R.id.playbackpanel_seek_coachmark);
                coachMark.setVisibility(GONE);
                if (!isPanelExpanded || getResources().getBoolean(R.bool.is_landscape)) {
                    AnimationUtils.fade(mTextViewContainer,
                            AnimationUtils.DURATION_PLAYBACKSEEKMODE, false, true);
                }
                AnimationUtils.fade(mCircularProgressBarContainer,
                        AnimationUtils.DURATION_PLAYBACKSEEKMODE, false, true);
                AnimationUtils.fade(mResolverImageView,
                        AnimationUtils.DURATION_PLAYBACKSEEKMODE, false, true);
                AnimationUtils.fade(mCompletionTimeTextView,
                        AnimationUtils.DURATION_PLAYBACKSEEKMODE, false, true);
                *//*AnimationUtils.fade(mProgressBarThumb,
                        AnimationUtils.DURATION_PLAYBACKSEEKMODE, true, true);*//*
                AnimationUtils.fade(mCurrentTimeTextView,
                        AnimationUtils.DURATION_PLAYBACKSEEKMODE, true, true);
                AnimationUtils.fade(mSeekTimeTextView,
                        AnimationUtils.DURATION_PLAYBACKSEEKMODE, true, true);
                AnimationUtils.fade(mProgressBar,
                        AnimationUtils.DURATION_PLAYBACKSEEKMODE, true, true);

                mCircularProgressBar.setOnTouchListener(new OnTouchListener() {
                    @Override
                    public boolean onTouch(View v, MotionEvent event) {
                        if (event.getAction() == MotionEvent.ACTION_UP) {
                            if (!isPanelExpanded || getResources()
                                    .getBoolean(R.bool.is_landscape)) {
                                AnimationUtils.fade(mTextViewContainer,
                                        AnimationUtils.DURATION_PLAYBACKSEEKMODE, true, true);
                            }
                            AnimationUtils.fade(mCircularProgressBarContainer,
                                    AnimationUtils.DURATION_PLAYBACKSEEKMODE, true, true);
                            AnimationUtils.fade(mResolverImageView,
                                    AnimationUtils.DURATION_PLAYBACKSEEKMODE, true, true);
                            AnimationUtils.fade(mCompletionTimeTextView,
                                    AnimationUtils.DURATION_PLAYBACKSEEKMODE, true, true);
                            *//*AnimationUtils.fade(mProgressBarThumb,
                                    AnimationUtils.DURATION_PLAYBACKSEEKMODE, false, true);*//*
                            AnimationUtils.fade(mCurrentTimeTextView,
                                    AnimationUtils.DURATION_PLAYBACKSEEKMODE, false, true);
                            AnimationUtils.fade(mSeekTimeTextView,
                                    AnimationUtils.DURATION_PLAYBACKSEEKMODE, false, true);
                            AnimationUtils.fade(mProgressBar,
                                    AnimationUtils.DURATION_PLAYBACKSEEKMODE, false, true);
                            mCircularProgressBar.setOnTouchListener(null);
                            if (!mAbortSeeking) {
                                int seekTime = (int) ((mLastThumbPosition - mProgressBar.getX())
                                        / mProgressBar.getWidth()
                                        * mPlaybackService.getCurrentTrack().getDuration());
                                mPlaybackService.seekTo(seekTime);
                            }
                        } else if (event.getAction() == MotionEvent.ACTION_MOVE) {
                            float eventX = event.getX();
                            float progressBarX = mProgressBar.getX();
                            float finalX;
                            if (eventX > mProgressBar.getWidth() + progressBarX) {
                                // Only fade out thumb if eventX is above the threshold
                                int threshold = getResources().getDimensionPixelSize(
                                        R.dimen.playback_panel_seekbar_threshold_end);
                                mAbortSeeking = eventX > mProgressBar.getWidth() + progressBarX
                                        + threshold;
                                finalX = mProgressBar.getWidth() + progressBarX;
                            } else if (eventX < progressBarX) {
                                // Only fade out thumb if eventX is below the threshold
                                int threshold = getResources().getDimensionPixelSize(
                                        R.dimen.playback_panel_seekbar_threshold_start);
                                mAbortSeeking = eventX < progressBarX - threshold;
                                finalX = progressBarX;
                            } else {
                                mAbortSeeking = false;
                                finalX = eventX;
                            }
                            if (mAbortSeeking) {
                               *//* AnimationUtils.fade(mProgressBarThumb,
                                        AnimationUtils.DURATION_PLAYBACKSEEKMODE_ABORT, false,
                                        true);*//*
                            } else {
                                *//*AnimationUtils.fade(mProgressBarThumb,
                                        AnimationUtils.DURATION_PLAYBACKSEEKMODE_ABORT, true,
                                        true);*//*
                            }
                            mLastThumbPosition = finalX;
                           // mProgressBarThumb.setX(finalX);
                            int seekTime = (int)
                                    ((finalX - mProgressBar.getX()) / mProgressBar.getWidth()
                                            * mPlaybackService.getCurrentTrack().getDuration());
                            mSeekTimeTextView.setText(ViewUtils.durationToString(seekTime));
                        }
                        return false;
                    }
                });
                return true;
            }
        });*/

        setupAnimations();
        setupAnimationsDown();
    }

    public long refreshNow() {
        if (mPlaybackService == null)
            return 500;
        try {
            long pos = mPosOverride < 0 ? mPlaybackService.getCurrentQuery().getMediaPlayerInterface().getPosition() : mPosOverride;
            if ((pos >= 0)) {
                mCurrentPosition.setText(ViewUtils.durationToString(pos));//
                       // .makeTimeString(this, pos / 1000));
                if (mPlaybackService.getCurrentTrack().getDuration() > 0) {
                    mSeekbar.setProgress((int) (1000 * pos / mPlaybackService.getCurrentTrack().getDuration()));
                } else {
                    mSeekbar.setProgress(1000);
                }

                if (mPlaybackService.isPlaying()) {
                    mCurrentPosition.setVisibility(View.VISIBLE);
                } else {
                    // blink the counter
                    int vis = mCurrentPosition.getVisibility();
                    mCurrentPosition
                            .setVisibility(vis == View.INVISIBLE ? View.VISIBLE
                                    : View.INVISIBLE);
                    return 500;
                }
            } else {
                mCurrentPosition.setText("--:--");
                mSeekbar.setProgress(1000);
            }
            // calculate the number of milliseconds until the next full second,
            // so
            // the counter can be updated at just the right time
            long remaining = 1000 - (pos % 1000);

            // approximate how often we would need to refresh the slider to
            // move it smoothly
            int width = mSeekbar.getWidth();
            if (width == 0)
                width = 320;
            long smoothrefreshtime = mPlaybackService.getCurrentTrack().getDuration() / width;

            if (smoothrefreshtime > remaining)
                return remaining;
            if (smoothrefreshtime < 20)
                return 20;
            return smoothrefreshtime;
        } catch (Exception ex) {
        }
        return 500;
    }

    public void update(PlaybackService playbackService) {
        if (mInitialized) {
            mPlaybackService = playbackService;
            showPlaybackPanel(state);
            onPlayPositionChanged(0, 0);
            updateTextViewCompleteTime();
            updateText();
            updateResolverIconImageView();
            refreshRepeatButtonState();
            refreshShuffleButtonState();
            refreshNow();
            if (mPlaybackService != null) {
                updatePlayPauseState(mPlaybackService.isPlaying());
            }
        }
    }

    /**
     * Refresh the information in this fragment to reflect that of the current repeatButton state.
     */
    protected void refreshRepeatButtonState() {
        if (mMainPanelContainer != null) {
            ImageButton imageButton = (ImageButton) mMainPanelContainer.findViewById(R.id.repeat_button);
            if (imageButton != null) {
                 if (mPlaybackService != null) {
                    if (mPlaybackService.getRepeatingMode() == PlaybackService.REPEAT_ALL) {
                        ImageUtils.loadDrawableIntoImageView(TomahawkApp.getContext(),
                                imageButton, R.drawable.repeat_all, R.color.tomahawk_red);
                    } else if (mPlaybackService.getRepeatingMode() == PlaybackService.REPEAT_ONE) {
                        ImageUtils.loadDrawableIntoImageView(TomahawkApp.getContext(),
                                imageButton, R.drawable.repeat_one, R.color.tomahawk_red);
                    } else {
                        ImageUtils.loadDrawableIntoImageView(TomahawkApp.getContext(),
                                imageButton, R.drawable.repeat_all);
                    }
                } else {
                    ImageUtils.clearTint(imageButton.getDrawable());
                }
            }
        }
    }

    /**
     * Refresh the information in this fragment to reflect that of the current shuffleButton state.
     */
    protected void refreshShuffleButtonState() {
        if (mMainPanelContainer != null) {
            ImageButton imageButton = (ImageButton) mMainPanelContainer
                    .findViewById(R.id.shuffle_button);
            if (imageButton != null) {
                  if (mPlaybackService != null && mPlaybackService.isShuffled()) {
                    ImageUtils.setTint(imageButton.getDrawable(), R.color.tomahawk_red);
                } else {
                    ImageUtils.clearTint(imageButton.getDrawable());
                }
            }
        }
    }

    public void updatePlayPauseState(boolean isPlaying) {
        if (mInitialized) {
            if (isPlaying) {
                mPauseButton.setVisibility(VISIBLE);
                mPlayButton.setVisibility(GONE);
                mPlayPauseBtn.setImageResource(R.drawable.ic_player_pause_light);
            } else {
                mPauseButton.setVisibility(GONE);
                mPlayButton.setVisibility(VISIBLE);
                mPlayPauseBtn.setImageResource(R.drawable.ic_player_play_light);
            }
        }
    }

    private void updateText() {
        if (mPlaybackService != null && mPlaybackService.getCurrentQuery() != null) {
            String name;
            if (mPlaybackService.getCurrentQuery().getArtist()==null)
                name = mPlaybackService.getCurrentQuery().getGenre().getPrettyName();
            else
            name =mPlaybackService.getCurrentQuery().getArtist().getPrettyName();
            mArtistTextView.setText(name);
            mTrackTextView.setText(mPlaybackService.getCurrentQuery().getPrettyName());
            ImageUtils.loadImageIntoImageView(TomahawkApp.getContext(), mSmallImageView,
                    mPlaybackService.getCurrentQuery().getAlbum().getImage(), Image.getSmallImageSize(), false);
        }
    }

    /**
     * Updates the position on seekbar and the related textviews
     */
    public void onPlayPositionChanged(long duration, int currentPosition) {
        if (duration != 0) {
           // mCircularProgressBar.setProgress((float) currentPosition / duration);
            //mProgressBar.setProgress((int) ((float) currentPosition / duration * 10000));
            mCurrentTimeTextView.setText(ViewUtils.durationToString(currentPosition));
        } else {
            //mProgressBar.setProgress(0);
            //mCircularProgressBar.setProgress(0);
            mCurrentTimeTextView.setText(ViewUtils.durationToString(0));
        }
    }

    private void updateResolverIconImageView() {
        if (mPlaybackService != null && mPlaybackService.getCurrentQuery() != null
                && mPlaybackService.getCurrentQuery().getPreferredTrackResult() != null) {
            Resolver resolver =
                    mPlaybackService.getCurrentQuery().getPreferredTrackResult().getResolvedBy();
            if (TomahawkApp.PLUGINNAME_USERCOLLECTION.equals(resolver.getId())) {
               // resolver.loadIconWhite(mResolverImageView);
            } else {// resolver.loadIcon(mResolverImageView, false);
            }
        }
    }

    /**
     * Updates the textview that shows the duration of the current track
     */
    private void updateTextViewCompleteTime() {
        if (mCompletionTimeTextView != null) {
            if (mPlaybackService != null
                    && mPlaybackService.getCurrentTrack() != null
                    && mPlaybackService.getCurrentTrack().getDuration() > 0) {
                mCompletionTimeTextView.setText(ViewUtils.durationToString(
                        mPlaybackService.getCurrentTrack().getDuration()));
                mDuration.setText(ViewUtils.durationToString(
                        mPlaybackService.getCurrentTrack().getDuration()));
            } else {
                mCompletionTimeTextView.setText(COMPLETION_STRING_DEFAULT);
                mDuration.setText(COMPLETION_STRING_DEFAULT);
            }
        }
    }

    private void setupAnimations() {
        ViewUtils.afterViewGlobalLayout(new ViewUtils.ViewRunnable(this) {
            @Override
            public void run() {
                mAnimators.clear();
                // get relevant dimension sizes first
                Resources resources = TomahawkApp.getContext().getResources();
                int panelHeight = resources.getDimensionPixelSize(
                        R.dimen.playback_panel_height);
                int padding = resources.getDimensionPixelSize(
                        R.dimen.padding_medium);
                int panelBottom = resources.getDimensionPixelSize(
                        R.dimen.playback_clear_space_bottom);
                int headerClearSpace = resources.getDimensionPixelSize(
                        R.dimen.header_clear_space_nonscrollable_playback);
                boolean isLandscape = resources.getBoolean(R.bool.is_landscape);



                // Setup mTextViewContainer animation
                Keyframe kfY0 = Keyframe.ofFloat(0f,
                        getHeight() - mTextViewContainer.getHeight() / 2 - panelHeight / 2);
                Keyframe kfY1 = Keyframe.ofFloat(0.5f, getHeight() - mTextViewContainer.getHeight() / 2 - panelHeight / 2);
                        //getHeight() + padding - panelBottom);
                Keyframe kfY2 = Keyframe.ofFloat(1f,getHeight() - mTextViewContainer.getHeight() / 2 - panelHeight / 2);
                       //headerClearSpace / 2 - mTextViewContainer.getHeight() / 2);
                PropertyValuesHolder pvhY =
                        PropertyValuesHolder.ofKeyframe("y", kfY0, kfY1, kfY2);
                Keyframe kfScale0 = Keyframe.ofFloat(0f, 1f);
                Keyframe kfScale1 = Keyframe.ofFloat(0.5f, isLandscape ? 1f : 1.5f);
                Keyframe kfScale2 = Keyframe.ofFloat(1f, isLandscape ? 1.25f : 1.5f);
                PropertyValuesHolder pvhScaleY =
                        PropertyValuesHolder.ofKeyframe("scaleY", kfScale0, kfScale1, kfScale2);
                PropertyValuesHolder pvhScaleX =
                        PropertyValuesHolder.ofKeyframe("scaleX", kfScale0, kfScale1, kfScale2);
                ValueAnimator animator = ObjectAnimator
                        .ofPropertyValuesHolder(mTextViewContainer, pvhY).setDuration(20000);
                animator.setInterpolator(new LinearInterpolator());
                animator.setCurrentPlayTime(mLastPlayTime);
                mAnimators.add(animator);

                // Setup mPanelContainer animation
                kfY0 = Keyframe.ofFloat(0f, getHeight() - mMainPanelContainer.getHeight());//mPanelContainer.getHeight());
                kfY1 = Keyframe.ofFloat(0.5f, getHeight() - mMainPanelContainer.getHeight());//mPanelContainer.getHeight());
                kfY2 = Keyframe.ofFloat(1f, headerClearSpace - mMainPanelContainer.getHeight());//mPanelContainer.getHeight());
                pvhY = PropertyValuesHolder.ofKeyframe("y", kfY0, kfY1, kfY2);
                animator = ObjectAnimator
                        .ofPropertyValuesHolder(mMainPanelContainer, pvhY).setDuration(20000);//mPanelContainer
                animator.setInterpolator(new LinearInterpolator());
                animator.setCurrentPlayTime(mLastPlayTime);
                mAnimators.add(animator);

                // Setup mTextViewContainer backgroundColor alpha animation
                Keyframe kfColor1 = Keyframe.ofInt(0f, 0x0);
                Keyframe kfColor2 = Keyframe.ofInt(0.5f, 0x0);
                Keyframe kfColor3 = Keyframe.ofInt(1f, 0xFF);
                PropertyValuesHolder pvhColor = PropertyValuesHolder
                        .ofKeyframe("color", kfColor1, kfColor2, kfColor3);
                animator = ValueAnimator.ofPropertyValuesHolder(pvhColor).setDuration(20000);
                animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                    @Override
                    public void onAnimationUpdate(ValueAnimator animation) {
                       mTextViewContainer.findViewById(R.id.textview_container_inner)
                               .getBackground().setAlpha((Integer) animation.getAnimatedValue());
                        mMainPanelContainer.setVisibility(INVISIBLE);
                        mOptionsContainer.setVisibility(INVISIBLE);
                        Log.d("Anim","UP_"+(Integer) animation.getAnimatedValue());
                    }
                });
                animator.setInterpolator(new LinearInterpolator());
                animator.setCurrentPlayTime(mLastPlayTime);
                mAnimators.add(animator);

                // Setup mPanelContainer background fade animation
                Keyframe kfBgAlpha0 = Keyframe.ofInt(0f, 0);
                Keyframe kfBgAlpha1 = Keyframe.ofInt(0.5f, 0);
                Keyframe kfBgAlpha2 = Keyframe.ofInt(1f, 255);
                PropertyValuesHolder pvhBgAlpha = PropertyValuesHolder
                        .ofKeyframe("alpha", kfBgAlpha0, kfBgAlpha1, kfBgAlpha2);
                animator = ObjectAnimator.ofPropertyValuesHolder(mMainPanelContainer.getBackground(),
                        pvhBgAlpha).setDuration(20000);//mPanelContainer
                animator.setInterpolator(new LinearInterpolator());
                animator.setCurrentPlayTime(mLastPlayTime);
                mAnimators.add(animator);
            }
        });
    }

    private void setupAnimationsDown() {
        ViewUtils.afterViewGlobalLayout(new ViewUtils.ViewRunnable(this) {
            @Override
            public void run() {
                mAnimators.clear();
                // get relevant dimension sizes first
                Resources resources = TomahawkApp.getContext().getResources();
                int panelHeight = resources.getDimensionPixelSize(
                        R.dimen.playback_panel_height);
                int padding = resources.getDimensionPixelSize(
                        R.dimen.padding_medium);
                int panelBottom = resources.getDimensionPixelSize(
                        R.dimen.playback_clear_space_bottom);
                int headerClearSpace = resources.getDimensionPixelSize(
                        R.dimen.header_clear_space_nonscrollable_playback);
                boolean isLandscape = resources.getBoolean(R.bool.is_landscape);



                // Setup mTextViewContainer animation
                Keyframe kfY0 = Keyframe.ofFloat(0f,
                        getHeight() - mOptionsContainer.getHeight() / 2 - panelHeight / 2);
                Keyframe kfY1 = Keyframe.ofFloat(0.5f,
                        getHeight()  - panelBottom);
                Keyframe kfY2 = Keyframe.ofFloat(1f,
                        headerClearSpace / 2 - mOptionsContainer.getHeight() / 2);
                PropertyValuesHolder pvhY =
                        PropertyValuesHolder.ofKeyframe("y", kfY0, kfY1, kfY2);
                Keyframe kfScale0 = Keyframe.ofFloat(0f, 1f);
                Keyframe kfScale1 = Keyframe.ofFloat(0.5f, isLandscape ? 1f : 1f);
                Keyframe kfScale2 = Keyframe.ofFloat(1f, isLandscape ? 1.25f : 1f);
                PropertyValuesHolder pvhScaleY =
                        PropertyValuesHolder.ofKeyframe("scaleY", kfScale0, kfScale1, kfScale2);
                PropertyValuesHolder pvhScaleX =
                        PropertyValuesHolder.ofKeyframe("scaleX", kfScale0, kfScale1, kfScale2);
                ValueAnimator animator = ObjectAnimator
                        .ofPropertyValuesHolder(mOptionsContainer, pvhY,
                                pvhScaleX, pvhScaleY).setDuration(20000);
                animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                    @Override
                    public void onAnimationUpdate(ValueAnimator animation) {
                      //  Log.d("Anim","y_scaley_scalex"+animation.getAnimatedFraction());
                    }
                });
                animator.setInterpolator(new LinearInterpolator());
                animator.setCurrentPlayTime(mLastPlayTime);
                mAnimators.add(animator);

                // Setup mPanelContainer animation
                kfY0 = Keyframe.ofFloat(0f, getHeight() - mMainPanelContainer.getHeight());
                kfY1 = Keyframe.ofFloat(0.5f, getHeight() - mMainPanelContainer.getHeight());
                kfY2 = Keyframe.ofFloat(1f, headerClearSpace - mMainPanelContainer.getHeight());
                pvhY = PropertyValuesHolder.ofKeyframe("y", kfY0, kfY1, kfY2);
                animator = ObjectAnimator
                        .ofPropertyValuesHolder(mMainPanelContainer, pvhY).setDuration(20000);//mPanelContainer
                animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                    @Override
                    public void onAnimationUpdate(ValueAnimator animation) {
                     //   Log.d("Anim","y_"+animation.getAnimatedFraction());
                    }
                });
                animator.setInterpolator(new LinearInterpolator());
                animator.setCurrentPlayTime(mLastPlayTime);
                mAnimators.add(animator);

                // Setup mTextViewContainer backgroundColor alpha animation
                Keyframe kfColor1 = Keyframe.ofInt(0f, 0x0);
                Keyframe kfColor2 = Keyframe.ofInt(0.5f, 0x0);
                Keyframe kfColor3 = Keyframe.ofInt(1f, 0xFF);
                PropertyValuesHolder pvhColor = PropertyValuesHolder
                        .ofKeyframe("color", kfColor1, kfColor2, kfColor3);
                animator = ValueAnimator.ofPropertyValuesHolder(pvhColor).setDuration(20000);
                animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                    @Override
                    public void onAnimationUpdate(ValueAnimator animation) {
                        mOptionsContainer.findViewById(R.id.playerFooter)//.setVisibility(INVISIBLE);
                                .getBackground().setAlpha((Integer) animation.getAnimatedValue());
                        mMainPanelContainer.setVisibility(INVISIBLE);
                        mOptionsContainer.setVisibility(INVISIBLE);
                    //   Log.d("Anim", "color_" + (Integer) animation.getAnimatedValue());
                    }
                });
                animator.setInterpolator(new LinearInterpolator());
                animator.setCurrentPlayTime(mLastPlayTime);
                mAnimators.add(animator);

                // Setup mPanelContainer background fade animation
                Keyframe kfBgAlpha0 = Keyframe.ofInt(0f, 0);
                Keyframe kfBgAlpha1 = Keyframe.ofInt(0.5f, 0);
                Keyframe kfBgAlpha2 = Keyframe.ofInt(1f, 255);
                PropertyValuesHolder pvhBgAlpha = PropertyValuesHolder
                        .ofKeyframe("alpha", kfBgAlpha0, kfBgAlpha1, kfBgAlpha2);
                animator = ObjectAnimator.ofPropertyValuesHolder(mMainPanelContainer.getBackground(),
                        pvhBgAlpha).setDuration(20000);
                animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
                    @Override
                    public void onAnimationUpdate(ValueAnimator animation) {
                      //  Log.d("Anim","alpha_"+animation.getAnimatedFraction());
                    }
                });
                animator.setInterpolator(new LinearInterpolator());
                animator.setCurrentPlayTime(mLastPlayTime);
                mAnimators.add(animator);
            }
        });
    }

    public void animate(int position) {
        mLastPlayTime = position;
        for (ValueAnimator animator : mAnimators) {
            if (animator != null && position != animator.getCurrentPlayTime()) {
                animator.setCurrentPlayTime(position);
            }
        }
    }


    public void showButtons() {
        mArtistNameButton.setClickable(true);
        TransitionDrawable drawable = (TransitionDrawable) mArtistNameButton.getBackground();
        drawable.startTransition(AnimationUtils.DURATION_CONTEXTMENU);
    }

    public void hideButtons() {
        mArtistNameButton.setClickable(false);
        TransitionDrawable drawable = (TransitionDrawable) mArtistNameButton.getBackground();
        drawable.reverseTransition(AnimationUtils.DURATION_CONTEXTMENU);
    }
}
