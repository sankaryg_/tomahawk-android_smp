package org.tomahawk.libtomahawk.infosystem;


public interface CalibrationEventListener
{

    public abstract void onCalibrationSet(int i, int j);
}
