package org.tomahawk.libtomahawk.infosystem;

import android.view.View;

/**
 * Created by YGS on 11/16/2015.
 */
public interface SwipeInterface {
    public void top2bottom(View v);
}
