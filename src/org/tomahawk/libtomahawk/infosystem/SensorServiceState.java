package org.tomahawk.libtomahawk.infosystem;


import org.tomahawk.tomahawk_android.sensor.SensorMode;

public interface SensorServiceState
{


    public abstract void disableSensorService();

    public abstract void enableSensorService();

    public abstract boolean isServiceRunning();

    public abstract void loadPreset(SensorMode sensormode);
}
