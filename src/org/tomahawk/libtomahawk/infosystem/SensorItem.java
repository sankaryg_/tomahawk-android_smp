package org.tomahawk.libtomahawk.infosystem;

import java.io.Serializable;

/**
 * Created by YGS on 9/24/2015.
 */
public class SensorItem implements Serializable {

    private String headerItem;
    private String spinnerItem;

    public SensorItem(String headerItem,String spinnerItem){
        this.headerItem = headerItem;
        this.spinnerItem = spinnerItem;
    }
    public String getHeaderItem() {
        return headerItem;
    }

    public String getSpinnerItem() {
        return spinnerItem;
    }

    public void setHeaderItem(String headerItem) {
        this.headerItem = headerItem;
    }

    public void setSpinnerItem(String spinnerItem) {
        this.spinnerItem = spinnerItem;
    }

}
