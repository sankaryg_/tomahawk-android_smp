package org.tomahawk.libtomahawk.utils;

import android.annotation.SuppressLint;
import android.app.Notification;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.Drawable;
import android.text.TextUtils;
import android.view.View;
import android.widget.ImageView;
import android.widget.RemoteViews;
import android.widget.TextView;

import com.squareup.picasso.Picasso;
import com.squareup.picasso.RequestCreator;
import com.squareup.picasso.Target;

import org.tomahawk.libtomahawk.collection.Image;
import org.tomahawk.libtomahawk.infosystem.User;
import org.tomahawk.tomahawk_android.R;
import org.tomahawk.tomahawk_android.TomahawkApp;
import org.tomahawk.tomahawk_android.utils.BlurTransformation;
import org.tomahawk.tomahawk_android.utils.CircularImageTransformation;
import org.tomahawk.tomahawk_android.utils.ColorTintTransformation;

import wseemann.media.FFmpegMediaMetadataRetriever;

public class ImageUtils {

    public static final String TAG = ImageUtils.class.getSimpleName();

    public static Bitmap bitmap;

    static Bitmap b=null;


    /**
     * Load a {@link android.graphics.Bitmap} asynchronously
     *
     * @param context   the context needed for fetching resources
     * @param imageView the {@link ImageView}, which will be used to show the {@link
     *                  android.graphics.Bitmap}
     * @param image     the path to load the image from
     * @param width     the width in density independent pixels to scale the image down to
     */
    public static void loadImageIntoImageView(Context context, ImageView imageView, Image image,
            int width, boolean isArtistImage) {
        loadImageIntoImageView(context, imageView, image, width, true, isArtistImage);
    }

    /**
     * Load a {@link android.graphics.Bitmap} asynchronously
     *
     * @param context   the context needed for fetching resources
     * @param imageView the {@link ImageView}, which will be used to show the {@link
     *                  android.graphics.Bitmap}
     * @param image     the path to load the image from
     * @param width     the width in density independent pixels to scale the image down to
     */
    public static void loadBlurredImageIntoImageView(Context context, ImageView imageView,
            Image image, int width, int placeHolderResId) {
        if (image != null && !TextUtils.isEmpty(image.getImagePath())) {
            String imagePath = buildImagePath(image, width);
            if (imagePath.contains("byte://")){

                Picasso.with(context).load(ImageUtils.preparePathForPicasso(imagePath))
                        .placeholder(placeHolderResId)
                        .transform(new BlurTransformation())
                        .into(imageView);
            }else {
                RequestCreator creator = Picasso.with(context).load(
                        ImageUtils.preparePathForPicasso(imagePath)).resize(width, width);
                if (placeHolderResId > 0) {
                    creator.placeholder(placeHolderResId);
                    creator.error(placeHolderResId);
                }
                creator.transform(new BlurTransformation());
                creator.into(imageView);
            }
            /*try {
                bitmap = creator.get();
            }catch (IOException e){
                bitmap = null;
            }*/
        } else {
            RequestCreator creator = Picasso.with(context).load(placeHolderResId)
                    .placeholder(placeHolderResId)
                    .error(placeHolderResId);
            creator.into(imageView);
           /* try {
                bitmap = creator.get();
            }catch (IOException e){
                bitmap = null;
            }*/

        }
    }

   /* public static int getColor(){
        int color;
        if (bitmap!=null){
            color = ColorAnalyser.analyse(bitmap);
        }else {
            color = Color.GRAY;
        }
        return color;
    }*/
    /**
     * Load a {@link android.graphics.Bitmap} asynchronously
     *
     * @param context   the context needed for fetching resources
     * @param imageView the {@link ImageView}, which will be used to show the {@link
     *                  android.graphics.Bitmap}
     * @param image     the path to load the image from
     * @param width     the width in pixels to scale the image down to
     */
    public static void loadImageIntoImageView(Context context, final ImageView imageView, Image image,
            int width, boolean fit, boolean isArtistImage) {
        int placeHolder = isArtistImage ? R.drawable.artist_placeholder_grid
                : R.drawable.ic_default_playback;//album_placeholder_grid
        if (image != null && !TextUtils.isEmpty(image.getImagePath())) {
            String imagePath = buildImagePath(image, width);
            if (imagePath.contains("byte://")){
                imagePath = imagePath.replace("byte:///","");
                try {
                    FFmpegMediaMetadataRetriever mmdr = new FFmpegMediaMetadataRetriever();
                    mmdr.setDataSource(imagePath);
                    byte[] byte1 = mmdr.getEmbeddedPicture();
                    if (byte1!=null){
                        b = BitmapFactory.decodeByteArray(byte1, 0, byte1.length);
                    }
                    mmdr.release();
                }catch (Exception e){

                }
                imageView.setImageBitmap(b);
               /* new Picasso.Builder(context).build().load(imagePath).into(new Target() {
                    @Override
                    public void onBitmapLoaded(Bitmap bitmap, Picasso.LoadedFrom from) {
                        if (b!=null){
                        imageView.setImageBitmap(b);
                        }
                    }

                    @Override
                    public void onBitmapFailed(Drawable errorDrawable) {

                    }

                    @Override
                    public void onPrepareLoad(Drawable placeHolderDrawable) {

                    }
                });*/
               /* if (fit){
                    Picasso.with(context).load(ImageUtils.preparePathForPicasso(imagePath))
                            .placeholder(placeHolder)
                            .resize(width,width)
                            .into(imageView);
                }else
                Picasso.with(context).load(imagePath)
                        .placeholder(placeHolder)
                        .into(imageView);*/
            }else {
                RequestCreator creator = Picasso.with(context).load(
                        ImageUtils.preparePathForPicasso(imagePath))
                        .placeholder(placeHolder)
                        .error(placeHolder);
                if (fit) {
                    creator.resize(width, width);
                }
                creator.into(imageView);
            }
        } else {
            RequestCreator creator = Picasso.with(context).load(placeHolder)
                    .placeholder(placeHolder)
                    .error(placeHolder);
            if (fit) {
                creator.resize(width, width);
            }
            creator.into(imageView);
        }
    }

    /**
     * Load a circle-shaped {@link android.graphics.Bitmap} asynchronously
     *
     * @param context   the context needed for fetching resources
     * @param imageView the {@link ImageView}, which will be used to show the {@link
     *                  android.graphics.Bitmap}
     * @param user      the user of which to load the data into the views
     * @param width     the width in pixels to scale the image down to
     * @param textView  the textview which is being used to display the first letter of the user's
     *                  name in the placeholder image
     */
    public static void loadUserImageIntoImageView(Context context, ImageView imageView,
            User user, int width, TextView textView) {
        int placeHolder = R.drawable.circle_black;
        if (user.getImage() != null && !TextUtils.isEmpty(user.getImage().getImagePath())) {
            textView.setVisibility(View.GONE);
            String imagePath = buildImagePath(user.getImage(), width);
            Picasso.with(context).load(ImageUtils.preparePathForPicasso(imagePath))
                    .transform(new CircularImageTransformation())
                    .placeholder(placeHolder)
                    .error(placeHolder)
                    .fit()
                    .into(imageView);
        } else {
            textView.setVisibility(View.VISIBLE);
            textView.setText(user.getName().substring(0, 1).toUpperCase());
            Picasso.with(context).load(placeHolder)
                    .placeholder(placeHolder)
                    .error(placeHolder)
                    .fit()
                    .into(imageView);
        }
    }

    /**
     * Load a {@link android.graphics.Bitmap} asynchronously
     *
     * @param context   the context needed for fetching resources
     * @param imageView the {@link ImageView}, which will be used to show the {@link
     *                  android.graphics.Bitmap}
     * @param path      the path to the image
     */
    public static void loadDrawableIntoImageView(Context context, ImageView imageView,
            String path) {
        loadDrawableIntoImageView(context, imageView, path, 0);
    }

    /**
     * Load a {@link android.graphics.Bitmap} asynchronously
     *
     * @param context    the context needed for fetching resources
     * @param imageView  the {@link ImageView}, which will be used to show the {@link
     *                   android.graphics.Bitmap}
     * @param path       the path to the image
     * @param colorResId the color with which to tint the imageview drawable
     */
    public static void loadDrawableIntoImageView(Context context, ImageView imageView,
            String path, int colorResId) {
        RequestCreator creator = Picasso.with(context).load(path);
        if (colorResId > 0) {
            creator.transform(new ColorTintTransformation(colorResId));
        }
        creator.error(R.drawable.ic_action_error).into(imageView);
    }

    /**
     * Load a {@link Drawable} asynchronously (convenience method)
     *
     * @param context       the context needed for fetching resources
     * @param imageView     the {@link ImageView}, which will be used to show the
     *                      {@link Drawable}
     * @param drawableResId the resource id of the drawable to load into the imageview
     */
    public static void loadDrawableIntoImageView(Context context, ImageView imageView,
            int drawableResId) {
        loadDrawableIntoImageView(context, imageView, drawableResId, 0);
    }

    /**
     * Load a {@link Drawable} asynchronously
     *
     * @param context       the context needed for fetching resources
     * @param imageView     the {@link ImageView}, which will be used to show the
     *                      {@link Drawable}
     * @param drawableResId the resource id of the drawable to load into the imageview
     * @param colorResId    the color with which to tint the imageview drawable
     */
    public static void loadDrawableIntoImageView(Context context, ImageView imageView,
            int drawableResId, int colorResId) {
        RequestCreator creator = Picasso.with(context).load(drawableResId);
        if (colorResId > 0) {
            creator.transform(new ColorTintTransformation(colorResId));
        }
        creator.error(R.drawable.ic_action_error).into(imageView);
    }

    /**
     * Load a {@link android.graphics.Bitmap} asynchronously
     *
     * @param context the context needed for fetching resources
     * @param image   the path to load the image from
     * @param target  the Target which the loaded image will be pushed to
     * @param width   the width in pixels to scale the image down to
     */
    public static void loadImageIntoBitmap(Context context, Image image, Target target, int width,
            boolean isArtistImage) {
        int placeHolder = isArtistImage ? R.drawable.artist_placeholder_grid
                : R.drawable.album_placeholder_grid;
        if (image != null && !TextUtils.isEmpty(image.getImagePath())) {
            String imagePath = buildImagePath(image, width);
            if (imagePath.startsWith("byte://")){

                Picasso.with(context).load(ImageUtils.preparePathForPicasso(imagePath))
                        .placeholder(placeHolder)
                        .into(target);
            }else {
                Picasso.with(context).load(ImageUtils.preparePathForPicasso(imagePath))
                        .resize(width, width)
                        .into(target);
            }

        } else {
            Picasso.with(context).load(placeHolder)
                    .resize(width, width)
                    .into(target);
        }
    }

    /**
     * Load a {@link android.graphics.Bitmap} asynchronously
     *
     * @param context the context needed for fetching resources
     * @param image   the path to load the image from
     * @param width   the width in pixels to scale the image down to
     */
    public static void loadImageIntoNotification(Context context, Image image,
            RemoteViews remoteViews, int viewId, int notificationId, Notification notification,
            int width, boolean isArtistImage) {
        int placeHolder = isArtistImage ? R.drawable.artist_placeholder_grid
                : R.drawable.album_placeholder_grid;
        if (image != null && !TextUtils.isEmpty(image.getImagePath())) {
            String imagePath = buildImagePath(image, width);
            Picasso.with(context).load(ImageUtils.preparePathForPicasso(imagePath))
                    .resize(width, width)
                    .into(remoteViews, viewId, notificationId, notification);
        } else {
            Picasso.with(context).load(placeHolder)
                    .resize(width, width)
                    .into(remoteViews, viewId, notificationId, notification);
        }
    }

    public static String preparePathForPicasso(String path) {
        if (TextUtils.isEmpty(path) || path.contains("https://") || path.contains("http://") || path.contains("byte://")) {
            if (path.startsWith("byte:")){
                path = path.replace("byte:","file:");
            }
            return path;
        }
        return path.startsWith("file:") ? path : "file:" + path;
    }

    private static String buildImagePath(Image image, int width) {
        if (image.isHatchetImage()) {
            int imageSize = Math.min(image.getHeight(), image.getWidth());
            int actualWidth;
            if (NetworkUtils.isWifiAvailable()) {
                actualWidth = Math.min(imageSize, width);
            } else {
                actualWidth = Math.min(imageSize, width * 2 / 3);
            }
            return image.getImagePath() + "?width=" + actualWidth + "&height=" + actualWidth;
        }
        return image.getImagePath();
    }

    @SuppressLint("NewApi")
    public static void setTint(final Drawable drawable, final int colorResId) {
        int color = TomahawkApp.getContext().getResources().getColor(colorResId);
        drawable.setColorFilter(color, android.graphics.PorterDuff.Mode.SRC_ATOP);
    }

    @SuppressLint("NewApi")
    public static void clearTint(final Drawable drawable) {
        drawable.clearColorFilter();
    }

    public static Bitmap grayScaleImage(Bitmap src) {
        // constant factors
        final double GS_RED = 0.299;
        final double GS_GREEN = 0.587;
        final double GS_BLUE = 0.114;

        // create output bitmap
        Bitmap bmOut = Bitmap.createBitmap(src.getWidth(), src.getHeight(), src.getConfig());
        // pixel information
        int A, R, G, B;
        int pixel;

        // get image size
        int width = src.getWidth();
        int height = src.getHeight();

        // scan through every single pixel
        for(int x = 0; x < width; ++x) {
            for(int y = 0; y < height; ++y) {
                // get one pixel color
                pixel = src.getPixel(x, y);
                // retrieve color of all channels
                A = Color.alpha(pixel);
                R = Color.red(pixel);
                G = Color.green(pixel);
                B = Color.blue(pixel);
                // take conversion up to one single value
                R = G = B = (int)(GS_RED * R + GS_GREEN * G + GS_BLUE * B);
                // set new pixel color to output bitmap
                bmOut.setPixel(x, y, Color.argb(A, R, G, B));
            }
        }

        // return final image
        return bmOut;
    }


    /*public static void sensorTypeChosen(int type,ImageView imageView){
        waveOverSensor.setImageResource(R.drawable.wave_over);
        pocketSensor.setImageResource(R.drawable.pocket);
        hammerSensor.setImageResource(R.drawable.hammer);
        customSensor.setImageResource(R.drawable.custom_acc_pro);
        customSensorProx.setImageResource(R.drawable.custom_prox_pro);
        if (!PrefsHelper.isAccelerometerAvailable(prefs))
        {
            isAccelerometer = false;
        }else
            isAccelerometer = true;

        if(!PrefsHelper.isProxSensorAvailable(prefs)){
            isProximity = false;
        }else
            isProximity = true;
        if(type == 1){
            if(isProximity){
                waveOverSensor.setImageBitmap(grayScaleImage(BitmapFactory.decodeResource(context.getResources(), R.drawable.wave_over)));
                smp.getSensorDataHelper().setIntentBroadcastEnabled(true);
            }
        }else if(type==2){
            pocketSensor.setImageBitmap(grayScaleImage(BitmapFactory.decodeResource(context.getResources()	, R.drawable.pocket)));
            smp.getSensorDataHelper().setIntentBroadcastEnabled(true);
        }else if(type==3){
            hammerSensor.setImageBitmap(grayScaleImage(BitmapFactory.decodeResource(context.getResources()	, R.drawable.hammer)));
            smp.getSensorDataHelper().setIntentBroadcastEnabled(true);
        }else if(type==4){
            if(isAccelerometer){
                customSensor.setImageBitmap(grayScaleImage(BitmapFactory.decodeResource(context.getResources()	, R.drawable.custom_acc)));
                smp.getSensorDataHelper().setIntentBroadcastEnabled(true);
            }
        }else if(type==5){
            if(isProximity){
                customSensorProx.setImageBitmap(grayScaleImage(BitmapFactory.decodeResource(context.getResources()	, R.drawable.custom_prox)));
                smp.getSensorDataHelper().setIntentBroadcastEnabled(true);
            }
        }
    }*/
}
