package org.tomahawk.libtomahawk.utils;

import android.content.ContentResolver;
import android.content.Context;
import android.content.res.Resources;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.util.DisplayMetrics;
import android.view.View;
import android.view.ViewStub;
import android.view.ViewTreeObserver;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.Transformation;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;

import org.tomahawk.tomahawk_android.TomahawkApp;

public class ViewUtils {

    public static final String TAG = ViewUtils.class.getSimpleName();

    public abstract static class ViewRunnable implements Runnable {

        private final View mView;

        public ViewRunnable(View view) {
            this.mView = view;
        }

        public View getLayedOutView() {
            return mView;
        }
    }

    public static View ensureInflation(View view, int stubResId, int inflatedId) {
        View stub = view.findViewById(stubResId);
        if (stub instanceof ViewStub) {
            return ((ViewStub) stub).inflate();
        } else {
            return view.findViewById(inflatedId);
        }
    }

    /**
     * This method converts dp unit to equivalent device specific value in pixels.
     *
     * @param dp A value in dp(Device independent pixels) unit. Which we need to convert into
     *           pixels
     * @return A float value to represent Pixels equivalent to dp according to device
     */
    public static int convertDpToPixel(int dp) {
        Resources resources = TomahawkApp.getContext().getResources();
        DisplayMetrics metrics = resources.getDisplayMetrics();
        return (int) (dp * (metrics.densityDpi / 160f));
    }

    /**
     * Converts a track duration int into the proper String format
     *
     * @param duration the track's duration
     * @return the formated string
     */
    public static String durationToString(long duration) {
        return String.format("%02d", (duration / 60000)) + ":" + String
                .format("%02.0f", (double) (duration / 1000) % 60);
    }

    public static int getCardId(Context context) {
        ContentResolver res = context.getContentResolver();
        Cursor c = res.query(Uri.parse("content://media/external/fs_id"), null, null, null, null);
        int id = -1;
        if (c != null) {
            c.moveToFirst();
            id = c.getInt(0);
            c.close();
        }
        return id;
    }
    /**
     * Convert millisseconds to hh:mm:ss format.
     *
     * @param milliseconds The input time in milliseconds to format.
     * @return The formatted time string.
     */
    public static String convertMillisToMinsSecs(long milliseconds) {

        long days = milliseconds / (1000 * 60 * 60 * 24);
        //int secondsValue = (int) (milliseconds / 1000) % 60;
        //int minutesValue = (int) ((milliseconds / (1000*60)) % 60);
        int hoursValue  = (int) ((milliseconds / (1000*60*60))-(days*24));// % 24);
        int minutesValue = (int)((milliseconds / (1000 * 60)) - (days * 24 * 60)
                - (hoursValue * 60));
        int secondsValue = (int)((milliseconds / (1000)) - (days * 24 * 60 * 60)
                - (hoursValue * 60 * 60) - (minutesValue * 60));

        String seconds = "";
        String minutes = "";
        String hours = "";

        if (secondsValue < 10) {
            seconds = "0" + secondsValue;
        } else {
            seconds = "" + secondsValue;
        }

        minutes = "" + minutesValue;
        hours = "" + hoursValue;

        String output = "";
        if (hoursValue!=0) {
            minutes = "0" + minutesValue;
            hours = "" + hoursValue;
            output = hours + ":" + minutes + ":" + seconds;
        } else {
            minutes = "" + minutesValue;
            hours = "" + hoursValue;
            output = minutes + ":" + seconds;
        }

        return output;
    }

    public static String getVoiceData(String str){
        String returnVal = "-1";
        int offsetVal = 0;
            offsetVal = Integer.parseInt(str);

        switch (offsetVal)
        {
            case 900:
                returnVal = "15 Min";
                break;
            case 1800:
                returnVal = "30 Min";
                break;
            case 3600:
                returnVal = "60 Min";
                break;

        }
        return returnVal;
    }
    public static String getAlarmSelected(String str){
        String returnVal = null;
        int offsetVal = 0;
        if(str.contains(" "))
            offsetVal = Integer.parseInt(str.split(" ")[1]);

        switch (offsetVal)
        {
            case 25:
                returnVal = "lessthan 25";
                break;
            case 40:
                returnVal = "lessthan 40";
                break;
            case 50:
                returnVal = "lessthan 50";
                break;
            case 60:
                returnVal = "lessthan 60";
                break;
        }
        return returnVal;
    }

    public static void afterViewGlobalLayout(final ViewRunnable viewRunnable) {
        if (viewRunnable.getLayedOutView().getHeight() > 0
                && viewRunnable.getLayedOutView().getWidth() > 0) {
            viewRunnable.run();
        } else {
            viewRunnable.getLayedOutView().getViewTreeObserver().addOnGlobalLayoutListener(
                    new ViewTreeObserver.OnGlobalLayoutListener() {
                        @Override
                        public void onGlobalLayout() {
                            viewRunnable.run();

                            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN) {
                                viewRunnable.getLayedOutView().getViewTreeObserver()
                                        .removeOnGlobalLayoutListener(this);
                            } else {
                                //noinspection deprecation
                                viewRunnable.getLayedOutView().getViewTreeObserver()
                                        .removeGlobalOnLayoutListener(this);
                            }
                        }
                    });
        }
    }

    public static void showSoftKeyboard(final EditText editText) {
        editText.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, final boolean hasFocus) {
                editText.post(new Runnable() {
                    @Override
                    public void run() {
                        InputMethodManager imm = (InputMethodManager) TomahawkApp.getContext()
                                .getSystemService(Context.INPUT_METHOD_SERVICE);
                        imm.showSoftInput(editText, InputMethodManager.SHOW_IMPLICIT);
                    }
                });
                editText.setOnFocusChangeListener(null);
            }
        });
        editText.requestFocus();
    }

    public static String alarm(String str){
        String returnVal = "0 minutes";
        int offsetVal = Integer.parseInt(str);

        switch (offsetVal)
        {
            case 300:
                returnVal = "5 minutes";
                break;
            case 600:
                returnVal = "10 minutes";
                break;
            case 900:
                returnVal = "15 minutes";
                break;
            case 1800:
                returnVal = "30 minutes";
                break;
            case 3600:
                returnVal = "1 hour";
                break;
            case 7200:
                returnVal = "2 hours";
                break;
            case 10800:
                returnVal = "3 hours";
                break;
            case 14400:
                returnVal = "4 hours";
                break;
            case 18000:
                returnVal = "5 hours";
                break;
            case 21600:
                returnVal = "6 hours";
                break;
            case 25200:
                returnVal = "7 hours";
                break;
            case 28800:
                returnVal = "8 hours";
                break;
            case 32400:
                returnVal = "9 hours";
                break;
            case 36000:
                returnVal = "10 hours";
                break;
            case 39600:
                returnVal = "11 hours";
                break;
            case 43200:
                returnVal = "0.5 days";
                break;
            case 64800:
                returnVal = "18 hours";
                break;
            case 86400:
                returnVal = "1 day";
                break;
            case 172800:
                returnVal = "2 days";
                break;
            case 259200:
                returnVal = "3 days";
                break;
            case 345600:
                returnVal = "4 days";
                break;
            case 604800:
                returnVal = "1 week";
                break;
            case 1209600:
                returnVal = "2 weeks";
                break;
        }
        return returnVal;
    }

    public static String convertAlarm(String str){
        String returnVal = "0";
        //int offsetVal = Integer.parseInt(str);

        if(str.equals("5 minutes"))
            returnVal = "300";

        else if(str.equals("10 minutes"))
            returnVal = "600";

        else if(str.equals("15 minutes"))
            returnVal = "900";

        else if(str.equals("30 minutes"))
            returnVal = "1800";
        else if(str.equals("1 hour"))
            returnVal = "3600";

        else if(str.equals("2 hours"))
            returnVal = "7200";

        else if(str.equals("3 hours"))
            returnVal = "10800";

        else if(str.equals("4 hours"))
            returnVal = "14400";

        else if(str.equals("5 hours"))
            returnVal = "18000";

        else if(str.equals("6 hours"))
            returnVal = "21600";

        else if(str.equals("7 hours"))
            returnVal = "25200";

        else if(str.equals("8 hours"))
            returnVal = "28800";

        else if(str.equals("9 hours"))
            returnVal = "32400";

        else if(str.equals("10 hours"))
            returnVal = "36000";

        else if(str.equals("11 hours"))
            returnVal = "39600";

        else if(str.equals("0.5 days"))
            returnVal = "43200";

        else if(str.equals("18 hours"))
            returnVal = "64800";

        else if(str.equals("1 day"))
            returnVal = "86400";

        else if(str.equals("2 days"))
            returnVal = "172800";

        else if(str.equals("3 days"))
            returnVal = "259200";

        else if(str.equals("4 days"))
            returnVal = "345600";

        else if(str.equals("3 days"))
            returnVal = "259200";

        else if(str.equals("1 week"))
            returnVal = "604800";

        else if(str.equals("2 weeks"))
            returnVal = "1209600";

        return returnVal;
    }

    public static void expand(final View v) {
        v.measure(WindowManager.LayoutParams.MATCH_PARENT, WindowManager.LayoutParams.WRAP_CONTENT);
        final int targetHeight = v.getMeasuredHeight();

        v.getLayoutParams().height = 0;
        v.setVisibility(View.VISIBLE);
        Animation a = new Animation()
        {
            @Override
            protected void applyTransformation(float interpolatedTime, Transformation t) {
                v.getLayoutParams().height = interpolatedTime == 1
                        ? WindowManager.LayoutParams.WRAP_CONTENT
                        : (int)(targetHeight * interpolatedTime);
                v.requestLayout();
            }

            @Override
            public boolean willChangeBounds() {
                return true;
            }
        };

        // 1dp/ms
        a.setDuration((int)(targetHeight / v.getContext().getResources().getDisplayMetrics().density));
        v.startAnimation(a);
    }

    public static void collapse(final View v) {
        final int initialHeight = v.getMeasuredHeight();

        Animation a = new Animation()
        {
            @Override
            protected void applyTransformation(float interpolatedTime, Transformation t) {
                if(interpolatedTime == 1){
                    v.setVisibility(View.GONE);
                }else{
                    v.getLayoutParams().height = initialHeight - (int)(initialHeight * interpolatedTime);
                    v.requestLayout();
                }
            }

            @Override
            public boolean willChangeBounds() {
                return true;
            }
        };

        // 1dp/ms
        a.setDuration((int)(initialHeight / v.getContext().getResources().getDisplayMetrics().density));
        v.startAnimation(a);
    }


    private static String timeFormat;
    public static String getTimeFormat() {
        return timeFormat;
    }
}
