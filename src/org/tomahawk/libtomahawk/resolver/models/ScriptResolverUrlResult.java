package org.tomahawk.libtomahawk.resolver.models;

/**
 * Created by YGS on 10/2/2015.
 */
public class ScriptResolverUrlResult {
    public String type;

    public String url;

    public String title;

    public String name;

    public String artist;

    public String guid;

    public String info;

    public String creator;

    public String hint;

    public ScriptResolverUrlResult[] tracks;

    public ScriptResolverUrlResult() {
    }
}
