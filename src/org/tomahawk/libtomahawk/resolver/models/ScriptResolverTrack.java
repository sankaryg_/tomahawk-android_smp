package org.tomahawk.libtomahawk.resolver.models;

/**
 * Created by YGS on 10/2/2015.
 */
public class ScriptResolverTrack {
    public String track;

    public String album;

    public String imagePath;

    public String artist;

    public String songId;

    public String playListName;

    public String playListId;

    public String playOrder;

    public String artistDisambiguation;

    public String albumArtist;

    public String albumArtistDisambiguation;

    public String url;

    public float duration;

    public String linkUrl;

    public String genre;

    public String genreDisambiguation;

    public int albumPos;

    public int folderId;

    public long lastModified;

    public int year;

    public boolean favourite;

    public String album_count;

    public String song_count;

    public String genre_count;

    public String playlist_count;

    public ScriptResolverTrack() {
    }
}
