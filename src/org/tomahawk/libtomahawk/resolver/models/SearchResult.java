package org.tomahawk.libtomahawk.resolver.models;

/**
 * Created by YGS on 11/14/2015.
 */
public class SearchResult {

    private float mScore;

    private Object mResult;

    public SearchResult(float score, Object result) {
        this.mScore = score;
        this.mResult = result;
    }

    public float getScore() {
        return mScore;
    }

    public Object getResult() {
        return mResult;
    }

}
