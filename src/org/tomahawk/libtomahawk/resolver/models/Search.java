package org.tomahawk.libtomahawk.resolver.models;

import java.util.List;

/**
 * Created by YGS on 11/14/2015.
 */
public class Search {

    private List<SearchResult> mSearchResults;

    public Search(List<SearchResult> searchResults) {
        mSearchResults = searchResults;
    }

    public List<SearchResult> getSearchResults() {
        return mSearchResults;
    }

}
