/* == This file is part of Tomahawk Player - <http://tomahawk-player.org> ===
 *
 *   Copyright 2015, Enno Gottschalk <mrmaffen@googlemail.com>
 *
 *   Tomahawk is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Tomahawk is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Tomahawk. If not, see <http://www.gnu.org/licenses/>.
 */
package org.tomahawk.libtomahawk.database;

import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.net.Uri;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.util.Log;
import android.widget.Toast;

import org.jdeferred.DoneCallback;
import org.tomahawk.libtomahawk.collection.Album;
import org.tomahawk.libtomahawk.collection.AlphaComparator;
import org.tomahawk.libtomahawk.collection.Artist;
import org.tomahawk.libtomahawk.collection.CollectionManager;
import org.tomahawk.libtomahawk.collection.Folder;
import org.tomahawk.libtomahawk.collection.Genre;
import org.tomahawk.libtomahawk.collection.MusicCollectionCursor;
import org.tomahawk.libtomahawk.collection.Playlist;
import org.tomahawk.libtomahawk.collection.PlaylistEntry;
import org.tomahawk.libtomahawk.collection.Track;
import org.tomahawk.libtomahawk.infosystem.User;
import org.tomahawk.libtomahawk.resolver.Query;
import org.tomahawk.libtomahawk.resolver.models.ScriptResolverTrack;
import org.tomahawk.tomahawk_android.R;
import org.tomahawk.tomahawk_android.TomahawkApp;
import org.tomahawk.tomahawk_android.activities.TomahawkMainActivity;

import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.regex.Pattern;

import de.greenrobot.event.EventBus;

public class CollectionDb extends SQLiteOpenHelper {

    public static final String TAG = CollectionDb.class.getSimpleName();

    private static final String LOVEDITEMS_PLAYLIST_ID = "loveditems_playlist_id";

    public static final String ALBUM_ARTIST = "album_artist";

    public static final String ID = "_id";

    public static final String TABLE_ARTISTS = "artists";

    public static final String ARTISTS_ARTIST = "artist";

    public static final String ARTISTS_ARTISTDISAMBIGUATION = "artistDisambiguation";

    public static final String ARTISTS_LASTMODIFIED = "artistLastModified";

    public static final String ARTISTS_LIKEDISLIKE = "artistLikeDislike";

    public static final String TABLE_GENRES = "genres";

    public static final String GENRES_ARTIST = "genreArtist";

    public static final String GENRES_ARTISTDISAMBIGUATION = "genreArtistDisambiguation";

    public static final String GENRES_LASTMODIFIED = "genreArtistLastModified";

    public static final String GENRES_LIKEDISLIKE = "genreLikeDislike";

    public static final String TABLE_FOLDERS = "folders";

    //public static final String FOLDERS_ID = "folderID";

    public static final String FOLDERS_ALBUM = "folderName";

    public static final String FOLDERS_PATH = "folderPath";

    public static final String FOLDERS_LASTMODIFIED = "folderAlbumLastModified";

    public static final String FOLDERS_LIKEDISLIKE = "folderLikeDislike";

    public static final String TABLE_ALBUMARTISTS = "albumArtists";

    public static final String ALBUMARTISTS_ALBUMARTIST = "albumArtist";

    public static final String ALBUMARTISTS_ALBUMARTISTDISAMBIGUATION = "albumArtistDisambiguation";

    public static final String ALBUMARTISTS_LASTMODIFIED = "albumArtistLastModified";

    public static final String TABLE_ALBUMGENRES = "albumGenres";

    public static final String ALBUMGENRES_ALBUMGENRE = "albumGenre";

    public static final String ALBUMGENRES_ALBUMGENREDISAMBIGUATION = "albumGenreDisambiguation";

    public static final String ALBUMGENRES_LASTMODIFIED = "albumGenreLastModified";

    public static final String TABLE_ALBUMS = "albums";

    public static final String ALBUMS_ALBUM = "album";

    public static final String ALBUMS_IMAGEPATH = "imagePath";

    public static final String ALBUMS_ALBUMARTISTID = "albumArtistId";

    public static final String ALBUMS_GENREARTISTID = "albumGenreId";

    public static final String ALBUMS_LASTMODIFIED = "albumLastModified";

    public static final String ALBUMS_LIKEDISLIKE = "albumLikeDislike";

    public static final String TABLE_ARTISTALBUMS = "artistAlbums";

    public static final String ARTISTALBUMS_ALBUMID = "albumId";

    public static final String ARTISTALBUMS_ARTISTID = "artistId";

    public static final String TABLE_GENREALBUMS = "genreAlbums";

    public static final String GENREALBUMS_ALBUMID = "genreAlbumId";

    public static final String GENREALBUMS_GENREID = "genreId";

    public static final String TABLE_TRACKS = "tracks";

    public static final String TRACKS_TRACK = "track";

    public static final String TRACKS_ARTISTID = "artistId";

    public static final String TRACKS_ALBUMID = "albumId";

    public static final String TRACKS_GENREID = "genreId";

    public static final String TRACKS_FOLDERID = "folderId";

    public static final String TRACKS_PLAYLISTID = "playlistId";

    public static final String TRACKS_URL = "url";

    public static final String TRACKS_COLUMN_RESULTHINT = "resulthint";

    public static final String TRACKS_DURATION = "duration";

    public static final String TRACKS_ALBUMPOS = "albumPos";

    public static final String TRACKS_LINKURL = "linkUrl";

    public static final String TRACKS_YEAR = "year";

    public static final String TRACKS_FAVOURITE = "favourite";

    public static final String TRACKS_ALBUM_COUNT = "albumCount";

    public static final String TRACKS_SONG_COUNT = "songCount";

    public static final String TRACKS_GENRE_COUNT = "genreCount";

    public static final String TRACKS_LASTMODIFIED = "trackLastModified";

    public static final String TRACKS_PLAYORDER = "playOrder";

    public static final String TRACKS_LIKEDISLIKE = "trackLikeDislike";

    public static final String TABLE_PLAYLISTS = "playlists";

    public static final String PLAYLISTS_COLUMN_ID = "id";

    public static final String PLAYLISTS_COLUMN_NAME = "name";

    public static final String PLAYLISTS_COLUMN_CURRENTTRACKINDEX = "currenttrackindex";

    public static final String PLAYLISTS_COLUMN_CURRENTREVISION = "currentrevision";

    //public static final String PLAYLISTS_COLUMN_HATCHETID = "hatchetid";

    public static final String PLAYLISTS_COLUMN_TOPARTISTS = "topartists";

    public static final String PLAYLISTS_COLUMN_TRACKCOUNT = "trackcount";

    private static ContentValues[] sContentValuesCache = null;

    private static final String CREATE_TABLE_PLAYLISTS =
            "CREATE TABLE `" + TABLE_PLAYLISTS + "` (  `"
                    + PLAYLISTS_COLUMN_ID + "` TEXT PRIMARY KEY ,  `"
                    + PLAYLISTS_COLUMN_NAME + "` TEXT , `"
                    + PLAYLISTS_COLUMN_CURRENTTRACKINDEX + "` INTEGER , `"
                    + PLAYLISTS_COLUMN_CURRENTREVISION + "` TEXT , `"
                    + PLAYLISTS_COLUMN_TOPARTISTS + "` TEXT , `"
                    + PLAYLISTS_COLUMN_TRACKCOUNT + "` INTEGER , "
                    + "UNIQUE ( " + PLAYLISTS_COLUMN_ID + ") ON CONFLICT IGNORE);";

    private static final String CREATE_TABLE_ARTISTS = "CREATE TABLE IF NOT EXISTS "
            + TABLE_ARTISTS + " ("
            + ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
            + ARTISTS_ARTIST + " TEXT,"
            + ARTISTS_ARTISTDISAMBIGUATION + " TEXT,"
            + ARTISTS_LASTMODIFIED + " INTEGER,"
            + ARTISTS_LIKEDISLIKE + " INTEGER,"
            + "UNIQUE (" + ARTISTS_ARTIST + ", " + ARTISTS_ARTISTDISAMBIGUATION
            + ") ON CONFLICT IGNORE);";

    private static final String CREATE_TABLE_GENRES = "CREATE TABLE IF NOT EXISTS "
            + TABLE_GENRES + " ("
            + ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
            + GENRES_ARTIST + " TEXT,"
            + GENRES_ARTISTDISAMBIGUATION + " TEXT,"
            + GENRES_LASTMODIFIED + " INTEGER,"
            + GENRES_LIKEDISLIKE + " INTEGER,"
            + "UNIQUE (" + GENRES_ARTIST + ", " + GENRES_ARTISTDISAMBIGUATION
            + ") ON CONFLICT IGNORE);";

    private static final String CREATE_TABLE_ALBUMARTISTS = "CREATE TABLE IF NOT EXISTS "
            + TABLE_ALBUMARTISTS + " ("
            + ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
            + ALBUMARTISTS_ALBUMARTIST + " TEXT,"
            + ALBUMARTISTS_ALBUMARTISTDISAMBIGUATION + " TEXT,"
            + ALBUMARTISTS_LASTMODIFIED + " INTEGER,"
            + "UNIQUE (" + ALBUMARTISTS_ALBUMARTIST + ", " + ALBUMARTISTS_ALBUMARTISTDISAMBIGUATION
            + ") ON CONFLICT IGNORE);";

    private static final String CREATE_TABLE_ALBUMGENRES = "CREATE TABLE IF NOT EXISTS "
            + TABLE_ALBUMGENRES + " ("
            + ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
            + ALBUMGENRES_ALBUMGENRE + " TEXT,"
            + ALBUMGENRES_ALBUMGENREDISAMBIGUATION + " TEXT,"
            + ALBUMGENRES_LASTMODIFIED + " INTEGER,"
            + "UNIQUE (" + ALBUMGENRES_ALBUMGENRE + ", " + ALBUMGENRES_ALBUMGENREDISAMBIGUATION
            + ") ON CONFLICT IGNORE);";

    private static final String CREATE_TABLE_ALBUMS = "CREATE TABLE IF NOT EXISTS "
            + TABLE_ALBUMS + " ("
            + ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
            + ALBUMS_ALBUM + " TEXT,"
            + ALBUMS_ALBUMARTISTID + " INTEGER,"
            + ALBUMS_GENREARTISTID + " INTEGER,"
            + ALBUMS_IMAGEPATH + " TEXT,"
            + ALBUMS_LASTMODIFIED + " INTEGER,"
            + ALBUMS_LIKEDISLIKE + " INTEGER,"
            + "UNIQUE (" + ALBUMS_ALBUM + ", " + ALBUMS_ALBUMARTISTID + ") ON CONFLICT IGNORE,"
            + "FOREIGN KEY(" + ALBUMS_ALBUMARTISTID + ") REFERENCES "
            + TABLE_ALBUMARTISTS + "(" + ID + "),"
            + "FOREIGN KEY(" + ALBUMS_GENREARTISTID + ") REFERENCES "
            + TABLE_ALBUMGENRES + "(" + ID + "));";

    private static final String CREATE_TABLE_FOLDERS = "CREATE TABLE IF NOT EXISTS "
            + TABLE_FOLDERS + " ("
            + ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
            + FOLDERS_ALBUM + " TEXT,"
            + FOLDERS_PATH + " TEXT,"
            + FOLDERS_LASTMODIFIED + " INTEGER,"
            + FOLDERS_LIKEDISLIKE + " INTEGER,"
            + "UNIQUE ( " + FOLDERS_PATH + ") ON CONFLICT IGNORE);"
            ;

    private static final String CREATE_TABLE_ARTISTALBUMS = "CREATE TABLE IF NOT EXISTS "
            + TABLE_ARTISTALBUMS + " ("
            + ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
            + ARTISTALBUMS_ALBUMID + " INTEGER,"
            + ARTISTALBUMS_ARTISTID + " INTEGER,"
            + "UNIQUE (" + ARTISTALBUMS_ALBUMID + ", " + ARTISTALBUMS_ARTISTID
            + ") ON CONFLICT IGNORE,"
            + "FOREIGN KEY(" + ARTISTALBUMS_ALBUMID + ") REFERENCES "
            + TABLE_ALBUMS + "(" + ID + "),"
            + "FOREIGN KEY(" + ARTISTALBUMS_ARTISTID + ") REFERENCES "
            + TABLE_ARTISTS + "(" + ID + "));";

    private static final String CREATE_TABLE_GENREALBUMS = "CREATE TABLE IF NOT EXISTS "
            + TABLE_GENREALBUMS + " ("
            + ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
            + GENREALBUMS_ALBUMID + " INTEGER,"
            + GENREALBUMS_GENREID + " INTEGER,"
            + "UNIQUE (" + GENREALBUMS_ALBUMID + ", " + GENREALBUMS_ALBUMID
            + ") ON CONFLICT IGNORE,"
            + "FOREIGN KEY(" + GENREALBUMS_ALBUMID + ") REFERENCES "
            + TABLE_ALBUMS + "(" + ID + "),"
            + "FOREIGN KEY(" + GENREALBUMS_GENREID + ") REFERENCES "
            + TABLE_GENRES + "(" + ID + "));";

    private static final String CREATE_TABLE_TRACKS = "CREATE TABLE IF NOT EXISTS "
            + TABLE_TRACKS + " ("
            + ID + " INTEGER PRIMARY KEY AUTOINCREMENT,"
            + TRACKS_TRACK + " TEXT,"
            + TRACKS_ARTISTID + " INTEGER,"
            + TRACKS_ALBUMID + " INTEGER,"
            + TRACKS_GENREID + " INTEGER,"
            + TRACKS_FOLDERID + " INTEGER,"
            + TRACKS_PLAYLISTID + " TEXT, "
            + TRACKS_PLAYORDER + " TEXT, "
            + TRACKS_URL + " TEXT,"
            + TRACKS_COLUMN_RESULTHINT + " TEXT,"
            + TRACKS_DURATION + " INTEGER,"
            + TRACKS_YEAR + " INTEGER,"
            + TRACKS_FAVOURITE + " INTEGER,"
            + TRACKS_ALBUM_COUNT + " TEXT,"
            + TRACKS_SONG_COUNT + " TEXT,"
            + TRACKS_GENRE_COUNT + " TEXT,"
            + TRACKS_ALBUMPOS + " INTEGER,"
            + TRACKS_LINKURL + " TEXT,"
            + TRACKS_LASTMODIFIED + " INTEGER,"
            + TRACKS_LIKEDISLIKE + " INTEGER,"
            + "UNIQUE (" + TRACKS_TRACK + ", " + TRACKS_ARTISTID + ", " + TRACKS_ALBUMID
            + ") ON CONFLICT IGNORE,"
            + "FOREIGN KEY(" + TRACKS_ARTISTID + ") REFERENCES "
            + TABLE_ARTISTS + "(" + ID + "),"
            + "FOREIGN KEY(" + TRACKS_FOLDERID + ") REFERENCES "
            + TABLE_FOLDERS + "(" + ID + "),"
            + "FOREIGN KEY(" + TRACKS_GENREID + ") REFERENCES "
            + TABLE_GENRES + "(" + ID + "),"
            + "FOREIGN KEY(" + TRACKS_PLAYLISTID + ") REFERENCES "
            + TABLE_PLAYLISTS + "(" + PLAYLISTS_COLUMN_ID + "),"
             + "FOREIGN KEY(" + TRACKS_ALBUMID + ") REFERENCES "
            + TABLE_ALBUMS + "(" + ID + "));";

    private static final int DB_VERSION = 3;

    private static final String DB_FILE_SUFFIX = "_collection.db";

    private final SQLiteDatabase mDb;

    private static final String LAST_COLLECTION_DB_UPDATE_SUFFIX = "_last_collection_db_update";

    private final String mLastUpdateStorageKey;

    private boolean mInitialized = false;

    public static class WhereInfo {

        public String connection;

        public Map<String, String[]> where = new HashMap<>();

    }

    private static class JoinInfo {

        String table;

        Map<String, String> conditions = new HashMap<>();

    }

    public CollectionDb(Context context, String collectionId) {
        super(context, collectionId + DB_FILE_SUFFIX, null, DB_VERSION);

        mLastUpdateStorageKey = collectionId + LAST_COLLECTION_DB_UPDATE_SUFFIX;

        close();
        mDb = getWritableDatabase();
    }

    public String getLastUpdateStorageKey() {
        return mLastUpdateStorageKey;
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_TABLE_ARTISTS);
        db.execSQL(CREATE_TABLE_GENRES);
        db.execSQL(CREATE_TABLE_ALBUMARTISTS);
        db.execSQL(CREATE_TABLE_ALBUMGENRES);
        db.execSQL(CREATE_TABLE_ALBUMS);
        db.execSQL(CREATE_TABLE_FOLDERS);
        db.execSQL(CREATE_TABLE_PLAYLISTS);
        db.execSQL(CREATE_TABLE_ARTISTALBUMS);
        db.execSQL(CREATE_TABLE_GENREALBUMS);
        db.execSQL(CREATE_TABLE_TRACKS);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        Log.d(TAG, "Upgrading database from version " + oldVersion + " to " + newVersion
                + ", which might destroy all old data");
        if (oldVersion < 2) {
            db.execSQL("ALTER TABLE `" + TABLE_ALBUMS + "` ADD COLUMN `"
                    + ALBUMS_IMAGEPATH + "` TEXT");
        }
        if (oldVersion < 3) {
            db.execSQL("DROP TABLE IF EXISTS `" + TABLE_ARTISTS + "`;");
            db.execSQL(CREATE_TABLE_ARTISTS);
            db.execSQL("DROP TABLE IF EXISTS `" + TABLE_GENRES + "`;");
            db.execSQL(CREATE_TABLE_GENRES);
            db.execSQL("DROP TABLE IF EXISTS `" + TABLE_ALBUMARTISTS + "`;");
            db.execSQL(CREATE_TABLE_ALBUMARTISTS);
            db.execSQL("DROP TABLE IF EXISTS `" + TABLE_ALBUMGENRES + "`;");
            db.execSQL(CREATE_TABLE_ALBUMGENRES);
            db.execSQL("DROP TABLE IF EXISTS `" + TABLE_ALBUMS + "`;");
            db.execSQL(CREATE_TABLE_ALBUMS);
            db.execSQL("DROP TABLE IF EXISTS `" + TABLE_FOLDERS + "`;");
            db.execSQL(CREATE_TABLE_FOLDERS);
            db.execSQL("DROP TABLE IF EXISTS `" + TABLE_PLAYLISTS + "`;");
            db.execSQL(CREATE_TABLE_PLAYLISTS);
            db.execSQL("DROP TABLE IF EXISTS `" + TABLE_ARTISTALBUMS + "`;");
            db.execSQL(CREATE_TABLE_ARTISTALBUMS);
            db.execSQL("DROP TABLE IF EXISTS `" + TABLE_GENREALBUMS + "`;");
            db.execSQL(CREATE_TABLE_GENREALBUMS);
            db.execSQL("DROP TABLE IF EXISTS `" + TABLE_TRACKS + "`;");
            db.execSQL(CREATE_TABLE_TRACKS);
        }
    }

    public boolean isInitialized() {
        return mInitialized;
    }

    public void deleteAlbumTracks(String albumName){

        String[] columns = new String[]{CollectionDb.ID,CollectionDb.ALBUMS_ALBUM};
        Cursor albumCursor = mDb.query(CollectionDb.TABLE_ALBUMS,
                columns, CollectionDb.ALBUMS_ALBUM + " = ?",
                new String[]{albumName}, null, null, null);
        String albumId = null;
        if (albumCursor.moveToFirst()) {
            albumId = albumCursor.getString(0);
            mDb.delete(TABLE_ALBUMS, ALBUMS_ALBUM + " =? ", new String[]{albumName});
        }
        albumCursor.close();
        if (albumId!=null){
            String[] trackColumns = new String[]{TRACKS_ARTISTID,TRACKS_ALBUMID,TRACKS_GENREID,TRACKS_FOLDERID,TRACKS_PLAYLISTID,TRACKS_PLAYORDER};
            Cursor trackCursor = mDb.query(CollectionDb.TABLE_TRACKS,
                    trackColumns, CollectionDb.TRACKS_ALBUMID + " = ?",
                    new String[]{albumId}, null, null, null);
            long[] list = new long[trackCursor.getCount()];
            int i=0;
            if (trackCursor.moveToFirst()) {
                do {
                    Log.d("t1",trackCursor.getString(0));
                    Log.d("t1",trackCursor.getString(1));
                    Log.d("t1",trackCursor.getString(2));
                    Log.d("t1",trackCursor.getString(3));
                    Log.d("t1",trackCursor.getString(4));
                    Log.d("t1",trackCursor.getString(5));
                    deleteTrackByAlbum(trackCursor.getString(5));
                    list[i]= Long.valueOf(trackCursor.getString(5));
                    i++;
                }while (trackCursor.moveToNext());

            }
            trackCursor.close();
            deleteTracks(list);
            Toast.makeText(TomahawkApp.getContext(),albumName +" deleted",Toast.LENGTH_SHORT).show();
            EventBus.getDefault().post(new CollectionManager.UpdatedEvent());
        }

    }

    public void toggleLovedAlbumTracks(Album album){

        String[] columns = new String[]{CollectionDb.ID,CollectionDb.ALBUMS_ALBUM};
        Cursor albumCursor = mDb.query(CollectionDb.TABLE_ALBUMS,
                columns, CollectionDb.ALBUMS_ALBUM + " = ?",
                new String[]{album.getName()}, null, null, null);
        String albumId = null;
        if (albumCursor.moveToFirst()) {
            albumId = albumCursor.getString(0);
            ContentValues values = new ContentValues();
            values.put(ALBUMS_LIKEDISLIKE, album.getmLike());
            if(mDb.update(TABLE_ALBUMS,values,ALBUMS_ALBUM + " =? ",new String[]{album.getName()})!= -1) {
                values = new ContentValues();
                values.put(TRACKS_LIKEDISLIKE,album.getmLike());
                mDb.update(TABLE_TRACKS,values,CollectionDb.TRACKS_ALBUMID + " = ?",
                        new String[]{albumId});
            }
        }
        albumCursor.close();
        EventBus.getDefault().post(new CollectionManager.UpdatedEvent());


    }

    public void toggleLovedArtistTracks(Artist album){

        String[] columns = new String[]{CollectionDb.ID,CollectionDb.ARTISTS_ARTIST};
        Cursor albumCursor = mDb.query(CollectionDb.TABLE_ARTISTS,
                columns, CollectionDb.ARTISTS_ARTIST + " = ?",
                new String[]{album.getName()}, null, null, null);
        String albumId = null;
        if (albumCursor.moveToFirst()) {
            albumId = albumCursor.getString(0);
            ContentValues values = new ContentValues();
            values.put(ARTISTS_LIKEDISLIKE,album.getmLike());
            if(mDb.update(TABLE_ARTISTS,values,ARTISTS_ARTIST + " =? ",new String[]{album.getName()})!= -1) {
                values = new ContentValues();
                values.put(TRACKS_LIKEDISLIKE,album.getmLike());
                mDb.update(TABLE_TRACKS,values,CollectionDb.TRACKS_ARTISTID + " = ?",
                        new String[]{albumId});
            }
        }
        albumCursor.close();
        EventBus.getDefault().post(new CollectionManager.UpdatedEvent());


    }

    public void toggleLovedGenreTracks(Genre album){

        String[] columns = new String[]{CollectionDb.ID,CollectionDb.GENRES_ARTIST};
        Cursor albumCursor = mDb.query(CollectionDb.TABLE_GENRES,
                columns, CollectionDb.GENRES_ARTIST + " = ?",
                new String[]{album.getName()}, null, null, null);
        String albumId = null;
        if (albumCursor.moveToFirst()) {
            albumId = albumCursor.getString(0);
            ContentValues values = new ContentValues();
            values.put(GENRES_LIKEDISLIKE,album.getmLike());
            if(mDb.update(TABLE_GENRES,values,GENRES_ARTIST + " =? ",new String[]{album.getName()})!= -1) {
                values = new ContentValues();
                values.put(TRACKS_LIKEDISLIKE,album.getmLike());
                mDb.update(TABLE_TRACKS,values,CollectionDb.TRACKS_GENREID + " = ?",
                        new String[]{albumId});
            }
        }

        albumCursor.close();
        EventBus.getDefault().post(new CollectionManager.UpdatedEvent());


    }

    public void toggleLovedFolderTracks(Folder album){

        String[] columns = new String[]{CollectionDb.ID,CollectionDb.FOLDERS_ALBUM};
        /*Cursor albumCursor = mDb.query(CollectionDb.TABLE_FOLDERS,
                columns, CollectionDb.FOLDERS_ALBUM + " = ?",
                new String[]{album.getFolderName()}, null, null, null);*/
        String albumId = null;
        /*if (albumCursor.moveToFirst()) {
            albumId = albumCursor.getString(0);
            ContentValues values = new ContentValues();
            values.put(FOLDERS_LIKEDISLIKE,album.getmLike());
            if(mDb.update(TABLE_FOLDERS,values,FOLDERS_ALBUM + " =? ",new String[]{album.getName()})!= -1) {
                values = new ContentValues();
                values.put(TRACKS_LIKEDISLIKE,album.getmLike());
                mDb.update(TABLE_TRACKS,values,CollectionDb.TRACKS_FOLDERID + " = ?",
                        new String[]{albumId});
            }
        }*/
        //albumCursor.close();
        ContentValues values = new ContentValues();
        values.put(FOLDERS_LIKEDISLIKE,album.getmLike());
        if(mDb.update(TABLE_FOLDERS,values,ID + " =? ",new String[]{String.valueOf(album.getFolderId())})!= -1) {
            values = new ContentValues();
            values.put(TRACKS_LIKEDISLIKE,album.getmLike());
            mDb.update(TABLE_TRACKS,values,CollectionDb.TRACKS_FOLDERID + " = ?",
                    new String[]{String.valueOf(album.getFolderId())});
        }
        CollectionManager.UpdatedEvent event = new CollectionManager.UpdatedEvent();
        event.mUpdatedItemIds = new HashSet<>();
        event.mUpdatedItemIds.add(album.getCacheKey());
        EventBus.getDefault().post(new CollectionManager.UpdatedEvent());


    }

    public void toggleLovedTracks(Track album){

        String[] columns = new String[]{CollectionDb.ID,CollectionDb.TRACKS_TRACK};
        Cursor albumCursor = mDb.query(CollectionDb.TABLE_TRACKS,
                columns, CollectionDb.TRACKS_PLAYORDER + " = ?",
                new String[]{album.getTrackId()}, null, null, null);
        String albumId = null;
        if (albumCursor.moveToFirst()) {
            albumId = albumCursor.getString(0);
            ContentValues values = new ContentValues();
            values.put(TRACKS_LIKEDISLIKE, album.getmLike());
            if(mDb.update(TABLE_TRACKS,values,TRACKS_PLAYORDER + " =? ",new String[]{album.getTrackId()})!= -1) {

            }
        }
        albumCursor.close();
        EventBus.getDefault().post(new CollectionManager.UpdatedEvent());


    }
    public void deleteArtistTracks(String albumName){

        String[] columns = new String[]{CollectionDb.ID,CollectionDb.ARTISTS_ARTIST};
        Cursor albumCursor = mDb.query(CollectionDb.TABLE_ARTISTS,
                columns, CollectionDb.ARTISTS_ARTIST + " = ?",
                new String[]{albumName}, null, null, null);
        String albumId = null;
        if (albumCursor.moveToFirst()) {
            albumId = albumCursor.getString(0);
            mDb.delete(TABLE_ARTISTS,ARTISTS_ARTIST + " =? ",new String[]{albumName});
        }
        albumCursor.close();
        if (albumId!=null){
            String[] trackColumns = new String[]{TRACKS_ARTISTID,TRACKS_ALBUMID,TRACKS_GENREID,TRACKS_FOLDERID,TRACKS_PLAYLISTID,TRACKS_PLAYORDER};
            Cursor trackCursor = mDb.query(CollectionDb.TABLE_TRACKS,
                    trackColumns, CollectionDb.TRACKS_ARTISTID + " = ?",
                    new String[]{albumId}, null, null, null);
            long[] list = new long[trackCursor.getCount()];
            int i=0;
            if (trackCursor.moveToFirst()) {
                do {
                    Log.d("t1",trackCursor.getString(0));
                    Log.d("t1",trackCursor.getString(1));
                    Log.d("t1",trackCursor.getString(2));
                    Log.d("t1",trackCursor.getString(3));
                    Log.d("t1",trackCursor.getString(4));
                    Log.d("t1",trackCursor.getString(5));
                    deleteTrackByAlbum(trackCursor.getString(5));
                    list[i]= Long.valueOf(trackCursor.getString(5));
                    i++;
                }while (trackCursor.moveToNext());

            }
            trackCursor.close();
            deleteTracks(list);
            Toast.makeText(TomahawkApp.getContext(),albumName +" deleted",Toast.LENGTH_SHORT).show();
            EventBus.getDefault().post(new CollectionManager.UpdatedEvent());
        }

    }

    public void deleteGenreTracks(String albumName){

        String[] columns = new String[]{CollectionDb.ID,CollectionDb.GENRES_ARTIST};
        Cursor albumCursor = mDb.query(CollectionDb.TABLE_GENRES,
                columns, CollectionDb.GENRES_ARTIST + " = ?",
                new String[]{albumName}, null, null, null);
        String albumId = null;
        if (albumCursor.moveToFirst()) {
            albumId = albumCursor.getString(0);
            mDb.delete(TABLE_GENRES,GENRES_ARTIST + " =? ",new String[]{albumName});
        }
        albumCursor.close();
        if (albumId!=null){
            String[] trackColumns = new String[]{TRACKS_ARTISTID,TRACKS_ALBUMID,TRACKS_GENREID,TRACKS_FOLDERID,TRACKS_PLAYLISTID,TRACKS_PLAYORDER};
            Cursor trackCursor = mDb.query(CollectionDb.TABLE_TRACKS,
                    trackColumns, CollectionDb.TRACKS_GENREID + " = ?",
                    new String[]{albumId}, null, null, null);
            long[] list = new long[trackCursor.getCount()];
            int i=0;
            if (trackCursor.moveToFirst()) {
                do {
                    Log.d("t1",trackCursor.getString(0));
                    Log.d("t1",trackCursor.getString(1));
                    Log.d("t1",trackCursor.getString(2));
                    Log.d("t1",trackCursor.getString(3));
                    Log.d("t1",trackCursor.getString(4));
                    Log.d("t1",trackCursor.getString(5));
                    deleteTrackByAlbum(trackCursor.getString(5));
                    list[i]= Long.valueOf(trackCursor.getString(5));
                    i++;
                }while (trackCursor.moveToNext());

            }
            trackCursor.close();
            deleteTracks(list);
            Toast.makeText(TomahawkApp.getContext(),albumName +" deleted",Toast.LENGTH_SHORT).show();
            EventBus.getDefault().post(new CollectionManager.UpdatedEvent());
        }

    }

    public void deleteFolderTracks(String folderPath,String folderName){

        String[] columns = new String[]{CollectionDb.ID,CollectionDb.FOLDERS_PATH};
        Cursor albumCursor = mDb.query(CollectionDb.TABLE_FOLDERS,
                columns, CollectionDb.FOLDERS_PATH + " = ?",
                new String[]{folderPath}, null, null, null);
        String albumId = null;
        if (albumCursor.moveToFirst()) {
            albumId = albumCursor.getString(0);
            mDb.delete(TABLE_FOLDERS,FOLDERS_PATH + " =? ",new String[]{folderPath});
        }
        albumCursor.close();
        if (albumId!=null){
            String[] trackColumns = new String[]{TRACKS_ARTISTID,TRACKS_ALBUMID,TRACKS_GENREID,TRACKS_FOLDERID,TRACKS_PLAYLISTID,TRACKS_PLAYORDER};
            Cursor trackCursor = mDb.query(CollectionDb.TABLE_TRACKS,
                    trackColumns, CollectionDb.TRACKS_FOLDERID + " = ?",
                    new String[]{albumId}, null, null, null);
            long[] list = new long[trackCursor.getCount()];
            int i=0;
            if (trackCursor.moveToFirst()) {
                do {
                    Log.d("t1",trackCursor.getString(0));
                    Log.d("t1",trackCursor.getString(1));
                    Log.d("t1",trackCursor.getString(2));
                    Log.d("t1",trackCursor.getString(3));
                    Log.d("t1",trackCursor.getString(4));
                    Log.d("t1",trackCursor.getString(5));
                    deleteTrackByAlbum(trackCursor.getString(5));
                    list[i]= Long.valueOf(trackCursor.getString(5));
                    i++;
                }while (trackCursor.moveToNext());

            }
            trackCursor.close();
            deleteTracks(list);
            Toast.makeText(TomahawkApp.getContext(),folderName +" deleted",Toast.LENGTH_SHORT).show();
            EventBus.getDefault().post(new CollectionManager.UpdatedEvent());
        }

    }

    public void deleteTracks(long[] list) {

        String[] cols = new String[] { MediaStore.Audio.Media._ID, MediaStore.Audio.Media.DATA,
                MediaStore.Audio.Media.ALBUM_ID };
        StringBuilder where = new StringBuilder();
        where.append(MediaStore.Audio.Media._ID + " IN (");
        for (int i = 0; i < list.length; i++) {
            where.append(list[i]);
            if (i < list.length - 1) {
                where.append(",");
            }
        }
        where.append(")");
        Cursor c = TomahawkApp.getContext().getContentResolver().query(MediaStore.Audio.Media.EXTERNAL_CONTENT_URI, cols, where.toString(), null,
                null);

        if (c != null) {

            // step 1: remove selected tracks from the current playlist, as well
            // as from the album art cache
            try {
                c.moveToFirst();
                while (!c.isAfterLast()) {
                    // remove from current playlist
                    long id = c.getLong(0);
                    //TODO
                    //Remove FromCurrent Playlist
                    //mService.removeTrack(id);
                    //deleteQueryInQueue
                    // remove from album art cache
                    long artIndex = c.getLong(2);
                    /*synchronized (mArtBitmapCache) {
                        mArtBitmapCache.remove(artIndex);
                    }*/
                    c.moveToNext();
                }
            } catch (Exception ex) {
            }

            // step 2: remove selected tracks from the database
            TomahawkApp.getContext().getContentResolver().delete(MediaStore.Audio.Media.EXTERNAL_CONTENT_URI,
                    where.toString(), null);

            // step 3: remove files from card
            c.moveToFirst();
            while (!c.isAfterLast()) {
                String name = c.getString(1);
                File f = new File(name);
                try { // File.delete can throw a security exception
                    if (!f.delete()) {
                        // I'm not sure if we'd ever get here (deletion would
                        // have to fail, but no exception thrown)
                        Log.e("t1", "Failed to delete file " + name);
                    }
                    c.moveToNext();
                } catch (SecurityException ex) {
                    c.moveToNext();
                }
            }
            c.close();
        }

        String message =  TomahawkApp.getContext().getResources().getQuantityString(R.plurals.NNNtracksdeleted,
                list.length, Integer.valueOf(list.length));

        Toast.makeText( TomahawkApp.getContext(), message, Toast.LENGTH_SHORT).show();
        // We deleted a number of tracks, which could affect any number of
        // things
        // in the media content domain, so update everything.
        TomahawkApp.getContext().getContentResolver().notifyChange(Uri.parse("content://media"), null);
    }
    public void deleteTrackByAlbum(String trackId) {
        mDb.delete(CollectionDb.TABLE_TRACKS, CollectionDb.TRACKS_PLAYORDER + " = ?", new String[]{trackId});
    }

    public String getPlaylistName(String playlistId) {
        String[] columns = new String[]{CollectionDb.PLAYLISTS_COLUMN_NAME};

        Cursor playlistsCursor = mDb.query(CollectionDb.TABLE_PLAYLISTS,
                columns, CollectionDb.PLAYLISTS_COLUMN_ID + " = ?",
                new String[]{playlistId}, null, null, null);
        String name = null;
        if (playlistsCursor.moveToFirst()) {
            name = playlistsCursor.getString(0);
        }
        playlistsCursor.close();
        return name;
    }

    /**
     * Checks if a query with the same track/artistName as the given query is included in the
     * lovedItems Playlist
     *
     * @return whether or not the given query is loved
     */
    public boolean isItemLoved(Query query) {
        String[] columns = new String[]{CollectionDb.TRACKS_TRACK,
                CollectionDb.TRACKS_FAVOURITE};

        Cursor tracksCursor = mDb.query(CollectionDb.TABLE_TRACKS, columns,
                CollectionDb.TRACKS_TRACK + " = ?",
                new String[]{query.getBasicTrack().getName()}, null, null, null
        );
        tracksCursor.moveToFirst();
        while (!tracksCursor.isAfterLast()) {
            String trackName = tracksCursor.getString(0);
            String favourite = tracksCursor.getString(1);
            /*Cursor artistCursor = mDb.query(CollectionDb.TABLE_ARTISTS,
                    new String[]{CollectionDb.ARTISTS_ARTIST}, CollectionDb.ID + " = ?",
                    new String[]{artistName}, null, null, null);
            if (artistCursor.moveToFirst()) {
                artistName = artistCursor.getString(0);
                artistCursor.close();
            }
*/
            if (query.getName().equalsIgnoreCase(trackName)
                    && favourite.equals("1")) {
                tracksCursor.close();
                return true;
            }

            tracksCursor.moveToNext();
        }
        tracksCursor.close();
        return false;
    }

    /**
     * Store the given query as a lovedItem, if isLoved is true. Otherwise remove(unlove) the
     * query.
     */
    public int setLovedItem(Query query, boolean isLoved) {

            ArrayList<Query> queries = new ArrayList<>();
            queries.add(query);

        int returnValue = -1;
        for (int i = 0; i < queries.size(); i++) {
            Query entry = queries.get(i);
            ContentValues values = new ContentValues();
            if (isLoved)
            values.put(CollectionDb.TRACKS_FAVOURITE,
                    "1");
            else
                values.put(CollectionDb.TRACKS_FAVOURITE,
                        "0");


            if(mDb.update(CollectionDb.TABLE_TRACKS,values,CollectionDb.TRACKS_TRACK + " = ?",
                    new String[]{ entry.getBasicTrack().getName()})!=-1){
               returnValue = returnValue +1;
            }
        }
       return returnValue;
    }
    /**
     * Add the given {@link ArrayList} of {@link Track}s to the {@link
     * org.tomahawk.libtomahawk.collection.Playlist} with the given playlistId
     */
    public void addEntriesToPlaylist(final String playlistId,
                                     final ArrayList<PlaylistEntry> entries) {
        long trackCount = getPlaylistTrackCount(playlistId);

        mDb.beginTransaction();
        // Store every single Track in the database and store the relationship
        // by storing the playlists's id with it
        long[] ids = new long[entries.size()];
        for (int i = 0; i < entries.size(); i++) {
            PlaylistEntry entry = entries.get(i);
            ContentValues values = new ContentValues();
            values.put(CollectionDb.TRACKS_PLAYLISTID,
                    playlistId);
            values.put(CollectionDb.TRACKS_TRACK,
                    entry.getQuery().getBasicTrack().getName());
            /*values.put(CollectionDb.TRACKS_ARTISTID,
                    entry.getQuery().getBasicTrack().getArtist().getName());
            values.put(CollectionDb.TRACKS_ALBUMID,
                    entry.getQuery().getBasicTrack().getAlbum().getName());*/
            values.put(CollectionDb.TRACKS_COLUMN_RESULTHINT,
                    entry.getQuery().getTopTrackResultKey());

            /*Cursor albumCursor = mDb.query(CollectionDb.TABLE_ALBUMS,
                    new String[]{CollectionDb.ID}, CollectionDb.ALBUMS_ALBUM + " = ?",
                    new String[]{entry.getQuery().getBasicTrack().getName()}, null, null, null);
            String albumId = entry.getQuery().getBasicTrack().getName();
            if (albumCursor.moveToFirst()) {
                albumId = albumCursor.getString(0);
                albumCursor.close();
            }
            Cursor cursor=mDb.query(CollectionDb.TABLE_TRACKS, new String[]{CollectionDb.TRACKS_PLAYORDER}, CollectionDb.TRACKS_ALBUMID + " = ?",
                    new String[]{albumId}, null, null, null);
            if (cursor.moveToFirst()){*/
               ids[i] = Long.valueOf(entry.getId());//cursor.getString(0));
            /*}
            cursor.close();*/
            /*if(mDb.update(CollectionDb.TABLE_TRACKS,values,CollectionDb.TRACKS_TRACK + " = ?",
                    new String[]{ entry.getQuery().getBasicTrack().getName()})!=-1){
                trackCount++;
            }*/
        }
        addToPlaylist(ids, Long.valueOf(playlistId));
        /*ContentValues values = new ContentValues();
        values.put(CollectionDb.PLAYLISTS_COLUMN_TRACKCOUNT, trackCount);

        *//*values.put(CollectionDb.PLAYLISTS_COLUMN_CURRENTTRACKINDEX,
                    entry.getId());*//*
        mDb.update(CollectionDb.TABLE_PLAYLISTS, values,
                CollectionDb.PLAYLISTS_COLUMN_ID + " = ?",
                new String[]{playlistId});*/
        mDb.setTransactionSuccessful();
        mDb.endTransaction();
        DatabaseHelper.PlaylistsUpdatedEvent event = new DatabaseHelper.PlaylistsUpdatedEvent();
        event.mPlaylistId = playlistId;
        EventBus.getDefault().post(event);
    }

    public void addToPlaylist(long[] ids, long playlistid) {

        if (ids == null) {
            // this shouldn't happen (the menuitems shouldn't be visible
            // unless the selected item represents something playable
            Log.e("MusicBase", "ListSelection null");
        } else {
            int size = ids.length;
            ContentResolver resolver = TomahawkApp.getContext().getContentResolver();
            // need to determine the number of items currently in the playlist,
            // so the play_order field can be maintained.
            String[] cols = new String[] { "count(*)" };
            Uri uri = MediaStore.Audio.Playlists.Members.getContentUri("external", playlistid);
            Cursor cur = resolver.query(uri, cols, null, null, null);
            cur.moveToFirst();
            int base = cur.getInt(0);
            cur.close();
            int numinserted = 0;
            for (int i = 0; i < size; i += 1000) {
                makeInsertItems(ids, i, 1000, base);
                numinserted += resolver.bulkInsert(uri, sContentValuesCache);
            }
            String message = TomahawkApp.getContext().getResources().getQuantityString(
                    R.plurals.NNNtrackstoplaylist, numinserted, numinserted);
            Toast.makeText(TomahawkApp.getContext(), message, Toast.LENGTH_SHORT).show();
            // mLastPlaylistSelected = playlistid;
        }
    }

    /**
     * @param ids
     *            The source array containing all the ids to be added to the
     *            playlist
     * @param offset
     *            Where in the 'ids' array we start reading
     * @param len
     *            How many items to copy during this pass
     * @param base
     *            The play order offset to use for this pass
     */
    private void makeInsertItems(long[] ids, int offset, int len, int base) {

        // adjust 'len' if would extend beyond the end of the source array
        if (offset + len > ids.length) {
            len = ids.length - offset;
        }
        // allocate the ContentValues array, or reallocate if it is the wrong
        // size
        if (sContentValuesCache == null || sContentValuesCache.length != len) {
            sContentValuesCache = new ContentValues[len];
        }
        // fill in the ContentValues array with the right values for this pass
        for (int i = 0; i < len; i++) {
            if (sContentValuesCache[i] == null) {
                sContentValuesCache[i] = new ContentValues();
            }

            sContentValuesCache[i].put(MediaStore.Audio.Playlists.Members.PLAY_ORDER, base + offset + i);
            sContentValuesCache[i].put(MediaStore.Audio.Playlists.Members.AUDIO_ID, ids[offset + i]);
        }
    }

    /**
     * Store the given {@link Playlist}
     *
     * @param playlist       the given {@link Playlist}
     * @param reverseEntries set to true, if the order of the entries should be reversed before
     *                       storing in the database
     */
    public void storePlaylist(Playlist playlist, boolean reverseEntries) {
        storePlaylist(playlist.getId(), playlist, reverseEntries);
    }

    /**
     * Store the given {@link Playlist}
     *
     * @param playlistId     the id under which to store the given {@link Playlist}
     * @param playlist       the given {@link Playlist}
     * @param reverseEntries set to true, if the order of the entries should be reversed before
     *                       storing in the database
     */
    private void storePlaylist(final String playlistId, final Playlist playlist,
                               final boolean reverseEntries) {
        List<PlaylistEntry> entries = playlist.getEntries();

        ContentValues values = new ContentValues();
        values.put(CollectionDb.PLAYLISTS_COLUMN_NAME, playlist.getName());
        values.put(CollectionDb.PLAYLISTS_COLUMN_CURRENTREVISION,
                playlist.getCurrentRevision());
        values.put(CollectionDb.PLAYLISTS_COLUMN_ID, playlistId);
        /*values.put(CollectionDb.PLAYLISTS_COLUMN_HATCHETID,
                playlist.getHatchetId());*/
        values.put(CollectionDb.PLAYLISTS_COLUMN_TRACKCOUNT, entries.size());

        mDb.beginTransaction();
        mDb.insertWithOnConflict(CollectionDb.TABLE_PLAYLISTS, null,
                values,
                SQLiteDatabase.CONFLICT_REPLACE);
        // Delete every already associated Track entry
        ContentValues v1 = new ContentValues();
        v1.put(CollectionDb.TRACKS_PLAYLISTID,"-1");
        mDb.update(CollectionDb.TABLE_TRACKS,v1,
                CollectionDb.TRACKS_PLAYLISTID + " = ?",
                new String[]{playlistId});

        // Store every single Track in the database and store the relationship
        // by storing the playlists's id with it
        for (int i = 0; i < entries.size(); i++) {
            PlaylistEntry entry;
            if (reverseEntries) {
                entry = entries.get(entries.size() - 1 - i);
            } else {
                entry = entries.get(i);
            }
            values.clear();
            values.put(CollectionDb.TRACKS_PLAYLISTID, playlistId);
            values.put(CollectionDb.TRACKS_TRACK,
                    entry.getQuery().getBasicTrack().getName());
            values.put(CollectionDb.TRACKS_COLUMN_RESULTHINT,
                    entry.getQuery().getTopTrackResultKey());
            values.put(CollectionDb.TRACKS_PLAYORDER, i);

            /*values.put(TomahawkSQLiteHelper.TRACKS_COLUMN_PLAYLISTENTRYID,
                    entry.getId());*/
            mDb.update(CollectionDb.TABLE_TRACKS,values,CollectionDb.TRACKS_TRACK + " = ?",
                    new String[]{ entry.getQuery().getBasicTrack().getName()});
        }
        mDb.setTransactionSuccessful();
        mDb.endTransaction();
        DatabaseHelper.PlaylistsUpdatedEvent event = new DatabaseHelper.PlaylistsUpdatedEvent();
        event.mPlaylistId = playlistId;
        EventBus.getDefault().post(event);
    }

    /**
     * @param playlistId the id by which to get the correct {@link org.tomahawk.libtomahawk.collection.Playlist}
     * @return the stored {@link org.tomahawk.libtomahawk.collection.Playlist} with playlistId as
     * its id
     */
    public Playlist getPlaylist(String playlistId) {
        return getPlaylist(playlistId, false);
    }

    /**
     * @param playlistId the id by which to get the correct {@link org.tomahawk.libtomahawk.collection.Playlist}
     * @return the stored {@link org.tomahawk.libtomahawk.collection.Playlist} with playlistId as
     * its id
     */
    private Playlist getPlaylist(String playlistId, boolean reverseEntries) {
        String[] columns = new String[]{CollectionDb.PLAYLISTS_COLUMN_NAME,
                CollectionDb.PLAYLISTS_COLUMN_CURRENTREVISION,
                CollectionDb.PLAYLISTS_COLUMN_TOPARTISTS};

        Cursor playlistsCursor = mDb.query(CollectionDb.TABLE_PLAYLISTS,
                columns, CollectionDb.PLAYLISTS_COLUMN_ID + " = ?",
                new String[]{playlistId}, null, null, null);
        if (playlistsCursor.moveToFirst()) {
            columns = new String[]{CollectionDb.TRACKS_TRACK,
                    CollectionDb.TRACKS_ARTISTID,
                    CollectionDb.TRACKS_ALBUMID,
                    CollectionDb.TRACKS_COLUMN_RESULTHINT,
                    //CollectionDb.TRACKS_COLUMN_ISFETCHEDVIAHATCHET,
                    CollectionDb.TRACKS_PLAYORDER,
            CollectionDb.TRACKS_URL};
            Cursor tracksCursor = mDb.query(CollectionDb.TABLE_TRACKS, columns,
                    CollectionDb.TRACKS_PLAYLISTID + " = ?",
                    new String[]{playlistId}, null, null,
                    CollectionDb.TRACKS_PLAYORDER + (reverseEntries
                            ? " DESC" : " ASC"));
            ArrayList<PlaylistEntry> entries = new ArrayList<>();
            tracksCursor.moveToFirst();
            while (!tracksCursor.isAfterLast()) {
                String trackName = tracksCursor.getString(0);
                String artistName = tracksCursor.getString(1);
                String trackId = tracksCursor.getString(4);
                String path = tracksCursor.getString(5);
                Cursor artistCursor = mDb.query(CollectionDb.TABLE_ARTISTS,
                        new String[]{CollectionDb.ARTISTS_ARTIST}, CollectionDb.ID + " = ?",
                        new String[]{artistName}, null, null, null);
                if (artistCursor.moveToFirst()) {
                    artistName = artistCursor.getString(0);
                    artistCursor.close();
                }

                String albumName = tracksCursor.getString(2);

                Cursor albumCursor = mDb.query(CollectionDb.TABLE_ALBUMS,
                        new String[]{CollectionDb.ALBUMS_ALBUM}, CollectionDb.ID + " = ?",
                        new String[]{albumName}, null, null, null);
                if (albumCursor.moveToFirst()) {
                    albumName = albumCursor.getString(0);
                    albumCursor.close();
                }

                String resultHint = tracksCursor.getString(3);
                Query query = Query.get(trackId,trackName,  albumName, artistName, resultHint, false,
                        false);
                query.getBasicTrack().setPath(path);
                String entryId;
                if (tracksCursor.getString(4) != null) {
                    entryId = tracksCursor.getString(4);
                } else {
                    entryId = TomahawkMainActivity.getLifetimeUniqueStringId();
                }
                PlaylistEntry entry = PlaylistEntry.get(playlistId, query, entryId);
                entries.add(entry);
                tracksCursor.moveToNext();
            }
            Playlist playlist = Playlist.fromEntriesList(playlistId, playlistsCursor.getString(0),
                    playlistsCursor.getString(1), entries);
            //playlist.setHatchetId(playlistsCursor.getString(2));
            playlist.setFilled(true);
            tracksCursor.close();
            String rawTopArtistsString = playlistsCursor.getString(3);
            if (rawTopArtistsString != null && rawTopArtistsString.length() > 0) {
                playlist.setTopArtistNames(rawTopArtistsString.split("\t\t"));
            }
            playlistsCursor.close();
            playlist.setCount(getPlaylistTrackCount(playlistId));
            return playlist;
        }
        playlistsCursor.close();
        return null;
    }

    /**
     * @param playlistId the id by which to get the correct {@link org.tomahawk.libtomahawk.collection.Playlist}
     * @return the stored {@link org.tomahawk.libtomahawk.collection.Playlist} with playlistId as
     * its id
     */
    public Playlist getEmptyPlaylist(String playlistId,String playlistName) {
        /*String[] columns = new String[]{CollectionDb.PLAYLISTS_COLUMN_NAME,
                CollectionDb.PLAYLISTS_COLUMN_CURRENTREVISION,
                CollectionDb.PLAYLISTS_COLUMN_TOPARTISTS};
        Cursor playlistsCursor = mDb.query(CollectionDb.TABLE_PLAYLISTS,
                columns, CollectionDb.PLAYLISTS_COLUMN_ID + " = ?",
                new String[]{playlistId}, null, null, null);*/
        //if (playlistsCursor.moveToFirst()) {
            Playlist playlist = Playlist.get(playlistId);
            playlist.setName(playlistName);
            //playlist.setCurrentRevision(playlistsCursor.getString(1));
            //playlist.setHatchetId(playlistsCursor.getString(2));
            //String rawTopArtistsString = playlistsCursor.getString(2);
            /*if (rawTopArtistsString != null && rawTopArtistsString.length() > 0) {
                playlist.setTopArtistNames(rawTopArtistsString.split("\t\t"));
            }
            playlistsCursor.close();*/
            playlist.setCount(getPlaylistTrackCount(playlistId));
            return playlist;
        //}
        //playlistsCursor.close();
        //return null;
    }

    /**
     * @param playlistId the id by which to get the correct {@link org.tomahawk.libtomahawk.collection.Playlist}
     * @return the stored {@link org.tomahawk.libtomahawk.collection.Playlist} with playlistId as
     * its id
     */
    public long getPlaylistTrackCount(String playlistId) {
        long trackCount = -1;
        //String[] columns = new String[]{CollectionDb.PLAYLISTS_COLUMN_TRACKCOUNT};

        String[] mPlaylistMemberCols = new String[] {
                MediaStore.Audio.Playlists.Members._ID,
                MediaStore.Audio.Media.TITLE, MediaStore.Audio.Media.DATA,
                MediaStore.Audio.Media.ALBUM, MediaStore.Audio.Media.ARTIST,
                MediaStore.Audio.Media.ARTIST_ID,
                MediaStore.Audio.Media.DURATION,
                MediaStore.Audio.Playlists.Members.PLAY_ORDER,
                MediaStore.Audio.Playlists.Members.AUDIO_ID,
                MediaStore.Audio.Media.IS_MUSIC };

        Uri uri = MediaStore.Audio.Playlists.Members.getContentUri(
                "external", Long.valueOf(playlistId));
        StringBuilder where = new StringBuilder();
        where.append(MediaStore.Audio.Media.TITLE + " != ''");

        String mSortOrder = MediaStore.Audio.Playlists.Members.DEFAULT_SORT_ORDER;

        Cursor playlistsCursor = TomahawkApp.getContext().getContentResolver().query(uri, mPlaylistMemberCols,
                where.toString(), null, mSortOrder);
        /*mDb.query(CollectionDb.TABLE_PLAYLISTS,
                columns, CollectionDb.PLAYLISTS_COLUMN_ID + " = ?",
                new String[]{playlistId}, null, null, null);*/
        if (playlistsCursor.moveToFirst()) {
            /*if (playlistsCursor.isNull(0)) {
                // if no trackcount is stored, we calculate it and store it
                trackCount = DatabaseUtils.queryNumEntries(mDb,
                        CollectionDb.TABLE_TRACKS,
                        CollectionDb.TRACKS_PLAYLISTID + " = ?",
                        new String[]{playlistId});
                mDb.beginTransaction();
                ContentValues values = new ContentValues();
                values.put(CollectionDb.PLAYLISTS_COLUMN_TRACKCOUNT, trackCount);
                mDb.update(CollectionDb.TABLE_PLAYLISTS, values,
                        CollectionDb.PLAYLISTS_COLUMN_ID + " = ?",
                        new String[]{playlistId});
                mDb.setTransactionSuccessful();
                mDb.endTransaction();
            } else {
                trackCount = playlistsCursor.getLong(0);
            }*/
            trackCount = playlistsCursor.getCount();
        }
        playlistsCursor.close();
        return trackCount;
    }

    private Set<String> mResolvingTopArtistNames = new HashSet<>();
    /**
     * @return every stored {@link org.tomahawk.libtomahawk.collection.Playlist} in the database
     */
    public List<Playlist> getPlaylists(int sortPosition) {
        final List<Playlist> playListList = new ArrayList<>();
        String[] columns = new String[]{CollectionDb.PLAYLISTS_COLUMN_ID};

        Cursor playlistsCursor = MusicCollectionCursor.getAllUniquePlaylists(TomahawkApp.getContext(),sortPosition);//mDb.query(CollectionDb.TABLE_PLAYLISTS,
                /*columns, CollectionDb.PLAYLISTS_COLUMN_ID + " != ?",
                new String[]{LOVEDITEMS_PLAYLIST_ID}, null, null, null);*/
        playlistsCursor.moveToFirst();
        while (!playlistsCursor.isAfterLast()) {
            final Playlist playlist = getEmptyPlaylist(playlistsCursor.getString(0),playlistsCursor.getString(1));
            if (playlist != null) {

                playListList.add(playlist);
            }
            playlistsCursor.moveToNext();
        }
        playlistsCursor.close();
        Collections.sort(playListList, new AlphaComparator());
        User.getSelf().done(new DoneCallback<User>() {
            @Override
            public void onDone(User user) {
                user.setPlaylists(playListList);
            }
        });
        return playListList;
    }

    public synchronized void addTracks(ScriptResolverTrack[] tracks) {
        long time = System.currentTimeMillis();
        mDb.beginTransaction();

        // Check if we want to store the album as a compilation album (with artist "Various Artists")
        Map<String, Set<String>> albumArtists = new HashMap<>();
        Map<String, Set<String>> genreArtists = new HashMap<>();
        for (ScriptResolverTrack track : tracks) {
            addFolder(track.url);
            if (track.artist == null) {
                track.artist = "";
            }
            if (track.artistDisambiguation == null) {
                track.artistDisambiguation = "";
            }
            if (track.genre == null){
                track.genre = "";
            }
            if (track.genreDisambiguation == null){
                track.genreDisambiguation = "";
            }
            if (track.album == null) {
                track.album = "";
            }
            if (track.albumArtist == null) {
                track.albumArtist = "";
            }
            if (track.albumArtistDisambiguation == null) {
                track.albumArtistDisambiguation = "";
            }
            if (track.track == null) {
                track.track = "";
            }

            if (track.playListName == null){
                track.playListName = "";
            }

            if (track.playListId == null){
                track.playListId = "";
            }

            Set<String> artists = albumArtists.get(track.album);
            if (artists == null) {
                artists = new HashSet<>();
                albumArtists.put(track.album, artists);
            }
            if (artists.size() < 2) {
                artists.add(track.artist);
            }

            Set<String> genres = genreArtists.get(track.album);
            if (genres == null) {
                genres = new HashSet<>();
                genreArtists.put(track.album, genres);
            }
            if (genres.size() < 2) {
                genres.add(track.genre);
            }
        }

        Map<String, Long> mArtistLastModifiedMap = new HashMap<>();
        // First we insert all artists and albumArtists
        for (ScriptResolverTrack track : tracks) {
            if (albumArtists.get(track.album).size() > 1) {
                ContentValues values = new ContentValues();
                values.put(ARTISTS_ARTIST, Artist.COMPILATION_ARTIST.getName());
                values.put(ARTISTS_ARTISTDISAMBIGUATION, "");
                String artistKey = Artist.COMPILATION_ARTIST.getName() + "♠" + "";
                Long lastModified = mArtistLastModifiedMap.get(artistKey);
                if (lastModified == null || lastModified < track.lastModified) {
                    mArtistLastModifiedMap.put(artistKey, track.lastModified);
                    lastModified = track.lastModified;
                }
                values.put(ARTISTS_LASTMODIFIED, lastModified);
                mDb.insert(TABLE_ARTISTS, null, values);
            }
            ContentValues values = new ContentValues();
            values.put(ARTISTS_ARTIST, track.artist);
            values.put(ARTISTS_ARTISTDISAMBIGUATION, track.artistDisambiguation);
            String artistKey = track.artist + "♠" + track.artistDisambiguation;
            Long lastModified = mArtistLastModifiedMap.get(artistKey);
            if (lastModified == null || lastModified < track.lastModified) {
                mArtistLastModifiedMap.put(artistKey, track.lastModified);
                lastModified = track.lastModified;
            }
            values.put(ARTISTS_LASTMODIFIED, lastModified);
            mDb.insert(TABLE_ARTISTS, null, values);
            values = new ContentValues();
            values.put(ALBUMARTISTS_ALBUMARTIST, track.albumArtist);
            values.put(ALBUMARTISTS_ALBUMARTISTDISAMBIGUATION, track.albumArtistDisambiguation);
            values.put(ALBUMARTISTS_LASTMODIFIED, lastModified);
            mDb.insert(TABLE_ALBUMARTISTS, null, values);
        }

        Map<String, Integer> cachedArtists = new HashMap<>();
        Map<Integer, String> cachedArtistIds = new HashMap<>();
        Cursor cursor = mDb.query(TABLE_ARTISTS,
                new String[]{ID, ARTISTS_ARTIST, ARTISTS_ARTISTDISAMBIGUATION},
                null, null, null, null, null);
        try {
            cursor.moveToFirst();
            if (!cursor.isAfterLast()) {
                do {
                    cachedArtists.put(concatKeys(cursor.getString(1), cursor.getString(2)),
                            cursor.getInt(0));
                    cachedArtistIds.put(cursor.getInt(0), cursor.getString(1));
                } while (cursor.moveToNext());
            }
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }

        Map<String, Long> mGenreLastModifiedMap = new HashMap<>();
        for (ScriptResolverTrack track : tracks) {
            if (genreArtists.get(track.album).size() > 1) {
                ContentValues values = new ContentValues();
                values.put(GENRES_ARTIST, track.genre);
                values.put(GENRES_ARTISTDISAMBIGUATION, track.genreDisambiguation);
                String genreKey = track.genre + "♠" + "";
                Long lastModified = mGenreLastModifiedMap.get(genreKey);
                if (lastModified == null || lastModified < track.lastModified) {
                    mGenreLastModifiedMap.put(genreKey, track.lastModified);
                    lastModified = track.lastModified;
                }
                values.put(GENRES_LASTMODIFIED, lastModified);
                mDb.insert(TABLE_GENRES, null, values);
            }
            ContentValues values = new ContentValues();
            values.put(GENRES_ARTIST, track.genre);
            values.put(GENRES_ARTISTDISAMBIGUATION, track.genreDisambiguation);
            String genreKey = track.genre + "♠" + track.genreDisambiguation;
            Long lastModified = mGenreLastModifiedMap.get(genreKey);
            if (lastModified == null || lastModified < track.lastModified) {
                mArtistLastModifiedMap.put(genreKey, track.lastModified);
                lastModified = track.lastModified;
            }
            values.put(GENRES_LASTMODIFIED, lastModified);
            mDb.insert(TABLE_GENRES, null, values);
            values = new ContentValues();
            values.put(ALBUMGENRES_ALBUMGENRE, track.albumArtist);
            values.put(ALBUMGENRES_ALBUMGENREDISAMBIGUATION, track.albumArtistDisambiguation);
            values.put(ALBUMGENRES_LASTMODIFIED, lastModified);
            mDb.insert(TABLE_ALBUMGENRES, null, values);
        }

        Map<String, Integer> cachedGenres = new HashMap<>();
        Map<Integer, String> cachedGenreIds = new HashMap<>();
        cursor = mDb.query(TABLE_GENRES,
                new String[]{ID, GENRES_ARTIST, GENRES_ARTISTDISAMBIGUATION},
                null, null, null, null, null);
        try {
            cursor.moveToFirst();
            if (!cursor.isAfterLast()) {
                do {
                    cachedGenres.put(concatKeys(cursor.getString(1), cursor.getString(2)),
                            cursor.getInt(0));
                    cachedGenreIds.put(cursor.getInt(0), cursor.getString(1));
                } while (cursor.moveToNext());
            }
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
        Map<String, Long> mAlbumLastModifiedMap = new HashMap<>();
        for (ScriptResolverTrack track : tracks) {
            ContentValues values = new ContentValues();
            values.put(ALBUMS_ALBUM, track.album);
            int albumArtistId;
            if (albumArtists.get(track.album).size() == 1) {
                albumArtistId = cachedArtists.get(
                        concatKeys(track.artist, track.artistDisambiguation));
            } else {
                albumArtistId = cachedArtists.get(
                        concatKeys(Artist.COMPILATION_ARTIST.getName(), ""));
            }
            values.put(ALBUMS_ALBUMARTISTID, albumArtistId);
            int albumGenreId;
            if (genreArtists.get(track.album).size() == 1) {
                albumGenreId = cachedGenres.get(
                        concatKeys(track.genre, track.genreDisambiguation));
            } else {
                albumGenreId = cachedGenres.get(
                        concatKeys(track.genre, ""));
            }
            values.put(ALBUMS_GENREARTISTID,albumGenreId);
            values.put(ALBUMS_IMAGEPATH, track.imagePath);
            String artistKey = track.album + "♠" + albumArtistId;
            Long lastModified = mAlbumLastModifiedMap.get(artistKey);
            if (lastModified == null || lastModified < track.lastModified) {
                mAlbumLastModifiedMap.put(artistKey, track.lastModified);
                lastModified = track.lastModified;
            }
            values.put(ALBUMS_LASTMODIFIED, lastModified);
            mDb.insert(TABLE_ALBUMS, null, values);
        }

        Map<String, Integer> cachedAlbums = new HashMap<>();
        Map<Integer, String> cachedAlbumIds = new HashMap<>();
        cursor = mDb.query(TABLE_ALBUMS,
                new String[]{ID, ALBUMS_ALBUM, ALBUMS_ALBUMARTISTID},
                null, null, null, null, null);
        try {
            cursor.moveToFirst();
            if (!cursor.isAfterLast()) {
                do {
                    cachedAlbums.put(concatKeys(cursor.getString(1), cursor.getString(2)),
                            cursor.getInt(0));
                    cachedAlbumIds.put(cursor.getInt(0), cursor.getString(1));
                } while (cursor.moveToNext());
            }
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }

        for (ScriptResolverTrack track : tracks) {
            ContentValues values = new ContentValues();
            Log.d("Test","we_"+track.folderId);
            String path = track.url.substring(0, track.url.lastIndexOf("/"));
            track.folderId = getFolderID(path);
            if(track.folderId != 0) {
                Folder folder = getFolderById(track.folderId);
                values.put(FOLDERS_ALBUM, folder.getFolderName());
                values.put(FOLDERS_PATH, folder.getFolderPath());
                values.put(FOLDERS_LASTMODIFIED,track.lastModified);
                mDb.insert(TABLE_FOLDERS, null, values);
                int id = (int) mDb.insertWithOnConflict(TABLE_FOLDERS, null, values, SQLiteDatabase.CONFLICT_IGNORE);
                if (id == -1) {
                    mDb.update(TABLE_FOLDERS, values, ID+"=?", new String[] {String.valueOf(folder.getFolderId())});
                }
                values = new ContentValues();
            }
            int albumArtistId = 0;
            if (albumArtists.get(track.album).size() == 1) {
                albumArtistId = cachedArtists.get(
                        concatKeys(track.artist, track.artistDisambiguation));
            } else {
                albumArtistId = cachedArtists.get(
                        concatKeys(Artist.COMPILATION_ARTIST.getName(), ""));
            }

            int albumGenreId;
            if (genreArtists.get(track.album).size() == 1) {
                albumGenreId = cachedGenres.get(
                        concatKeys(track.genre, track.genreDisambiguation));
            } else {
                albumGenreId = cachedGenres.get(
                        concatKeys(track.genre, ""));
            }
            int artistId = cachedArtists.get(concatKeys(track.artist, track.artistDisambiguation));
            int albumId = cachedAlbums.get(concatKeys(track.album, albumArtistId));
            int genreId = albumGenreId;//cachedGenres.get(concatKeys(track.genre,albumGenreId));
            values.put(ARTISTALBUMS_ARTISTID, artistId);
            values.put(ARTISTALBUMS_ALBUMID, albumId);
            mDb.insert(TABLE_ARTISTALBUMS, null, values);
            values = new ContentValues();
            values.put(GENREALBUMS_GENREID, genreId);
            values.put(GENREALBUMS_ALBUMID, albumId);
            mDb.insert(TABLE_GENREALBUMS, null, values);
            values = new ContentValues();
            values.put(PLAYLISTS_COLUMN_ID,track.playListId);
            values.put(PLAYLISTS_COLUMN_NAME,track.playListName);
            values.put(PLAYLISTS_COLUMN_CURRENTTRACKINDEX,Integer.valueOf(track.playOrder));
            values.put(PLAYLISTS_COLUMN_CURRENTREVISION, "");
            values.put(PLAYLISTS_COLUMN_TRACKCOUNT, track.playlist_count);
            long inserted = mDb.insert(TABLE_PLAYLISTS, null, values);
            if (inserted != -1){
                DatabaseHelper.PlaylistsUpdatedEvent event = new DatabaseHelper.PlaylistsUpdatedEvent();
                event.mPlaylistId = track.playListId;
                EventBus.getDefault().post(event);
            }
            values = new ContentValues();
            values.put(TRACKS_TRACK, track.track);
            values.put(TRACKS_ARTISTID, artistId);
            values.put(TRACKS_ALBUMID, albumId);
            values.put(TRACKS_GENREID,genreId);
            if (track.folderId!=0)
            values.put(TRACKS_FOLDERID,track.folderId);
            values.put(TRACKS_URL, track.url);
            values.put(TRACKS_DURATION, (int) track.duration);
            values.put(TRACKS_LINKURL, track.linkUrl);
            values.put(TRACKS_ALBUMPOS, track.albumPos);
            values.put(TRACKS_YEAR, track.year);
            values.put(TRACKS_FAVOURITE, track.favourite);
            values.put(TRACKS_ALBUM_COUNT, track.album_count);
            values.put(TRACKS_SONG_COUNT, track.song_count);
            values.put(TRACKS_GENRE_COUNT, track.genre_count);
            values.put(TRACKS_LASTMODIFIED, track.lastModified);
            values.put(TRACKS_PLAYLISTID,track.playListId);
            values.put(TRACKS_PLAYORDER,track.songId);
            mDb.insert(TABLE_TRACKS, null, values);
        }

        mDb.setTransactionSuccessful();
        mDb.endTransaction();
        mInitialized = true;
        Log.d(TAG, "Added " + tracks.length + " tracks in " + (System.currentTimeMillis() - time)
                + "ms");
        SharedPreferences preferences =
                PreferenceManager.getDefaultSharedPreferences(TomahawkApp.getContext());
        preferences.edit().putLong(mLastUpdateStorageKey, System.currentTimeMillis()).apply();
        preferences.edit().putBoolean("Scan",false).apply();
    }

    public synchronized void addFolder(String absolutePath) {
        Log.d(TAG, "Adding mediaDir: " + absolutePath);
        mDb.beginTransaction();
        /*mDatabase.delete(SensitiveSQLiteHelper.TABLE_FOLDER,
                SensitiveSQLiteHelper.FOLDER_PATH + " LIKE ? || '%'", new String[]{absolutePath});*/
        Log.d(TAG, "Removed mediaDir from white/blacklist: " + absolutePath);
        // if (!isMediaDirWhiteListed(absolutePath)) {
        try {
            ContentValues values = new ContentValues();
            String path = absolutePath.substring(0, absolutePath.lastIndexOf("/"));
            String folderName = path.substring(path.lastIndexOf("/") + 1, path.length());
             values.put(FOLDERS_ALBUM, folderName);
            values.put(FOLDERS_PATH, path);
            mDb.insert(TABLE_FOLDERS, null, values);
            if (!getFolder(path))
                mDb.insertOrThrow(TomahawkSQLiteHelper.TABLE_FOLDER, null, values);
            Log.d(TAG, "Added mediaDir to whitelist: " + path);
            //}
        }catch (Exception e){
            e.printStackTrace();
        }
        mDb.setTransactionSuccessful();
        mDb.endTransaction();
    }
    public synchronized Folder getFolderById(int folderId) {

        Cursor cursor;
        Folder folder = null;
        //String replacedPath = path.replaceAll(Pattern.quote("%20")," ");
        try {
            cursor = mDb.query(
                    TABLE_FOLDERS,
                    new String[]{
                            ID,
                            FOLDERS_ALBUM,
                            FOLDERS_PATH
                    },
                    ID + "=?",
                    new String[]{String.valueOf(folderId)},
                    null, null, null);
        } catch (IllegalArgumentException e) {
            // java.lang.IllegalArgumentException: the bind value at index 1 is null
            return folder;
        }
        if (cursor.moveToFirst()) {
            int folder_Id = cursor.getInt(cursor.getColumnIndex(ID));
            String folderName = cursor.getString(cursor.getColumnIndex(FOLDERS_ALBUM));
            String folderPath = cursor.getString(cursor.getColumnIndex(FOLDERS_PATH));
            folder = Folder.get(folderPath);
            folder.setFolderId(folder_Id);
            folder.setFolderName(folderName);
            folder.setFolderPath(folderPath);
        }
        cursor.close();
        return folder;
    }

    public synchronized int getFolderID(String path) {

        Cursor cursor;
        int folderId = 0;
        String replacedPath = path.replaceAll(Pattern.quote("%20")," ");
        try {
            cursor = mDb.query(
                    TABLE_FOLDERS,
                    new String[]{
                           ID
                    },
                    TomahawkSQLiteHelper.FOLDER_PATH + "=?",
                    new String[]{replacedPath},
                    null, null, null);
        } catch (IllegalArgumentException e) {
            // java.lang.IllegalArgumentException: the bind value at index 1 is null
            return folderId;
        }
        if (cursor.moveToFirst()) {
            folderId = cursor.getInt(cursor.getColumnIndex(ID));
        }
        cursor.close();
        return folderId;
    }
    public synchronized boolean getFolder(String location) {
        Cursor cursor;
        boolean folderAvailable = false;
        cursor = mDb.query(
                TABLE_FOLDERS,
                new String[]{FOLDERS_PATH},
                FOLDERS_PATH + "=?",
                new String[]{location},
                null, null, null);
        if (cursor.moveToFirst()) {
            String folderPath = cursor.getString(cursor.getColumnIndex(FOLDERS_PATH));
            if (folderPath != null && folderPath.length() > 1 ) {
                folderAvailable = true;
            }
        }
        cursor.close();
        return folderAvailable;
    }
    public synchronized void wipe() {
        mDb.execSQL("DROP TABLE IF EXISTS `" + TABLE_ARTISTS + "`;");
        mDb.execSQL(CREATE_TABLE_ARTISTS);
        mDb.execSQL("DROP TABLE IF EXISTS `" + TABLE_ALBUMARTISTS + "`;");
        mDb.execSQL(CREATE_TABLE_ALBUMARTISTS);
        mDb.execSQL("DROP TABLE IF EXISTS `" + TABLE_ALBUMS + "`;");
        mDb.execSQL(CREATE_TABLE_ALBUMS);
        mDb.execSQL("DROP TABLE IF EXISTS `" + TABLE_ARTISTALBUMS + "`;");
        mDb.execSQL(CREATE_TABLE_ARTISTALBUMS);
        mDb.execSQL("DROP TABLE IF EXISTS `" + TABLE_TRACKS + "`;");
        mDb.execSQL(CREATE_TABLE_TRACKS);
    }

    /**
     * Convenience method. Uses a default set of fields.
     */
    public synchronized Cursor tracks(WhereInfo where, String[] orderBy,String whereStr) {

        String[] fields = new String[]{ARTISTS_ARTIST, TRACKS_LINKURL, ALBUMS_ALBUM,
                TRACKS_TRACK, TRACKS_DURATION, TRACKS_URL, TRACKS_PLAYORDER, TRACKS_ALBUMPOS,
                TRACKS_LASTMODIFIED,TRACKS_LIKEDISLIKE};
        return tracks(where, orderBy, fields,whereStr);
    }

    public synchronized Cursor tracksFolder(WhereInfo where, String[] orderBy) {
        String[] fields = new String[]{ARTISTS_ARTIST, TRACKS_LINKURL, ALBUMS_ALBUM,
                TRACKS_TRACK, TRACKS_DURATION, TRACKS_URL, TRACKS_PLAYORDER, TRACKS_ALBUMPOS,
                TRACKS_LASTMODIFIED,FOLDERS_ALBUM};
        return tracksFolder(where, orderBy, fields);
    }

    public synchronized Cursor tracksFolder(WhereInfo where, String[] orderBy, String[] fields) {
        List<JoinInfo> joinInfos = new ArrayList<>();
        JoinInfo joinInfo = new JoinInfo();
        joinInfo.table = TABLE_FOLDERS;
        joinInfo.conditions.put(TABLE_TRACKS + "." + TRACKS_FOLDERID, TABLE_FOLDERS + "." + ID);
        joinInfos.add(joinInfo);
        joinInfo = new JoinInfo();
        joinInfo.table = TABLE_ALBUMS;
        joinInfo.conditions.put(TABLE_TRACKS + "." + TRACKS_ALBUMID, TABLE_ALBUMS + "." + ID);
        joinInfos.add(joinInfo);
        joinInfo = new JoinInfo();
        joinInfo.table = TABLE_ARTISTS;
        joinInfo.conditions.put(TABLE_TRACKS + "." + TRACKS_ARTISTID, TABLE_ARTISTS + "." + ID);
        joinInfos.add(joinInfo);

        return sqlSelect(TABLE_TRACKS, fields, where, joinInfos, orderBy, null,null);
    }

    public synchronized Cursor tracks(WhereInfo where, String[] orderBy, String[] fields,String whereStr) {
        List<JoinInfo> joinInfos = new ArrayList<>();
        JoinInfo joinInfo = new JoinInfo();
        joinInfo.table = TABLE_ARTISTS;
        joinInfo.conditions.put(TABLE_TRACKS + "." + TRACKS_ARTISTID, TABLE_ARTISTS + "." + ID);
        joinInfos.add(joinInfo);
        joinInfo = new JoinInfo();
        joinInfo.table = TABLE_ALBUMS;
        joinInfo.conditions.put(TABLE_TRACKS + "." + TRACKS_ALBUMID, TABLE_ALBUMS + "." + ID);
        joinInfos.add(joinInfo);
        return sqlSelect(TABLE_TRACKS, fields, where, joinInfos, orderBy, null, whereStr);
    }

    public synchronized Cursor tracksGenre(WhereInfo where, String[] orderBy) {
        String[] fields = new String[]{GENRES_ARTIST, TRACKS_LINKURL, ALBUMS_ALBUM,
                TRACKS_TRACK, TRACKS_DURATION, TRACKS_URL, TRACKS_PLAYORDER, TRACKS_ALBUMPOS,
                TRACKS_LASTMODIFIED};
        return tracksGenre(where, orderBy, fields);
    }

    public synchronized Cursor tracksGenre(WhereInfo where, String[] orderBy, String[] fields) {
        List<JoinInfo> joinInfos = new ArrayList<>();
        JoinInfo joinInfo = new JoinInfo();
        joinInfo.table = TABLE_GENRES;
        joinInfo.conditions.put(TABLE_TRACKS + "." + TRACKS_GENREID, TABLE_GENRES + "." + ID);
        joinInfos.add(joinInfo);
        joinInfo = new JoinInfo();
        joinInfo.table = TABLE_ALBUMS;
        joinInfo.conditions.put(TABLE_TRACKS + "." + TRACKS_ALBUMID, TABLE_ALBUMS + "." + ID);
        joinInfos.add(joinInfo);
        return sqlSelect(TABLE_TRACKS, fields, where, joinInfos, orderBy, null,null);
    }
    public synchronized long tracksCurrentRevision() {
        String[] fields = new String[]{TRACKS_LASTMODIFIED};
        long currentRevision = -1;
        Cursor cursor = null;
        try {
            cursor = sqlSelect(TABLE_TRACKS, fields, null, null,
                    new String[]{TRACKS_LASTMODIFIED + " DESC"},null,null);
            if (cursor.moveToFirst()) {
                currentRevision = cursor.getLong(0);
            } else {
                Log.e(TAG, "tracksCurrentRevision - no tracks in table!");
                return -1;
            }
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
        return currentRevision;
    }

    public synchronized Cursor albums(String[] orderBy,WhereInfo where) {
        String[] fields = new String[]{ALBUMS_ALBUM, ARTISTS_ARTIST, ARTISTS_ARTISTDISAMBIGUATION,
                ALBUMS_IMAGEPATH, ALBUMS_LASTMODIFIED,ALBUMS_LIKEDISLIKE};
        List<JoinInfo> joinInfos = new ArrayList<>();
        JoinInfo joinInfo = new JoinInfo();
        joinInfo.table = TABLE_ARTISTS;
        joinInfo.conditions.put(
                TABLE_ALBUMS + "." + ALBUMS_ALBUMARTISTID, TABLE_ARTISTS + "." + ID);
        /*if (where !=null){
            for (String whereKey : where.where.keySet()) {
                String[] whereValues = where.where.get(whereKey);
                for (String whereValue : whereValues) {
                    joinInfo.conditions.put(
                            TABLE_ARTISTS + "." + ARTISTS_ARTIST + " LIKE ", whereValue);

                }
            }

        }*/
        joinInfos.add(joinInfo);
        return sqlSelect(TABLE_ALBUMS, fields, where, joinInfos, orderBy,null,null);
    }

    public synchronized Cursor folders(String[] orderBy,WhereInfo where) {
        String[] fields = new String[]{FOLDERS_ALBUM, ALBUMS_ALBUM, ALBUMS_ALBUMARTISTID,
                ALBUMS_IMAGEPATH, FOLDERS_PATH,TRACKS_FOLDERID,FOLDERS_LIKEDISLIKE};
        List<JoinInfo> joinInfos = new ArrayList<>();
        JoinInfo joinInfo = new JoinInfo();
        joinInfo.table = TABLE_TRACKS;
        joinInfo.conditions.put(
                TABLE_FOLDERS + "." + ID, TABLE_TRACKS + "." + TRACKS_FOLDERID);
        joinInfos.add(joinInfo);
        joinInfo = new JoinInfo();
        joinInfo.table = TABLE_ALBUMS;
        joinInfo.conditions.put(
                TABLE_TRACKS + "." + TRACKS_ALBUMID, TABLE_ALBUMS + "." + ID);
        joinInfos.add(joinInfo);
        return sqlSelect(TABLE_FOLDERS, fields, where, joinInfos, orderBy,FOLDERS_PATH,null);
    }

    public synchronized Cursor artists(String[] orderBy, WhereInfo where) {
        String[] fields = new String[]{ARTISTS_ARTIST, ARTISTS_ARTISTDISAMBIGUATION,
                ARTISTS_LASTMODIFIED,ARTISTS_LIKEDISLIKE};
        return sqlSelect(TABLE_ARTISTS, fields, where, null, orderBy,null,null);
    }

    public synchronized Cursor genres(String[] orderBy, WhereInfo where) {
         String[] fields = new String[]{GENRES_ARTIST,GENRES_ARTISTDISAMBIGUATION,GENRES_LASTMODIFIED,GENRES_LIKEDISLIKE};
        return sqlSelect(TABLE_GENRES,fields,where,null,orderBy,null,null);
    }
    public synchronized Cursor albumArtists(String[] orderBy) {
        String[] fields = new String[]{ALBUMARTISTS_ALBUMARTIST,
                ALBUMARTISTS_ALBUMARTISTDISAMBIGUATION, ALBUMARTISTS_LASTMODIFIED};
        return sqlSelect(TABLE_ALBUMARTISTS, fields, null, null, orderBy,null,null);
    }
    public synchronized Cursor albumGenres(String[] orderBy) {
        String[] fields = new String[]{ALBUMGENRES_ALBUMGENRE,
                ALBUMGENRES_ALBUMGENREDISAMBIGUATION, ALBUMGENRES_LASTMODIFIED};
        return sqlSelect(TABLE_ALBUMGENRES, fields, null, null, orderBy,null,null);
    }

    public synchronized long artistCurrentRevision(String artist, String artistDisambiguation) {
        String[] fields = new String[]{ARTISTS_LASTMODIFIED};
        WhereInfo whereInfo = new WhereInfo();
        whereInfo.connection = "AND";
        whereInfo.where.put(ARTISTS_ARTIST, new String[]{artist});
        whereInfo.where.put(ARTISTS_ARTISTDISAMBIGUATION, new String[]{artistDisambiguation});
        long currentRevision = -1;
        Cursor cursor = null;
        try {
            cursor = sqlSelect(TABLE_ARTISTS, fields, whereInfo, null, null,null,null);
            if (cursor.moveToFirst()) {
                currentRevision = cursor.getLong(0);
            } else {
                Log.e(TAG, "artistCurrentRevision - Couldn't find artist with given name!");
                return -1;
            }
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
        return currentRevision;
    }

    public synchronized Cursor artistAlbums(String artist, String artistDisambiguation) {
        String[] fields = new String[]{ID};
        WhereInfo whereInfo = new WhereInfo();
        whereInfo.connection = "AND";
        whereInfo.where.put(ARTISTS_ARTIST, new String[]{artist});
        whereInfo.where.put(ARTISTS_ARTISTDISAMBIGUATION, new String[]{artistDisambiguation});
        int artistId;
        Cursor cursor = null;
        try {
            cursor = sqlSelect(TABLE_ARTISTS, fields, whereInfo, null, null,null,null);
            if (cursor.moveToFirst()) {
                artistId = cursor.getInt(0);
            } else {
                Log.e(TAG, "artistAlbums - Couldn't find artist with given name!");
                return null;
            }
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
        fields = new String[]{ALBUMS_ALBUM, ARTISTS_ARTIST, ARTISTS_ARTISTDISAMBIGUATION,
                ALBUMS_IMAGEPATH, ALBUMS_LASTMODIFIED};
        whereInfo = new WhereInfo();
        whereInfo.connection = "AND";
        whereInfo.where.put(ARTISTALBUMS_ARTISTID, new String[]{String.valueOf(artistId)});
        List<JoinInfo> joinInfos = new ArrayList<>();
        JoinInfo joinInfo = new JoinInfo();
        joinInfo.table = TABLE_ALBUMS;
        joinInfo.conditions.put(
                TABLE_ARTISTALBUMS + "." + ARTISTALBUMS_ALBUMID, TABLE_ALBUMS + "." + ID);
        joinInfos.add(joinInfo);
        joinInfo = new JoinInfo();
        joinInfo.table = TABLE_ARTISTS;
        joinInfo.conditions.put(
                TABLE_ALBUMS + "." + ALBUMS_ALBUMARTISTID, TABLE_ARTISTS + "." + ID);
        joinInfos.add(joinInfo);
        return sqlSelect(TABLE_ARTISTALBUMS, fields, whereInfo, joinInfos,
                new String[]{ALBUMS_ALBUM},null,null);
    }

    public synchronized Cursor genreAlbums(String genre, String genreDisambiguation) {
        String[] fields = new String[]{ID};
        WhereInfo whereInfo = new WhereInfo();
        whereInfo.connection = "AND";
        whereInfo.where.put(GENRES_ARTIST, new String[]{genre});
        whereInfo.where.put(GENRES_ARTISTDISAMBIGUATION, new String[]{genreDisambiguation});
        int artistId;
        Cursor cursor = null;
        try {
            cursor = sqlSelect(TABLE_GENRES, fields, whereInfo, null, null,null,null);
            if (cursor.moveToFirst()) {
                artistId = cursor.getInt(0);
            } else {
                Log.e(TAG, "artistAlbums - Couldn't find artist with given name!");
                return null;
            }
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
        fields = new String[]{ALBUMS_ALBUM, GENRES_ARTIST, GENRES_ARTISTDISAMBIGUATION,
                ALBUMS_IMAGEPATH, ALBUMS_LASTMODIFIED};
        whereInfo = new WhereInfo();
        whereInfo.connection = "AND";
        whereInfo.where.put(GENREALBUMS_GENREID, new String[]{String.valueOf(artistId)});
        List<JoinInfo> joinInfos = new ArrayList<>();
        JoinInfo joinInfo = new JoinInfo();
        joinInfo.table = TABLE_ALBUMS;
        joinInfo.conditions.put(
                TABLE_GENREALBUMS + "." + GENREALBUMS_ALBUMID, TABLE_ALBUMS + "." + ID);
        joinInfos.add(joinInfo);
        joinInfo = new JoinInfo();
        joinInfo.table = TABLE_GENRES;
        joinInfo.conditions.put(
                TABLE_ALBUMS + "." + ALBUMS_GENREARTISTID, TABLE_GENRES + "." + ID);
        joinInfos.add(joinInfo);

        return sqlSelect(TABLE_GENREALBUMS, fields, whereInfo, joinInfos,
                new String[]{ALBUMS_ALBUM},null,null);
    }

    public synchronized Cursor folderAlbums(String genre, String genreDisambiguation) {
        String[] fields = new String[]{ID};
        WhereInfo whereInfo = new WhereInfo();
        whereInfo.connection = "AND";
        whereInfo.where.put(FOLDERS_ALBUM, new String[]{genre});
        //whereInfo.where.put(FOLDERS_ALBUMARTISTID, new String[]{genreDisambiguation});
        int artistId;
        Cursor cursor = null;
        try {
            cursor = sqlSelect(TABLE_GENRES, fields, whereInfo, null, null,null,null);
            if (cursor.moveToFirst()) {
                artistId = cursor.getInt(0);
            } else {
                Log.e(TAG, "artistAlbums - Couldn't find artist with given name!");
                return null;
            }
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
        fields = new String[]{ALBUMS_ALBUM, ARTISTS_ARTIST, ARTISTS_ARTISTDISAMBIGUATION,
                ALBUMS_IMAGEPATH, ALBUMS_LASTMODIFIED};
        whereInfo = new WhereInfo();
        whereInfo.connection = "AND";
        whereInfo.where.put(ARTISTALBUMS_ARTISTID, new String[]{String.valueOf(artistId)});
        List<JoinInfo> joinInfos = new ArrayList<>();
        JoinInfo joinInfo = new JoinInfo();
        joinInfo.table = TABLE_ALBUMS;
        joinInfo.conditions.put(
                TABLE_ARTISTALBUMS + "." + ARTISTALBUMS_ALBUMID, TABLE_ALBUMS + "." + ID);
        joinInfos.add(joinInfo);
        joinInfo = new JoinInfo();
        joinInfo.table = TABLE_ARTISTS;
        joinInfo.conditions.put(
                TABLE_ALBUMS + "." + ALBUMS_ALBUMARTISTID, TABLE_ARTISTS + "." + ID);
        joinInfos.add(joinInfo);
        return sqlSelect(TABLE_ARTISTALBUMS, fields, whereInfo, joinInfos,
                new String[]{ALBUMS_ALBUM},null,null);
    }

    public synchronized long albumCurrentRevision(String album, String albumArtist,
            String albumArtistDisambiguation) {
        String[] fields = new String[]{ID};
        WhereInfo whereInfo = new WhereInfo();
        whereInfo.connection = "AND";
        whereInfo.where.put(ARTISTS_ARTIST, new String[]{albumArtist});
        whereInfo.where.put(ARTISTS_ARTISTDISAMBIGUATION, new String[]{albumArtistDisambiguation});
        int artistId;
        Cursor cursor = null;
        try {
            cursor = sqlSelect(TABLE_ARTISTS, fields, whereInfo, null, null,null,null);
            if (cursor.moveToFirst()) {
                artistId = cursor.getInt(0);
            } else {
                Log.e(TAG, "albumCurrentRevision - Couldn't find artist with given name!");
                return -1;
            }
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
        fields = new String[]{ALBUMS_LASTMODIFIED};
        whereInfo = new WhereInfo();
        whereInfo.connection = "AND";
        whereInfo.where.put(ALBUMS_ALBUM, new String[]{album});
        whereInfo.where.put(ALBUMS_ALBUMARTISTID, new String[]{String.valueOf(artistId)});
        long currentRevision = -1;
        cursor = null;
        try {
            cursor = sqlSelect(TABLE_ALBUMS, fields, whereInfo, null, null,null,null);
            if (cursor.moveToFirst()) {
                currentRevision = cursor.getLong(0);
            } else {
                Log.e(TAG, "albumCurrentRevision - Couldn't find album with given name!");
                return -1;
            }
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
        return currentRevision;
    }

    public synchronized long artistCurrentRevision(String artist) {
        String[] fields = new String[]{ID,ARTISTS_LASTMODIFIED};
        WhereInfo whereInfo = new WhereInfo();
        whereInfo.connection = "AND";
        whereInfo.where.put(ARTISTS_ARTIST, new String[]{artist});
        int artistId;
        Cursor cursor = null;
        long currentRevision = -1;
        try {
            cursor = sqlSelect(TABLE_ARTISTS, fields, whereInfo, null, null,null,null);
            if (cursor.moveToFirst()) {
                artistId = cursor.getInt(0);
                currentRevision = cursor.getLong(1);
            } else {
                Log.e(TAG, "albumCurrentRevision - Couldn't find artist with given name!");
                return -1;
            }
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }

        return currentRevision;
    }

    public synchronized long genreCurrentRevision(String artist) {
        String[] fields = new String[]{ID,GENRES_LASTMODIFIED};
        WhereInfo whereInfo = new WhereInfo();
        whereInfo.connection = "AND";
        whereInfo.where.put(GENRES_ARTIST, new String[]{artist});
        int artistId;
        Cursor cursor = null;
        long currentRevision = -1;
        try {
            cursor = sqlSelect(TABLE_GENRES, fields, whereInfo, null, null,null,null);
            if (cursor.moveToFirst()) {
                artistId = cursor.getInt(0);
                currentRevision = cursor.getLong(1);
            } else {
                Log.e(TAG, "albumCurrentRevision - Couldn't find artist with given name!");
                return -1;
            }
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }

        return currentRevision;
    }

    public synchronized long folderCurrentRevision(String folder) {
        String[] fields = new String[]{ID,FOLDERS_LASTMODIFIED};
        WhereInfo whereInfo = new WhereInfo();
        whereInfo.connection = "AND";
        whereInfo.where.put(FOLDERS_PATH, new String[]{folder});
        int artistId;
        Cursor cursor = null;
        long currentRevision = -1;
        try {
            cursor = sqlSelect(TABLE_FOLDERS, fields, whereInfo, null, null,null,null);
            if (cursor.moveToFirst()) {
                artistId = cursor.getInt(0);
                currentRevision = cursor.getLong(1);
            } else {
                Log.e(TAG, "albumCurrentRevision - Couldn't find artist with given name!");
                return -1;
            }
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }

        return currentRevision;
    }
    public synchronized long genreCurrentRevision(String album, String albumArtist,
                                                  String albumArtistDisambiguation) {
        String[] fields = new String[]{ID};
        WhereInfo whereInfo = new WhereInfo();
        whereInfo.connection = "AND";
        whereInfo.where.put(GENRES_ARTIST, new String[]{albumArtist});
        whereInfo.where.put(GENRES_ARTISTDISAMBIGUATION, new String[]{albumArtistDisambiguation});
        int artistId;
        Cursor cursor = null;
        try {
            cursor = sqlSelect(TABLE_GENRES, fields, whereInfo, null, null,null,null);
            if (cursor.moveToFirst()) {
                artistId = cursor.getInt(0);
            } else {
                Log.e(TAG, "albumCurrentRevision - Couldn't find artist with given name!");
                return -1;
            }
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
        fields = new String[]{ALBUMS_LASTMODIFIED};
        whereInfo = new WhereInfo();
        whereInfo.connection = "AND";
        whereInfo.where.put(ALBUMS_ALBUM, new String[]{album});
        whereInfo.where.put(ALBUMS_ALBUMARTISTID, new String[]{String.valueOf(artistId)});
        long currentRevision = -1;
        cursor = null;
        try {
            cursor = sqlSelect(TABLE_ALBUMS, fields, whereInfo, null, null,null,null);
            if (cursor.moveToFirst()) {
                currentRevision = cursor.getLong(0);
            } else {
                Log.e(TAG, "albumCurrentRevision - Couldn't find album with given name!");
                return -1;
            }
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
        return currentRevision;
    }

    public synchronized Cursor albumTracks(String album, String albumArtist,
            String albumArtistDisambiguation) {
        String[] fields = new String[]{ID};
        WhereInfo whereInfo = new WhereInfo();
        whereInfo.connection = "AND";
        whereInfo.where.put(ARTISTS_ARTIST, new String[]{albumArtist});
        whereInfo.where.put(ARTISTS_ARTISTDISAMBIGUATION, new String[]{albumArtistDisambiguation});
        int artistId;
        Cursor cursor = null;
        try {
            cursor = sqlSelect(TABLE_ARTISTS, fields, whereInfo, null, null,null,null);
            if (cursor.moveToFirst()) {
                artistId = cursor.getInt(0);
            } else {
                Log.e(TAG, "albumTracks - Couldn't find artist with given name!");
                return null;
            }
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
        fields = new String[]{ID};
        whereInfo = new WhereInfo();
        whereInfo.connection = "AND";
        whereInfo.where.put(ALBUMS_ALBUM, new String[]{album});
        whereInfo.where.put(ALBUMS_ALBUMARTISTID, new String[]{String.valueOf(artistId)});
        int albumId;
        cursor = null;
        try {
            cursor = sqlSelect(TABLE_ALBUMS, fields, whereInfo, null, null,null,null);
            if (cursor.moveToFirst()) {
                albumId = cursor.getInt(0);
            } else {
                Log.e(TAG, "albumTracks - Couldn't find album with given name!");
                return null;
            }
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }

        whereInfo = new WhereInfo();
        whereInfo.connection = "AND";
        whereInfo.where.put(TRACKS_ALBUMID, new String[]{String.valueOf(String.valueOf(albumId))});
        return tracks(whereInfo, new String[]{TRACKS_ALBUMPOS},null);
    }

    public synchronized Cursor artistTracks(String artist) {
        String[] fields = new String[]{ID};
        WhereInfo whereInfo = new WhereInfo();
        whereInfo.connection = "AND";
        whereInfo.where.put(ARTISTS_ARTIST, new String[]{artist});
        int artistId;
        Cursor cursor = null;
        try {
            cursor = sqlSelect(TABLE_ARTISTS, fields, whereInfo, null, null,null,null);
            if (cursor.moveToFirst()) {
                artistId = cursor.getInt(0);
            } else {
                Log.e(TAG, "albumTracks - Couldn't find artist with given name!");
                return null;
            }
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
        whereInfo = new WhereInfo();
        whereInfo.connection = "AND";
        whereInfo.where.put(TRACKS_ARTISTID, new String[]{String.valueOf(String.valueOf(artistId))});
        return tracks(whereInfo, new String[]{TRACKS_ALBUMPOS},null);
    }

    public synchronized Cursor genreTracks(String genre) {
        String[] fields = new String[]{ID};
        WhereInfo whereInfo = new WhereInfo();
        whereInfo.connection = "AND";
        whereInfo.where.put(GENRES_ARTIST, new String[]{genre});
        int artistId;
        Cursor cursor = null;
        try {
            cursor = sqlSelect(TABLE_GENRES, fields, whereInfo, null, null,null,null);
            if (cursor.moveToFirst()) {
                artistId = cursor.getInt(0);
            } else {
                Log.e(TAG, "albumTracks - Couldn't find artist with given name!");
                return null;
            }
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
        whereInfo = new WhereInfo();
        whereInfo.connection = "AND";
        whereInfo.where.put(TRACKS_GENREID, new String[]{String.valueOf(String.valueOf(artistId))});
        return tracks(whereInfo, new String[]{TRACKS_ALBUMPOS},null);
    }

    public synchronized Cursor folderTracks(Folder folder) {
        //String[] fields = new String[]{FOLDERS_ID};
        WhereInfo whereInfo = new WhereInfo();
        whereInfo.connection = "AND";
        /*whereInfo.where.put(FOLDERS_ALBUM, new String[]{folder.getFolderName()});
        whereInfo.where.put(FOLDERS_PATH, new String[]{folder.getFolderPath()});
        */

        whereInfo.where.put(TRACKS_FOLDERID, new String[]{String.valueOf(folder.getFolderId())});
        /* int artistId;
        Cursor cursor = null;
        try {
            cursor = sqlSelect(TABLE_FOLDERS, fields, whereInfo, null, null,null);
            if (cursor.moveToFirst()) {
                artistId = cursor.getInt(0);
            } else {
                Log.e(TAG, "albumTracks - Couldn't find artist with given name!");
                return null;
            }
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }*/
        /*fields = new String[]{ID};
        whereInfo = new WhereInfo();
        whereInfo.connection = "AND";
        whereInfo.where.put(ALBUMS_ALBUM, new String[]{album});
        whereInfo.where.put(ALBUMS_ALBUMARTISTID, new String[]{String.valueOf(artistId)});
        int albumId;
        cursor = null;
        try {
            cursor = sqlSelect(TABLE_ALBUMS, fields, whereInfo, null, null,null);
            if (cursor.moveToFirst()) {
                albumId = cursor.getInt(0);
            } else {
                Log.e(TAG, "albumTracks - Couldn't find album with given name!");
                return null;
            }
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }*/

        /*whereInfo = new WhereInfo();
        whereInfo.connection = "AND";
        whereInfo.where.put(TRACKS_ALBUMID, new String[]{String.valueOf(String.valueOf(albumId))});*/
        return tracksFolder(whereInfo, new String[]{TRACKS_ALBUMPOS});
    }
    public synchronized Cursor albumGenres(String album, String albumArtist,
                                           String albumArtistDisambiguation) {
        String[] fields = new String[]{ID};
        WhereInfo whereInfo = new WhereInfo();
        whereInfo.connection = "AND";
        whereInfo.where.put(GENRES_ARTIST, new String[]{albumArtist});
        whereInfo.where.put(GENRES_ARTISTDISAMBIGUATION, new String[]{albumArtistDisambiguation});
        int artistId;
        Cursor cursor = null;
        try {
            cursor = sqlSelect(TABLE_GENRES, fields, whereInfo, null, null,null,null);
            if (cursor.moveToFirst()) {
                artistId = cursor.getInt(0);
            } else {
                Log.e(TAG, "albumTracks - Couldn't find artist with given name!");
                return null;
            }
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
        fields = new String[]{ID};
        whereInfo = new WhereInfo();
        whereInfo.connection = "AND";
        whereInfo.where.put(ALBUMS_ALBUM, new String[]{album});
        whereInfo.where.put(ALBUMS_GENREARTISTID, new String[]{String.valueOf(artistId)});
        int albumId;
        cursor = null;
        try {
            cursor = sqlSelect(TABLE_ALBUMS, fields, whereInfo, null, null,null,null);
            if (cursor.moveToFirst()) {
                albumId = cursor.getInt(0);
            } else {
                Log.e(TAG, "albumTracks - Couldn't find album with given name!");
                return null;
            }
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }

        whereInfo = new WhereInfo();
        whereInfo.connection = "AND";
        whereInfo.where.put(TRACKS_ALBUMID, new String[]{String.valueOf(String.valueOf(albumId))});
        return tracksGenre(whereInfo, new String[]{TRACKS_ALBUMPOS});
    }

    private Cursor sqlSelect(String table, String[] fields, WhereInfo where,
            List<JoinInfo> joinInfos, String[] orderBy,String groupBy,String whereStr) {
        String whereString = "";
        List<String> allWhereValues = new ArrayList<>();
        if (where != null) {
            whereString = " WHERE ";
            boolean notFirst = false;
            for (String whereKey : where.where.keySet()) {
                String[] whereValues = where.where.get(whereKey);
                for (String whereValue : whereValues) {
                    if (notFirst) {
                        whereString += " " + where.connection + " ";
                    }
                    notFirst = true;
                    if (whereKey.contains(" like ")){
                        whereString += table + "." + whereKey + " ? ";
                    }else {
                        whereString += table + "." + whereKey + " = ?";
                    }
                    allWhereValues.add(whereValue);
                }
            }
        }

        if (whereStr != null){
            whereString = " WHERE ";
            whereString += whereStr;
        }
        String joinString = "";
        if (joinInfos != null) {
            for (JoinInfo joinInfo : joinInfos) {
                joinString += " INNER JOIN " + joinInfo.table + " ON ";
                boolean notFirst = false;
                for (String joinKey : joinInfo.conditions.keySet()) {
                    if (notFirst) {
                        joinString += " AND ";
                    }
                    notFirst = true;

                    joinString += joinKey + " = " + joinInfo.conditions.get(joinKey);
                }
            }
        }

        String orderString = "";
        if (orderBy != null) {
            orderString = " ORDER BY ";
            boolean notFirst = false;
            for (String orderingTerm : orderBy) {
                if (notFirst) {
                    joinString += " , ";
                }
                notFirst = true;
                orderString += orderingTerm;
            }
        }

        String groupString = "";
        if (groupBy != null){
            groupString = " GROUP BY ";
            groupString += groupBy;
        }

        String fieldsString = "*";
        if (fields != null) {
            fieldsString = "";
            boolean notFirst = false;
            for (String field : fields) {
                if (notFirst) {
                    fieldsString += ", ";
                }
                notFirst = true;
                fieldsString += field;
            }
        }
        String statement = "SELECT " + fieldsString + " FROM " + table + joinString + whereString
                 + groupString + orderString;
        String[] allWhereValuesArray = null;
        if (allWhereValues.size() > 0) {
            allWhereValuesArray = allWhereValues.toArray(new String[allWhereValues.size()]);
        }
        return mDb.rawQuery(statement, allWhereValuesArray);
    }

    private static String concatKeys(Object... keys) {
        String result = "";
        for (int i = 0; i < keys.length; i++) {
            if (i > 0) {
                result += "♣";
            }
            result += keys[i];
        }
        return result;
    }

}
