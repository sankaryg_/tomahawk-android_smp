package org.tomahawk.libtomahawk.collection;

import android.content.ContentResolver;
import android.content.ContentUris;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.SQLException;
import android.net.Uri;
import android.os.PowerManager;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.util.Log;

import org.tomahawk.libtomahawk.database.CollectionDb;
import org.tomahawk.libtomahawk.database.DatabaseHelper;
import org.tomahawk.libtomahawk.resolver.models.ScriptResolverTrack;
import org.tomahawk.tomahawk_android.R;
import org.tomahawk.tomahawk_android.TomahawkApp;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import wseemann.media.FFmpegMediaMetadataRetriever;

/**
 * Created by YGS on 10/12/2015.
 */
public class MusicCollectionCursor {

    private Context mContext;
    private String mCurrentTask = "";
    private int mOverallProgress = 0;
    private Date date = new Date();

    private String mMediaStoreSelection = null;
    private HashMap<String, String> mGenresHashMap = new HashMap<String, String>();
    private HashMap<String, Integer> mGenresSongCountHashMap = new HashMap<String, Integer>();
    private HashMap<String, String> mPlaylistsHashMap = new HashMap<String, String>();
    private HashMap<String, Integer> mPlaylistsSongCountHashMap = new HashMap<String, Integer>();
    private HashMap<String, Integer> mAlbumsCountMap = new HashMap<String, Integer>();
    private HashMap<String, Integer> mSongsCountMap = new HashMap<String, Integer>();
    private HashMap<String, Uri> mMediaStoreAlbumArtMap = new HashMap<String, Uri>();
    private HashMap<String, String> mFolderArtHashMap = new HashMap<String, String>();
    private FFmpegMediaMetadataRetriever mMMDR = new FFmpegMediaMetadataRetriever();

    private PowerManager pm;
    private PowerManager.WakeLock wakeLock;

    public MusicCollectionCursor(Context context) {
        mContext = context;

    }

    /**
     * Queries MediaStore and returns a cursor with all songs.
     */
    public static Cursor getAllSongs(Context context,
                                     String[] projection,
                                     String sortOrder) {

        ContentResolver contentResolver = context.getContentResolver();
        Uri uri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
        String selection = MediaStore.Audio.Media.IS_MUSIC + "!= 0";

        return contentResolver.query(uri, null, selection, null, sortOrder);

    }

    /**
     * Retrieves a cursor of songs from MediaStore. The cursor
     * is limited to songs that are within the folders that the user
     * selected.
     */
    public Cursor getSongsFromMediaStore() {
        //Get a cursor of all active music folders.
        Cursor musicFoldersCursor = null;

        //Build the appropriate selection statement.
        Cursor mediaStoreCursor = null;
        String sortOrder = null;
        String projection[] = { MediaStore.Audio.Media.TITLE,
                MediaStore.Audio.Media.ARTIST,
                MediaStore.Audio.Media.ALBUM,
                MediaStore.Audio.Media.ALBUM_ID,
                MediaStore.Audio.Media.DURATION,
                MediaStore.Audio.Media.TRACK,
                MediaStore.Audio.Media.YEAR,
                MediaStore.Audio.Media.DATA,
                MediaStore.Audio.Media.DATE_ADDED,
                MediaStore.Audio.Media.DATE_MODIFIED,
                MediaStore.Audio.Media._ID,
                CollectionDb.ALBUM_ARTIST };

        //Grab the cursor of MediaStore entries.
        if (musicFoldersCursor==null || musicFoldersCursor.getCount() < 1) {
            //No folders were selected by the user. Grab all songs in MediaStore.
            mediaStoreCursor = getAllSongs(mContext, projection, sortOrder);
        }

        return mediaStoreCursor;
    }

    /**
     * Iterates through mediaStoreCursor and transfers its data
     * over to Jams' private database.
     */
    public List<ScriptResolverTrack> saveMediaStoreDataToDB(Cursor mediaStoreCursor) {
        List<ScriptResolverTrack> tracks = new ArrayList<>();

        try {

            SharedPreferences preferences =
                    PreferenceManager.getDefaultSharedPreferences(TomahawkApp.getContext());
            //Populate a hash of all songs in MediaStore and their genres.
            buildGenresLibrary();

            buildPlaylistLibrary();

            //Populate a hash of all artists and their number of albums.
            buildArtistsLibrary();

            //Populate a hash of all albums and their number of songs.
            buildAlbumsLibrary();

            //Populate a has of all albums and their album art path.
            buildMediaStoreAlbumArtHash();

            //Prefetch each column's index.
            final int titleColIndex = mediaStoreCursor.getColumnIndex(MediaStore.Audio.Media.TITLE);
            final int artistColIndex = mediaStoreCursor.getColumnIndex(MediaStore.Audio.Media.ARTIST);
            final int albumColIndex = mediaStoreCursor.getColumnIndex(MediaStore.Audio.Media.ALBUM);
            final int albumIdColIndex = mediaStoreCursor.getColumnIndex(MediaStore.Audio.Media.ALBUM_ID);
            final int durationColIndex = mediaStoreCursor.getColumnIndex(MediaStore.Audio.Media.DURATION);
            final int trackColIndex = mediaStoreCursor.getColumnIndex(MediaStore.Audio.Media.TRACK);
            final int yearColIndex = mediaStoreCursor.getColumnIndex(MediaStore.Audio.Media.YEAR);
            final int dateAddedColIndex = mediaStoreCursor.getColumnIndex(MediaStore.Audio.Media.DATE_ADDED);
            final int dateModifiedColIndex = mediaStoreCursor.getColumnIndex(MediaStore.Audio.Media.DATE_MODIFIED);
            final int filePathColIndex = mediaStoreCursor.getColumnIndex(MediaStore.Audio.Media.DATA);
            final int idColIndex = mediaStoreCursor.getColumnIndex(MediaStore.Audio.Media._ID);
            int albumArtistColIndex = mediaStoreCursor.getColumnIndex(CollectionDb.ALBUM_ARTIST);

    		/* The album artist field is hidden by default and we've explictly exposed it.
    		 * The field may cease to exist at any time and if it does, use the artists
    		 * field instead.
    		 */
            if (albumArtistColIndex==-1) {
                albumArtistColIndex = artistColIndex;
            }

            //Iterate through MediaStore's cursor and save the fields to Jams' DB.
            for (int i=0; i < mediaStoreCursor.getCount(); i++) {

                mediaStoreCursor.moveToPosition(i);
                String songTitle = mediaStoreCursor.getString(titleColIndex);
                String songArtist = mediaStoreCursor.getString(artistColIndex);
                String songAlbum = mediaStoreCursor.getString(albumColIndex);
                String songAlbumId = mediaStoreCursor.getString(albumIdColIndex);
                String songAlbumArtist = mediaStoreCursor.getString(albumArtistColIndex);
                String songFilePath = mediaStoreCursor.getString(filePathColIndex);
                String songGenre = getSongGenre(songFilePath);
                String songPlaylist = getSongForPlaylist(songFilePath);
                String songPlayListId = "-1";
                String songPlaylistPlayOrder = "0";
                if (songPlaylist!=null && songPlaylist.contains("_")){
                    songPlayListId = songPlaylist.split("_")[1];
                    songPlaylistPlayOrder = songPlaylist.split("_")[2];
                    songPlaylist = songPlaylist.split("_")[0];

                }
                String songDuration = mediaStoreCursor.getString(durationColIndex);
                String songTrackNumber = mediaStoreCursor.getString(trackColIndex);
                String songYear = mediaStoreCursor.getString(yearColIndex);
                String songDateAdded = mediaStoreCursor.getString(dateAddedColIndex);
                String songDateModified = mediaStoreCursor.getString(dateModifiedColIndex);
                String songId = mediaStoreCursor.getString(idColIndex);
                String numberOfAlbums = "" + mAlbumsCountMap.get(songArtist);
                String numberOfTracks = "" + mSongsCountMap.get(songAlbum + songArtist);
                String numberOfSongsInGenre = "" + getGenreSongsCount(songGenre);
                String numberOfSongsInPlaylist = "" + getPlaylistSongsCount(songPlaylist);
                String songSavedPosition = "-1";

                String songAlbumArtPath = "";
                if (mMediaStoreAlbumArtMap.get(songAlbumId)!=null)
                    songAlbumArtPath = mMediaStoreAlbumArtMap.get(songAlbumId).toString();

                if (numberOfAlbums.equals("1"))
                    numberOfAlbums += " " + mContext.getResources().getString(R.string.album_small);
                else
                    numberOfAlbums += " " + mContext.getResources().getString(R.string.albums_small);

                if (numberOfTracks.equals("1"))
                    numberOfTracks += " " + mContext.getResources().getString(R.string.song_small);
                else
                    numberOfTracks += " " + mContext.getResources().getString(R.string.songs_small);

                if (numberOfSongsInGenre.equals("1"))
                    numberOfSongsInGenre += " " + mContext.getResources().getString(R.string.song_small);
                else
                    numberOfSongsInGenre += " " + mContext.getResources().getString(R.string.songs_small);

                if (numberOfSongsInPlaylist.equals("1"))
                    numberOfSongsInPlaylist += " " + mContext.getResources().getString(R.string.song_small);
                else
                    numberOfSongsInPlaylist += " " + mContext.getResources().getString(R.string.songs_small);

                //Check if any of the other tags were empty/null and set them to "Unknown xxx" values.
                if (songArtist==null || songArtist.isEmpty()) {
                    songArtist = mContext.getResources().getString(R.string.unknown_artist);
                }

                if (songAlbumArtist==null || songAlbumArtist.isEmpty()) {
                    if (songArtist!=null && !songArtist.isEmpty()) {
                        songAlbumArtist = songArtist;
                    } else {
                        songAlbumArtist = mContext.getResources().getString(R.string.unknown_album_artist);
                    }

                }

                if (songAlbum==null || songAlbum.isEmpty()) {
                    songAlbum = mContext.getResources().getString(R.string.unknown_album);;
                }

                if (songGenre==null || songGenre.isEmpty()) {
                    songGenre = mContext.getResources().getString(R.string.unknown_genre);
                }

                if (songPlaylist==null || songPlaylist.isEmpty()) {
                    songPlaylist = mContext.getResources().getString(R.string.unknown_title);
                }

                //Filter out track numbers and remove any bogus values.
                if (songTrackNumber!=null) {
                    if (songTrackNumber.contains("/")) {
                        int index = songTrackNumber.lastIndexOf("/");
                        songTrackNumber = songTrackNumber.substring(0, index);
                    }

                    try {
                        if (Integer.parseInt(songTrackNumber) <= 0) {
                            songTrackNumber = "";
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                        songTrackNumber = "";
                    }

                }

                long durationLong = 0;
                try {
                    durationLong = Long.parseLong(songDuration);
                } catch (Exception e) {
                    e.printStackTrace();
                }

                /*String filePath = songFilePath;
                String artworkPath = "";
                if (preferences.getInt("ALBUM_ART_SOURCE", 0)==0 ||
                        preferences.getInt("ALBUM_ART_SOURCE", 0)==1) {
                    artworkPath = getEmbeddedArtwork(filePath);
                } else {
                    artworkPath = getArtworkFromFolder(filePath);
                }

                String normalizedFilePath = filePath.replace("'", "''");*/


               // DatabaseHelper.get().addFolder(songFilePath);
                      ScriptResolverTrack track = new ScriptResolverTrack();
                        track.album = songAlbum;
                        track.albumArtist = songAlbumArtist;
                        track.track = songTitle;
                        track.artist = songArtist;
                track.songId = songId;
                track.playListName = songPlaylist;
                track.playListId = songPlayListId;
                track.playOrder = songPlaylistPlayOrder;
                        track.duration = durationLong;
                        if(songTrackNumber == null || songTrackNumber.equals(""))
                            songTrackNumber = "0";
                        track.albumPos = Integer.valueOf(songTrackNumber);
                        track.url = songFilePath;
                        track.imagePath = songAlbumArtPath;
                        track.lastModified = Long.valueOf(songDateAdded);//songDateModified);
                        track.genre = songGenre;
                if(songYear == null || songYear.equals(""))
                    songYear = "0";
                        track.year = Integer.valueOf(songYear);
                        track.favourite = false;
                track.album_count = numberOfAlbums;
                track.song_count = numberOfTracks;
                track.genre_count = numberOfSongsInGenre;
                track.playlist_count = numberOfSongsInPlaylist;
                        String path = songFilePath.substring(0, songFilePath.lastIndexOf("/"));
                       track.folderId = DatabaseHelper.get().getFolderID(path);
                        tracks.add(track);

                  //Add all the entries to the database to build the songs library.


            }

        } catch (SQLException e) {
            // TODO Auto-generated method stub.
            e.printStackTrace();
        } finally {

        }
return tracks;
    }


    /**
     * Builds a HashMap of all songs and their genres.
     */
    private void buildGenresLibrary() {
        //Get a cursor of all genres in MediaStore.
        Cursor genresCursor = mContext.getContentResolver().query(MediaStore.Audio.Genres.EXTERNAL_CONTENT_URI,
                new String[]{MediaStore.Audio.Genres._ID, MediaStore.Audio.Genres.NAME},
                null,
                null,
                null);

        //Iterate thru all genres in MediaStore.
        for (genresCursor.moveToFirst(); !genresCursor.isAfterLast(); genresCursor.moveToNext()) {
            String genreId = genresCursor.getString(0);
            String genreName = genresCursor.getString(1);

            if (genreName==null || genreName.isEmpty() ||
                    genreName.equals(" ") || genreName.equals("   ") ||
                    genreName.equals("    "))
                genreName = mContext.getResources().getString(R.string.unknown_genre);

        	/* Grab a cursor of songs in the each genre id. Limit the songs to
        	 * the user defined folders using mMediaStoreSelection.
        	 */
            Cursor cursor = mContext.getContentResolver().query(makeGenreUri(genreId),
                    new String[] { MediaStore.Audio.Media.DATA },
                    mMediaStoreSelection,
                    null,
                    null);

            //Add the songs' file paths and their genre names to the hash.
            if (cursor!=null) {
                for (int i=0; i < cursor.getCount(); i++) {
                    cursor.moveToPosition(i);
                    mGenresHashMap.put(cursor.getString(0), genreName);
                    mGenresSongCountHashMap.put(genreName, cursor.getCount());
                }

                cursor.close();
            }

        }

        if (genresCursor!=null)
            genresCursor.close();

    }


    /**
     * Builds a HashMap of all songs and their playlists.
     */
    private void buildPlaylistLibrary() {
        //Get a cursor of all genres in MediaStore.
        Cursor genresCursor = MusicCollectionCursor.getAllUniquePlaylists(TomahawkApp.getContext(),0);/*mContext.getContentResolver().query(MediaStore.Audio.Genres.EXTERNAL_CONTENT_URI,
                new String[] { MediaStore.Audio.Genres._ID, MediaStore.Audio.Genres.NAME },
                null,
                null,
                null);*/

        //Iterate thru all genres in MediaStore.
        for (genresCursor.moveToFirst(); !genresCursor.isAfterLast(); genresCursor.moveToNext()) {
            String playListId = genresCursor.getString(0);
            String playListName = genresCursor.getString(1);

            if (playListName==null || playListName.isEmpty() ||
                    playListName.equals(" ") || playListName.equals("   ") ||
                    playListName.equals("    "))
                playListName = mContext.getResources().getString(R.string.unknown_title);

        	/* Grab a cursor of songs in the each genre id. Limit the songs to
        	 * the user defined folders using mMediaStoreSelection.
        	 */
            Uri uri = MediaStore.Audio.Playlists.Members.getContentUri("external", Long.valueOf(playListId));
            Cursor cursor = mContext.getContentResolver().query(uri,
                    new String[]{MediaStore.Audio.Media.DATA, MediaStore.Audio.Playlists.Members.PLAY_ORDER
                    },
                    mMediaStoreSelection,
                    null,
                    null);

            //Add the songs' file paths and their genre names to the hash.
            if (cursor!=null) {
                for (int i=0; i < cursor.getCount(); i++) {
                    cursor.moveToPosition(i);
                    mPlaylistsHashMap.put(cursor.getString(0), playListName+"_"+playListId+"_"+cursor.getString(1));
                    mPlaylistsSongCountHashMap.put(playListName, cursor.getCount());
                }

                cursor.close();
            }

        }

        if (genresCursor!=null)
            genresCursor.close();

    }

    /**
     * Builds a HashMap of all artists and their individual albums count.
     */
    private void buildArtistsLibrary() {
        Cursor artistsCursor = mContext.getContentResolver().query(MediaStore.Audio.Artists.EXTERNAL_CONTENT_URI,
                new String[]{MediaStore.Audio.Artists.ARTIST, MediaStore.Audio.Artists.NUMBER_OF_ALBUMS},
                null,
                null,
                null);

        if (artistsCursor==null)
            return;

        for (int i=0; i < artistsCursor.getCount(); i++) {
            artistsCursor.moveToPosition(i);
            mAlbumsCountMap.put(artistsCursor.getString(0), artistsCursor.getInt(1));

        }

        artistsCursor.close();
    }

    /**
     * Builds a HashMap of all albums and their individual songs count.
     */
    private void buildAlbumsLibrary() {
        Cursor albumsCursor = mContext.getContentResolver().query(MediaStore.Audio.Albums.EXTERNAL_CONTENT_URI,
                new String[]{MediaStore.Audio.Albums.ALBUM, MediaStore.Audio.Albums.ARTIST, MediaStore.Audio.Albums.NUMBER_OF_SONGS},
                null,
                null,
                null);

        if (albumsCursor==null)
            return;

        for (int i=0; i < albumsCursor.getCount(); i++) {
            albumsCursor.moveToPosition(i);
            mSongsCountMap.put(albumsCursor.getString(0) + albumsCursor.getString(1), albumsCursor.getInt(2));

        }

        albumsCursor.close();
    }

    /**
     * Builds a HashMap of all albums and their album art path.
     */
    private void buildMediaStoreAlbumArtHash() {
        Cursor albumsCursor = mContext.getContentResolver().query(MediaStore.Audio.Media.EXTERNAL_CONTENT_URI,
                new String[] { MediaStore.Audio.Media.ALBUM_ID },
                MediaStore.Audio.Media.IS_MUSIC + "=1",
                null,
                null);

        final Uri ART_CONTENT_URI = Uri.parse("content://media/external/audio/albumart");
        if (albumsCursor==null)
            return;

        for (int i=0; i < albumsCursor.getCount(); i++) {
            albumsCursor.moveToPosition(i);
            Uri albumArtUri = ContentUris.withAppendedId(ART_CONTENT_URI, albumsCursor.getLong(0));
            mMediaStoreAlbumArtMap.put(albumsCursor.getString(0), albumArtUri);
        }

        albumsCursor.close();
    }

    /**
     * Returns the genre of the song at the specified file path.
     */
    private String getSongGenre(String filePath) {
        if (mGenresHashMap!=null)
            return mGenresHashMap.get(filePath);
        else
            return mContext.getResources().getString(R.string.unknown_genre);
    }

    private String getSongForPlaylist(String filePath) {
        if (mPlaylistsHashMap!=null)
            return mPlaylistsHashMap.get(filePath);
        else
            return mContext.getResources().getString(R.string.unknown_title);
    }

    /**
     * Returns the number of songs in the specified genre.
     */
    private int getGenreSongsCount(String genre) {
        if (mGenresSongCountHashMap!=null)
            if (genre!=null)
                if (mGenresSongCountHashMap.get(genre)!=null)
                    return mGenresSongCountHashMap.get(genre);
                else
                    return 0;
            else
            if (mGenresSongCountHashMap.get(mContext.getResources().getString(R.string.unknown_genre))!=null)
                return mGenresSongCountHashMap.get(mContext.getResources().getString(R.string.unknown_genre));
            else
                return 0;
        else
            return 0;
    }

    private int getPlaylistSongsCount(String playlistName) {
        if (mPlaylistsSongCountHashMap!=null)
            if (playlistName!=null)
                if (mPlaylistsSongCountHashMap.get(playlistName)!=null)
                    return mPlaylistsSongCountHashMap.get(playlistName);
                else
                    return 0;
            else
            if (mPlaylistsSongCountHashMap.get(mContext.getResources().getString(R.string.unknown_title))!=null)
                return mPlaylistsSongCountHashMap.get(mContext.getResources().getString(R.string.unknown_title));
            else
                return 0;
        else
            return 0;
    }


    /**
     * Returns a Uri of a specific genre in MediaStore.
     * The genre is specified using the genreId parameter.
     */
    private Uri makeGenreUri(String genreId) {
        String CONTENTDIR = MediaStore.Audio.Genres.Members.CONTENT_DIRECTORY;
        return Uri.parse(new StringBuilder().append(MediaStore.Audio.Genres.EXTERNAL_CONTENT_URI.toString())
                .append("/")
                .append(genreId)
                .append("/")
                .append(CONTENTDIR)
                .toString());
    }



    /**
     * Queries MediaStore and returns a cursor with all unique playlists.
     */
    public static Cursor getAllUniquePlaylists(Context context,int sortPosition) {
        ContentResolver contentResolver = context.getContentResolver();
        String[] projection = { MediaStore.Audio.Playlists._ID,
                MediaStore.Audio.Playlists.NAME };
        String sortBy;
        if (sortPosition == 0){
           sortBy = MediaStore.Audio.Playlists.DATE_ADDED;
        }else {
            sortBy = MediaStore.Audio.Playlists.NAME;
        }
        return contentResolver.query(MediaStore.Audio.Playlists.EXTERNAL_CONTENT_URI,
                projection,
                null,
                null,
                sortBy + " ASC");

    }



    /**
     * Loops through a cursor of all local songs in
     * the library and searches for their album art.
     */
  public void getAlbumArt(List<ScriptResolverTrack> tracks) {


      SharedPreferences preferences =
              PreferenceManager.getDefaultSharedPreferences(TomahawkApp.getContext());
        if (tracks==null || tracks.size() < 1)
            return;

        //Tracks the progress of this method.


        try {
           //Loop through the cursor and retrieve album art.
            for (int i=0; i < tracks.size(); i++) {

                try {

                    String filePath = tracks.get(i).url;//cursor.getString(cursor.getColumnIndex(DBAccessHelper.SONG_FILE_PATH));
                    String artworkPath = "";
                    if (preferences.getInt("ALBUM_ART_SOURCE", 0)==0 ||
                            preferences.getInt("ALBUM_ART_SOURCE", 0)==1) {
                        artworkPath = getEmbeddedArtwork(filePath);
                    } else {
                        artworkPath = getArtworkFromFolder(filePath);
                    }

                    String normalizedFilePath = filePath.replace("'", "''");
                    Log.d("t1",artworkPath);
                    tracks.get(i).imagePath = artworkPath;
                    tracks.get(i).linkUrl = artworkPath;

                } catch (Exception e) {
                    e.printStackTrace();
                    continue;
                }

            }

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    /**
     * Searchs for folder art within the specified file's
     * parent folder. Returns a path string to the artwork
     * image file if it exists. Returns an empty string
     * otherwise.
     */
    public String getArtworkFromFolder(String filePath) {

        File file = new File(filePath);
        if (!file.exists()) {
            return "";

        } else {
            //Create a File that points to the parent directory of the album.
            File directoryFile = file.getParentFile();
            String directoryPath = "";
            String albumArtPath = "";
            try {
                directoryPath = directoryFile.getCanonicalPath();
            } catch (IOException e1) {
                // TODO Auto-generated catch block
                e1.printStackTrace();
            }

            //Check if album art was already found in this directory.
            if (mFolderArtHashMap.containsKey(directoryPath))
                return mFolderArtHashMap.get(directoryPath);

            //Get a list of images in the album's folder.
            FileExtensionFilter IMAGES_FILTER = new FileExtensionFilter(new String[] {".jpg", ".jpeg",
                    ".png", ".gif"});
            File[] folderList = directoryFile.listFiles(IMAGES_FILTER);

            //Check if any image files were found in the folder.
            if (folderList.length==0) {
                //No images found.
                return "";

            } else {

                //Loop through the list of image files. Use the first jpeg file if it's found.
                for (int i=0; i < folderList.length; i++) {

                    try {
                        albumArtPath = folderList[i].getCanonicalPath();
                        if (albumArtPath.endsWith("jpg") ||
                                albumArtPath.endsWith("jpeg")) {

                            //Add the folder's album art file to the hash.
                            mFolderArtHashMap.put(directoryPath, albumArtPath);
                            return albumArtPath;
                        }

                    } catch (Exception e) {
                        //Skip the file if it's corrupted or unreadable.
                        continue;
                    }

                }

                //If an image was not found, check for gif or png files (lower priority).
                for (int i=0; i < folderList.length; i++) {

                    try {
                        albumArtPath = folderList[i].getCanonicalPath();
                        if (albumArtPath.endsWith("png") ||
                                albumArtPath.endsWith("gif")) {

                            //Add the folder's album art file to the hash.
                            mFolderArtHashMap.put(directoryPath, albumArtPath);
                            return albumArtPath;
                        }

                    } catch (Exception e) {
                        //Skip the file if it's corrupted or unreadable.
                        continue;
                    }

                }

            }

            //Add the folder's album art file to the hash.
            mFolderArtHashMap.put(directoryPath, albumArtPath);
            return "";
        }

    }

    /**
     * Searchs for embedded art within the specified file.
     * Returns a path string to the artwork if it exists.
     * Returns an empty string otherwise.
     */
  public String getEmbeddedArtwork(String filePath) {
      SharedPreferences preferences =
              PreferenceManager.getDefaultSharedPreferences(TomahawkApp.getContext());
        File file = new File(filePath);
        if (!file.exists()) {
            if (preferences.getInt("ALBUM_ART_SOURCE", 0)==0) {
                return getArtworkFromFolder(filePath);
            } else {
                return "";
            }

        } else {
            //Log.d("t1", filePath + "_" + file.getAbsolutePath());
            byte[] embeddedArt = null;
                //mMMDR.setDataSource(file.getAbsolutePath());
                //embeddedArt = mMMDR.getEmbeddedPicture();

                //FileInputStream inputStream = new FileInputStream(file.getAbsolutePath());
                mMMDR.setDataSource(filePath);//inputStream.getFD());
                //inputStream.close();
                embeddedArt = mMMDR.getEmbeddedPicture();


            if (embeddedArt!=null) {
                return "byte://" + file.getAbsolutePath();
            } else {
                if (preferences.getInt("ALBUM_ART_SOURCE", 0)==0) {
                    return getArtworkFromFolder(filePath);
                } else {
                    return "";
                }

            }

        }

    }

}
