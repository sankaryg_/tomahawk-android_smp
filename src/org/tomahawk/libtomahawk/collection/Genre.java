/* == This file is part of Tomahawk Player - <http://tomahawk-player.org> ===
 *
 *   Copyright 2012, Christopher Reichert <creichert07@gmail.com>
 *
 *   Tomahawk is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Tomahawk is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Tomahawk. If not, see <http://www.gnu.org/licenses/>.
 */
package org.tomahawk.libtomahawk.collection;

import java.util.concurrent.ConcurrentHashMap;


/**
 * This class represents an {@link Genre}.
 */
public class Genre extends Cacheable implements AlphaComparable {

    public static final Genre COMPILATION_GENRE = new Genre("Various Genres");

    private final String mName;

    //private ListItemString mBio;

    private Image mImage;

    private int mLike;

    /**
     * Construct a new {@link Artist}
     */
    private Genre(String genreName) {
        super(Genre.class, getCacheKey(genreName));

        mName = genreName != null ? genreName : "";
    }

    /**
     * Returns the {@link Artist} with the given album name. If none exists in the cache yet,
     * construct and add it.
     */
    public static Genre get(String genreName) {
        Cacheable cacheable = get(Genre.class, getCacheKey(genreName));
        return cacheable != null ? (Genre) cacheable : new Genre(genreName);
    }

    public static Genre getByKey(String cacheKey) {
        return (Genre) get(Genre.class, cacheKey);
    }

    /**
     * @return this object's name
     */
    public String getName() {
        return mName;
    }

    /**
     * @return the name that should be displayed
     */
    public String getPrettyName() {
        return getName().length() > 0 ? getName() : "<unknown>";
    }

    public Image getImage() {
        return mImage;
    }

    public void setImage(Image image) {
        mImage = image;
    }

    public int getmLike() {
        return mLike;
    }

    public void setmLike(int mLike) {
        this.mLike = mLike;
    }

    /*public ListItemString getBio() {
        return mBio;
    }

    public void setBio(ListItemString bio) {
        mBio = bio;
    }
*/
    /*private static final ConcurrentHashMap<String, Genre> sGenres
            = new ConcurrentHashMap<>();

    private String mCacheKey;

    private final String mName;

    private ListItemString mBio;

    private final ConcurrentHashMap<String, Album> mAlbums = new ConcurrentHashMap<>();

    private final ConcurrentHashMap<String, Query> mQueries = new ConcurrentHashMap<>();

    private Image mImage;

    *//**
     * Construct a new {@link Genre} with the given name
     *//*
    private Genre(String artistName) {
        if (artistName == null) {
            mName = "";
        } else {
            mName = artistName;
        }
        if (mCacheKey == null) {
            mCacheKey = TomahawkUtils.getCacheKey(this);
        }
    }

    *//**
     * Returns the {@link Genre} with the given id. If none exists in our static {@link
     * ConcurrentHashMap} yet, construct and add it.
     *
     * @return {@link Genre} with the given id
     *//*
    public static Genre get(String artistName) {
        Genre artist = new Genre(artistName);
        return ensureCache(artist);
    }

    *//**
     * If Artist is already in our cache, return that. Otherwise add it to the cache.
     *//*
    private static Genre ensureCache(Genre artist) {
        if (!sGenres.containsKey(artist.getCacheKey())) {
            sGenres.put(artist.getCacheKey(), artist);
        }
        return sGenres.get(artist.getCacheKey());
    }

    public String getCacheKey() {
        return mCacheKey;
    }

    *//**
     * Get the {@link org.tomahawk.libtomahawk.collection.Genre} by providing its cache key
     *//*
    public static Genre getArtistByKey(String key) {
        return sGenres.get(key);
    }

    *//**
     * @return A {@link java.util.List} of all {@link Genre}s
     *//*
    public static ArrayList<Genre> getArtists() {
        ArrayList<Genre> artists = new ArrayList<>(sGenres.values());
        Collections.sort(artists,
                new TomahawkListItemComparator(TomahawkListItemComparator.COMPARE_ALPHA));
        return artists;
    }

    *//**
     * @return this object's name
     *//*
    @Override
    public String toString() {
        return mName;
    }

    *//**
     * @return this object' name
     *//*
    @Override
    public String getName() {
        return mName;
    }

    *//**
     * @return this object
     *//*
    @Override
    public Genre getGenre() {
        return this;
    }

    *//**
     * This method returns the first {@link Album} of this object. If none exists, returns null.
     * It's needed to comply to the {@link TomahawkListItem} interface.
     *
     * @return First {@link Album} of this object. If none exists, returns null.
     *//*
    @Override
    public Album getAlbum() {
        if (!mAlbums.isEmpty()) {
            ArrayList<Album> albums = new ArrayList<>(mAlbums.values());
            return albums.get(0);
        }
        return null;
    }

    *//**
     * @param query the {@link org.tomahawk.libtomahawk.resolver.Query} to be added
     *//*
    public void addQuery(Query query) {
        synchronized (this) {
            if (!mQueries.containsKey(query.getCacheKey())) {
                mQueries.put(query.getCacheKey(), query);
            }
        }
    }

    *//**
     * @return list of all {@link org.tomahawk.libtomahawk.resolver.Query}s from this object.
     *//*
    @Override
    public ArrayList<Query> getQueries() {
        ArrayList<Query> queries;
        queries = new ArrayList<>(mQueries.values());
        Collections.sort(queries, new QueryComparator(QueryComparator.COMPARE_ALPHA));
        return queries;
    }

    @Override
    public Image getImage() {
        return mImage;
    }

    *//**
     * Add an {@link Album} to this object
     *
     * @param album the {@link Album} to be added
     *//*
    public void addAlbum(Album album) {
        synchronized (this) {
            if (!mAlbums.containsKey(album.getCacheKey())) {
                mAlbums.put(album.getCacheKey(), album);
            }
        }
    }

       
    *//**
     * Get a list of all {@link Album}s from this object.
     *
     * @return list of all {@link Album}s from this object.
     *//*
    public ArrayList<Album> getAlbums() {
        ArrayList<Album> albums = new ArrayList<>(mAlbums.values());
        Collections.sort(albums,
                new TomahawkListItemComparator(TomahawkListItemComparator.COMPARE_ALPHA));
        return albums;
    }

    public void setImage(Image image) {
        mImage = image;
    }

    public ListItemString getBio() {
        return mBio;
    }

    public void setBio(ListItemString bio) {
        mBio = bio;
    }

	@Override
	public Artist getArtist() {
		// TODO Auto-generated method stub
		return null;
	}

    @Override
    public sensitivemusicplayer.smartfoxsoft.com.sense.libtomahawk.collection.Folder getFolder() {
        return null;
    }*/
}
