/* == This file is part of Tomahawk Player - <http://tomahawk-player.org> ===
 *
 *   Copyright 2015, Enno Gottschalk <mrmaffen@googlemail.com>
 *
 *   Tomahawk is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Tomahawk is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Tomahawk. If not, see <http://www.gnu.org/licenses/>.
 */
package org.tomahawk.libtomahawk.collection;

import android.content.ContentUris;
import android.content.Context;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.net.Uri;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.util.Log;

import org.jdeferred.Deferred;
import org.jdeferred.DoneCallback;
import org.jdeferred.Promise;
import org.tomahawk.libtomahawk.database.CollectionDb;
import org.tomahawk.libtomahawk.database.CollectionDbManager;
import org.tomahawk.libtomahawk.resolver.FuzzyIndex;
import org.tomahawk.libtomahawk.resolver.PipeLine;
import org.tomahawk.libtomahawk.resolver.Query;
import org.tomahawk.libtomahawk.resolver.Resolver;
import org.tomahawk.libtomahawk.resolver.Result;
import org.tomahawk.libtomahawk.utils.ADeferredObject;
import org.tomahawk.tomahawk_android.TomahawkApp;
import org.tomahawk.tomahawk_android.utils.ThreadManager;
import org.tomahawk.tomahawk_android.utils.TomahawkRunnable;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Set;
import java.util.concurrent.ConcurrentHashMap;

/**
 * This class represents a Collection which contains tracks/albums/artists which are being stored in
 * a local sqlite db.
 */
public abstract class DbCollection extends Collection {

    private final static String TAG = DbCollection.class.getSimpleName();

    private FuzzyIndex mFuzzyIndex;

    private Set<Query> mWaitingQueries = Collections
            .newSetFromMap(new ConcurrentHashMap<Query, Boolean>());

    private Resolver mResolver;

    public DbCollection(Resolver resolver) {
        super(resolver.getId(), resolver.getPrettyName());

        mResolver = resolver;
    }

    protected void initFuzzyIndex() {
        getCollectionId().done(new DoneCallback<String>() {
            @Override
            public void onDone(final String collectionId) {
                TomahawkRunnable r = new TomahawkRunnable(
                        TomahawkRunnable.PRIORITY_IS_DATABASEACTION) {
                    @Override
                    public void run() {
                        mFuzzyIndex = new FuzzyIndex(collectionId);
                        for (Query query : mWaitingQueries) {
                            mWaitingQueries.remove(query);
                            resolve(query);
                        }
                        Log.d(TAG, collectionId
                                + " - Fuzzy index initialized. Resolving all waiting queries.");
                    }
                };
                ThreadManager.get().execute(r);
            }
        });
    }


    public Promise<Boolean, Throwable, Void> isInitializing() {
        final Deferred<Boolean, Throwable, Void> deferred = new ADeferredObject<>();
        getCollectionId().done(new DoneCallback<String>() {
            @Override
            public void onDone(String collectionId) {
                deferred.resolve(
                        !CollectionDbManager.get().getCollectionDb(collectionId).isInitialized());
            }
        });
        return deferred;
    }

    public abstract Promise<String, Throwable, Void> getCollectionId();

    public boolean resolve(final Query query) {
        getCollectionId().done(new DoneCallback<String>() {
            @Override
            public void onDone(final String collectionId) {
                if (mFuzzyIndex == null) {
                    mWaitingQueries.add(query);
                    Log.d(TAG, collectionId + " - Added query to the waiting queue because the "
                            + "FuzzyIndex is still initializing.");
                } else {
                    // Always make sure that no queries are waiting to be resolved
                    for (Query query : mWaitingQueries) {
                        mWaitingQueries.remove(query);
                        resolve(query);
                    }
                    TomahawkRunnable r = new TomahawkRunnable(
                            TomahawkRunnable.PRIORITY_IS_RESOLVING) {
                        @Override
                        public void run() {
                            List<FuzzyIndex.IndexResult> indexResults =
                                    mFuzzyIndex.searchIndex(query);
                            if (indexResults.size() > 0) {
                                String[] ids = new String[indexResults.size()];
                                for (int i = 0; i < indexResults.size(); i++) {
                                    FuzzyIndex.IndexResult indexResult = indexResults.get(i);
                                    ids[i] = String.valueOf(indexResult.id);
                                }
                                CollectionDb.WhereInfo whereInfo = new CollectionDb.WhereInfo();
                                whereInfo.connection = "OR";
                                whereInfo.where.put(CollectionDb.ID, ids);
                                Cursor cursor = CollectionDbManager.get()
                                        .getCollectionDb(collectionId).tracks(whereInfo, null,null);
                                CollectionCursor<Result> collectionCursor = new CollectionCursor<>(
                                        cursor, Result.class, mResolver, null,null);
                                ArrayList<Result> results = new ArrayList<>();
                                for (int i = 0; i < collectionCursor.size(); i++) {
                                    results.add(collectionCursor.get(i));
                                }
                                collectionCursor.close();
                                PipeLine.get().reportResults(query, results, mResolver.getId());
                            }
                        }
                    };
                    ThreadManager.get().execute(r, query);
                }
            }
        });
        return true;
    }

    @Override
    public Promise<Playlist, Throwable, Void> getQueries(final int sortMode1, final String playlistId) {
        final Deferred<Playlist, Throwable, Void> deferred = new ADeferredObject<>();
        getCollectionId().done(new DoneCallback<String>() {
            @Override
            public void onDone(final String collectionId) {
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        String orderBy;
                        switch (sortMode1) {
                            case SORT_ALPHA:
                                orderBy = MediaStore.Audio.Playlists.Members.PLAY_ORDER;//MediaStore.Audio.Media.TITLE;//CollectionDb.TRACKS_TRACK};
                                break;
                            case SORT_ARTIST_ALPHA:
                                orderBy = MediaStore.Audio.Media.ARTIST;//CollectionDb.ARTISTS_ARTIST};
                                break;
                            case SORT_LAST_MODIFIED:
                                orderBy = MediaStore.Audio.Media.DATE_MODIFIED;//CollectionDb.TRACKS_LASTMODIFIED};
                                break;
                            default:
                                Log.e(TAG,
                                        collectionId + " - getQueries - sortMode not supported!");
                                return;
                        }
                        String[] mPlaylistMemberCols = new String[] {
                                MediaStore.Audio.Media.ARTIST,
                                MediaStore.Audio.Media.ARTIST_ID,
                                MediaStore.Audio.Media.ALBUM,
                                MediaStore.Audio.Media.TITLE,
                                MediaStore.Audio.Media.DURATION,
                                MediaStore.Audio.Media.DATA,
                                MediaStore.Audio.Media._ID,//MediaStore.Audio.Playlists.Members.AUDIO_ID,
                                MediaStore.Audio.Playlists.Members.PLAY_ORDER,
                                MediaStore.Audio.Playlists.Members._ID,
                                MediaStore.Audio.Media.IS_MUSIC };
                        CollectionDb db = CollectionDbManager.get().getCollectionDb(collectionId);
                        String currentRevision = String.valueOf(db.tracksCurrentRevision());

                        Uri uri = MediaStore.Audio.Playlists.Members.getContentUri(
                                "external", Long.valueOf(playlistId));
                        StringBuilder where = new StringBuilder();
                        where.append(MediaStore.Audio.Media.TITLE + " != ''");

                        Cursor cursor = TomahawkApp.getContext().getContentResolver().query(uri, mPlaylistMemberCols,
                                where.toString(), null, orderBy);

                        Playlist playlist =
                                Playlist.get(playlistId);
                        //if (playlist.getCurrentRevision().isEmpty()) {
                            /*CollectionDb.WhereInfo whereInfo = new CollectionDb.WhereInfo();
                            whereInfo.connection = "OR";
                            whereInfo.where.put(CollectionDb.TRACKS_PLAYLISTID, new String[]{playlistId});
                            Cursor cursor = db.tracks(whereInfo, orderBy);*/
                            if (cursor == null) {
                                deferred.resolve(null);
                                return;
                            }
                            CollectionCursor<PlaylistEntry> collectionCursor
                                    = new CollectionCursor<>(
                                    cursor, PlaylistEntry.class, mResolver, playlist,null);
                            playlist.setCursor(collectionCursor);
                            playlist.setFilled(true);
                            playlist.setCurrentRevision(currentRevision);

                       // }
                        deferred.resolve(playlist);
                    }
                }).start();
            }
        });
        return deferred;
    }

    @Override
    public Promise<Cursor, Throwable, Void> getQueries(final int position, final String playlistId,String string) {
        final Deferred<Cursor, Throwable, Void> deferred = new ADeferredObject<>();
        getCollectionId().done(new DoneCallback<String>() {
            @Override
            public void onDone(final String collectionId) {
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        String orderBy;

                        String[] mPlaylistMemberCols = new String[] {
                                MediaStore.Audio.Media.ARTIST,
                                MediaStore.Audio.Media.ARTIST_ID,
                                MediaStore.Audio.Media.ALBUM,
                                MediaStore.Audio.Media.TITLE,
                                MediaStore.Audio.Media.DURATION,
                                MediaStore.Audio.Media.DATA,
                                MediaStore.Audio.Media._ID,//MediaStore.Audio.Playlists.Members.AUDIO_ID,
                                MediaStore.Audio.Playlists.Members.PLAY_ORDER,
                                MediaStore.Audio.Playlists.Members._ID,
                                MediaStore.Audio.Media.IS_MUSIC };
                        CollectionDb db = CollectionDbManager.get().getCollectionDb(collectionId);
                        String currentRevision = String.valueOf(db.tracksCurrentRevision());

                        Uri uri = MediaStore.Audio.Playlists.Members.getContentUri(
                                "external", Long.valueOf(playlistId));
                        StringBuilder where = new StringBuilder();
                        where.append(MediaStore.Audio.Media.TITLE + " != ''");

                        Cursor cursor = TomahawkApp.getContext().getContentResolver().query(uri, mPlaylistMemberCols,
                                where.toString(), null, null);

                        if (cursor == null) {
                            deferred.resolve(null);
                            return;
                        }else {
                            int colidx = cursor
                                    .getColumnIndexOrThrow(MediaStore.Audio.Playlists.Members._ID);
                            cursor.moveToPosition(position-1);
                            long id = cursor.getLong(colidx);
                            Uri uri1 = MediaStore.Audio.Playlists.Members.getContentUri(
                                    "external", Long.valueOf(playlistId));
                            TomahawkApp.getContext().getContentResolver().delete(
                                    ContentUris.withAppendedId(uri1, id), null, null);
                        }

                        deferred.resolve(cursor);
                    }
                }).start();
            }
        });
        return deferred;
    }

    @Override
    public Promise<Playlist, Throwable, Void> getPlaylist() {
        final Deferred<Playlist, Throwable, Void> deferred = new ADeferredObject<>();
        getCollectionId().done(new DoneCallback<String>() {
            @Override
            public void onDone(final String collectionId) {
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        CollectionDb db = CollectionDbManager.get().getCollectionDb(collectionId);
                        String currentRevision = String.valueOf(db.tracksCurrentRevision());
                        Playlist playlist =
                                Playlist.get(collectionId + "_tracks_" + currentRevision);
                        Cursor cursor = null;
                        SharedPreferences mPreferences =
                                PreferenceManager.getDefaultSharedPreferences(TomahawkApp.getContext());
                        int qlen = mPreferences.getInt("queue_len", -1);


                        StringBuilder where = new StringBuilder();
                        where.append(CollectionDb.TABLE_TRACKS+ "." +CollectionDb.TRACKS_PLAYORDER + " IN (");
                        if(qlen != -1) {
                            for (int i=1;i<=qlen;i++) {
                                where.append(mPreferences.getString("queue_"+i, ""));
                                if (i < qlen) {
                                    where.append(",");
                                }
                            }
                        }
                        where.append(")");
                        if (cursor == null) {
                            cursor = db.tracks(null, null,where.toString());
                        }
                        if (cursor == null) {
                            deferred.resolve(null);
                            return;
                        }
                        CollectionCursor<PlaylistEntry> collectionCursor
                                = new CollectionCursor<>(
                                cursor, PlaylistEntry.class, mResolver, playlist,null);
                        playlist.setCursor(collectionCursor);
                        playlist.setFilled(true);
                        playlist.setCurrentRevision(currentRevision);
                        //}
                        deferred.resolve(playlist);
                    }
                }).start();
            }
        });
        return deferred;
    }

    @Override
    public Promise<Playlist, Throwable, Void> getQueries(final int sortMode) {
        final Deferred<Playlist, Throwable, Void> deferred = new ADeferredObject<>();
        getCollectionId().done(new DoneCallback<String>() {
            @Override
            public void onDone(final String collectionId) {
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        Cursor cursor = null;
                        String[] orderBy;
                        switch (sortMode) {
                            case SORT_ALPHA:
                            case FAVOURITES:
                                orderBy = new String[]{CollectionDb.TRACKS_TRACK};
                                break;
                            case SORT_ARTIST_ALPHA:
                                orderBy = new String[]{CollectionDb.ARTISTS_ARTIST};
                                break;
                            case SORT_LAST_MODIFIED:
                                orderBy = new String[]{CollectionDb.TRACKS_LASTMODIFIED};
                                break;
                            case RECENTLY_ADDED:
                                orderBy = new String[]{CollectionDb.TRACKS_LASTMODIFIED};
                                int X = getIntPref(TomahawkApp.getContext(), "numweeks", 2) * (3600 * 24 * 7);
                                String[] mPlaylistMemberCols = new String[] {
                                        MediaStore.Audio.Media.ARTIST,
                                        MediaStore.Audio.Media.ARTIST_ID,
                                        MediaStore.Audio.Media.ALBUM,
                                        MediaStore.Audio.Media.TITLE,
                                        MediaStore.Audio.Media.DURATION,
                                        MediaStore.Audio.Media.DATA,
                                        MediaStore.Audio.Media._ID,//COMPOSER
                                        MediaStore.Audio.Media.DATE_ADDED,
                                        MediaStore.Audio.Media.DATE_MODIFIED};
                                String where = MediaStore.MediaColumns.DATE_ADDED + ">" + (System.currentTimeMillis() / 1000 - X);
                                cursor = TomahawkApp.getContext().getContentResolver().query(MediaStore.Audio.Media.EXTERNAL_CONTENT_URI,
                                        mPlaylistMemberCols, where, null, MediaStore.Audio.Media.DEFAULT_SORT_ORDER);


                                break;
                            default:
                                Log.e(TAG,
                                        collectionId + " - getQueries - sortMode not supported!");
                                return;
                        }
                        CollectionDb db = CollectionDbManager.get().getCollectionDb(collectionId);
                        String currentRevision = String.valueOf(db.tracksCurrentRevision());
                        Playlist playlist =
                                Playlist.get(collectionId + "_tracks_" + currentRevision);

                        //if (playlist.getCurrentRevision().isEmpty()) {
                            CollectionDb.WhereInfo whereInfo = null;
                            if (sortMode == FAVOURITES){
                                whereInfo = new CollectionDb.WhereInfo();
                                whereInfo.connection = "OR";
                                whereInfo.where.put(CollectionDb.TRACKS_LIKEDISLIKE, new String[]{"1"});

                            }
                            if (cursor == null) {
                                cursor = db.tracks(whereInfo, orderBy,null);
                            }
                            if (cursor == null) {
                                deferred.resolve(null);
                                return;
                            }
                            CollectionCursor<PlaylistEntry> collectionCursor
                                    = new CollectionCursor<>(
                                    cursor, PlaylistEntry.class, mResolver, playlist,null);
                            playlist.setCursor(collectionCursor);
                            playlist.setFilled(true);
                        playlist.setCurrentRevision(currentRevision);
                        //}
                        deferred.resolve(playlist);
                    }
                }).start();
            }
        });
        return deferred;
    }

    @Override
    public Promise<Playlist, Throwable, Void> getQueries(final String tracksLike,final int sortMode) {
        final Deferred<Playlist, Throwable, Void> deferred = new ADeferredObject<>();
        getCollectionId().done(new DoneCallback<String>() {
            @Override
            public void onDone(final String collectionId) {
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        Cursor cursor = null;
                        String[] orderBy;
                        switch (sortMode) {
                            case SORT_ALPHA:
                                orderBy = new String[]{CollectionDb.TRACKS_TRACK};
                                break;
                            case SORT_ARTIST_ALPHA:
                                orderBy = new String[]{CollectionDb.ARTISTS_ARTIST};
                                break;
                            case SORT_LAST_MODIFIED:
                                orderBy = new String[]{CollectionDb.TRACKS_LASTMODIFIED};
                                break;
                            case RECENTLY_ADDED:
                                orderBy = new String[]{CollectionDb.TRACKS_LASTMODIFIED};
                                int X = getIntPref(TomahawkApp.getContext(), "numweeks", 2) * (3600 * 24 * 7);
                                String[] mPlaylistMemberCols = new String[] {
                                        MediaStore.Audio.Media.ARTIST,
                                        MediaStore.Audio.Media.ARTIST_ID,
                                        MediaStore.Audio.Media.ALBUM,
                                        MediaStore.Audio.Media.TITLE,
                                        MediaStore.Audio.Media.DURATION,
                                        MediaStore.Audio.Media.DATA,
                                        MediaStore.Audio.Media._ID,//COMPOSER
                                        MediaStore.Audio.Media.DATE_ADDED,
                                        MediaStore.Audio.Media.DATE_MODIFIED};
                                String where = MediaStore.MediaColumns.DATE_ADDED + ">" + (System.currentTimeMillis() / 1000 - X);
                                cursor = TomahawkApp.getContext().getContentResolver().query( MediaStore.Audio.Media.EXTERNAL_CONTENT_URI,
                                        mPlaylistMemberCols, where, null, MediaStore.Audio.Media.DEFAULT_SORT_ORDER);


                                break;
                            default:
                                Log.e(TAG,
                                        collectionId + " - getQueries - sortMode not supported!");
                                return;
                        }
                        CollectionDb db = CollectionDbManager.get().getCollectionDb(collectionId);
                        String currentRevision = String.valueOf(db.tracksCurrentRevision());
                        Playlist playlist =
                                Playlist.get(collectionId + "_tracks_" + currentRevision);

                        //if (playlist.getCurrentRevision().isEmpty()) {

                        CollectionDb.WhereInfo whereInfo = null;
                        String type = null;
                        if (tracksLike!=null && !tracksLike.equals("")){
                            type = "Search";
                            whereInfo = new CollectionDb.WhereInfo();
                            whereInfo.connection = "OR";
                            whereInfo.where.put(CollectionDb.TRACKS_TRACK + " like ", new String[]{"%" + tracksLike + "%"});
                            //whereInfo.where.put(CollectionDb.ARTISTS_ARTIST + " like ", new String[]{"%" + albumLike + "%"});
                        }
                        if (cursor == null) {
                            cursor = db.tracks(whereInfo, orderBy,null);
                        }
                        if (cursor == null) {
                            deferred.resolve(null);
                            return;
                        }
                        CollectionCursor<PlaylistEntry> collectionCursor
                                = new CollectionCursor<>(
                                cursor, PlaylistEntry.class, mResolver, playlist,type);
                        playlist.setCursor(collectionCursor);
                        playlist.setFilled(true);
                        playlist.setCurrentRevision(currentRevision);
                        //}
                        deferred.resolve(playlist);
                    }
                }).start();
            }
        });
        return deferred;
    }

    public static int getIntPref(Context context, String name, int def) {
        SharedPreferences preferences =
                PreferenceManager.getDefaultSharedPreferences(context);
        return preferences.getInt(name, def);
    }
    @Override
    public Promise<CollectionCursor<Artist>, Throwable, Void> getArtists(final int sortMode, final String artistLike) {
        final Deferred<CollectionCursor<Artist>, Throwable, Void> deferred
                = new ADeferredObject<>();
        getCollectionId().done(new DoneCallback<String>() {
            @Override
            public void onDone(final String collectionId) {
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        String[] orderBy;
                        switch (sortMode) {
                            case SORT_ALPHA:
                                orderBy = new String[]{CollectionDb.ARTISTS_ARTIST};
                                break;
                            case SORT_LAST_MODIFIED:
                                orderBy = new String[]{CollectionDb.ARTISTS_LASTMODIFIED};
                                break;
                            default:
                                Log.e(TAG,
                                        collectionId + " - getArtists - sortMode not supported!");
                                return;
                        }
                        CollectionDb.WhereInfo whereInfo = null;
                        String type = null;
                        if (artistLike!=null && !artistLike.equals("")){
                            type = "Search";
                            whereInfo = new CollectionDb.WhereInfo();
                            whereInfo.connection = "OR";
                            whereInfo.where.put(CollectionDb.ARTISTS_ARTIST + " like ", new String[]{"%" + artistLike + "%"});
                            //whereInfo.where.put(CollectionDb.ARTISTS_ARTIST + " like ", new String[]{"%" + albumLike + "%"});
                        }
                        Cursor cursor = CollectionDbManager.get().getCollectionDb(collectionId)
                                .artists(orderBy, whereInfo);
                        CollectionCursor<Artist> collectionCursor =
                                new CollectionCursor<>(cursor, Artist.class, null, null,type);
                        deferred.resolve(collectionCursor);
                    }
                }).start();
            }
        });
        return deferred;
    }

    @Override
    public Promise<CollectionCursor<Genre>, Throwable, Void> getGenres(final int sortMode, final String artistLike) {
        final Deferred<CollectionCursor<Genre>, Throwable, Void> deferred
                = new ADeferredObject<>();
        getCollectionId().done(new DoneCallback<String>() {
            @Override
            public void onDone(final String collectionId) {
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        String[] orderBy;
                        switch (sortMode) {
                            case SORT_ALPHA:
                                orderBy = new String[]{CollectionDb.GENRES_ARTIST};
                                break;
                            case SORT_LAST_MODIFIED:
                                orderBy = new String[]{CollectionDb.GENRES_LASTMODIFIED};
                                break;
                            default:
                                Log.e(TAG,
                                        collectionId + " - getArtists - sortMode not supported!");
                                return;
                        }
                        CollectionDb.WhereInfo whereInfo = null;
                        String type = null;
                        if (artistLike!=null && !artistLike.equals("")){
                            type = "Search";
                            whereInfo = new CollectionDb.WhereInfo();
                            whereInfo.connection = "OR";
                            whereInfo.where.put(CollectionDb.GENRES_ARTIST + " like ", new String[]{"%" + artistLike + "%"});
                            //whereInfo.where.put(CollectionDb.ARTISTS_ARTIST + " like ", new String[]{"%" + albumLike + "%"});
                        }
                        Cursor cursor = CollectionDbManager.get().getCollectionDb(collectionId)
                                .genres(orderBy, whereInfo);
                        CollectionCursor<Genre> collectionCursor =
                                new CollectionCursor<>(cursor, Genre.class, null, null,type);
                        deferred.resolve(collectionCursor);
                    }
                }).start();
            }
        });
        return deferred;
    }

    @Override
    public Promise<CollectionCursor<Folder>, Throwable, Void> getFolders(final int sortMode, final String albumLike) {
        final Deferred<CollectionCursor<Folder>, Throwable, Void> deferred
                = new ADeferredObject<>();
        getCollectionId().done(new DoneCallback<String>() {
            @Override
            public void onDone(final String collectionId) {
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        String[] orderBy;
                        switch (sortMode) {
                            case SORT_ALPHA:
                                orderBy = new String[]{CollectionDb.FOLDERS_ALBUM};
                                break;
                            case SORT_LAST_MODIFIED:
                                orderBy = new String[]{CollectionDb.FOLDERS_PATH};
                                break;
                            default:
                                Log.e(TAG,
                                        collectionId + " - getArtists - sortMode not supported!");
                                return;
                        }
                        CollectionDb.WhereInfo whereInfo = null;
                        String type = null;
                        if (albumLike!=null && !albumLike.equals("")){
                            type = "Search";
                            whereInfo = new CollectionDb.WhereInfo();
                            whereInfo.connection = "OR";
                            whereInfo.where.put(CollectionDb.FOLDERS_ALBUM + " like ", new String[]{"%" + albumLike + "%"});
                            //whereInfo.where.put(CollectionDb.ARTISTS_ARTIST + " like ", new String[]{"%" + albumLike + "%"});
                        }
                        Cursor cursor = CollectionDbManager.get().getCollectionDb(collectionId)
                                .folders(orderBy, whereInfo);
                        CollectionCursor<Folder> collectionCursor =
                                new CollectionCursor<>(cursor, Folder.class, null, null,type);
                        deferred.resolve(collectionCursor);
                    }
                }).start();
            }
        });
        return deferred;
    }

    @Override
    public Promise<CollectionCursor<Artist>, Throwable, Void> getAlbumArtists(final int sortMode) {
        final Deferred<CollectionCursor<Artist>, Throwable, Void> deferred
                = new ADeferredObject<>();
        getCollectionId().done(new DoneCallback<String>() {
            @Override
            public void onDone(final String collectionId) {
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        String[] orderBy;
                        switch (sortMode) {
                            case SORT_ALPHA:
                                orderBy = new String[]{CollectionDb.ARTISTS_ARTIST};
                                break;
                            case SORT_LAST_MODIFIED:
                                orderBy = new String[]{CollectionDb.ARTISTS_LASTMODIFIED+" DESC"};
                                break;
                            default:
                                Log.e(TAG, collectionId
                                        + " - getAlbumArtists - sortMode not supported!");
                                return;
                        }
                        Cursor cursor = CollectionDbManager.get().getCollectionDb(collectionId)
                                .albumArtists(orderBy);
                        CollectionCursor<Artist> collectionCursor =
                                new CollectionCursor<>(cursor, Artist.class, null, null,null);
                        deferred.resolve(collectionCursor);
                    }
                }).start();
            }
        });
        return deferred;
    }

    @Override
    public Promise<CollectionCursor<Album>, Throwable, Void> getAlbums(final int sortMode, final String albumLike) {
        final Deferred<CollectionCursor<Album>, Throwable, Void> deferred = new ADeferredObject<>();
        getCollectionId().done(new DoneCallback<String>() {
            @Override
            public void onDone(final String collectionId) {
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        String[] orderBy;
                        switch (sortMode) {
                            case SORT_ALPHA:
                                orderBy = new String[]{CollectionDb.ALBUMS_ALBUM};
                                break;
                            case SORT_ARTIST_ALPHA:
                                orderBy = new String[]{CollectionDb.ARTISTS_ARTIST};
                                break;
                            case SORT_LAST_MODIFIED:
                                orderBy = new String[]{CollectionDb.ALBUMS_LASTMODIFIED +" DESC"};
                                break;
                            default:
                                Log.e(TAG, collectionId + " - getAlbums - sortMode not supported!");
                                return;
                        }
                        CollectionDb.WhereInfo whereInfo = null;
                        String type = null;
                        if (albumLike!=null && !albumLike.equals("")){
                            type = "Search";
                            whereInfo = new CollectionDb.WhereInfo();
                            whereInfo.connection = "OR";
                            whereInfo.where.put(CollectionDb.ALBUMS_ALBUM + " like ", new String[]{"%" + albumLike + "%"});
                            //whereInfo.where.put(CollectionDb.ARTISTS_ARTIST + " like ", new String[]{"%" + albumLike + "%"});
                        }
                        Cursor cursor = CollectionDbManager.get().getCollectionDb(collectionId)
                                .albums(orderBy, whereInfo);
                        CollectionCursor<Album> collectionCursor =
                                new CollectionCursor<>(cursor, Album.class, null, null,type);
                        deferred.resolve(collectionCursor);
                    }
                }).start();
            }
        });
        return deferred;
    }

    @Override
    public Promise<CollectionCursor<Album>, Throwable, Void> getArtistAlbums(final Artist artist) {
        final Deferred<CollectionCursor<Album>, Throwable, Void> deferred = new ADeferredObject<>();
        getCollectionId().done(new DoneCallback<String>() {
            @Override
            public void onDone(final String result) {
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        Cursor cursor = CollectionDbManager.get().getCollectionDb(result)
                                .artistAlbums(artist.getName(), "");
                        if (cursor == null) {
                            deferred.resolve(null);
                            return;
                        }
                        CollectionCursor<Album> collectionCursor =
                                new CollectionCursor<>(cursor, Album.class, null, null,null);
                        deferred.resolve(collectionCursor);
                    }
                }).start();
            }
        });
        return deferred;
    }

    @Override
    public Promise<CollectionCursor<Album>, Throwable, Void> getGenreAlbums(final Genre genre) {
        final Deferred<CollectionCursor<Album>, Throwable, Void> deferred = new ADeferredObject<>();
        getCollectionId().done(new DoneCallback<String>() {
            @Override
            public void onDone(final String result) {
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        Cursor cursor = CollectionDbManager.get().getCollectionDb(result)
                                .genreAlbums(genre.getName(), "");
                        if (cursor == null) {
                            deferred.resolve(null);
                            return;
                        }
                        CollectionCursor<Album> collectionCursor =
                                new CollectionCursor<>(cursor, Album.class, null, null,null);
                        deferred.resolve(collectionCursor);
                    }
                }).start();
            }
        });
        return deferred;
    }

    @Override
    public Promise<CollectionCursor<Album>, Throwable, Void> getFolderAlbums(final  Folder genre) {
        final Deferred<CollectionCursor<Album>, Throwable, Void> deferred = new ADeferredObject<>();
        getCollectionId().done(new DoneCallback<String>() {
            @Override
            public void onDone(final String result) {
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        Cursor cursor = CollectionDbManager.get().getCollectionDb(result)
                                .folderAlbums(genre.getName(), "");
                        if (cursor == null) {
                            deferred.resolve(null);
                            return;
                        }
                        CollectionCursor<Album> collectionCursor =
                                new CollectionCursor<>(cursor, Album.class, null, null,null);
                        deferred.resolve(collectionCursor);
                    }
                }).start();
            }
        });
        return deferred;
    }

    @Override
    public Promise<CollectionCursor<Genre>, Throwable, Void> getAlbumGenres(final int sortMode) {
        final Deferred<CollectionCursor<Genre>, Throwable, Void> deferred
                = new ADeferredObject<>();
        getCollectionId().done(new DoneCallback<String>() {
            @Override
            public void onDone(final String collectionId) {
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        String[] orderBy;
                        switch (sortMode) {
                            case SORT_ALPHA:
                                orderBy = new String[]{CollectionDb.GENRES_ARTIST};
                                break;
                            case SORT_LAST_MODIFIED:
                                orderBy = new String[]{CollectionDb.GENRES_ARTISTDISAMBIGUATION};
                                break;
                            default:
                                Log.e(TAG, collectionId
                                        + " - getAlbumArtists - sortMode not supported!");
                                return;
                        }
                        Cursor cursor = CollectionDbManager.get().getCollectionDb(collectionId)
                                .albumGenres(orderBy);
                        CollectionCursor<Genre> collectionCursor =
                                new CollectionCursor<>(cursor, Genre.class, null, null,null);
                        deferred.resolve(collectionCursor);
                    }
                }).start();
            }
        });
        return deferred;
    }

    @Override
    public Promise<Playlist, Throwable, Void> getAlbumTracks(final Album album) {
        final Deferred<Playlist, Throwable, Void> deferred = new ADeferredObject<>();
        getCollectionId().done(new DoneCallback<String>() {
            @Override
            public void onDone(final String collectionId) {
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        CollectionDb db = CollectionDbManager.get().getCollectionDb(collectionId);
                        String currentRevision;
                        if (album.getArtist() != null)
                         currentRevision = String.valueOf(db.albumCurrentRevision(
                                album.getName(), album.getArtist().getName(), ""));
                        else
                        currentRevision = String.valueOf(db.genreCurrentRevision(album.getName(),album.getGenre().getName(),""));
                        Playlist playlist = Playlist.get(
                                collectionId + "_" + album.getCacheKey() + "_" + currentRevision);
                        if (playlist.getCurrentRevision().isEmpty()) {
                            Cursor cursor;
                            if (album.getArtist()!=null) {
                                cursor = db.albumTracks(
                                        album.getName(), album.getArtist().getName(), "");
                            }else{
                                cursor = db.albumGenres(album.getName(),album.getGenre().getName(),"");
                            }
                            if (cursor == null) {
                                deferred.resolve(null);
                                return;
                            }
                            CollectionCursor<PlaylistEntry> collectionCursor
                                    = new CollectionCursor<>(
                                    cursor, PlaylistEntry.class, mResolver, playlist,null);
                            playlist.setCursor(collectionCursor);
                            playlist.setFilled(true);
                            playlist.setCurrentRevision(currentRevision);
                        }
                        deferred.resolve(playlist);
                    }
                }).start();
            }
        });
        return deferred;
    }

    @Override
    public Promise<Playlist, Throwable, Void> getArtistTracks(final Artist artist) {
        final Deferred<Playlist, Throwable, Void> deferred = new ADeferredObject<>();
        getCollectionId().done(new DoneCallback<String>() {
            @Override
            public void onDone(final String collectionId) {
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        CollectionDb db = CollectionDbManager.get().getCollectionDb(collectionId);
                             Cursor cursor = null;
                        String currentRevision = String.valueOf(db.artistCurrentRevision(artist.getName()));
                        Playlist playlist = Playlist.get(
                                collectionId + "_" + artist.getCacheKey() + "_" + currentRevision);
                            if (artist!=null) {
                                cursor = db.artistTracks(
                                        artist.getName());
                            }
                            if (cursor == null) {
                                deferred.resolve(null);
                                return;
                            }
                            CollectionCursor<PlaylistEntry> collectionCursor
                                    = new CollectionCursor<>(
                                    cursor, PlaylistEntry.class, mResolver, playlist,null);
                            playlist.setCursor(collectionCursor);
                            playlist.setFilled(true);
                            playlist.setCurrentRevision(currentRevision);

                        deferred.resolve(playlist);
                    }
                }).start();
            }
        });
        return deferred;
    }

    @Override
    public Promise<Playlist, Throwable, Void> getGenreTracks(final Genre album) {
        final Deferred<Playlist, Throwable, Void> deferred = new ADeferredObject<>();
        getCollectionId().done(new DoneCallback<String>() {
            @Override
            public void onDone(final String collectionId) {
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        CollectionDb db = CollectionDbManager.get().getCollectionDb(collectionId);
                        Cursor cursor = null;
                        String currentRevision = String.valueOf(db.genreCurrentRevision(album.getName()));
                        Playlist playlist = Playlist.get(
                                collectionId + "_" + album.getCacheKey() + "_" + currentRevision);
                        if (album!=null) {
                            cursor = db.genreTracks(
                                    album.getName());
                        }
                        if (cursor == null) {
                            deferred.resolve(null);
                            return;
                        }
                        CollectionCursor<PlaylistEntry> collectionCursor
                                = new CollectionCursor<>(
                                cursor, PlaylistEntry.class, mResolver, playlist,null);
                        playlist.setCursor(collectionCursor);
                        playlist.setFilled(true);
                        playlist.setCurrentRevision(currentRevision);

                        deferred.resolve(playlist);
                    }
                }).start();
            }
        });
        return deferred;
    }

    @Override
    public Promise<Playlist, Throwable, Void> getFolderTracks(final Folder album) {
        final Deferred<Playlist, Throwable, Void> deferred = new ADeferredObject<>();
        getCollectionId().done(new DoneCallback<String>() {
            @Override
            public void onDone(final String collectionId) {
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        CollectionDb db = CollectionDbManager.get().getCollectionDb(collectionId);
                        String currentRevision = String.valueOf(db.folderCurrentRevision(album.getFolderPath()));
                                //album.getName(), album.getFolderName(), ""));
                        Playlist playlist = Playlist.get(
                                collectionId + "_" + album.getCacheKey() + "_" + currentRevision);
                        if (playlist.getCurrentRevision().isEmpty()) {
                            Cursor cursor = db.folderTracks(album);
                            if (cursor == null) {
                                deferred.resolve(null);
                                return;
                            }
                            CollectionCursor<PlaylistEntry> collectionCursor
                                    = new CollectionCursor<>(
                                    cursor, PlaylistEntry.class, mResolver, playlist,null);
                            playlist.setCursor(collectionCursor);
                            playlist.setFilled(true);
                            playlist.setCurrentRevision(currentRevision);
                        }
                        deferred.resolve(playlist);
                    }
                }).start();
            }
        });
        return deferred;
    }

    @Override
    public Promise<Integer, Throwable, Void> getAlbumTrackCount(final Album album) {
        final Deferred<Integer, Throwable, Void> deferred = new ADeferredObject<>();
        getCollectionId().done(new DoneCallback<String>() {
            @Override
            public void onDone(final String collectionId) {
                new Thread(new Runnable() {
                    @Override
                    public void run() {

                        Cursor cursor = CollectionDbManager.get().getCollectionDb(collectionId)
                                .albumTracks(album.getName(), album.getArtist().getName(), "");
                        if (cursor == null) {
                            deferred.resolve(null);
                            return;
                        }
                        deferred.resolve(cursor.getCount());
                        cursor.close();
                    }
                }).start();
            }
        });
        return deferred;
    }
    @Override
    public Promise<Integer, Throwable, Void> getGenreAlbumTrackCount(final Album album) {
        final Deferred<Integer, Throwable, Void> deferred = new ADeferredObject<>();
        getCollectionId().done(new DoneCallback<String>() {
            @Override
            public void onDone(final String collectionId) {
                new Thread(new Runnable() {
                    @Override
                    public void run() {

                        Cursor cursor = CollectionDbManager.get().getCollectionDb(collectionId)
                                .albumGenres(album.getName(), album.getGenre().getName(), "");
                        if (cursor == null) {
                            deferred.resolve(null);
                            return;
                        }
                        deferred.resolve(cursor.getCount());
                        cursor.close();
                    }
                }).start();
            }
        });
        return deferred;
    }

    public Promise<Integer, Throwable, Void> getFolderTrackCount(final Folder album) {
        final Deferred<Integer, Throwable, Void> deferred = new ADeferredObject<>();
        getCollectionId().done(new DoneCallback<String>() {
            @Override
            public void onDone(final String collectionId) {
                new Thread(new Runnable() {
                    @Override
                    public void run() {
                        Cursor cursor = CollectionDbManager.get().getCollectionDb(collectionId)
                                .folderTracks(album);
                        if (cursor == null) {
                            deferred.resolve(null);
                            return;
                        }
                        deferred.resolve(cursor.getCount());
                        cursor.close();
                    }
                }).start();
            }
        });
        return deferred;
    }
}
