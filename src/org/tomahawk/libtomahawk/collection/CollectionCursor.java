/* == This file is part of Tomahawk Player - <http://tomahawk-player.org> ===
 *
 *   Copyright 2015, Enno Gottschalk <mrmaffen@googlemail.com>
 *
 *   Tomahawk is free software: you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation, either version 3 of the License, or
 *   (at your option) any later version.
 *
 *   Tomahawk is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with Tomahawk. If not, see <http://www.gnu.org/licenses/>.
 */
package org.tomahawk.libtomahawk.collection;

import android.database.Cursor;
import android.text.TextUtils;
import android.util.Log;
import android.util.SparseArray;

import org.tomahawk.libtomahawk.database.CollectionDb;
import org.tomahawk.libtomahawk.resolver.Query;
import org.tomahawk.libtomahawk.resolver.Resolver;
import org.tomahawk.libtomahawk.resolver.Result;
import org.tomahawk.tomahawk_android.activities.TomahawkMainActivity;

import java.util.List;

public class CollectionCursor<T> {

    private final static String TAG = CollectionCursor.class.getSimpleName();

    private int mSortMode;

    private List<T> mMergedItems;

    private SparseArray<Index> mIndex;

    private static class Index {

        int index;

        boolean fromMergedItems;
    }

    private SparseArray<T> mCursorCache = new SparseArray<>();

    private Cursor mCursor;

    private int mCursorCount;

    private List<T> mItems;

    private Class<T> mClass;

    private Resolver mResolver;

    private Playlist mPlaylist;

    private String mType;

    public CollectionCursor(Cursor cursor, Class<T> clss, Resolver resolver, Playlist playlist,String type) {
        mCursor = cursor;
        mType = type;
        mCursorCount = cursor.getCount();
        mClass = clss;
        if (clss == PlaylistEntry.class || clss == Result.class) {
            if (resolver != null) {
                mResolver = resolver;
            } else {
                throw new RuntimeException("Resolver is required for "
                        + "CollectionCursor<PlaylistEntry> or CollectionCursor<Result>!");
            }
        }
        if (clss == PlaylistEntry.class) {
            if (playlist != null) {
                mPlaylist = playlist;
            } else {
                throw new RuntimeException("Playlist is required for "
                        + "CollectionCursor<PlaylistEntry>!");
            }
        }
    }

    public CollectionCursor(List<T> items, Class<T> clss) {
        mItems = items;
        mClass = clss;
    }

    public void close() {
        if (mCursor != null) {
            mCursor.close();
        }
    }

    public T get(int location) {
        boolean fromMergedItems = false;
        if (mIndex != null) {
            Index index = mIndex.get(location);
            location = index.index;
            fromMergedItems = index.fromMergedItems;
        }
        if (fromMergedItems) {
            return mMergedItems.get(location);
        } else {
            return rawGet(location);
        }
    }

    private T rawGet(int location) {
        if (mCursor != null) {
            if (mCursor.isClosed()) {
                Log.d(TAG, "rawGet - Cursor has been closed.");
                return null;
            }
            T cachedItem = mCursorCache.get(location);
            if (cachedItem == null) {
                mCursor.moveToPosition(location);
                if (mClass == PlaylistEntry.class) {
                    Genre genre = null;
                    Album album = null;
                    Track track = null;
                    PlaylistEntry entry;
                    if(mCursorCount == 0){
                        Artist artist = Artist.get("");
                        album = Album.get("Album Empty", artist);
                        track = Track.get("-1", "Track Empty", album, artist);
                        Result result = Result.get("", track, mResolver);
                        Query query = Query.get(result, false);
                        query.addTrackResult(result, 1.0f);
                        String id = TomahawkMainActivity.getLifetimeUniqueStringId();
                        entry = PlaylistEntry.get(mPlaylist.getId(), query,
                                id);
                    }else {
                        Artist artist = Artist.get(mCursor.getString(0));
                        if (artist == null) {
                            genre = Genre.get(mCursor.getString(0));
                            album = Album.get(mCursor.getString(2), genre);
                            track = Track.get(mCursor.getString(6), mCursor.getString(3), album, genre);
                        } else {
                            album = Album.get(mCursor.getString(2), artist);
                            track = Track.get(mCursor.getString(6), mCursor.getString(3), album, artist);
                        }

                        track.setDuration(mCursor.getInt(4));// * 1000);
                        track.setAlbumPos(mCursor.getInt(7));
                        track.setPath(mCursor.getString(5));
                        if (mCursor.getColumnIndex(CollectionDb.TRACKS_LIKEDISLIKE) != -1)
                        {
                            track.setmLike(mCursor.getInt(9));
                        }
                        Result result = Result.get(mCursor.getString(5), track, mResolver);
                        result.getTrack().setPath(mCursor.getString(5));
                        Query query = Query.get(result, false);
                        query.getBasicTrack().setPath(mCursor.getString(5));
                        query.addTrackResult(result, 1.0f);
                        String id = TomahawkMainActivity.getLifetimeUniqueStringId();
                        if (mCursor.getString(6) != null) {
                            id = mCursor.getString(6);
                        }
                        entry = PlaylistEntry.get(mPlaylist.getId(), query,
                                id);
                    }
                    cachedItem = (T) entry;
                } else if (mClass == Result.class) {
                    Artist artist = Artist.get(mCursor.getString(0));
                    Album album = Album.get(mCursor.getString(2), artist);
                    Track track = Track.get(mCursor.getString(6),mCursor.getString(3), album, artist);
                    track.setDuration(mCursor.getInt(4));// * 1000);
                    track.setAlbumPos(mCursor.getInt(7));
                    track.setPath(mCursor.getString(5));
                    Result result = Result.get(mCursor.getString(5), track, mResolver);
                    cachedItem = (T) result;
                } else if (mClass == Album.class) {
                    Genre genre = null;
                    Album album;
                    Artist artist = null;
                    if(mCursorCount == 0){
                        artist = Artist.get("");
                        album = Album.get("Album Empty", artist);
                    }else {
                        if (mCursor.getColumnIndex(CollectionDb.ARTISTS_ARTIST) != -1)
                            artist = Artist.get(mCursor.getString(mCursor.getColumnIndex(CollectionDb.ARTISTS_ARTIST)));
                        if (artist == null) {
                            genre = Genre.get(mCursor.getString(mCursor.getColumnIndex(CollectionDb.GENRES_ARTIST)));
                            album = Album.get(mCursor.getString(0), genre);
                        } else
                            album = Album.get(mCursor.getString(0), artist);
                        String imagePath = mCursor.getString(3);
                        if (!TextUtils.isEmpty(imagePath)) {
                            album.setImage(Image.get(imagePath, false));
                            if (artist != null) {
                                artist.setImage(Image.get(imagePath, false));
                            }
                            if (genre != null) {
                                genre.setImage(Image.get(imagePath, false));
                            }
                        }
                        if (mCursor.getColumnIndex(CollectionDb.ALBUMS_LIKEDISLIKE) != -1){
                            album.setmLike(mCursor.getInt(5));
                        }
                    }
                    cachedItem = (T) album;
                } else if (mClass == Artist.class) {
                    Artist artist;
                    if(mCursorCount == 0){
                        artist = Artist.get("Artist Empty");
                    }else {
                        artist = Artist.get(mCursor.getString(0));
                        if (mCursor.getColumnIndex(CollectionDb.ARTISTS_LIKEDISLIKE) != -1){
                            artist.setmLike(mCursor.getInt(3));
                        }
                    }
                    cachedItem = (T) artist;
                } else if (mClass == Genre.class) {
                    Genre genre;
                    if(mCursorCount == 0){
                        genre = Genre.get("Genre Empty");
                    }else {
                        genre = Genre.get(mCursor.getString(0));
                        if (mCursor.getColumnIndex(CollectionDb.GENRES_LIKEDISLIKE) != -1){
                            genre.setmLike(mCursor.getInt(3));
                        }
                    }
                        cachedItem = (T)genre;
                } else if (mClass == Folder.class){
                    Folder folder;
                    if(mCursorCount == 0){
                        folder = Folder.get("Folder Empty");
                        folder.setFolderName("Folder Empty");

                    }else {
                        folder = Folder.get(mCursor.getString(4));
                        folder.setFolderName(mCursor.getString(0));
                        folder.setFolderPath(mCursor.getString(4));
                        folder.setFolderId(Integer.valueOf(mCursor.getString(5)));
                        String imgPath = mCursor.getString(3);
                        if (!TextUtils.isEmpty(imgPath)) {
                            folder.setImage(Image.get(imgPath, false));
                        }
                        if (mCursor.getColumnIndex(CollectionDb.FOLDERS_LIKEDISLIKE) != -1){
                            folder.setmLike(mCursor.getInt(6));
                        }
                    }
                        cachedItem = (T)folder;
                }
                mCursorCache.put(location, cachedItem);
            }
            return cachedItem;
        } else {
            return mItems.get(location);
        }
    }

    public int size() {
        if (mIndex != null) {
            return mIndex.size();
        } else if (mCursor != null) {
            if (mType!=null){
                return (mCursorCount==0)?1:mCursorCount;
            }else
            return mCursorCount;
        } else {
            return mItems.size();
        }
    }

    public void mergeItems(int sortMode, List<T> items) {
        mMergedItems = items;
        mSortMode = sortMode;

        updateIndex();
    }

    private void updateIndex() {
        mIndex = new SparseArray<>();
        int size1 = mCursor != null ? mCursorCount : mItems.size();
        int size2 = mMergedItems.size();
        int counter1 = 0;
        int counter2 = 0;
        int i = 0;
        while (counter1 < size1 || counter2 < size2) {
            if (mCursor != null && mCursor.isClosed()) {
                Log.e(TAG, "updateIndex - Aborting. Cursor has been closed.");
                return;
            }
            int compareResult;
            if (counter1 < size1 && counter2 < size2) {
                compareResult =
                        getSortString(mMergedItems, counter2).compareTo(getSortString(counter1));
            } else if (counter1 >= size1) {
                compareResult = -1;
            } else {
                compareResult = 1;
            }
            if (compareResult > 0) {
                Index index = new Index();
                index.fromMergedItems = false;
                index.index = counter1++;
                mIndex.put(i++, index);
            } else if (compareResult < 0) {
                Index index = new Index();
                index.fromMergedItems = true;
                index.index = counter2++;
                mIndex.put(i++, index);
            } else {
                if (rawGet(counter1) != mMergedItems.get(counter2)) {
                    Index index = new Index();
                    index.fromMergedItems = true;
                    index.index = counter2++;
                    mIndex.put(i++, index);
                } else {
                    counter2++;
                }
                Index index = new Index();
                index.fromMergedItems = false;
                index.index = counter1++;
                mIndex.put(i++, index);
            }
        }
    }

    public String getArtistName(int location) {
        if (mCursor != null) {
            mCursor.moveToPosition(location);
            if (mClass == PlaylistEntry.class || mClass == Result.class || mClass == Artist.class) {
                return mCursor.getString(0);
            } else if (mClass == Album.class) {
                return mCursor.getString(1);
            }
        } else {
            Object o = mItems.get(location);
            if (o instanceof PlaylistEntry) {
                return ((PlaylistEntry) o).getArtist().getName();
            } else if (o instanceof Result) {
                return ((Result) o).getArtist().getName();
            } else if (o instanceof Album) {
                return ((Album) o).getArtist().getName();
            } else if (o instanceof Artist) {
                return ((Artist) o).getName();
            }
            return ((ArtistAlphaComparable) mItems.get(location)).getArtist().getName();
        }
        Log.e(TAG, "getArtistName(int location) - Couldn't return a string");
        return null;
    }

    private String getSortString(int location) {
        if (mCursor != null) {
            mCursor.moveToPosition(location);
            if (mClass == PlaylistEntry.class || mClass == Result.class) {
                if (mSortMode == Collection.SORT_ALPHA) {
                    return mCursor.getString(3);
                } else if (mSortMode == Collection.SORT_ARTIST_ALPHA) {
                    return mCursor.getString(0);
                } else if (mSortMode == Collection.SORT_LAST_MODIFIED) {
                    return mCursor.getString(8);
                }
            } else if (mClass == Album.class) {
                if (mSortMode == Collection.SORT_ALPHA) {
                    return mCursor.getString(0);
                } else if (mSortMode == Collection.SORT_ARTIST_ALPHA) {
                    return mCursor.getString(1);
                } else if (mSortMode == Collection.SORT_LAST_MODIFIED) {
                    return mCursor.getString(4);
                }
            } else if (mClass == Artist.class) {
                if (mSortMode == Collection.SORT_ALPHA) {
                    return mCursor.getString(0);
                } else if (mSortMode == Collection.SORT_LAST_MODIFIED) {
                    return mCursor.getString(2);
                }
            }
        } else {
            return getSortString(mItems, location);
        }
        Log.e(TAG, "getSortString(int location) - Couldn't return a string");
        return null;
    }

    private String getSortString(List<T> items, int location) {
        if (mSortMode == Collection.SORT_ALPHA) {
            AlphaComparable item = (AlphaComparable) items.get(location);
            return item.getName();
        } else if (mSortMode == Collection.SORT_ARTIST_ALPHA) {
            ArtistAlphaComparable item = (ArtistAlphaComparable) items.get(location);
            return item.getArtist().getName();
        } else if (mSortMode == Collection.SORT_LAST_MODIFIED) {
            AlphaComparable item = (AlphaComparable) items.get(location); //TODO
            return item.getName();
        }
        Log.e(TAG, "getSortString(List<T> items, int location) - Couldn't return a string");
        return null;
    }
}
