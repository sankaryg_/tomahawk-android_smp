package org.tomahawk.libtomahawk.collection;

import org.tomahawk.libtomahawk.resolver.Query;

import java.util.concurrent.ConcurrentHashMap;

//import org.tomahawk.tomahawk_android.utils.MediaWrapper;


/**
 * Created by YGS on 9/10/2015.
 */
public class Folder extends Cacheable implements AlphaComparable{

    //private ArrayList<MediaWrapper> mediaWrapper;

    private int mFolderId;

    private String mFolderName;

    private String mFolderPath;

    private int mLike;


    private static final ConcurrentHashMap<String, Folder> sFolders
            = new ConcurrentHashMap<>();

    /*public ArrayList<MediaWrapper> getMediaWrapper() {
        return mediaWrapper;
    }

    public void setMediaWrapper(ArrayList<MediaWrapper> mediaWrapper) {
        this.mediaWrapper = mediaWrapper;
    }*/

    public int getFolderId() {
        return mFolderId;
    }

    public void setFolderId(int mFolderId) {
        this.mFolderId = mFolderId;
    }

    public String getFolderName() {
        return mFolderName;
    }

    public void setFolderName(String mFolderName) {
        this.mFolderName = mFolderName;
    }

    public String getFolderPath() {
        return mFolderPath;
    }

    public void setFolderPath(String mFolderPath) {
        this.mFolderPath = mFolderPath;
    }



        private final ConcurrentHashMap<String, Album> mAlbums = new ConcurrentHashMap<>();

    private final ConcurrentHashMap<String, Query> mQueries = new ConcurrentHashMap<>();

    private Image mImage;

    /**
     * Construct a new {@link Artist}
     */
    private Folder(String genreName) {
        super(Folder.class, getCacheKey(genreName));

        mFolderPath = genreName != null ? genreName : "";
    }

    /**
     * Returns the {@link Artist} with the given album name. If none exists in the cache yet,
     * construct and add it.
     */
    public static Folder get(String genreName) {
        Cacheable cacheable = get(Folder.class, getCacheKey(genreName));
        return cacheable != null ? (Folder) cacheable : new Folder(genreName);
    }

    public static Folder getByKey(String cacheKey) {
        return (Folder) get(Folder.class, cacheKey);
    }

    /**
     * @return this object's name
     */
    public String getName() {
        return mFolderPath;
    }

    /**
     * @return the name that should be displayed
     */
    public String getPrettyName() {
        return getName().length() > 0 ? getName() : "<unknown>";
    }

    public Image getImage() {
        return mImage;
    }

    public void setImage(Image image) {
        mImage = image;
    }

    public int getmLike() {
        return mLike;
    }

    public void setmLike(int mLike) {
        this.mLike = mLike;
    }
}
